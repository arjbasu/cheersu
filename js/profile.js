function blockui(){
	$.blockUI({ message: '<img src="img/ajax-loader.gif"/>' ,
		themedCSS: { 
	        width:  '30px', 
	        top:    '40%', 
	        left:   '35%' 
	    }, 
		css: { 
            border: 'none', 
            padding: '5px', 
            backgroundColor: '#272B30', 
            '-webkit-border-radius': '0px', 
            '-moz-border-radius': '0px',
            'border-radius':'0px',
            opacity: .5, 
            color: '#fff' 
        }
		});
}

function showinlinemessage(parent,message){
	//console.log(parent);
	$(parent).parents(".news").find(".message").html(message).show();
	setTimeout(function(parent){
		$(parent).parents(".news").find(".message").fadeOut("200");
	},"1000",parent);
}

function timer(){
	setTimeout(function(){
		$(".errordiv,.successdiv").slideUp('200');
	},"1500");
}


(function ($, F) {
    F.transitions.resizeIn = function() {
        var previous = F.previous,
            current  = F.current,
            startPos = previous.wrap.stop(true).position(),
            endPos   = $.extend({opacity : 1}, current.pos);

        startPos.width  = previous.wrap.width();
        startPos.height = previous.wrap.height();

        previous.wrap.stop(true).trigger('onReset').remove();

        delete endPos.position;

        current.inner.hide();

        current.wrap.css(startPos).animate(endPos, {
            duration : current.nextSpeed,
            easing   : current.nextEasing,
            step     : F.transitions.step,
            complete : function() {
                F._afterZoomIn();

                current.inner.fadeIn("fast");
            }
        });
    };

}(jQuery, jQuery.fancybox));

$(document).ready(function(){
	var feedtable = $("#feedtable");
	var displaycommentstable = feedtable.find(".displaycomments");
	var news = $("#feedtable .news");
	$("#cheers").click(function(){
		blockui();
		var userid = $("#personalsection").attr('data-userid');
		var data = {"userid":userid};
		$.ajax({
			url:"ajaxapi/cheers.php",
			type:"POST",
			dataType:"JSON",
			data:data,
			success:function(data){
				$.unblockUI();
				if(data.status == "success"){
					$("#activitysection .successdiv").html("<p>Cheers Sent Successfully</p>").slideDown();
					timer();
					$("#cheers").html("Cheers Sent").fadeOut('3000',function(){
						$("#cheers").remove();
					})
				}
				else{
					$("#activitysection .errordiv").html(data.message).slideDown('300');
					timer();
				}
			},
			error:function(){
				$.unblockUI();
				html = "An error occurred";
				$("#activitysection .errordiv").html(html).slideDown('300');
				timer();
			}
		});
		return false;
	});
	
	if($(".fancybox").length>0){
		$(".fancybox").fancybox({
			nextMethod : 'resizeIn',
	        nextSpeed  : 250,
	        prevMethod : false,
	        preload: 8,
				helpers	: {
					title	: {
						type: 'inside'
					},
					thumbs	: {
						width	: 50,
						height	: 50
					}
				}
		});
	}
	check_notifications();
	initialisechat();
	$("#remove").hover(function(){
		$(this).removeClass("btn-success").addClass("btn-danger")
		.html("<i class = 'icon-ban-circle'></i> Unfriend");
	},function(){
		$(this).removeClass("btn-danger").addClass("btn-success")
		.html("<i class = 'icon-ok'></i> Friends");
	})
	
	.click(function(){
		var confirmation = window.confirm("Do you really want to unfriend?");
		if(confirmation == false){
			return false;
		}
		$(this).fadeOut("200");
		blockui();
		var userid = $("#personalsection").attr("data-userid");
		var data = {"user_id":userid,"action":"delete"};
//		console.log(data);
		$.ajax({
			url:"ajaxapi/friend.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated")
					window.location = "index.php";
				else if(data.status == "success"){
					$.unblockUI();
					$(".successdiv").html(data.message).slideDown('200');
					setTimeout(function(){ window.location = "";},600);
				}
				else{
					$.unblockUI();
					$(".errordiv").html(data.message).slideDown("200");
					$("#remove").fadeIn("200");
				}
			},
			error:function(){
				$.unblockUI();
				$(".errordiv").html("An error occurred").slideDown("200");
				$("#remove").fadeIn("200");
			}
		})
		return false;
	});
	
	$(".displaycomments").each(function(){
		if($(this).find("tr").length>0){
			$(this).show().parent().find(".commentform").show();
		}
	});
	
	$(".loadmore",news).click(function(){
		$(this).append("&nbsp;&nbsp;<img src = 'img/linear-loader.gif'/>");
		parent = this;
		var activity_id = $(this).parents(".news").attr("data-activity-id");
		var timestamp = $(this).parents(".news").find(".displaycomments tr:eq(0)").attr("data-time");
		var data = {"action":"comment","activity_id":activity_id,"type":"get","timestamp":timestamp};
		console.log(data);
		$.ajax({
			url:"ajaxapi/socialactions.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					len = data.message.length;
					var html = "";
					for(var i = len-1; i >=0; i--){
						html+= "<tr data-commentid = '" + data.message[i].commentid +
							"'> <td class = 'dp-container'> <img src = 'img/icons/" + data.message[i].user_dp_icon+
						"' class = 'small-activity-container'/> </td> <td class = 'details'>";
						if(data.message[i].del === true){
							html+= "<small class = 'pull-right'> <a href = '#' class = 'deletecomment'>x</a></small>";
						}
						html+= "<p class = 'school'><b>"  + data.message[i].user_name + "</b></p><p class = 'actualcomment'>" +
						data.message[i].user_comment + "</p></td></tr>";
					}
					$(parent).parents(".news").find(".displaycomments").prepend(html);
					$(parent).hide();
				}
				else{
					showinlinemessage(parent,data.message);
					$(parent).find("img").remove();
					
				}
			},
			error:function(data){
				showinlinemessage(parent,"An error occurred");
				$(parent).find("img").remove();
			}
		});
		return false;
		
	});
	
	
	news.on("click",".like",function(){
//		//console.log("like clicked");
		parent = this;
		var activity_id = $(this).parents(".news").attr("data-activity-id");
		var data = {"action":"like","activity_id":activity_id,"type":"post"};
		$.ajax({
			url:"ajaxapi/socialactions.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					var numlikes = parseInt($(parent).parents(".vote").find(".counter .numlikes").html());
					numlikes++;
					
					if(numlikes == 2){
						var html = "<span class = 'numlikes'> 2</span> likes"; 
						$(parent).find(".counter").html(html).html();
					}
					else if(numlikes == 1){
						var html = "<span class = 'numlikes'> 1</span> like"; 
						$(parent).parents(".vote").find(".counter").html(html);
					}
					else{
						$(parent).parents(".vote").find(".numlikes").html(numlikes);
					}
					$(parent).removeClass("like").addClass("unlike").find(".likeaction").html("<i class = 'icon-heart'></i>");
//					$(parent).find('.likeaction').hide().parent().find('.counter').show();
				}
				else{
					showinlinemessage(parent,data.message);
					
				}
			},
			error:function(){
				showinlinemessage(parent,"An error occurred");
			}
		});
		return false;
	}).on("click",".unlike",function(){
//		//console.log("unlike clicked");
		parent = this;
		var activity_id = $(this).parents(".news").attr("data-activity-id");
		var data = {"action":"like","activity_id":activity_id,"type":"delete"};
		$.ajax({
			url:"ajaxapi/socialactions.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					var numlikes = parseInt($(parent).parents(".vote").find(".counter .numlikes").html());
					numlikes--;
					if(numlikes == 1){
						var html = "<span class = 'numlikes'> 1</span> like"; 
						$(parent).find(".counter").html(html).html();
					}
					else if(numlikes == 0){
						var html = "<span class = 'numlikes'>0</span> likes"; 
						$(parent).parents(".vote").find(".counter").html(html);
					}
					else{
						$(parent).parents(".vote").find(".numlikes").html(numlikes);
					}
					$(parent).removeClass("unlike").addClass("like").find(".likeaction").html("<i class = 'icon-heart-empty'></i>");
//					$(parent).find('.likeaction').hide().parent().find('.counter').show();
				}
				else{
					showinlinemessage(parent,data.message);
					
				}
			},
			error:function(){
				showinlinemessage(parent,"An error occurred");
			}
		});
		return false;
	})
	.on("click",".counter",function(){
		parent = this;
		
		var numlikes = parseInt($(this).find(".numlikes").html());
		if(numlikes != 0){
			blockui();
			var activity_id = $(this).parents(".news").attr("data-activity-id");
			var data = {"action":"like","activity_id":activity_id,"type":"get"};
			console.log(data);
			$.ajax({
				url:"ajaxapi/socialactions.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					$.unblockUI();
					if(data.status == "success"){
						var content = data.message;
						var html = "";
						for(var i = 0; i < content.length; i++){
							html += "<tr><td class = 'dp-container'><img class = 'dp-container' src = 'img/icons/"+content[i].user_dp_icon +
							"'/></td><td><h4 class = 'name'>"+content[i].user_name + "</h4></td></tr>";
						}
						$("#likemodal table").html(html).parents("#likemodal").modal("show");
					}
					else{
						showinlinemessage(parent,data.message);
					}
//					console.log(data);
					
				},
				error:function(){
					$.unblockUI();
					showinlinemessage(parent,"An error occurred");
				}
			});
		}
		return false;
	});
	
	
	displaycommentstable.on("mouseenter",".details",function(){
		$(this).find(".deletecomment").show();
	}).on("mouseleave",".details",function(){
		$(this).find(".deletecomment").hide();
	}).on("click",".deletecomment",function(){
		var parent = $(this).parents("tr");
		var commentid = parent.attr("data-commentid");
		var activityid = parent.parents(".news").attr('data-activity-id');
		var data = {"comment_id":commentid,"action":"comment","type":"delete","activity_id":activityid}
		console.log(data);
		$.ajax({
			url:"ajaxapi/socialactions.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					$(parent).eq(0).fadeOut("200");
					var numcomments = parseInt(parent.parents(".news").find(".vote .comments").html());
					numcomments--;
					if(numcomments == 1){
						$(parent).parents(".news").find(".vote .grammar").html("comment");
					}
					else if(numcomments == 0){
						$(parent).parents(".news").find(".vote .grammar").html("comments");
					}
					parent.parents(".news").find(".vote .comments").html(numcomments);
				}
				else{
					showinlinemessage(parent,data.message);
				}
			},
			error:function(){
				showinlinemessage(parent,"An error occurred");
			}
		});
		return false;
	});
	
	$(".numcomments").click(function(){
//		console.log(".comments clicked");
		
//		if($(this).parents(".news").find(".displaycomments:visible").length == 0){
//			$(this).parents(".news").find(".commentform").show();
//		}
		return false;
	});
	
	
	$(".commentform").submit(function(){
		var tabledisplay = false;
		var parent = this;
		if($(parent).parents(".news").find(".displaycomments:visible").length>0){
			tabledisplay = true;
		}
		
//		$(this).parents(".news").find(".displaycomments").hide();
		var comment = $(this).find(".text-input").val();
		if(comment === "" || null === ""){
			return false;
		}
		var activity_id = $(this).parents(".news").attr("data-activity-id");
		var data = {"action":"comment","activity_id":activity_id,"type":"post","comment":comment};
		$.ajax({
			url:"ajaxapi/socialactions.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					
					var numcomments = parseInt($(parent).parents(".news").find(".vote .comments").html());
					numcomments++;
					if(numcomments == 1){
						$(parent).parents(".news").find(".vote .grammar").html("comment");
					}
					else if(numcomments == 2){
						$(parent).parents(".news").find(".vote .grammar").html("comments");
					}
					
					$(parent).parents(".news").find(".vote .comments").html(numcomments);
					
					
					var imgurl = $("#dp").attr("src");
					var name = $("#name").html();
					
					var html = "<tr data-commentid = '" + data.message + "'> <td class = 'dp-container'> <img src = '" + imgurl + "' class = 'small-activity-container'/></td>"+
					"<td class = 'details'><small class = 'pull-right'><a href = '#' class = 'deletecomment'>x</a></small>" +
					"<p class = 'school'><b> " + name + "</b></p><p class = 'actualcomment'>"+
					comment + "</p></td></tr";
					
					$(parent).trigger("reset").parents(".news").find(".displaycomments").append(html);
					
					if(tabledisplay === false){
						$(parent).parents(".news").find(".displaycomments").show();
						
					}
					
				}
				else{
					showinlinemessage(parent,data.message);
				}
				
			},
			error:function(){
				showinlinemessage(parent,data.message);
			}
		})
		return false;
	});
	
	
	
	$("#add").click(function(){
		$(this).fadeOut("200");
		blockui();
		var userid = $("#personalsection").attr("data-userid");
		var data = {"user_id":userid,"action":"add"};
//		console.log(data);
		$.ajax({
			url:"ajaxapi/friend.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated")
					window.location = "index.php";
				else if(data.status == "success"){
					$.unblockUI();
					$(".successdiv").html(data.message).slideDown('200');
					setTimeout(function(){ window.location = "";},600);
				}
				else{
					$.unblockUI();
					$(".errordiv").html(data.message).slideDown("200");
					$("#add").fadeIn("200");
				}
			},
			error:function(){
				$.unblockUI();
				$(".errordiv").html("An error occurred").slideDown("200");
				$("#add").fadeIn("200");
			}
		});
		return false;
		
	});
	
	$("#confirm").click(function(){
		$(this).fadeOut("200");
		blockui();
		var userid = $("#personalsection").attr("data-userid");
		var data = {"user_id":userid,"action":"confirm"};
//		console.log(data);
		$.ajax({
			url:"ajaxapi/friend.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated")
					window.location = "index.php";
				else if(data.status == "success"){
					$.unblockUI();
					$(".successdiv").html(data.message).slideDown('200');
					setTimeout(function(){ window.location = "";},600);
				}
				else{
					$.unblockUI();
					$(".errordiv").html(data.message).slideDown("200");
					$("#confirm").fadeIn("200");
				}
			},
			error:function(){
				$.unblockUI();
				$(".errordiv").html("An error occurred").slideDown("200");
				$("#confirm").fadeIn("200");
			}
		});
		return false;
		
	});
});