<?php
session_start();
include 'authentication_ajax_api.php';
include '../connect.php';
$userid = $_SESSION['user_id'];
if(isset($_POST['user_id']) && isset ($_POST['action'])){
	$friendid = $_POST['user_id'];
	$action = $_POST['action'];
	if($action == "delete"){
		$query = "SELECT * FROM cheersu_friends_$userid WHERE friend_user_id = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($friendid));
		if($stmt->rowCount() == 0 ){
			$query = "SELECT * FROM cheersu_friend_requests_$userid WHERE friend_user_id = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($friendid));
			if($stmt->rowCount() == 0){
				$status = "error";
				$message = "No friend found";
			}
			else{
				$query = "DELETE FROM cheersu_friends_$friendid WHERE friend_user_id = ?";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($userid));
				if($stmt->rowCount() == 0){
					$status = "error";
					$message = "No friends found";
				}
				else{
					$query = "DELETE FROM cheersu_friend_requests_$userid WHERE friend_user_id = ?";
					$stmt = $pdo->prepare($query);
					$stmt->execute(array($friendid));
					if($stmt->rowCount() <1){
						$status = "error";
						$message = "Unable to delete friend";
					}
					else{
						$status = "success";
						$message = "Friend Request successfully declined";
					}
					
				}
			}
		}
		else{
			$temp = $stmt->fetch(PDO::FETCH_ASSOC);
			if($temp['friend_status'] == "pending"){
				$query = "DELETE FROM cheersu_friends_$userid WHERE friend_user_id = ?";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($friendid));
				if($stmt->rowCount() < 1){
					$status = "error";
					$message = "Unable to delete from database";
					include 'json_encoding.php';
					die();
				}		
			}
			else if($temp['friend_status'] == "confirmed"){
				$query = "DELETE FROM cheersu_friends_$userid WHERE friend_user_id = ?";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($friendid));
				if($stmt->rowCount() < 1){
					$status = "error";
					$message = "Unable to delete from database";
					include 'json_encoding.php';
					die();
				}
				if($friendid>$userid){
					$data = array($userid,$friendid);
				}
				else{
					$data = array($friendid,$userid);
				}
				$query = "DELETE FROM cheersu_social_edges WHERE user_1 = ? AND user_2 = ?";
				$stmt = $pdo->prepare($query);
				$stmt->execute($data);
				
				$query = "DELETE FROM cheersu_friends_$friendid WHERE friend_user_id = ?";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($userid));
				if($stmt->rowCount() < 1){
					$status = "error";
					$message = "Unable to delete from database";
					include 'json_encoding.php';
					die();
				}
				
				$query = "DELETE FROM cheersu_cliques_users_$friendid WHERE clique_user_id = ?";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($userid));
				
				$query = "DELETE FROM cheersu_cliques_users_$userid WHERE clique_user_id = ?";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($friendid));
				
				$query = "DELETE FROM cheersu_notifications_$userid WHERE notification_user_id = ?";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($friendid));
				
				$query = "DELETE FROM cheersu_notifications_$friendid WHERE notification_user_id = ?";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($user_id));
				$status = "success";
				$message = "Successfully unfriended";
				
			}
			else{
				$status = "error";
				$message = "something wrong";
			}
		}		
	}
	else if($action == "confirm"){
		$query = "INSERT INTO cheersu_friends_$userid(friend_user_id,friend_status) VALUES(?,?)";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($friendid,"confirmed"));
		if($stmt->rowCount() == 0 ){
			$status = "error";
			$message = "Unable to confirm friend";
		}
		else{
			$data = array();
			if($friendid>$userid){
				$data = array($userid,$friendid);
			}
			else{
				$data = array($friendid,$userid);
			}
			$query = "INSERT INTO cheersu_social_edges(user_1,user_2) VALUES(?,?)";
			$stmt = $pdo->prepare($query);
			$stmt->execute($data);
			$query = "DELETE FROM cheersu_friend_requests_$userid WHERE friend_user_id = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($friendid));
			if($stmt->rowCount() == 0 ){
				$status = "error";
				$message = "Unable to change confirmation status friend";
			}	
			else{
				$query = "UPDATE cheersu_friends_$friendid SET friend_status = 'confirmed' WHERE friend_user_id = ?";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($userid));
				$query = "INSERT INTO cheersu_notifications_$friendid(notification_activity_id,notification_user_id,notification_type) ".
						"VALUES (?,?,?)";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array(0,$userid,"acceptance"));
				if($stmt->rowCount() < 1){
					$status = "error";
					$message = "Unable to create notification";
				}
				else{
					$status = "success";
					$message = "Friend confirmed successfully";
				}
				
			}
		}
	}
	else if($action == "add"){
		$query = "INSERT INTO cheersu_friends_$userid(friend_user_id,friend_status) VALUES(?,?)";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($friendid,"pending"));
		if($stmt->rowCount() == 0 ){
			$status = "error";
			$message = "Unable to add friend";
		}
		else{
			$query = "INSERT INTO cheersu_friend_requests_$friendid (friend_user_id) VALUES(?)";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($userid));
			if($stmt->rowCount() == 0 ){
				$status = "error";
				$message = "Unable to add friend";
			}
			else{
				$status = "success";
				$message = "Friend Request Successfully sent.";
			}
			
		}
	}
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}
}
else{
	$status = "error";
	$message = "Improper parameters passed";
}

include 'json_encoding.php';

?>