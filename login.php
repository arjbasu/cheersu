<?php
	session_start();
	include 'connect.php';
	if(isset($_POST['email']) && isset($_POST['password'])){
		if(isset($_POST['rememberme']))
			$remember = $_POST['rememberme'];
		else
			$remember = "";
		
		$email = $_POST['email'];
		$password = $_POST['password'];
		$hashedpassword = crypt($password,'$1$foreverdope12$');
		$query = "SELECT user_id,user_firstname,user_verified,user_blocked FROM cheersu_users WHERE user_email = ? AND user_passhash = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($email,$hashedpassword));
		if($stmt->rowCount() == 1){
			$result = $stmt->fetch();
			$status = $result['user_verified'];
			$blocked = $result['user_blocked'];
			$name = $result['user_firstname'];
			$user_id = $result['user_id'];
				
			if($status == 0){
				$status = "success";
				$message = "You have not yet verified your email. Please click on the link sent to your mail
				to verify your email.<br/>If you did not receive the verification mail or facing problems verifying, send us your".
				"details at support@cheersu.com clearly mentioning your problems";
				$_SESSION['email_pending'] = true;
				$_SESSION['logged'] = true;
				$_SESSION['username'] = $name;
				$_SESSION['user_id'] = $user_id;
			}
			else if($blocked == 1){
				$status = "error";
				$message = "You have been blocked by the admin. Please contact support";
			}
			else{
				if($remember == "remember"){
					setcookie("user",$name,time()+14*24*60*60,"/");
					setcookie("user_id",$user_id,time()+14*24*60*60,"/");
				}
					
				$_SESSION['logged'] = true;
				$_SESSION['username'] = $name;
				$_SESSION['user_id'] = $user_id;
				$status = "success";
				$message = "You have successfully logged in. One moment...";
			}
		}
		else{
			$status = "error";
			$message = "Incorrect email/password combination";
		}
	}
	else{
		$status = "Failure";
		$message = "Improper parameters passed";
	}
	include 'json_encoding.php';
	
?>