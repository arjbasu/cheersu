<?php
session_start();
include 'authentication_ajax_api.php';
if(isset($_POST['api_type']) && isset($_POST['api_key'])){
	$apitype = $_POST['api_type'];
	$apikey = $_POST['api_key'];
	include '../connect.php';
	$userid = $_SESSION['user_id'];
	if($apitype == "fb"){
		$query = "UPDATE cheersu_users SET user_fb_id = ?, user_fb_autopost = ? WHERE user_id = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($apikey,1,$userid));
		$status = "success";
		$message = "Facebook userid successfully stored";
	}
	else{
		$status = "error";
		$message = "Unable to set api userid";
	}
}
else{
	$status = "error";
	$message = "improper parameters passed";
}
include 'json_encoding.php';

?>