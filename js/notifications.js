function setFirefoxcorrection(){
	if(BrowserDetect.browser == "Firefox"){
		$("h1,h2,h3,h4,h5,h6").css("letter-spacing","-2px");
		$("p,a").css("letter-spacing","-0.2px");
	}
}

function check_notifications(){
	$.ajax({
		url:"ajaxapi/checknotifications.php",
		type:"POST",
		dataType:"JSON",
		success:function(data){
			if(data.status == "success"){
				$("#unseen").html(data.message).show();
			}
			
		}
	});
	
}

setFirefoxcorrection();
check_notifications();
setInterval("check_notifications",60*1000);