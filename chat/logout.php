<?php
session_start();
session_unset();
session_destroy();
?>
<!DOCTYPE html>
<html>
<head>
<title>Facebook User Chat System</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
</head>
<body>
<script type="text/javascript" src="js/jquery.js"></script>
<script src="js/jquery.cookie.js"></script>
<script>
$.cookie("c_openbox","", { expires: -30 });
$.cookie("min_openbox","", { expires: -30 });
window.location = "index.php";
</script>
</body>
</html>