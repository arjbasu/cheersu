<?php
	session_start();
	include 'check_authorization.php';
	include 'connect.php';
	include 'twiginit.php';
	$own = true;
	$myuserid = $_SESSION['user_id'];
	if(isset($_GET['userid']) && $_GET['userid'] != ""){
		$userid = $_GET['userid'];
		if($userid != $myuserid){
			$own = false;
		}
			
	}
	else{
		$userid = $_SESSION['user_id'];
	}
	
// 	$query = "SELECT user_firstname,user_lastname,user_bio,user_status,user_bio,".
// 	"user_dp,school_name count(activity_timestamp) as checkins,count(friend_user_id) as friends FROM cheersu_users,".
// 	"cheersu_activity,cheersu_friends_$userid,cheersu_schools ".
// 	"WHERE user_id = ? AND activity_user_id = user_id AND activity_type = 'checkin' AND school_id = user_schoolid";
	$query = "SELECT user_firstname, user_lastname, user_bio, user_status, user_dp,user_dp_icon ".
	"FROM ".
	"cheersu_users WHERE".
	" user_id = '$userid'";
	$data = array();
	$result = mysql_query($query);
	if(!$result){
		die("Unable to query databases");
	}
	else{
		
		$temp = mysql_fetch_assoc($result);
		$data['id'] = $userid;
		$firstname = $temp['user_firstname'];
		$lastname = $temp['user_lastname'];
		$bio = $temp['user_bio'];
		$status = $temp['user_status'];
		$img = $temp['user_dp'];
		$imgicon = $temp['user_dp_icon'];
		if($img == ""){
			$img = "300x200.gif";
			$imgicon = "#";
		}
		
		$query = "SELECT * FROM cheersu_activity WHERE activity_type = 'checkin' AND activity_user_id = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($userid));
		$checkins = $stmt->rowCount();
		
		$query = "SELECT * FROM cheersu_friends_$userid WHERE friend_status = 'confirmed'";
		$result = mysql_query($query);
		$friends = mysql_num_rows($result);
		
// 		error_log("Own",0);
		
		$query = "SELECT school_name FROM cheersu_schools , cheersu_users WHERE user_schoolid = school_id AND user_id = '$userid'";
		$result = mysql_query($query);
		if(!$result){
			die("Unable to query databases");
		} 
		$temp = mysql_fetch_assoc($result);
		$school = $temp['school_name'];
		
		$data['name'] = stripslashes($firstname." ".$lastname);
		
		if($own == true){
			$data['title'] = "My Profile";
		}
		else{
			$data['title'] = $firstname."'s Profile";
			error_log("own is false",0);
			$query = "SELECT * FROM cheersu_friends_$myuserid WHERE friend_user_id = ? AND friend_status = 'confirmed'";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($userid));
			if($stmt->rowCount() != 0){
				$data['confirmedfriends'] = true;
			}
			else{
				$query = "SELECT * FROM cheersu_friends_$myuserid WHERE friend_user_id = ? AND friend_status = 'pending'";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($userid));
				if($stmt->rowCount() != 0){
					$data['friendspending'] = true;
				}
				else{
					$query = "SELECT * FROM cheersu_friend_requests_$myuserid WHERE friend_user_id = ?";
					$stmt = $pdo->prepare($query);
					$stmt->execute(array($userid));
					if($stmt->rowCount() != 0){
						$data['friendawaiting'] = true;
					}
					else{
						$data['notfriends'] = true;
					}
					
				}
			}
		}
		
		$query = "SELECT user_dp_icon FROM cheersu_users WHERE user_id = $myuserid";
		$result = mysql_query($query);
		$temp = mysql_fetch_row($result);
		$data['chat_dp_icon'] = $temp[0];
		$data['id'] = $userid;
		$data['dp'] = $img;
		$data['dpicon'] = $imgicon;
		$data['numfriends'] = $friends;
		$data['numcheckins'] = $checkins;
		$data['bio'] = stripslashes($bio);
		$data['status'] = stripslashes($status);
		$data['school'] = $school;
		$query = "SELECT * FROM cheersu_user_photos WHERE user_photo_user_id = '$userid' ORDER BY user_photo_timestamp DESC";
		$resultimg = mysql_query($query);
		if(!$resultimg){
			die("unable to fetch");
		}
		else{
			if(mysql_num_rows($resultimg) == 0){
				$data['nophotos'] = true;
			}
			else{
				$photos = array();
				while($tempimg = mysql_fetch_assoc($resultimg)){
					array_push($photos,$tempimg['user_photo_source']);
				}
				$data['photos'] = $photos;
			}
		}
		
		$query = "SELECT activity_id FROM cheersu_activity WHERE activity_user_id  = '$userid' ORDER BY activity_timestamp DESC LIMIT 100";
		error_log("activityidquery:$query",0);
		$result = mysql_query($query);
		if(!$result){
			die("unable to proceed");
		}
		
		$commentarray = array();
		if(mysql_num_rows($result) != 0){
			$activityidlist = "(";
			while($temp = mysql_fetch_row($result)){
				$activityidlist.=$temp[0].",";
			}
			$activityidlist = substr($activityidlist, 0, -1).")";
			
			$query = "SELECT user_comment_id AS commentid,user_comment_activity_id AS actid,user_comment_message AS comment,user_comment_id AS id,user_comment_timestamp AS time,
			 		CONCAT(user_firstname,' ',user_lastname) AS name,user_id AS userid,user_dp_icon AS dp 
					FROM cheersu_user_comments INNER JOIN cheersu_users ON user_comment_user_id = user_id WHERE user_comment_activity_id IN $activityidlist
					ORDER BY user_comment_activity_id DESC,user_comment_timestamp DESC";
			error_log("commentquery:$query",0);
			
			$result = mysql_query($query);
			if(!$result){
				die("unable to proceed");
			}
			
			while($temp = mysql_fetch_assoc($result)){
				$activityid = $temp['actid'];
				$temp['comment'] = stripslashes($temp['comment']);
				if(array_key_exists($activityid,$commentarray)){
					if(count($commentarray[$activityid]) < 3){
						
						array_unshift($commentarray[$activityid],$temp);
					}
				}
				else{
					$commentarray[$activityid] = array();
					array_unshift($commentarray[$activityid],$temp);
				}
			}
		}
		
		$query = "SELECT user_id,user_firstname,user_lastname,user_dp_icon,venue_name,school_name,activity_type,activity_comment,".
				"school_id, activity_id,activity_timestamp, datediff(now(),activity_timestamp) as datedif , timediff(now(),activity_timestamp) as timedif,".
				" COUNT(distinct like_user_id) AS likes , COUNT(distinct user_comment_id) AS user_comments, like_act ".
				" FROM cheersu_activity INNER JOIN cheersu_users ON activity_user_id = user_id INNER JOIN (cheersu_venues INNER JOIN ".
				"cheersu_schools ON venue_schoolid = school_id) ON activity_venue_id = venue_id LEFT OUTER JOIN cheersu_likes ON ".
				"like_activity_id = activity_id LEFT OUTER JOIN cheersu_user_comments ON user_comment_activity_id = activity_id ".
				"LEFT OUTER JOIN (SELECT like_activity_id AS like_act FROM cheersu_likes WHERE like_user_id = '$userid') ld ON like_act = activity_id ".
				"WHERE user_id = $userid GROUP BY activity_id ORDER BY activity_timestamp DESC LIMIT 100";
		$result = mysql_query($query);
		if(mysql_num_rows($result) == 0){
			$data['noresults'] = true;
		}
		else{
			$activity = array();
			
			while($temp = mysql_fetch_assoc($result)){
				$tempactivity = array();
				$img = $temp['user_dp_icon'];
				if($img == "")
					$tempactivity['dp'] = "cheersu_icon.png";
				else{
					$tempactivity['dp'] = $img;
				}
				$tempactivity['comment'] = stripcslashes($temp['activity_comment']);
				$tempactivity['venue'] = stripslashes($temp['venue_name']);
				$tempactivity['schoolname'] = stripslashes($temp['school_name']);
				$tempactivity['schoolid'] = stripslashes($temp['school_id']);
				$tempactivity['likes'] = $temp['likes'];
				$tempactivity['liked'] = $temp['like_act'];
				$tempactivity['activity_id'] = $temp['activity_id'];
				$tempactivity['user_comments'] = $temp['user_comments'];
				if(array_key_exists($temp['activity_id'],$commentarray)){
					$tempactivity['topcomments'] = $commentarray[$temp['activity_id']];
				}
				$type = $temp['activity_type'];
					if($type == "checkin"){
						$tempactivity['type'] = "checked in to";
					}
					else if($type == "reserve"){
						$tempactivity['type'] = "Made a reservation at";
					}
					else if($type == "comment"){
						$tempactivity['type'] = "commented on";
					}
					else if($type == "taggedstatus"){
						$tempactivity['status'] = true;
						$activityid = $temp['activity_id'];
						$query = "SELECT user_tag_userid AS id,user_tag_username AS name FROM cheersu_user_tag WHERE user_tag_activityid = '$activityid'";
						$result3 = mysql_query($query);
						if($result3){
							$taglist = array();
							while($tempres = mysql_fetch_assoc($result3)){
								array_push($taglist,$tempres);
							}
							$tempactivity['tagged'] = $taglist;
						}
						else{
							die("error");
						}
					}
					else if($type == "photo-activity"){
						// 					error_log("name:".$temp['user_firstname']);
						$photos = array();
						$numphotos = 0;
						$timestamp = $temp['activity_timestamp'];
						error_log("timestamp:$timestamp");
						$query = "SELECT user_photo_source FROM cheersu_user_photos WHERE user_photo_timestamp = '$timestamp'";
						$result2 = mysql_query($query);
						if(!$result2){
							die("unable to query databases");
						}
						else{
							$numphotos = mysql_num_rows($result2);
							while($tempimg = mysql_fetch_assoc($result2)){
								array_push($photos,$tempimg['user_photo_source']);
								//error_log("img".$tempimg,0);
									
							}
						}
						$tempactivity['photoactivity'] = true;
						$tempactivity['photos'] = $photos;
						$tempactivity['numphotos'] = $numphotos;
					}
					else if($type == "status"){
						$tempactivity['type'] = "updated his status:";
						$tempactivity['commentactivity'] = true;
					}
					else if(stristr($type, "external") != false){
						error_log("$type",0);
						$tempactivity['external'] = true;
						$typearray = explode("|",$type);
						$tempact = $typearray[0];
						error_log("$tempact",0);
						if($tempact == "reserve"){
							$tempactivity['type'] = "reserved on to";
						}
						else if($tempact == "checkin"){
							$tempactivity['type'] = "checked in to";
						}
						$details = explode("|",$tempactivity['comment']);
						$tempactivity['schoolname'] = $details[1];
						$tempactivity['venue'] = $details[0];
					}
				$datediff = $temp['datedif'];
				$timediff = $temp['timedif'];
				if($datediff > 0){
					$tempactivity['timestamp'] = $datediff."d";
				}
				else{
					$timearray = explode(":", $timediff);
					if($timearray[0] > 0){
						if($timearray[0]<10){
							$timearray[0] = substr($timearray[0], 1,1);		
						}
						$tempactivity['timestamp'] = $timearray[0]."h";
						
					}
					else if($timearray[1] > 0){
						if($timearray[1]<10){
							$timearray[1] = substr($timearray[1], 1,1);
						}
						$tempactivity['timestamp'] = $timearray[1]."m";
					}
					else{
						if($timearray[2]<10){
							$timearray[2] = substr($timearray[2], 1,1);
						}
						$tempactivity['timestamp'] = $timearray[2]."s";
					}
					
				}
				
				
				array_push($activity,$tempactivity);
			}
			$data['activitylist'] = $activity;
		}
		echo $twig->render("profile.twig",$data);
	}
	
	
?>