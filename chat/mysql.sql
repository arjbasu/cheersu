CREATE TABLE IF NOT EXISTS `d1_guests` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `guest_id` varchar(32) NOT NULL,
  `guestname` varchar(255) NOT NULL DEFAULT '0',
  `token` varchar(255) NOT NULL DEFAULT '0',
  `ip_addresse` varchar(255) NOT NULL DEFAULT '0',
  `lastactivity` int(32) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `d1_user` (
  `user_id` int(32) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `lastactivity` int(32) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `ip_addresse` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`)
);


INSERT INTO `d1_user` (`user_id`, `username`, `password`, `lastactivity`, `picture`, `token`, `ip_addresse`) VALUES
(1, 'subhiksha', 'f0f9de98e9f791fad225acc2343c3702', 1360458147, 'photoofme.jpg', '', ''),
(2, 'jasmin', 'f0f9de98e9f791fad225acc2343c3702', 1360458147, 'photoofme.jpg', '', '');


CREATE TABLE IF NOT EXISTS `d1_user_chat` (
  `user_chat_id` int(32) unsigned NOT NULL AUTO_INCREMENT,
  `client_from_id` varchar(255) NOT NULL,
  `client_from_name` varchar(255) NOT NULL,
  `client_to_id` varchar(255) NOT NULL,
  `client_to_name` varchar(255) NOT NULL,
  `user_chat_message` text NOT NULL,
  `time_sent` int(32) NOT NULL,
  `client_read_to` int(32) NOT NULL,
  `client_read_from` int(32) NOT NULL,
  PRIMARY KEY (`user_chat_id`)
);