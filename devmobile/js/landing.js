$(document).on("pageshow",function(){
	var ajaxinprogress = false;
	$("body").ajaxStart(function(){
		ajaxinprogress = true;
	});
	$("body").ajaxStop(function(){
		ajaxinprogress = false;
	});
	$("#registerform").submit(function(){
		if(ajaxinprogress === true)
			return false;
		var validation = true;
		$(this).find("input,selector").each(function(){
			if($(this).val() == ""|| $(this).val() == "NULL"){
				validation = false;
				return;
			}
		});
		
		if(validation == false){
			alert("You have to complete all fields before submission");
			
		}
		else{
			$.mobile.loading('show');
			var data = $(this).serialize();
			console.log(data)
			$.ajax({
				url:"ajaxapi/signup.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						$.mobile.loading('hide');
						alert(data.message);
					}
					else{
						$.mobile.loading('hide');
						alert(data.message);
					}
				},
				error:function(){
					$.mobile.loading('hide');
					alert("An error occurred");
				}
			});
		}
		return false;
	});
	
	$("#loginform").submit(function(){
		if(ajaxinprogress === true)
			return false;
		var validation = true;
		
		if($("#loginemail").val() == "" || $("#loginpassword").val() ==  ""){
			validation = false;
		}
		
		if(validation == false){
			alert("You have to complete all fields before submission");
			
		}
		else{
			$.mobile.loading('show');
			var data = $(this).serialize();
			console.log(data);
			$.ajax({
				url:"ajaxapi/login.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						window.location = "dashboard.php";
					}
					else{
						alert(data.message);
					}
				},
				error:function(){
					alert("An error occurred");
				}
			});
		}
		
		return false;
	});
});