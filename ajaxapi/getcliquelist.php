<?php
	session_start();
	include 'authentication_ajax_api.php';
	include '../connect.php';
	$userid = $_SESSION['user_id'];
	if(!isset($_POST['clique_id'])){
		$status = "error";
		$message = "Improper parameters passed";
		include 'json_encoding.php';
		die();
	}
	$cliqueid = $_POST['clique_id'];
	
	$query = "SELECT user_firstname,user_lastname,user_dp,user_id FROM cheersu_users, cheersu_cliques_users_$userid".
			" WHERE user_id =  clique_user_id AND clique_myclique_id = '$cliqueid'";
	$result = mysql_query($query);
	if(!$result){
		$status = "error";
		$message = "Unable to interact with database";
	}
	else if(mysql_num_rows($result) == 0){
		$status = "success";
		$message = "No friends added";
	}
	else{
		$status = "success";
		$message = array();
		$tempfriend = array();
		while($temp = mysql_fetch_assoc($result)){
			$tempfriend['name'] = $temp['user_firstname']." ".$temp['user_lastname'];
			if($temp['user_dp'] == ""){
				$tempfriend['dp'] = "cheersu_icon.png";
			}
			else{
				$tempfriend['dp'] = $temp['user_dp'];
			}
			
			$tempfriend['id'] = $temp['user_id'];
			array_push($message,$tempfriend);
		}
	}
	include 'json_encoding.php';
?>