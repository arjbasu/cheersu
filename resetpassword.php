<?php
	include 'twiginit.php';
	if(!isset($_GET['hash']) && !isset($_GET['name'])){
		echo $twig->render("generic.html",array(
					"title"=>"404 error",
					"error"=>"Page not found"
				));
	}
	else{
		include 'connect.php';
		$hash = $_GET['hash'];
		$name = urldecode($_GET['email']);
		$query = "SELECT user_id FROM cheersu_users WHERE user_email = ? AND user_verification_id = ? AND user_password_reset = 1";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($name,$hash));
		if($stmt->rowCount() != 1){
			echo $twig->render("generic.html",array(
						"title"=>"Expired Link",
						"error"=>"The Link has expired"
					));
		}
		else{
			$temp = $stmt->fetch(PDO::FETCH_ASSOC);
			$userid = $temp['user_id'];
			echo $twig->render("resetpassword.twig",array(
						"hashvalue" => $hash,
						"user_id" => $userid
					));
		}
		
		
	}
		
?>