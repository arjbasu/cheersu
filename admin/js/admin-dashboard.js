function blockui(){
	$.blockUI({ message: '<img src="img/ajax-loader.gif"/>' ,
		themedCSS: { 
	        width:  '30px', 
	        top:    '40%', 
	        left:   '35%' 
	    }, 
		css: { 
            border: 'none', 
            padding: '5px', 
            backgroundColor: '#272B30', 
            '-webkit-border-radius': '0px', 
            '-moz-border-radius': '0px',
            'border-radius':'0px',
            opacity: .5, 
            color: '#fff' 
        }
		});
}

photohtml = "<div class = 'photo-preview'>" +
"<img src = 'img/photo-upload-ajax.png' class = 'photo-loading'/>"+
"</div>";

function removephotos(event){
	// console.log("event");
	// console.log(event);
	var confirmation = confirm("This will delete all your uploaded photos! Do you want to continue?");
	if(confirmation === true){
		blockui();
		var data = "";
		len = $(".photo-preview").length;
		for(var i = 0; i < len; i++){
			var img = $(".photo-preview").eq(i).find(".preview-img").attr("data-image-name");
			data+="filename%5B%5D="+img+"&";
		}
		data+="type=delete&venue_id=0";
		console.log(data);
		$.ajax({
			url:"ajaxapi/venue_photo_action.php",
			data:data,
			type:"POST",
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					$.unblockUI();
					$("#upload-section").fadeOut("300",function(){
						$("#photo-upload,#img-thumbnails").fadeIn("200");
						$(".photo-preview").remove();
						$("#upload-all").addClass("disabled");
					});
				}
				else{
					$.unblockUI();
					alert("unable to delete photos , please try again later");
				}
			},
			error:function(){
				$.unblockUI();
				alert("unable to delete photos , please try again later");
			}
		});
		return true;
		
	}
	else if(arguments.length){
		// console.log("event is defined");
		return false;
	}

}

function upload(){
	var file = $("#file").val();
	var extension = file.split(".")
	extension = extension[extension.length-1];
	if(file!="" && (extension == "jpg"|| extension == "jpeg" || extension == "png" || extension == "bmp"||
			extension == "JPG"|| extension == "JPEG" || extension == "PNG" || extension == "BMP"		
	)){
		$(".previews").append(photohtml);
		var selection = $(".previews .photo-preview:last-child .photo-loading").show();
		$("#upload-all,#upload-file,#close-upload,#close-photo-modal").addClass("disabled");
		$.ajaxFileUpload
		(
			{
				url:'ajaxapi/upload_venue_cover.php',
				fileElementId:'file',
				dataType: 'JSON',
				success: function (data){
					data = $.parseJSON(data);
					if(data.status == "success"){
						html = "<img src = 'http://dev.cheersu.com/img/venue_cover_img/icon_square/"+ data.message+"' class = 'preview-img' data-image-name='"+data.message+"'/>";
						$("#venue_cover_img").val(data.message);
						$("#addcover").hide();
						$("#removecover").show();
						$("#file-upload").trigger("reset");
						$("#file").off("change",upload).on("change",upload);
						$("#venueform").append(html);
						
					}
					else{
						alert(data.message);
						$("#file-upload").trigger("reset");
						$("#file").off("change",upload).on("change",upload);
					}
					
				},
				error: function (data, status, e){
					alert("There was an error uploading the image.Please try again");
					$("#file-upload").trigger("reset");
					$("#file").off("change",upload).on("change",upload);
				}
			}
		)
	}
	else{
		alert("Please choose an image file with jpeg,jpg or png extension");
	}
}



$(document).ready(function(){
	
	$("#addcover").click(function(){
		$("#file").trigger("click");
		return false;
	});
	
	$("#removecover").click(function(){
		$("#addvenuemodal .modal-body .preview-img").remove();
		$("#venue_cover_img").val("");
		$(this).hide();
		$("#addcover").show();
		return false;
	})
	
	
	
	
	$("#file").on("change",upload);
	$("#upload-file").click(function(){
		if(!$(this).hasClass("disabled"))
			$("#file").trigger("click");
		return false;
	});
	$("#close-photo-modal").click(function(e){
		if($(this).hasClass("disabled") === true){
			return false;
		}
		else if($(".photo-preview").length>0){
			var val = removephotos(e);
			if(val === false){
				return false;
			}
		}
	});
	$("#photomodal").on("hidden",function(){
		$("#upload-section,#file-upload").hide();
		$("#img-thumbnails,#photo-upload").show();
	});
	$("#photo-upload").click(function(){
		
		$("#photo-upload,#img-thumbnails").fadeOut("200",function(){
			$("#file-upload").hide();
			$("#upload-section").fadeIn("200");
		});
		return false;
	});
	$("#upload-all").click(function(){
		venueid = $("#photomodal").attr("data-venue-id")
		if($(this).hasClass("disabled") != true){
			$("#photomodal").block({
						message: "<img src = 'img/ajax-loader.gif'/>",
						themedCSS: { 
					        position:'absolute',
					        top:'60%',
					        left:'0%',
					    }, 
						css: { 
				            border: 'none', 
				            padding: '5px', 
				            backgroundColor: '#272B30', 
				            '-webkit-border-radius': '0px', 
				            '-moz-border-radius': '0px',
				            'border-radius':'0px',
				            opacity: .5, 
				            color: '#fff' 
				        }
					})
			var data = "";
			len = $(".photo-preview").length;
			for(var i = 0; i < len; i++){
				var img = $(".photo-preview").eq(i).find(".preview-img").attr("data-image-name");
				data+="filename%5B%5D="+img+"&";
			}
			data+="type=post&venue_id="+venueid;
			console.log(data);
			$.ajax({
				url:"ajaxapi/venue_photo_action.php",
				data:data,
				type:"POST",
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						$("#photomodal").unblock();
						blockui("upload-complete");
						setTimeout(function(){$.unblockUI();},1000);
						var html = "";
						for(var i = 0; i < len; i++){
							var img = $(".photo-preview").eq(i).find(".preview-img").attr("data-image-name");
							html += "<a rel = 'venuegallery' class = 'fancybox' href = 'img/venue_img/" + img + "'>"+
							"<img src = 'img/venue_img/icons/"+ img +"'/></a>"
						}
						console.log(html);
						$("#img-thumbnails").prepend(html);
						$(".fancybox").fancybox({
							nextMethod : 'resizeIn',
					        nextSpeed  : 150,
					        preload:8,
					        prevMethod : false,
								helpers	: {
									title	: {
										type: 'inside'
									},
									thumbs	: {
										width	: 50,
										height	: 50
									}
								}
						});
						$(".photo-preview").remove();
						$("#close-upload").trigger("click");
						
					}
					else{
						$("#photomodal").unblock();
						alert(data.message);
					}
				},
				error:function(){
					$("#photomodal").unblock();
					alert("unable to post photos , please try again later");
				}
			});
			
		}
		return false;
	});
	$("#close-upload").click(function(){
		if($(this).hasClass("disabled") === true){
			return false;
		}
		// console.log("close upload clicked");
		if($(".photo-preview").length>0){
			removephotos();
		}
		else{
			$("#upload-section").fadeOut("300",function(){
				$("#photo-upload,#img-thumbnails").fadeIn("200");
				$(".photo-preview").remove();
				$("#upload-all").addClass("disabled");
			});
		}
		return false;
	});
	
	
	(function ($, F) {
	    F.transitions.resizeIn = function() {
	        var previous = F.previous,
	            current  = F.current,
	            startPos = previous.wrap.stop(true).position(),
	            endPos   = $.extend({opacity : 1}, current.pos);

	        startPos.width  = previous.wrap.width();
	        startPos.height = previous.wrap.height();

	        previous.wrap.stop(true).trigger('onReset').remove();

	        delete endPos.position;

	        current.inner.hide();

	        current.wrap.css(startPos).animate(endPos, {
	            duration : current.nextSpeed,
	            easing   : current.nextEasing,
	            step     : F.transitions.step,
	            complete : function() {
	                F._afterZoomIn();

	                current.inner.fadeIn("fast");
	            }
	        });
	    };

	}(jQuery, jQuery.fancybox));
	
	
	
	
	
	
	
	$("body").ajaxStart(function(){
		$(".successdiv,.errordiv").hide();
	})
	$("#addvenuemodal").on("hidden",function(){
		
		$("#venueform").trigger("reset").show().find("input[name = 'venue_id']").val("NULL").parents(".modal")
		.find("h2").html("Add a new venue").parents(".modal")
		.find(".modal-footer button").html("Add").parents("#addvenuemodal")
		.find(".successdiv,.errordiv,.loading").hide();
		$(".preview-img").remove();
		$("#removecover").hide();
		$("#addcover").show();
	});
	
	$("#addschoolmodal").on("hidden",function(){
		
		$("#schoolform").trigger("reset").show().find("input[name = 'school_id']").val("NULL").parent().
		find("input[name = 'school_address']").val("").parents(".modal")
		.find("h2").html("Add a new school").parents(".modal")
		.find(".modal-footer button").html("Add").parents("#addschoolmodal")
		.find(".successdiv,.errordiv,.loading").hide();
	});
	$("#schoolform").submit(function(){
		$(this).slideUp("200");
		$(this).parents(".modal").find(".loading").fadeIn("200");
		var data = $(this).serialize();
		var actiondecider = $("#addschoolmodal button").html();
		if(actiondecider == "Add"){
			action = "insert";
		}
		else{
			action = "update";
		}
		data += "&action="+action;
		console.log(data);
		$.ajax({
			url:"ajaxapi/schoolaction.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				else if(data.status == "success"){
					$("#addschoolmodal .loading").fadeOut("200");
					$("#addschoolmodal .successdiv").html(data.message).slideDown("200");
					//setTimeout(function(){window.location = "";},"600");
					
				}
				else{
					$("#addschoolmodal .loading").fadeOut("200");
					$("#addschoolmodal .errordiv").html(data.message).slideDown("200");
				}
			},
			error:function(){
				$("#addschoolmodal .loading").fadeOut("200");
				$("#addschoolmodal .errordiv").html("An error occurred").slideDown("200");
			}
		})
		
		
		return false;
	})
	
	
	$("#venueform").submit(function(){
		$(this).add("#addvenuemodal .modal-body .preview-img").slideUp("200");
		$(this).parents(".modal").find(".loading").fadeIn("200");
		var data = $(this).serialize();
		var venueid = $(this).find("input[name = 'venue_id']");
		var actiondecider = $("#addvenuemodal button").html();
		if(actiondecider == "Add"){
			action = "insert"
		}
		else{
			action = "update";
		}
		data += "&action="+action;
		console.log(data);
		$.ajax({
			url:"ajaxapi/venueaction.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				else if(data.status == "success"){
					$("#addvenuemodal .loading").fadeOut("200");
					$("#addvenuemodal .successdiv").html(data.message).slideDown("200");
					//setTimeout(function(){window.location = "";},"600");
					
				}
				else{
					$("#addvenuemodal .loading").fadeOut("200");
					$("#addvenuemodal .errordiv").html(data.message).slideDown("200");
				}
			},
			error:function(){
				$("#addvenuemodal .loading").fadeOut("200");
				$("#addvenuemodal .errordiv").html("An error occurred").slideDown("200");
			}
		})
		
		
		return false;
	});
	$(".unblock-user").click(function(){
		blockui();
		var userid = $(this).parents("tr").attr("data-userid");
		var data = {"user_id":userid,"action":"unblock"};
		$.ajax({
			url:"ajaxapi/useraction.php",
			type:"POST",
			dataType:"JSON",
			data:data,
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				else if(data.status == "success"){
					$.unblockUI();
					$("#mainarea .successdiv").html(data.message).slideDown("300");
					setTimeout(function(){
						window.location = "";
					},"600")
				}
				else{
					$.unblockUI();
					$("#mainarea .errordiv").html(data.message).slideDown("300");
				}
			},
			error:function(){
				$.unblockUI();
				$("#mainarea .errordiv").html("An error occurred").slideDown("300");
			}
		});
	});
	$(".block-user").click(function(){
		var userid = $(this).parents("tr").attr("data-userid");
		var data = {"user_id":userid,"action":"block"};
		$.ajax({
			url:"ajaxapi/useraction.php",
			type:"POST",
			dataType:"JSON",
			data:data,
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				else if(data.status == "success"){
					$.unblockUI();
					$("#mainarea .successdiv").html(data.message).slideDown("300");
					setTimeout(function(){
						window.location = "";
					},"600");
				}
				else{
					$.unblockUI();
					$("#mainarea .errordiv").html(data.message).slideDown("300");
				}
			},
			error:function(){
				$.unblockUI();
				$("#mainarea .errordiv").html("An error occurred").slideDown("300");
			}
		});
	});
	$(".tabs").click(function(){
		$(".tabs").stop(true,false);
		var tab = $(this).attr("href");
		$(".tabs").parents("li").removeClass("active");
		var parent = this;
		$(".information:visible").fadeOut("200",function(){
			$(tab).fadeIn("600");
			$(parent).parents("li").addClass("active");
		});
		return false;
	});
	
	$(".editschool").click(function(){
		blockui();
		var schoolid = $(this).parents("tr").attr("data-schoolid");
		var data = {"school_id":schoolid,"action":"get"};
		console.log(data);
		$.ajax({
			url:"ajaxapi/schoolaction.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				if(data.status == "success"){
					var venuedata = data.message;
					$("#schoolform input[name = 'school_name']").val(venuedata.school_name);
					$("#schoolform input[name = 'school_id']").val(venuedata.school_id);
					$("#schoolform input[name = 'school_address']").val(venuedata.school_postal_address);
					$("#schoolform select").val(venuedata.school_state);
					$("#addschoolmodal .modal-header h2").html("Edit School");
					$("#addschoolmodal .modal-footer button").html("Update");
					$.unblockUI();
					$("#addschoolmodal").modal();
				}
				else{
					$.unblockUI();
					$("#mainarea .errordiv").html(data.message).slideDown("300");
				}
			},
			error:function(){
				$("#mainarea .errordiv").html("Unable to fetch school details").slideDown("300");
			}
		})
		
		
	});
	$(".deleteschool").click(function(){
		var schoolid = $(this).parents("tr").attr("data-schoolid");
		var response = confirm("Delete this school?");
		if(response == false){
			return false;
		}
		var data = {"school_id":schoolid,"action":"delete"};
		$.ajax({
			url:"ajaxapi/schoolaction.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				if(data.status == "success"){
					$.unblockUI();
					window.location("");
				}
				else{
					$.unblockUI();
					$("#mainarea .errordiv").html(data.message).slideDown("300");
				}
			},
			error: function(){
				$.unblockUI();
				$("#mainarea .errordiv").html("Unable to delete venue").slideDown("300");
			}
		});
	});
	
	$(".deletevenue").click(function(){
		var response = confirm("Delete this venue?");
		if(response == false){
			return false;
		}
		blockui();
		var venueid = $(this).parents("tr").attr("data-venueid");
		var data = {"venue_id":venueid,"action":"delete"};
		$.ajax({
			url:"ajaxapi/venueaction.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				if(data.status == "success"){
					$.unblockUI();
					window.location("");
				}
				else{
					$.unblockUI();
					$("#mainarea .errordiv").html(data.message).slideDown("300");
				}
			},
			error: function(){
				$.unblockUI();
				$("#mainarea .errordiv").html("Unable to delete venue").slideDown("300");
			}
		});
	});
	$(".editvenue").click(function(){
		blockui();
		var venueid = $(this).parents("tr").attr("data-venueid");
		var data = {"venue_id":venueid,"action":"get"};
		console.log(data);
		$.ajax({
			url:"ajaxapi/venueaction.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				if(data.status == "success"){
					var venuedata = data.message;
					$("#venueform input[name = 'venue_name']").val(venuedata.venue_name);
					$("#venueform input[name = 'venue_address']").val(venuedata.venue_address);
					$("#venueform select").val(venuedata.venue_schoolid);
					$("#venueform input[name = 'venue_id']").val(venuedata.venue_id);
					$("#venueform #venue_cover_img").val(venuedata.venue_cover);
					$("#addvenuemodal .modal-header h2").html("Edit Venue");
					$("#addvenuemodal .modal-footer button").html("Update");
					if(venuedata.venue_cover != ""){
						photohtml = "<img src = 'http://dev.cheersu.com/img/venue_cover_img/icon_square/"+ venuedata.venue_cover+"' class = 'preview-img'/>";
						$("#venueform").append(photohtml);
						$("#addcover").hide();
						$("#removecover").show();
					}
					
					$.unblockUI();
					$("#addvenuemodal").modal();
				}
				else{
					$.unblockUI();
					$("#mainarea .errordiv").html("Unable to fetch venue details").slideDown("300");
				}
			},
			error:function(){
				$("#mainarea .errordiv").html("Unable to fetch venue details").slideDown("300");
			}
		})
	})
});