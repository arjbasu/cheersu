<?php
session_start();
include 'authentication_ajax_api.php';
if(isset($_POST['venue_id']) && isset($_POST['action'])){
	include '../connect.php';
	$venueid = $_POST['venue_id'];
	$action = $_POST['action'];
	if($action == "set"){
		if(!isset($_POST['comment'])){
			$status = "error";
			$message = "Improper Parameters passed";
			include 'json_encoding.php';
			die();
		}
		$userid = $_SESSION['user_id'];
		$comment = $_POST['comment'];
		$query = "INSERT INTO cheersu_comments(comment_user_id,comment_venue_id,comment_comment) VALUES (?,?,?)";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($userid,$venueid,$comment));
		if($stmt->rowCount() == 1){
			$query = "INSERT into cheersu_activity(activity_user_id,activity_type,activity_venue_id,activity_comment) ".
					"VALUES(?,?,?,?)";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($userid,"comment",$venueid,$comment));
			if($stmt->rowCount() == 1){
				$status = "success";
				$message = "Your comment has been successfully posted";
			}
			else{
				$status = "error";
				$message = "Unable to interact with database";
			}
		}
		else{
			$status = "error";
			$message = "Unable to interact with database";
		}
	}
	else if($action == "get"){
		$query = "SELECT user_id,comment_timestamp,comment_comment,user_firstname,user_lastname,user_dp ".
				"FROM cheersu_comments,cheersu_users ".
				"WHERE user_id = comment_user_id AND comment_venue_id = ? ORDER BY comment_timestamp DESC";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($venueid));
		if($stmt->rowCount() == 0){
			$status = "success";
			$message = "No comments available";
		}
		else{
			$message = array();
			
			while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
				include 'removeslashes.php';
				$tempcomment = array();
				$tempcomment['userid'] = $temp['comment_user_id'];
				$tempcomment['name'] = $temp['user_firstname']." ".$temp['user_lastname'];
			if($temp['user_dp'] == ""){
				$tempcomment['dp'] = "cheersu_icon.png";
			}
			else{
				$tempcomment['dp'] = stripslashes($temp['user_dp']);
			}
				$tempcomment['time'] = $temp['comment_timestamp'];
				$tempcomment['comment'] = stripcslashes($temp['comment_comment']);
				array_push($message,$tempcomment);
				
			}
			$status = "success";
		}
	}
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}
}
else{
	$status = "error";
	$message = "Improper parameters passed";
}
include 'json_encoding.php';
?>