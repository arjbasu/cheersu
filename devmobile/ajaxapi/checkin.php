<?php
session_start();
include 'authentication_ajax_api.php';
if(isset($_POST['venue_id']) && isset($_POST['action'])){
	include '../connect.php';
	$venueid = $_POST['venue_id'];
	$action = $_POST['action'];
	$userid = $_SESSION['user_id'];
	if($action == "set"){
		
		$query = "SELECT * FROM cheersu_checkins WHERE checkin_user_id = ? ORDER BY checkin_timestamp DESC LIMIT 1";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($userid));
		if($stmt->rowCount() > 0){
			$temp = $stmt->fetch(PDO::FETCH_ASSOC);
			if ($venueid == $temp['checkin_venue_id']){
				$status = "error";
				$message = "You are currently checked-in to this venue";
				include 'json_encoding.php';
				die();
			}
		}
		
		
		$query = "INSERT INTO cheersu_checkins(checkin_user_id,checkin_venue_id) VALUES (?,?)";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($userid,$venueid));
		if($stmt->rowCount() == 1){
			$query = "INSERT into cheersu_activity(activity_user_id,activity_type,activity_venue_id) ".
					"VALUES(?,?,?)";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($userid,"checkin",$venueid));
			if($stmt->rowCount() == 1){
				$query = "SELECT * FROM cheersu_checkins WHERE checkin_user_id = '$userid' AND checkin_venue_id = '$venueid'";
				$result = mysql_query($query);
				if(!$result){
					$status = "error";
					$message = "Unable to checkin";
				}
				else if(mysql_num_rows($result) == 1){
					$query = "UPDATE cheersu_venues SET venue_checkins = venue_checkins+1 WHERE venue_id = '$venueid'";
					$result = mysql_query($query);
					if(!$result){
						$status = "error";
						$message = "Unable to interact with database";
					}
					else if(mysql_affected_rows() == 1){
						$status = "success";
						$message = "You have successfully checked in";
					}
				}
				else{
					$status = "success";
					$message = "You have successfully checked in";
				}
			}
			else{
				$status = "error";
				$message = "Unable to interact with database";
			}
		}
		else{
			$status = "error";
			$message = "Unable to interact with database";
		}
	}
	else if($action == "get"){
		$query = "SELECT user_id,user_firstname,user_lastname,user_dp ".
				"FROM cheersu_checkins,cheersu_users ".
				"WHERE user_id = checkin_user_id AND checkin_venue_id = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($venueid));
		if($stmt->rowCount() == 0){
			$status = "error";
			$message = "No Checkins for this venue yet";
		}
		else{
			$message = array();
			
			while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
				$tempcheckin = array();
				$tempcheckin['userid'] = $temp['user_id'];
				include 'removeslashes.php';
				$tempcheckin['name'] = $temp['user_firstname']." ".$temp['user_lastname'];
				//$tempcheckin['time'] = $temp['checkin_timestamp'];
				if($temp['user_dp'] == ""){
					$tempcheckin['dp'] = "cheersu_icon.png";
				}
				else{
					$tempcheckin['dp'] = $temp['user_dp'];
				}
				array_push($message,$tempcheckin);
				
			}
			$status = "success";
			$message = array_map("unserialize", array_unique(array_map("serialize", $message)));
		}
	}
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}
}
else{
	$status = "error";
	$message = "Improper parameters passed";
}
include 'json_encoding.php';
?>