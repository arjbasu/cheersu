<?php
	include 'detectmobilebrowser.php';
	session_start();
	include 'readcookie.php';
	if(isset($_SESSION['logged']) && isset($_SESSION['username']) && isset($_SESSION['user_id'])){
		header("Location:dashboard.php");
	}
	include 'connect.php';
	include 'twiginit.php';
	$query = "SELECT school_name,school_id FROM cheersu_schools ORDER BY school_name ASC";
	$result = mysql_query($query);
	if(!$result){
		die("Unable to connect to databases");
	}
	else{
		$schools = array();
		$tempschool = array();
		while($temp = mysql_fetch_assoc($result)){
			$tempschool['name'] = $temp['school_name'];
			$tempschool['id'] = $temp['school_id'];
			array_push($schools, $tempschool);
		}
	}
	echo $twig->render('index.twig',array("schools"=>$schools));
?>