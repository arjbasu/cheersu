<?php
	function post_notification($type){
		global $pdo;
		global $cliqueid;
		global $userid;
		$query = "SELECT clique_creator,clique_name FROM cheersu_cliques WHERE clique_id = ?";
		$stmt = $pdo->prepare($query);
		$result = $stmt->execute(array($cliqueid));
		if($result){
			$temp = $stmt->fetch(PDO::FETCH_ASSOC);
			$creator = $temp['clique_creator'];
			if($creator != $userid){
				$cliquename = $temp['clique_name'];
				$query = "INSERT INTO cheersu_notifications_$creator (notification_activity_id,notification_user_id,notification_type) VALUES (?,?,?)";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array(0,$userid,"clique|own$type|$cliquename|$cliqueid"));	
			}
			
		}
	}
?>
