
$(document).on("pageshow",function(){
	check_notifications();
	var ajaxinprogress = false;
	$("body").ajaxStart(function(){
		ajaxinprogress = true;
	});
	$("body").ajaxStop(function(){
		ajaxinprogress = false;
	});

	$(".confirmfriend").click(function(){
		if(ajaxinprogress === true)
			return false;
		$.mobile.loading('show');
		var userid = $(this).parents("tr").attr("data-userid");
		var data = {"user_id":userid,"action":"confirm"};
//				console.log(data);
		$.ajax({
			url:"ajaxapi/friend.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated")
					window.location = "index.php";
				else if(data.status == "success"){
					$.mobile.loading('hide');
					$("#notification").find("p").html(data.message).parents("#notification").popup("open");
					setTimeout(function(){ $.mobile.changePage("notifications.php");},600);
				}
				else{
					$.mobile.loading('hide');
					$("#notification").find("p").html(data.message).parents("#notification").popup("open");
				}
			},
			error:function(){
				$.mobile.loading('show');
				$("#notification").find("p").html("An error occurred").parents("#notification").popup("open");
			}
		});
	});

	$(".deletefriend").click(function(){
		if(ajaxinprogress === true)
			return false;
		$.mobile.loading('show');
		var userid = $(this).parents("tr").attr("data-userid");
		var data = {"user_id":userid,"action":"delete"};
//				console.log(data);
		$.ajax({
			url:"ajaxapi/friend.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated")
					window.location = "index.php";
				else if(data.status == "success"){
					$.mobile.loading('hide');
					$("#notification").find("p").html(data.message).parents("#notification").popup("open");
					setTimeout(function(){ $.mobile.changePage("notifications.php");},600);
				}
				else{
					$.mobile.loading('hide');
					$("#notification").find("p").html(data.message).parents("#notification").popup("open");
				}
			},
			error:function(){
				$.mobile.loading('hide');
				$("#notification").find("p").html(data.message).parents("#notification").popup("open");
			}
		});
	});
});