<?php
    session_start();
	include 'check_authorization.php';
	include 'twiginit.php';
	include 'connect.php';
	$data = array();
	if(isset($_GET['id'])){
		$cliqueid = stripslashes($_GET['id']);
		$userid = $_SESSION['user_id'];
		$query = "SELECT member_id FROM cheersu_clique_members WHERE member_cliqueid = $cliqueid AND member_userid = $userid";
		$result = mysql_query($query);
		if(mysql_num_rows($result) == 0){
			header("Location:index.php");
		}
		
		
		
		$query = "SELECT user_dp_icon,CONCAT(user_firstname,' ',user_lastname),user_id FROM cheersu_users WHERE user_id = $userid";
		$result = mysql_query($query);
		$temp = mysql_fetch_row($result);
		$data['chat_dp_icon'] = $temp[0];
		$data['chat_dp_name'] = $temp[1];
		$data['userid'] = $temp[2];
	
		$query = "SELECT clique_name AS name,clique_creator, COUNT(member_userid) AS members,clique_image AS img FROM cheersu_cliques INNER JOIN cheersu_clique_members ".
		"ON clique_id = member_cliqueid AND clique_id = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($cliqueid));
		if($stmt->rowCount() == 1){
			
			$temp = $stmt->fetch(PDO::FETCH_ASSOC);
			if(!isset($temp['name']) || $temp['name'] == ""){
				header("Location:dashboard.php");				
			}
			if($temp['clique_creator'] == $userid)
				$data['myclique'] = true;
			$data['image'] = $temp['img'];
			$data['title'] = $temp['name'];
			$data['cliqueid'] = $cliqueid;
			$data['nummembers'] = $temp['members'];
			
			$query = "SELECT user_id AS id, CONCAT(user_firstname,' ',user_lastname) AS name,
			user_dp_icon AS dp FROM cheersu_users INNER JOIN (SELECT member_userid
			AS mid, COUNT(clique_activity_userid) AS popularity FROM 
			cheersu_clique_members LEFT JOIN (SELECT clique_activity_userid FROM 
			cheersu_cliques_activity a WHERE clique_activity_cliqueid = ?) c ON 
			member_userid = c.clique_activity_userid WHERE member_cliqueid = ? GROUP BY member_userid )
			 AS m ON m.mid = user_id ORDER BY popularity DESC ";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($cliqueid,$cliqueid));
			$members = array();
			while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
				if($temp['dp'] == ""){
					$temp['dp'] = "cheersu_icon.png";
				}
				array_push($members,$temp);
			}
			$data['members'] = $members;
			
			
			
			$query = "SELECT clique_activityid FROM cheersu_cliques_activity WHERE clique_activity_cliqueid = '$cliqueid' ORDER BY clique_activity_timestamp DESC LIMIT 100";
			//error_log("activityidquery:$query",0);
			$result = mysql_query($query);
			if(!$result){
				die("unable to proceed1");
			}
	
			$commentarray = array();
			if(mysql_num_rows($result) != 0){
				$activityidarray = array();
				while($temp = mysql_fetch_row($result)){
					array_push($activityidarray,$temp[0]);
				}
				$activityidlist = "(".implode(",",$activityidarray).")";
				
				$query = "SELECT user_comment_id AS commentid,user_comment_cliqueactivityid AS actid,user_comment_message AS comment,user_comment_id AS id,user_comment_timestamp AS time,
				 		CONCAT(user_firstname,' ',user_lastname) AS name,user_id AS userid,user_dp_icon AS dp 
						FROM cheersu_user_comments INNER JOIN cheersu_users ON user_comment_user_id = user_id WHERE user_comment_cliqueactivityid IN $activityidlist
						ORDER BY user_comment_cliqueactivityid DESC,user_comment_timestamp DESC";
				//error_log("commentquery:$query",0);
				
				$result = mysql_query($query);
				if(!$result){
					die("unable to proceed2");
				}
				
				while($temp = mysql_fetch_assoc($result)){
					$activityid = $temp['actid'];
					$temp['comment'] = stripslashes($temp['comment']);
					if(array_key_exists($activityid,$commentarray)){
						if(count($commentarray[$activityid]) < 3){
							array_unshift($commentarray[$activityid],$temp);
						}
					}
					else{
						$commentarray[$activityid] = array();
						array_unshift($commentarray[$activityid],$temp);
					}
				}	
			}
			
			
			// print_r($commentarray);
			
			
			
			
			$query = "SELECT user_id AS id, CONCAT(user_firstname,' ',user_lastname) AS name,
			user_dp_icon AS dp, clique_activity_type AS type,clique_activity_timestamp as timestamp, datediff(now(),clique_activity_timestamp) as datedif ,
			 timediff(now(),clique_activity_timestamp) as timedif, post_content AS post, venue_name AS venuename,like_act,
			 COUNT(distinct like_user_id) AS likes , COUNT(distinct user_comment_id) AS user_comments,
			 school_name AS schoolname , school_id AS schoolid, clique_activityid AS actid FROM cheersu_cliques_activity INNER JOIN cheersu_users
			 ON user_id = clique_activity_userid LEFT JOIN cheersu_likes ON clique_activityid = like_cliqueactivity_id
			 LEFT JOIN cheersu_user_comments ON clique_activityid = user_comment_cliqueactivityid LEFT JOIN (cheersu_venues INNER JOIN 
			cheersu_schools ON venue_schoolid = school_id) ON clique_activity_venueid = venue_id LEFT JOIN cheersu_clique_posts
			ON clique_activity_postid = post_id  
			LEFT OUTER JOIN (SELECT like_cliqueactivity_id AS like_act FROM cheersu_likes WHERE like_user_id = '$userid') ld ON like_act = clique_activityid 
			WHERE clique_activity_cliqueid = ? GROUP BY clique_activityid ORDER BY clique_activity_timestamp DESC";
			//error_log("mainquery:$query");
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($cliqueid));
			
			if($stmt->rowCount() > 0){
				$cliquefeed = array();
				while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
					
					$temp['liked'] = $temp['like_act'];
					
					if($temp['dp'] == ""){
						$temp['dp'] = "cheersu_icon.png";
					}

					if($temp['type'] == 'photo-activity'){
						$activityuserid = $temp['id'];
						$timestamp = $temp['timestamp'];
						$query = "SELECT clique_photos_source FROM cheersu_clique_photos WHERE clique_photos_timestamp = '$timestamp' AND clique_photos_userid = '$activityuserid'";
						$result = mysql_query($query);
						if(!$result){
							die("Unable to get photos");
						}
						else {
							$photos = array();
							while($tempphotos = mysql_fetch_assoc($result)){
								array_push($photos,$tempphotos['clique_photos_source']);
							}				
							$temp['photos'] = $photos;
						}
					}
					
					if(array_key_exists($temp['actid'],$commentarray)){
						$temp['topcomments'] = $commentarray[$temp['actid']];
					}
					
					$datediff = $temp['datedif'];
					$timediff = $temp['timedif'];
					if($datediff > 0){
						$temp['timestamp'] = $datediff."d";
							
					}
					else{
						$timearray = explode(":", $timediff);
						if($timearray[0] > 0){
							if($timearray[0]<10){
								$timearray[0] = substr($timearray[0], 1,1);
							}
							$temp['timestamp'] = $timearray[0]."h";
	
						}
						else if($timearray[1] > 0){
							if($timearray[1]<10){
								$timearray[1] = substr($timearray[1], 1,1);
							}
							$temp['timestamp'] = $timearray[1]."m";
						}
						else{
							if($timearray[2]<10){
								$timearray[2] = substr($timearray[2], 1,1);
							}
							$temp['timestamp'] = $timearray[2]."s";
						}
					}
					array_push($cliquefeed,$temp);
				}
				$data['activity'] = $cliquefeed;
				// print_r($cliquefeed);	
			}
			$query = "SELECT school_id AS id, school_name AS name FROM cheersu_schools WHERE school_id <> 0 ORDER BY school_name";
			$result = mysql_query($query);
			if(!$result){
				die("Unable to process");
			}
			else {
				$schoollist = array();
				while($temp = mysql_fetch_assoc($result)){
					array_push($schoollist,$temp);
				}
				$data['schoollist'] = $schoollist;
			}
			// print_r($data);
			echo $twig->render('cliques.twig',$data);	
		}	
		else {
			header("Location:dashboard.php");	
		}
	}
	else {
		header("Location:dashboard.php");	
	}
?>