<?php
	/* multifileupload.php
	 * Author: Arjun Basu
	 * 
	 * This script handles multiple file uploads which is fired from the fallback
	 * fileuploader. Some parameters are to be passed so that this single 
	 * file can take care of file uploads in the dashboard,cliques and venues.
	 * 
	 */
	session_start();
	require_once 'check_authorization.php';
	
	function dataEntry($filenames,$page,$extraparam){
		global $pdo;
		$userid = $_SESSION['user_id'];
		$query = "SELECT now() AS timestamp";
		$result = mysql_query($query);
		error_log("NUm rows:".mysql_num_rows($result));
		if($result){
			$temp = mysql_fetch_assoc($result);
			$timestamp = $temp['timestamp'];
			$query = "BEGIN";
			$stmt = $pdo->prepare($query);
			$stmt->execute();
			switch ($page) {
				case 'dashboard':
					$query = "INSERT INTO cheersu_user_photos(user_photo_user_id,user_photo_source,user_photo_timestamp) VALUES ";
					break;
				case 'venue':
					$query = "INSERT INTO cheersu_venue_photos(venue_photo_user_id,venue_photo_source,venue_photo_timestamp,venue_photo_venue_id) VALUES ";
					break;
					
				case 'clique':
					$query = "INSERT INTO cheersu_clique_photos(clique_photos_userid,clique_photos_source,clique_photos_timestamp,clique_photos_cliqueid) VALUES ";
					break;
				default:
					
					break;
			}
			
			$insertquery = array();
			$insertdata = array();
			foreach($filenames as $data){
				if($extraparam == 0){
					$insertquery[] = '(?,?,?)';
					$insertdata[] = $userid;
					$insertdata[] = $data;
					$insertdata[] = $timestamp;
				}
				else{
					$insertquery[] = '(?,?,?,?)';
					$insertdata[] = $userid;
					$insertdata[] = $data;
					$insertdata[] = $timestamp;
					$insertdata[] = $extraparam;
				}
				
			}
			if(!empty($insertquery)){
				$query.= implode(', ',$insertquery);
				$stmt = $pdo->prepare($query);
				error_log("query:".$query."|");
				$stmt->execute($insertdata);
				if($stmt->rowCount() >= 1){
					if($extraparam == 0){
						$query = "INSERT INTO cheersu_activity(activity_user_id,activity_type,activity_venue_id,activity_timestamp) VALUES (?,?,?,?)";
						$stmt = $pdo->prepare($query);
						$stmt->execute(array($userid,"photo-activity",0,$timestamp));
						if($stmt->rowCount() == 1){
							$query = "COMMIT";
							$stmt = $pdo->prepare($query);
							$stmt->execute();
							
						}
						else{
							$query = "ROLLBACK";
							$stmt = $pdo->prepare($query);
							$stmt->execute();
							
						}
					}
					else if($page == "clique"){
						$query = "INSERT INTO cheersu_cliques_activity(clique_activity_cliqueid,clique_activity_type,clique_activity_userid,clique_activity_timestamp) VALUES (?,?,?,?)";
						$stmt = $pdo->prepare($query);
						$stmt->execute(array($extraparam,"photo-activity",$userid,$timestamp));
						if($stmt->rowCount() == 1){
							$query = "COMMIT";
							$stmt = $pdo->prepare($query);
							$stmt->execute();
							
						}
						else{
							$query = "ROLLBACK";
							$stmt = $pdo->prepare($query);
							$stmt->execute();
							
						}
					}
					else{
						$query = "COMMIT";
						$stmt = $pdo->prepare($query);
						$stmt->execute();
					}
				}
				else{
					$query = "ROLLBACK";
					$stmt = $pdo->prepare($query);
					$stmt->execute();
					
				}
			}
		}
	}
	
	if(isset($_POST['redirect']) && isset($_FILES['file']) && isset($_POST['page'])
	&& isset($_POST['extraparam'])){
		
		error_log("got all params");
		$filenamearray = array();
		require_once 'connect.php';
		include('ajaxapi/SimpleImage.php');
		$redirPage = $_POST['redirect'];
		$extraparam = $_POST['extraparam'];
		$page = $_POST['page'];
		$path = "";
		switch ($page) {
			case 'dashboard':
				$path = "img/wall_img";
				break;
			case 'clique':
				$path = "img/clique_img";
				break;
				
			case 'venue':
				$path = "img/venue_img";
				break;
				
			default:
				header("Location:$redirPage");
		}
		
		
		$tmppath = $_FILES['file']['tmp_name'];
		//print_r($tmppath);
		$count = 0;
		define('DESIRED_IMAGE_WIDTH', 200);
		define('DESIRED_IMAGE_HEIGHT', 200);
		foreach($_FILES['file']['name'] as $file){
			if($file == "" || $file == null)
				continue;
			//print(count($_FILES['photoupload']));
			//print_r($_FILES['photoupload']);
			$extarray = array();
			$extarray = explode(".", $file);
			$extension = $extarray[count($extarray)-1];
			error_log("extension|$extension",0);
			$name = md5($file.$_SESSION['username']);
			$name .= ".$extension";
			error_log("File name: $name");
			
			if (!file_exists("$path/$name")){
				move_uploaded_file($tmppath[$count],
					"$path/$name");
				$image = new SimpleImage();
				$image->load("$path/$name");
				if($image->getWidth() > 1200)
					$image->resizeToWidth("1200");
				$image->save("$path/$name");
				$source_path = "$path/$name";
					
				
					
				list($source_width, $source_height, $source_type) = getimagesize($source_path);
				$type = "";
				switch ($source_type) {
					case IMAGETYPE_GIF:{
						$source_gdim = imagecreatefromgif($source_path);
						$type = "GIF";
						break;
					}
					case IMAGETYPE_JPEG:{
						$type = "jpeg";
						$source_gdim = imagecreatefromjpeg($source_path);
						break;
					}
					case IMAGETYPE_PNG:{
						$type = "png";
						$source_gdim = imagecreatefrompng($source_path);
						break;
					}
				}
					
				$source_aspect_ratio = $source_width / $source_height;
				$desired_aspect_ratio = DESIRED_IMAGE_WIDTH / DESIRED_IMAGE_HEIGHT;
					
				if ($source_aspect_ratio > $desired_aspect_ratio) {
					/*
					 * Triggered when source image is wider
					*/
					$temp_height = DESIRED_IMAGE_HEIGHT;
					$temp_width = ( int ) (DESIRED_IMAGE_HEIGHT * $source_aspect_ratio);
				} else {
					/*
					 * Triggered otherwise (i.e. source image is similar or taller)
					*/
					$temp_width = DESIRED_IMAGE_WIDTH;
					$temp_height = ( int ) (DESIRED_IMAGE_WIDTH / $source_aspect_ratio);
				}
					
				/*
				 * Resize the image into a temporary GD image
				*/
					
				$temp_gdim = imagecreatetruecolor($temp_width, $temp_height);
				imagecopyresampled(
				$temp_gdim,
				$source_gdim,
				0, 0,
				0, 0,
				$temp_width, $temp_height,
				$source_width, $source_height
				);
					
				/*
				 * Copy cropped region from temporary image into the desired GD image
				*/
					
				$x0 = ($temp_width - DESIRED_IMAGE_WIDTH) / 2;
				$y0 = ($temp_height - DESIRED_IMAGE_HEIGHT) / 2;
				$desired_gdim = imagecreatetruecolor(DESIRED_IMAGE_WIDTH, DESIRED_IMAGE_HEIGHT);
				imagecopy(
				$desired_gdim,
				$temp_gdim,
				0, 0,
				$x0, $y0,
				DESIRED_IMAGE_WIDTH, DESIRED_IMAGE_HEIGHT
				);
					
				/*
				 * Render the image
				* Alternatively, you can save the image in file-system or database
				*/
				if($type == "jpeg"){
					imagejpeg($desired_gdim,"$path/icons/$name");
				}
				else if($type == "png"){
					imagepng($desired_gdim,"$path/icons/$name");
				}
				else if($type = "gif"){
					imagegif($desired_gdim,"$path/icons/$name");
				}
				array_push($filenamearray,$name);		
			}
            $count++;
		}
		if(count($filenamearray) != 0){
			dataEntry($filenamearray,$page,$extraparam);
		}
		error_log("header string: $redirPage");
		header("Location:$redirPage");
	}
	
	else if(isset($_POST['redirect'])){
		error_log("redirect got");
		$redirPage = $_POST['redirect'];
		$headerString = "Location:$redirPage";
		header($headerString);
	}
	else{
		error_log("redirect failed");
		header("Location:index.php");
	}
?>