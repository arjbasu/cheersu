<?php
session_start();
include 'authentication_ajax_api.php';
include '../connect.php';
$userid = $_SESSION['user_id'];
if(!(isset($_POST['activitytype']) && isset($_POST['venuename']) && $_POST['schoolname'])){
	$status = "error";
	$message = "Improper parameters passed";
	include 'json_encoding.php';
	die();
}
$activitytype = $_POST['activitytype']."|external";
$venuename = $_POST['venuename'];
$schoolname = $_POST['schoolname'];
$query = "INSERT INTO cheersu_activity(activity_user_id,activity_type,activity_venue_id,activity_comment) ".
		"VALUES (?,?,?,?)";
$stmt = $pdo->prepare($query);
$stmt->execute(array($userid,$activitytype,0,"$venuename|$schoolname"));
if($stmt->rowCount() == 1){
	$status = "success";
	$message = "You have successfully updated your activity";
}
else{
	$status = "error";
	$message = "Unable to insert into database";
}

include 'json_encoding.php';
?>