<?php
	if(isset($_POST['email'])){
		include 'connect.php';
		$email = $_POST['email'];
		$query = "SELECT user_id FROM cheersu_users WHERE user_email = ?";
		$message['query'] = $email;
		$stmt = $pdo->prepare($query);
		$result = $stmt->execute(array($email));
		if($stmt->rowCount() >= 1){
			$user = $stmt->fetch();
			$user_id = $user['user_id'];
			$hash = md5($user_id.$email.date('m/d/Y h:i:s a', time()));
			$querynew = "UPDATE cheersu_users SET user_password_reset=1, user_verification_id='$hash' ".
			"WHERE user_id = '$user_id'";
			$result = mysql_query($querynew);
			if(!$result){
				$status = "Error";
				$message = "Error generating password reset link";
			}
			else{
				$subject = "[Cheersu] Password Reset Link";
				$content = "Your password reset link is:\n\n".
				"http://cheersu.com/resetpassword.php?email=$email&hash=$hash\n\n".
				"Please visit the above link to reset your password. If you did not request for a password\n".
				"reset, then ignore this email";
				$from = "no-reply@cheersu.com";
				$headers = "From: CheersU <$from> \r\n" .
				'Reply-To: no-reply@cheersu.com' . "\r\n" .
				'X-Mailer: PHP/' . phpversion();
				mail($email, $subject, $content,$headers);
				$status = "success";
				$message = "Password reset link has been successfully sent to $email. Please follow the".
				" instructions provided in the mail";
			}
			
		}
		else{
			$status = "Error";
			$message = "No records found";
		}
		
	}
	else{
		$status = "Error";
		$message = "Improper parameters passed";
	}
	include 'json_encoding.php';
?>