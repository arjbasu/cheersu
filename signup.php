<?php
	session_start();
	include 'connect.php';
	include 'createtables.php';
	if(isset($_POST['firstname']) && isset($_POST['lastname']) && isset($_POST['schoolemail']) &&
			isset($_POST['school']) && isset($_POST['sex']) && isset($_POST['password'])){
		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
		$email = $_POST['schoolemail'];
		$emailbreakup = explode(".",$email);
		$length = count($emailbreakup);
		$last = $emailbreakup[$length-1];
		if($last != "edu"){
			$status = "error";
			$message = "You can only register with a .edu email id";
			include 'json_encoding.php';
			die();
		}
		$school = $_POST['school'];
		$sex = $_POST['sex'];
		$password = $_POST['password'];
		
		$query = "SELECT * FROM cheersu_users WHERE user_email = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($email));
		if($stmt->rowCount()>=1){
			$status = "Error";
			$message = "Email already exists.";
		}
		else{
			$passhash = crypt($password,'$1$foreverdope12$');
			$str = "$email$password";
			$hashvalue = md5($str);
			error_log($str,0);
			error_log($hashvalue,0);
			$stmt = $pdo->prepare("INSERT INTO cheersu_users(user_firstname, user_lastname,user_email,user_passhash,user_schoolid,user_sex,user_verification_id,user_verified) VALUES (?,?,?,?,?,?,?,?)");
			$stmt->execute(array($firstname,$lastname,$email,$passhash,$school,$sex,$hashvalue,1));
			if($stmt->rowCount() == 1){
				$subject = "[Cheersu] Verification of account for $firstname";
				$message = "Congratulations!! You have succesfully registered for CheersU.\n\n".
						"Please click on the link below to verify your account:\n\n".
						"http://cheersu.com/verify.php?name=".urlencode($firstname)."&hash=$hashvalue\n\n".
						"If you did not register with Cheersu, please ignore this email";
				$from = "no-reply@cheersu.com";
				$headers = "From: Cheersu <$from> \r\n";
				if($school == 3){
					$headers .= "BCC: support@cheersu.com \r\n";
				}
				$headers .='Reply-To: no-reply@cheersu.com' . "\r\n" .
				'X-Mailer: PHP/' . phpversion();
				error_log($message,0);
				//mail($email, $subject, $message,$headers);
				$query = "SELECT user_id FROM cheersu_users ORDER BY user_id DESC LIMIT 1";
				$stmt = $pdo->prepare($query);
				$stmt->execute();
				if($stmt->rowCount() == 1){
					$tempuser = $stmt->fetch(PDO::FETCH_ASSOC);
					$user_id = $tempuser['user_id'];
					createTables($user_id);
					$status = "success";
					$message = "Congratulations! You have successfully registered for CheersU.<br/> Use your email id and password to login now";
				}
				else{
					$status = "error";
					$message = "Unable to create tables";
				}
// 				$status = "success";
// 				$message = "Congratulations! You have successfully registered for CheersU.<br/>We have sent you a link to the "
// 				."email address you provided for verification. Please click on the link to verify you account.Remember to ".
// 				"check the spam folder too. <br/>If you did not receive the email or facing any other problems verifying, please send us your details to support@cheersu.com clearly mentioning your issues";
			}
			else{
				$status = "error";
				$message = "Unable to insert into db";
			}
		}
		
	}
	else{
		$status = "error";
		$message = "improper parameters passed";
	}
	include 'json_encoding.php';
?>