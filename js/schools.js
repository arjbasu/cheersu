$(document).ready(function(){
	check_notifications();
	initialisechat();
	$("area").each(function(){
		id = this.id
		$(this).attr({	
			"href":"#"
		})
	});
	$(".modal").on("hidden",function(){
		$("body").css("overflow","auto");
		$(".errordiv").hide();
		$(this).find(".loading,.errordiv,.successdiv").hide();
		$(this).find("form").trigger("reset").show();
	})
	$("#requestschoolform").submit(function(){
		var validation = true;
		var parent = $("#requestschoolmodal");
		$(this).find("input,select").each(function(){
			if($(this).val() == "" || $(this).val() == "NULL"){
				validation = false;
				return false;
			}
		});
		if(validation == false){
			alert("Please complete all the fields");
			return false;
		}
		else{
			var data = $(this).serialize();
			$(this).slideUp("200");
			$(parent).find(".loading").fadeIn("200");
			$.ajax({
				url:"ajaxapi/requests.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						$(parent).find(".loading").fadeOut("200").parents(".modal")
						.find(".successdiv").html(data.message).slideDown();
					}
					else{
						$(parent).find(".loading").fadeOut("200").parents(".modal")
						.find(".errordiv").html(data.message).slideDown();
						$("#requestvenueform").slideDown("200")
					}
				},
				error:function(){
					$(parent).find(".loading").fadeOut("200").parents(".modal")
					.find(".errordiv").html("An error occurred").slideDown();
					$("#requestvenueform").slideDown("200")
				}
			})
		}
		return false;
	});
	$(".modal").on("show",function(){
		$("body").css("overflow","hidden");
	})
	
	$("body").ajaxStart(function(){
		$(".errordiv").hide();
	});
	
	$("area").click(function(){
		$("#countrymapmouse").stop(false,false).hide();
		$("#schoollist .modal-header span").html(this.id);
		$.blockUI({ message: '<img src="img/ajax-loader.gif"/>' ,
			themedCSS: { 
		        width:  '30px', 
		        top:    '40%', 
		        left:   '35%' 
		    }, 
			css: { 
	            border: 'none', 
	            padding: '5px', 
	            backgroundColor: '#272B30', 
	            '-webkit-border-radius': '0px', 
	            '-moz-border-radius': '0px',
	            'border-radius':'0px',
	            opacity: .5, 
	            color: '#fff' 
	        }
			});
		statename = this.id;
		data = {"paramtype":"name","param":statename};
		console.log(data);
		var req = $.ajax({
			url:"ajaxapi/getschoollist.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				else if(data.status == "success"){
					var html = "";
					schools = data.message;
					schoolssize = schools.length;
					for(var i = 0;i < schoolssize;i++){
						id = schools[i].id;
						name = schools[i].name;
						html += "<a href = 'venues.php?schoolid="+
								id + "' class = 'btn btn-block'>" +
								name + "</a>";
					}
					console.log(html);
					$.unblockUI();
					$("#schoollist .modal-body").html(html);
					$("#schoollist").modal({
						"keyboard":true
					});
				}
				else{
					$.unblockUI();
					$(".errordiv").html(data.message).slideDown("300");
				}
			},
			error:function(){
				$.unblockUI();
				$(".errordiv").html("Unable to fetch the School list for <strong>"+statename+"</strong>. Please try again" +
						" later").slideDown("300");
			}
		})
		return false;
	})
	
	$("area").hover(function(){
		$("#txtStateName").text($(this).attr('id'));
		$('#countrymapmouse').fadeIn("100");
	},
	function(){
		$("#countrymapmouse").stop(false,false).hide();
    });
	

	$('#countrymapmouse').tooltip();
	$(document).bind('mousemove', function (e) {
        $('#countrymapmouse').css({
            left: e.pageX + 10,
            top: e.pageY + 10
        });
    });
});