<?php
	session_start();
	include 'authentication_ajax_api.php';
	include '../connect.php';
	$userid = $_SESSION['user_id'];
	
	$query = "SELECT notification_timestamp FROM cheersu_notifications_$userid ".
	"ORDER BY notification_timestamp DESC LIMIT 1";
	$stmt = $pdo->prepare($query);
	$stmt->execute();
	$count = 0;
	$query = "SELECT COUNT(*) AS unread FROM cheersu_friend_requests_$userid WHERE friend_request_seen = 'unread'";
	$result = mysql_query($query);
	if(!$result){
		$status = "error";
		$message= "unable to get unread friend requests";
	}
	else{
		$temp = mysql_fetch_assoc($result);
		$count+=$temp['unread'];
		error_log("Friendlist:".$count,0);
	}
	if($stmt->rowCount() != 1){
		$clause = "";
	}
	else{
// 		error_log("Usrid: ".$userid,0);
		$query = "SELECT COUNT(*) AS unread FROM cheersu_notifications_$userid WHERE ".
				"notification_status = 'unread'";
		error_log("randomquery: ".$query,0);
		$result = mysql_query($query);
		if(!$result){
			$status = "error";
			$message = "Unable to fetch unread notifications from table";
			include 'json_encoding.php';
			die();
		}
		else{
			$temp = mysql_fetch_assoc($result);
			$count += $temp['unread'];
		}
		$temp = $stmt->fetch(PDO::FETCH_ASSOC);
		$time = $temp['notification_timestamp'];
		$clause = "AND (DATEDIFF(activity_timestamp,'$time')>0 OR TIMEDIFF(activity_timestamp,'$time') > 0)";
		
# 		error_log("Clause:".$clause.":",0);
	}
	
	
	$query = "SELECT DISTINCT(clique_user_id) FROM cheersu_cliques_users_$userid";
	$result = mysql_query($query);
	if(!$result){
		$status = "error";
		$message = "Unable to select clique users";
		include 'json_encoding.php';
		die();
	}
	if(mysql_num_rows($result) == 0){
		if($count == 0){
			$status = "error";
			$message = "No new notifications";
		}
		else{
			$status = "success";
			$message = $count;
		}
		include 'json_encoding.php';
		die();
	}
	$list = "(";
	while($temp = mysql_fetch_assoc($result)){
		$list.=$temp['clique_user_id'].",";
	}
# 	error_log("LIST before:".$list,0);
	$list = substr($list, 0, -1).")";
	$list = "(0)";
	$query = "SELECT * FROM cheersu_activity WHERE activity_user_id IN $list $clause";
# 	error_log("CLause:".$query.":",0);
	$result = mysql_query($query);
	if(!$result){
		$status = "error";
		$message = "Unable to read activity table";
		include 'json_encoding.php';
		die();
	}
	if(mysql_num_rows($result) == 0 && $count == 0){
		$status = "error";
		$message = "No new notifications";
		include 'json_encoding.php';
		die();
	}
	else if(mysql_num_rows($result) == 0 && $count != 0){
		$status = "success";
		$message = $count;
	}
	else{
		$query = "";
		while($temp = mysql_fetch_assoc($result)){
			$activityid = $temp['activity_id'];
			$activityuserid = $temp['activity_user_id'];
			$timestamp = $temp['activity_timestamp'];
			$count++;
			$query.= "INSERT INTO cheersu_notifications_$userid(notification_activity_id,notification_user_id,".
					"notification_timestamp) VALUES ('$activityid','$activityuserid','$timestamp');";
			
		}
# 		error_log("CLause:".$query.":",0);
		$stmt = $pdo->prepare($query);
		$stmt->execute();
		if($stmt->rowCount() < 1){
			$status = "error";
			$message = "Unable to insert into database";
		}
		else{
			$status = "success";
			$message = $count;
		}
	}
	include 'json_encoding.php';
	
?>