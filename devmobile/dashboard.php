<?php
session_start();

include 'check_authorization.php';
include 'twiginit.php';
include 'connect.php';
$userid = $_SESSION['user_id'];


function cmp($a, $b)
{
	return $b['mutual'] - $a['mutual'];
}
function get_mutual_friends($userida,$useridb){
	//echo "<br/>$userida:$useridb<br/>";
	$query = "SELECT friend_user_id FROM cheersu_friends_$userida WHERE friend_status = 'confirmed'";
	$result = mysql_query($query);
	//echo $query;
	if(!$result){
		die("Unable to get friendlist");
	}
	else if(mysql_num_rows($result) == 0){
		return 0;
	}
	else{
		$friendlist = "(";
		while($temp = mysql_fetch_assoc($result)){
			$friendlist.= $temp['friend_user_id'].",";
		}
		$friendlist = substr($friendlist, 0, -1).")";
		//echo "<br/>$friendlist<br/>";
		$query = "SELECT COUNT(*) AS count FROM cheersu_friends_$useridb WHERE friend_user_id IN $friendlist AND friend_status = 'confirmed'";
		//echo $query."<br/>";
		$result = mysql_query($query);
		if(!$result || mysql_num_rows($result) == 0){
			die("Unable to tally results");
		}
		else{
			$temp = mysql_fetch_assoc($result);
			//echo "<br/>".$temp['count']."<br/>";
			return $temp['count'];
		}
	}
	
}

function return_main_users(){
	$query = "SELECT user_id,school_name,user_firstname,user_lastname,user_bio,user_dp FROM cheersu_users,cheersu_schools ".
	"WHERE user_id IN (5,6) AND school_id = user_schoolid";
	$result = mysql_query($query);
	if(!$result){
		die("unable to get suggestions");
	}
	$tempresult = array();

	while($temp = mysql_fetch_assoc($result)){

		if($temp['user_dp'] == ""){
			$temp['user_dp'] = "cheersu_icon.png";
		}
		include 'removeslashes.php';
		array_push($tempresult,$temp);
	}
	return $tempresult;
}
function generate_suggestions(){
	global $userid;
	global $pdo;
	$returnval = array();
	$query = "SELECT user_id,friend_status FROM cheersu_friends_$userid,cheersu_users WHERE "
	." friend_user_id = user_id AND user_blocked = 0";
	$result = mysql_query($query);
	if(!$result){
		die("unable to query databases");
	}
	if(mysql_num_rows($result) == 0){
		$nofriends = true;
		$query = "SELECT school_state, user_schoolid FROM cheersu_users,cheersu_schools WHERE user_id = ? ".
				" AND school_id = user_schoolid";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($userid));
		if($stmt->rowCount() < 1){
			die("Unable to query databases");
		}
		else{
			$temp = $stmt->fetch(PDO::FETCH_ASSOC);
			$state = $temp['school_state'];
			$school = $temp['schoolid'];
			$query = "SELECT user_firstname,school_name,user_id,user_lastname,user_bio,user_dp FROM cheersu_users,cheersu_schools WHERE user_schoolid = ? AND user_id <> '$userid' ".
			"AND school_id = user_schoolid AND user_verified = 1 AND user_blocked = 0";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($school));
			if($stmt->rowCount() == 0){
				$query = "SELECT user_firstname,school_name,user_id,user_lastname,user_bio,user_dp FROM cheersu_users,cheersu_schools WHERE user_schoolid IN ".
						"(SELECT school_id FROM cheersu_schools WHERE school_state = '$state') AND user_id <> '$userid' AND user_verified = 1 AND ".
						"user_blocked = 0 AND school_id = user_schoolid";
				$result = mysql_query($query);
				if(!$result){
					die("Unable to get users");
				}
				else if(mysql_num_rows($result) == 0){
					$tempresult = return_main_users();
					$returnval = $tempresult;
					
				}
				else{
					$tempresult = array();
					while($temp = mysql_fetch_assoc($result)){
						if($temp['user_dp'] == ""){
							$temp['user_dp'] = "cheersu_icon.png";
						}
						include 'removeslashes.php';
						array_push($tempresult,$temp);
					}
					$temps = return_main_users();
					array_merge($tempresult,$temps);
					$returnval = $tempresult;
					$returnval = array_map("unserialize", array_unique(array_map("serialize", $returnval)));
					
				}
					
			}
			else{
				$tempresult = array();
				while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
					if($temp['user_dp'] == ""){
						$temp['user_dp'] = "cheersu_icon.png";
					}
					include 'removeslashes.php';
					array_push($tempresult,$temp);
				}
				$temps = return_main_users();
				array_merge($tempresult,$temps);
				$returnval['suggestions'] = $tempresult;
				$returnval = array_map("unserialize", array_unique(array_map("serialize", $returnval)));
				
			}


		}
	}
	else{
		
		$myfriends = array(); 
		$list = "(";
		while($temp = mysql_fetch_assoc($result)){
			if($temp['friend_status'] == "confirmed")
				array_push($myfriends,$temp['user_id']);
			$list.=$temp['user_id'].",";
		}
		$list .= $userid.")";
		$friendlistlen = count($myfriends);
		//echo $list;
		for($i = 0; $i < $friendlistlen; $i++){
			$friend = $myfriends[$i];
			$query = "SELECT user_id,user_firstname,user_lastname,user_dp,school_name FROM cheersu_friends_$friend,".
					"cheersu_schools,cheersu_users WHERE user_id = friend_user_id AND school_id = user_schoolid AND user_id NOT IN $list";
			$result = mysql_query($query);
			if(!$result){
				die("unable to fetch friends of friends");
			}
			else if(mysql_num_rows($result)!=0){
				while($temp = mysql_fetch_assoc($result)){
					if($temp['user_dp'] == ""){
						$temp['user_dp'] = "cheersu_icon.png";
					}
					include 'removeslashes.php';
					$temp['mutual'] = get_mutual_friends($userid, $temp['user_id']);
					array_push($returnval,$temp);
				}
			}
		}
		
		$query = "SELECT user_id,user_firstname,user_lastname,user_dp,school_name FROM ".
					"cheersu_schools,cheersu_users WHERE (user_id = 5 OR user_id = 6) AND school_id = user_schoolid AND user_id NOT IN $list";
		//echo $query;
		$result = mysql_query($query);
		if(!$result){
			die("Unable to query");
		}
		else if(mysql_num_rows($result) != 0){
			while($temp = mysql_fetch_assoc($result)){
				if($temp['user_dp'] == ""){
					$temp['user_dp'] = "cheersu_icon.png";
				}
				include 'removeslashes.php';
				$temp['mutual'] = get_mutual_friends($userid, $temp['user_id']);
				array_push($returnval,$temp);
			}
		}
		$returnval = array_map("unserialize", array_unique(array_map("serialize", $returnval)));
		usort($returnval,"cmp");
		
	}
	//print_r($returnval);
	return $returnval;
}
$query = "SELECT user_firstname, user_lastname, user_bio, user_status, user_dp ".
		"FROM ".
		"cheersu_users WHERE".
		" user_id = '$userid'";
$result = mysql_query($query);
$data = array();
if(!$result){
	die("Unable to query databases");
}
else{
	$temp = mysql_fetch_assoc($result);
	$firstname = stripslashes($temp['user_firstname']);
	$lastname = stripslashes($temp['user_lastname']);
	$bio = $temp['user_bio'];
	$status = stripslashes($temp['user_status']);
	$img = stripslashes($temp['user_dp']);
	if($img == ""){
		$img = "300x200.gif";
	}

	$data['name'] = $firstname." ".$lastname;
	$data['dp'] = $img;
	$data['status'] = stripslashes($status);
	$data['username'] = $_SESSION['username'];
	$query = "SELECT * FROM cheersu_activity WHERE activity_type = 'checkin' AND activity_user_id = ?";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($userid));
	$checkins = $stmt->rowCount();
	$data['checkins'] = $checkins;

	$query = "SELECT myclique_id,myclique_title FROM cheersu_mycliques_$userid";
	$stmt = $pdo->prepare($query);
	$stmt->execute();
	if($stmt->rowCount() == 0){
		$nocliques = true;
		$data['nocliques'] = true;
	}
	else{
		$cliques = array();
		while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
			array_push($cliques,$temp);
		}
		$data['cliques'] = $cliques;
	}
	$query = "SELECT * FROM cheersu_friends_$userid WHERE friend_status = 'confirmed'";
	$result = mysql_query($query);
	$friends = mysql_num_rows($result);
	$data['numfriends'] = $friends;
	if($friends == 0){
		$nofriends = true;
		$data['nofriends'] = true;
	}
	else{
		$list = "($userid,";
		while($temp = mysql_fetch_assoc($result)){
			$list.=$temp['friend_user_id'].",";
		}
		// 			error_log("LIST before:".$list,0);
		$list = substr($list, 0, -1).")";
			
		$query = "SELECT user_id,user_firstname,user_lastname,user_dp,venue_name,school_name,activity_type,activity_comment,".
				"school_id, datediff(now(),activity_timestamp) as datedif , timediff(now(),activity_timestamp) as timedif".
				" FROM cheersu_users,cheersu_schools, cheersu_activity,".
				"cheersu_venues WHERE activity_user_id = user_id AND venue_id = activity_venue_id ".
				"AND school_id = venue_schoolid AND user_id IN $list ORDER BY activity_timestamp DESC LIMIT 100";
		error_log("LIST before:".$query,0);
		$stmt = $pdo->prepare($query);
		$stmt->execute();
		if($stmt->rowCount() == 0){
			$nofriendactivity = true;
			$data['nofriendactivity'] = true;
		}
		else{
			$activitylist = array();
			$tempactivity = array();
			while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
				$tempactivity = array();
				$tempactivity['dp'] = $temp['user_dp'];
				if($tempactivity['dp'] == "")
					$tempactivity['dp'] = "cheersu_icon.png";
					
				$type = $temp['activity_type'];
				$tempactivity['comment'] = stripslashes($temp['activity_comment']);
				$tempactivity['venue'] = stripslashes($temp['venue_name']);
				$tempactivity['schoolname'] = stripslashes($temp['school_name']);
				if($type == "checkin"){
					$tempactivity['type'] = "checked in to";
				}
				else if($type == "reserve"){
					$tempactivity['type'] = "reserved on to";
				}
				else if($type == "comment"){
					$tempactivity['type'] = "commented on";
				}
				else if($type == "status"){
					$tempactivity['type'] = "updated his status:";
					$tempactivity['commentactivity'] = true;
				}
				else if(stristr($type, "external") != false){
					error_log("$type",0);
					$tempactivity['external'] = true;
					$typearray = explode("|",$type);
					$tempact = $typearray[0];
					error_log("$tempact",0);
					if($tempact == "reserve"){
						$tempactivity['type'] = "reserved on to";
					}
					else if($tempact == "checkin"){
						$tempactivity['type'] = "checked in to";
					}
					$details = explode("|",$tempactivity['comment']);
					$tempactivity['schoolname'] = $details[1];
					$tempactivity['venue'] = $details[0];
				}
					
				$tempactivity['name'] = stripslashes($temp['user_firstname']." ".$temp['user_lastname']);
				$tempactivity['schoolid'] = $temp['school_id'];
				$tempactivity['id'] = $temp['user_id'];
				$datediff = $temp['datedif'];
				$timediff = $temp['timedif'];
				if($datediff > 0){
					$tempactivity['timestamp'] = $datediff."d";
				}
				else{
					$timearray = explode(":", $timediff);
					if($timearray[0] > 0){
						if($timearray[0]<10){
							$timearray[0] = substr($timearray[0], 1,1);
						}
						$tempactivity['timestamp'] = $timearray[0]."h";

					}
					else if($timearray[1] > 0){
						if($timearray[1]<10){
							$timearray[1] = substr($timearray[1], 1,1);
						}
						$tempactivity['timestamp'] = $timearray[1]."m";
					}
					else{
						if($timearray[2]<10){
							$timearray[2] = substr($timearray[2], 1,1);
						}
						$tempactivity['timestamp'] = $timearray[2]."s";
					}
				}
				array_push($activitylist,$tempactivity);
			}
			$data['activitylist'] = $activitylist;
			
		}
	}
// 	$suggestions = generate_suggestions();
// 	if(count($suggestions) > 0){
// 		if(count($suggestions)>3){
// 			$suggestions = array_splice($suggestions,3);
// 		}
// 		$data['suggestions'] = $suggestions;
// 	}
	
	echo $twig->render("dashboard.twig",$data);

}

?>