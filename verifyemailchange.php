<?php
session_start();
if(isset($_GET['userid']) && isset($_GET['hash'])){
	$user_id = $_GET['userid'];
	$hash = $_GET['hash'];
	include 'connect.php';
	include 'twiginit.php';
	$query = "SELECT user_email_pending,user_firstname FROM cheersu_users WHERE user_id = ? AND user_verification_id = ?";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($user_id,$hash));
	if($stmt->rowCount() != 1){
		echo $twig->render("generic.html",array(
				"title"=>"Expired Link",
				"error"=>"Bad Parameters passed"
		));
	}
	else{
		$user = $stmt->fetch();
		$pending = $user['user_email_pending'];
		$user_name = $user['user_firstname'];
		if($pending == ""){
			echo $twig->render("generic.html",array(
						"title"=>"Expired Link",
						"error"=>"No pending email requests"
					));
		}
		else{
			$query = "UPDATE cheersu_users SET user_email = '$pending', user_email_pending = ''";
			$result = mysql_query($query);
			if(!$result){
				echo $twig->render("generic.html",array(
						"title"=>"Expired Link",
						"error"=>"Unable to interact with database"
				));
			}
			else{
				$_SESSION['logged'] = true;
				$_SESSION['user_id'] = $user_id;
				$_SESSION['username'] = $user_name;
				header("Location:settings.php");
			}
		}
	}
}
else{
	echo $twig->render("generic.html",array(
						"title"=>"Expired Link",
						"error"=>"The Link has expired"
					));
}
?>