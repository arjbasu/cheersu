<?php
	session_start();
	include 'authentication_ajax_api.php';
	$status = "";
	$message = "";
	if(isset($_POST['type']) && isset($_POST['venue_id'])){
		include '../connect.php';
		$userid = $_SESSION['user_id'];
		$type = $_POST['type'];
		$venueid = $_POST['venue_id'];
		if($type == "getimages"){
			$query = "SELECT * FROM cheersu_venue_photos WHERE venue_photo_venue_id = ? ORDER BY venue_photo_timestamp DESC";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($venueid));
			if($stmt->rowCount() == 0){
				$status = "success";
				$message = "No Images available";
			}
			else{
				$status = "success";
				$venue_image = array();
				while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
					array_push($venue_image,$temp);
				}
				$message = $venue_image;
			}
		}
		else if($type == "delete"){
			if(isset($_POST['filename'])){
				chdir("../");
				foreach($_POST['filename'] as $imgname){
					unlink("img/venue_img/$imgname");
					unlink("img/venue_img/icons/$imgname");
				}
				$status = "success";
				$message = "files successfully deleted";
			}
			else{
				$status = "error";
				$message = "Improper parameters passed";
			}
		}
		else if($type == "post"){
			if(isset($_POST['filename'])){
				$query = "SELECT now() AS timestamp";
				$result = mysql_query($query);
				error_log("NUm rows:".mysql_num_rows($result));
				if($result){
					$temp = mysql_fetch_assoc($result);
					$timestamp = $temp['timestamp'];
					$query = "BEGIN";
					$stmt = $pdo->prepare($query);
					$stmt->execute();
					$query = "INSERT INTO cheersu_venue_photos(venue_photo_user_id,venue_photo_source,venue_photo_timestamp,venue_photo_venue_id) VALUES ";
					$insertquery = array();
					$insertdata = array();
					foreach($_POST['filename'] as $data){
						$insertquery[] = '(?,?,?,?)';
						$insertdata[] = $userid;
						$insertdata[] = $data;
						$insertdata[] = $timestamp;
						$insertdata[] = $venueid;
					}
					if(!empty($insertquery)){
						$query.= implode(', ',$insertquery);
						$stmt = $pdo->prepare($query);
						error_log("venuequery:".$query."|");
						$stmt->execute($insertdata);
						if($stmt->rowCount() >= 1){
								
							// 						$query = "INSERT INTO cheersu_activity(activity_venue_id,activity_type,activity_venue_id,activity_timestamp) VALUES (?,?,?,?)";
							// 						$stmt = $pdo->prepare($query);
							// 						$stmt->execute(array($userid,"photo-activity",0,$timestamp));
							// 						if($stmt->rowCount() == 1){
							// 							$query = "COMMIT";
							// 							$stmt = $pdo->prepare($query);
							// 							$stmt->execute();
							// 							$status = "success";
							// 							$message = "Photos successfully posted";
							// 						}
							// 						else{
							// 							$query = "ROLLBACK";
							// 							$stmt = $pdo->prepare($query);
							// 							$stmt->execute();
							// 							$status = "success";
							// 							$message = "photos successfully uploaded";
							// 						}
							$query = "COMMIT";
							$stmt = $pdo->prepare($query);
							$stmt->execute();
							$status = "success";
							$message = "photos successfully posted";
								
						}
						else{
							$query = "ROLLBACK";
							$stmt = $pdo->prepare($query);
							$stmt->execute();
							$status = "error";
							$message = "unable to insert into database";
						}
					}
						
				}
				else{
					$status = "error";
					$message = "unable to retrieve time";
				}
			}
			else{
				$status = "error";
				$message = "Improper parameters passed";
			}
		}
	}
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}
	include 'json_encoding.php';
?>