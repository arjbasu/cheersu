function timer(){
	setTimeout(function(){
		$(".successdiv,.errordiv").slideUp("300");
	},"1500");
}

function trimfield(str) 
{ 
    return str.replace(/^\s+|\s+$/g,''); 
}

function blockui(message){
	if(message === undefined){
		message = '<img src="img/ajax-loader.gif"/>';
	}
	else if(message == "upload-complete"){
		message = "<h4 class = 'notification-text'>All photos uploaded successfully</h4>";
	}
	$.blockUI({ message: message ,
		themedCSS: { 
	        width:  '30px', 
	        top:    '40%', 
	        left:   '35%' 
	    }, 
		css: { 
            border: 'none', 
            padding: '5px', 
            backgroundColor: '#272B30', 
            '-webkit-border-radius': '0px', 
            '-moz-border-radius': '0px',
            'border-radius':'0px',
            opacity: .5, 
            color: 'white' 
        }
		});
}

function scrolltotop(){
	$("#back-top").hide();
	
	// fade in #back-top
	$(window).scroll(function () {
		if ($(this).scrollTop() > 150) {
			$('#back-top').fadeIn();
		} else {
			$('#back-top').hide();
		}
	});

	// scroll body to 0px on click
	$('#back-top a').click(function () {
		console.log("back top clicked");
		$('body,html').animate({
			scrollTop: 0
		}, 800);
		return false;
	});
}

jQuery.expr[':'].Contains = function(a,i,m){
    return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase())>=0;
};
function searchablelist(inputelement,elements){
	 inputelementid = "#" + inputelement;
	 elementsid = "." + elements;
	 
	 $(inputelementid).on("change",function(){
		 filter = $(this).val();
		 if(filter == ""){
			 $(elementsid).show();
		 }
		 else{
			 var trueexpr = elementsid +":Contains(" + filter + ")";
			 var falseexpr = elementsid +":not(:Contains(" + filter + "))";
			 $(trueexpr).show();
			 $(falseexpr).hide();
		 }
	 }).on("keyup",function(){
		 $(this).trigger("change");
	 });
}