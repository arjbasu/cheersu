<?php
session_start();
include 'check_authorization.php';
include 'connect.php';
include 'twiginit.php';
$userid = $_SESSION['user_id'];
if(isset($_POST['status'])){
	
	$status = $_POST['status'];
	if($status != ""){
		$type = "status";
		if($_POST['taggedpeople'] != ""){	
			$type = "taggedstatus";
		}
		
		$query = "SELECT NOW() AS time";
		$stmt = $pdo->prepare($query);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		$time = $result['time'];
		$query = "UPDATE cheersu_users SET user_status = ? WHERE user_id = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($status,$userid));
		$query = "INSERT INTO cheersu_activity(activity_user_id,activity_type,activity_venue_id,activity_comment) VALUES(?,?,?,?)";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($userid,$type,"0",$status));
		
		
		if($type == "taggedstatus"){
			//echo "Tagged Status";
			//echo $_POST['taggedpeople'];
			$query = "BEGIN";
			$stmt = $pdo->prepare($query);
			$stmt->execute();
			$query = "SELECT activity_id FROM cheersu_activity WHERE activity_timestamp = '$time' AND activity_user_id = '$userid' ORDER BY activity_timestamp DESC LIMIT 1";
			$stmt = $pdo->prepare($query);
			$stmt->execute();
			if($stmt->rowCount()==1){
				$result = $stmt->fetch(PDO::FETCH_ASSOC);
				//print_r($result);
				$activityid = $result['activity_id'];
				$query = "INSERT INTO cheersu_user_tag (user_tag_activityid,user_tag_userid,user_tag_username) VALUES";
				$insertquery = array();
				$insertdata = array();
				$tagarray = json_decode(stripslashes($_POST['taggedpeople']));
				$len = count($tagarray);
				//echo "Hello".$len;
				//print_r($tagarray);
				for($i = 0; $i < $len; $i++){
					$insertquery[] = "(?,?,?)";
					$insertdata[] = $activityid;
					$insertdata[] = $tagarray[$i]->id;
					$insertdata[] = $tagarray[$i]->name;
				}
				
				$query.= implode(",",$insertquery);
				////echo $query;
				$stmt = $pdo->prepare($query);
				$stmt->execute($insertdata);
				if($stmt->rowCount() >= 1){
					//echo "Success";
					$flag = true;
					for($i = 0; $i < $len; $i++){
						//echo "<br/>Iteration:$i:$activityid:$userid<br/>";
						$taguserid = stripslashes($tagarray[$i]->id);
						$username = $tagarray[$i]->name;
						$query = "INSERT INTO cheersu_notifications_$taguserid (notification_activity_id,notification_user_id,notification_type)  VALUES (?,?,?)";
						////echo $query;
						$stmt = $pdo->prepare($query);
						$stmt->execute(array($activityid,$userid,"tag"));
						if($stmt->rowCount() != 1){
							$flag = false;
							break;		
						}
					}
					if($flag){
						$query = "COMMIT";
						$stmt = $pdo->prepare($query);
						$stmt->execute();		
					}
					else{
						$query = "ROLLBACK";
						$stmt = $pdo->prepare($query);
						$stmt->execute();							
					}
				}
				else{
					$query = "ROLLBACK";
					$stmt = $pdo->prepare($query);
					$stmt->execute();	
				}
				
			}
			else {
				$query = "ROLLBACK";
				$stmt = $pdo->prepare($query);
				$stmt->execute();
				die("Unable to process query");
			}
		}
		else{
			$query = "COMMIT";
			$stmt = $pdo->prepare($query);
			$stmt->execute();	
		}
	}
}
header("Location:dashboard.php");
?>
