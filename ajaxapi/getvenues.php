<?php
	session_start();
	include 'authentication_ajax_api.php';
	include '../connect.php';
	if(isset($_POST['school_id'])){
		$schoolid = $_POST['school_id'];
		// error_log("Schoolid:".$schoolid,0);
		$query = "SELECT venue_id AS id, venue_name AS name, venue_address AS img FROM cheersu_venues WHERE venue_schoolid = ? AND venue_id <> 0";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($schoolid));
		if($stmt->rowCount() == 0){
			$status = "error";
			$message = "No venues found for the school";
		}
		else{
			$venues = array();
			
			while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
				$temp['img'] = urlencode($temp['img']);
				array_push($venues,$temp);	
			}
			$status = "success";
			$message = $venues;
			
		}
	}
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}
	include 'json_encoding.php';
?>