<?php
	session_start();
	include 'check_authorization.php';
	include 'connect.php';
	include 'twiginit.php';
	$own = true;
	$myuserid = $_SESSION['user_id'];
	if(isset($_GET['userid']) && $_GET['userid'] != ""){
		$userid = $_GET['userid'];
		if($userid != $myuserid){
			$own = false;
		}
			
	}
	else{
		$userid = $_SESSION['user_id'];
	}
	
// 	$query = "SELECT user_firstname,user_lastname,user_bio,user_status,user_bio,".
// 	"user_dp,school_name count(activity_timestamp) as checkins,count(friend_user_id) as friends FROM cheersu_users,".
// 	"cheersu_activity,cheersu_friends_$userid,cheersu_schools ".
// 	"WHERE user_id = ? AND activity_user_id = user_id AND activity_type = 'checkin' AND school_id = user_schoolid";
	$query = "SELECT user_firstname, user_lastname, user_bio, user_status, user_dp ".
	"FROM ".
	"cheersu_users WHERE".
	" user_id = '$userid'";
	$data = array();
	$result = mysql_query($query);
	if(!$result){
		die("Unable to query databases");
	}
	else{
		
		$temp = mysql_fetch_assoc($result);
		$data['id'] = $userid;
		$firstname = $temp['user_firstname'];
		$lastname = $temp['user_lastname'];
		$bio = $temp['user_bio'];
		$status = $temp['user_status'];
		$img = $temp['user_dp'];
		if($img == ""){
			$img = "300x200.gif";
		}
		
		$query = "SELECT * FROM cheersu_activity WHERE activity_type = 'checkin' AND activity_user_id = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($userid));
		$checkins = $stmt->rowCount();
		
		$query = "SELECT * FROM cheersu_friends_$userid WHERE friend_status = 'confirmed'";
		$result = mysql_query($query);
		$friends = mysql_num_rows($result);
		
// 		error_log("Own",0);
		
		$query = "SELECT school_name FROM cheersu_schools , cheersu_users WHERE user_schoolid = school_id AND user_id = '$userid'";
		$result = mysql_query($query);
		if(!$result){
			die("Unable to query databases");
		} 
		$temp = mysql_fetch_assoc($result);
		$school = $temp['school_name'];
		
		$data['name'] = stripslashes($firstname." ".$lastname);
		
		if($own == true){
			$data['title'] = "My Profile";
		}
		else{
			$data['title'] = $firstname."'s Profile";
			error_log("own is false",0);
			$query = "SELECT * FROM cheersu_friends_$myuserid WHERE friend_user_id = ? AND friend_status = 'confirmed'";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($userid));
			if($stmt->rowCount() != 0){
				$data['confirmedfriends'] = true;
			}
			else{
				$query = "SELECT * FROM cheersu_friends_$myuserid WHERE friend_user_id = ? AND friend_status = 'pending'";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($userid));
				if($stmt->rowCount() != 0){
					$data['friendspending'] = true;
				}
				else{
					$query = "SELECT * FROM cheersu_friend_requests_$myuserid WHERE friend_user_id = ?";
					$stmt = $pdo->prepare($query);
					$stmt->execute(array($userid));
					if($stmt->rowCount() != 0){
						$data['friendawaiting'] = true;
					}
					else{
						$data['notfriends'] = true;
					}
					
				}
			}
		}
		$data['id'] = $userid;
		$data['dp'] = $img;
		$data['numfriends'] = $friends;
		$data['numcheckins'] = $checkins;
		$data['bio'] = stripslashes($bio);
		$data['status'] = stripslashes($status);
		$data['school'] = $school;
		$query = "SELECT activity_type,activity_comment,datediff(now(),activity_timestamp) as datedif , timediff(now(),activity_timestamp) as timedif,venue_name,school_name,school_id".
		" FROM cheersu_activity,cheersu_venues,cheersu_schools ".
		"WHERE school_id = venue_schoolid AND venue_id = activity_venue_id AND activity_user_id = '$userid' ".
		"ORDER BY activity_timestamp DESC LIMIT 50";
		$result = mysql_query($query);
		if(mysql_num_rows($result) == 0){
			$data['noresults'] = true;
		}
		else{
			$activity = array();
			
			while($temp = mysql_fetch_assoc($result)){
				$tempactivity = array();
				if($img == "")
					$tempactivity['dp'] = "cheersu_icon.png";
				else{
					$tempactivity['dp'] = $img;
				}
				$tempactivity['comment'] = stripcslashes($temp['activity_comment']);
				$tempactivity['venue'] = stripslashes($temp['venue_name']);
				$tempactivity['schoolname'] = stripslashes($temp['school_name']);
				$tempactivity['schoolid'] = stripslashes($temp['school_id']);
				$type = $temp['activity_type'];
					if($type == "checkin"){
						$tempactivity['type'] = "checked in to";
					}
					else if($type == "reserve"){
						$tempactivity['type'] = "reserved on to";
					}
					else if($type == "comment"){
						$tempactivity['type'] = "commented on";
					}
					else if($type == "status"){
						$tempactivity['type'] = "updated his status:";
						$tempactivity['commentactivity'] = true;
					}
					else if(stristr($type, "external") != false){
						error_log("$type",0);
						$tempactivity['external'] = true;
						$typearray = explode("|",$type);
						$tempact = $typearray[0];
						error_log("$tempact",0);
						if($tempact == "reserve"){
							$tempactivity['type'] = "reserved on to";
						}
						else if($tempact == "checkin"){
							$tempactivity['type'] = "checked in to";
						}
						$details = explode("|",$tempactivity['comment']);
						$tempactivity['schoolname'] = $details[1];
						$tempactivity['venue'] = $details[0];
					}
				$datediff = $temp['datedif'];
				$timediff = $temp['timedif'];
				if($datediff > 0){
					$tempactivity['timestamp'] = $datediff."d";
				}
				else{
					$timearray = explode(":", $timediff);
					if($timearray[0] > 0){
						if($timearray[0]<10){
							$timearray[0] = substr($timearray[0], 1,1);		
						}
						$tempactivity['timestamp'] = $timearray[0]."h";
						
					}
					else if($timearray[1] > 0){
						if($timearray[1]<10){
							$timearray[1] = substr($timearray[1], 1,1);
						}
						$tempactivity['timestamp'] = $timearray[1]."m";
					}
					else{
						if($timearray[2]<10){
							$timearray[2] = substr($timearray[2], 1,1);
						}
						$tempactivity['timestamp'] = $timearray[2]."s";
					}
					
				}
				
				array_push($activity,$tempactivity);
			}
			$data['activitylist'] = $activity;
		}
		$data["username"] = $_SESSION['username'];
		echo $twig->render("profile.twig",$data);
	}
	
	
?>