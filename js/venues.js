console.log("v1.0");
function blockui(){
	$.blockUI({ message: '<img src="img/ajax-loader.gif"/>' ,
		themedCSS: { 
	        width:  '30px', 
	        top:    '40%', 
	        left:   '35%' 
	    }, 
		css: { 
            border: 'none', 
            padding: '5px', 
            backgroundColor: '#272B30', 
            '-webkit-border-radius': '0px', 
            '-moz-border-radius': '0px',
            'border-radius':'0px',
            opacity: .5, 
            color: '#fff' 
        }
		});
}
fallbackmode = false;
var filehtml = "<input type = 'file' class = 'file' name = 'file[]'/>";
fallbackphotohtml = "<div class = 'photo-preview'>" +
"<img src = 'img/imguploaderplaceholder.gif' class = 'preview-img'/>"+
"</div>";

function fallback(){
	$(".file").on("change",$(this),fallback_upload);
	
}

//This is the regular image uploading function which would work on Firefox , Chrome, Opera
//and Safari newer versions

function regular(){
	$(".file").on("change",$(this),upload);
}

//This is the fallback image uploading function for browsers which don't
//support the ajaxfileupload plugin like IE (even 10 !) and older versions of Safari.

function fallback_upload(){
	console.log("fallback upload");
	var element = $(".file:last");
	console.log("element");
	console.log("fallback called");
	var file = element.val();
	var extension = file.split(".");
	extension = extension[extension.length-1];
	if(file!="" && (extension == "jpg"|| extension == "jpeg" || extension == "png" || extension == "bmp"||
			extension == "JPG"|| extension == "JPEG" || extension == "PNG" || extension == "BMP"		
	)){
		$(".previews").append(fallbackphotohtml);
		$("#file-upload").append(filehtml);
		$(".file").off("change",fallback_upload).on("change",$(this),fallback_upload);
		$("#upload-all").removeClass("disabled");
	}
	else{
		alert("Please choose an image file with jpeg,jpg or png extension");
	}
}



photohtml = "<div class = 'photo-preview'>" +
"<img src = 'img/photo-upload-ajax.png' class = 'photo-loading'/>"+
"</div>";

function removephotos(event){
	// console.log("event");
	// console.log(event);
	var confirmation = confirm("This will delete all your uploaded photos! Do you want to continue?");
	if(confirmation === true){
		blockui();
		var data = "";
		len = $(".photo-preview").length;
		for(var i = 0; i < len; i++){
			var img = $(".photo-preview").eq(i).find(".preview-img").attr("data-image-name");
			data+="filename%5B%5D="+img+"&";
		}
		data+="type=delete&venue_id=0";
		console.log(data);
		$.ajax({
			url:"ajaxapi/venue_photo_action.php",
			data:data,
			type:"POST",
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					$.unblockUI();
					$("#upload-section").fadeOut("300",function(){
						$("#photo-upload,#img-thumbnails").fadeIn("200");
						$(".photo-preview").remove();
						$("#upload-all").addClass("disabled");
					});
				}
				else{
					$.unblockUI();
					alert("unable to delete photos , please try again later");
				}
			},
			error:function(){
				$.unblockUI();
				alert("unable to delete photos , please try again later");
			}
		});
		return true;
		
	}
	else if(arguments.length){
		// console.log("event is defined");
		return false;
	}

}


function upload(){
	
	console.log("change");
	var file = $("#file").val();
	var extension = file.split(".")
	extension = extension[extension.length-1];
	if(file!="" && (extension == "jpg"|| extension == "jpeg" || extension == "png" || extension == "bmp"||
			extension == "JPG"|| extension == "JPEG" || extension == "PNG" || extension == "BMP"		
	)){
		$(".previews").append(photohtml);
		var selection = $(".previews .photo-preview:last-child .photo-loading").show();
		$("#upload-all,#upload-file,#close-upload,#close-photo-modal").addClass("disabled");
		$.ajaxFileUpload
		(
			{
				url:'ajaxapi/upload_venue.php',
				fileElementId:'file',
				dataType: 'JSON',
				success: function (data){
					data = $.parseJSON(data);
					if(data.status == "success"){
						html = "<img src = 'img/venue_img/icons/"+ data.message+"' class = 'preview-img' data-image-name='"+data.message+"'/>";
						selection.hide().parent(".photo-preview").append(html);
						$("#file-upload").trigger("reset");
						$("#file").off("change",upload).on("change",upload);
						$("#upload-all").removeClass("disabled");
						$("#upload-file,#close-upload,#close-photo-modal").removeClass("disabled");
						
					}
					else{
						alert(data.message);
						selection.parent(".photo-preview").remove();
						if($(".photo-preview").length !=0){
							$("#upload-all").removeClass("disabled");
						}
						$("#upload-file,#close-upload,#close-photo-modal").removeClass("disabled");
						$("#file-upload").trigger("reset");
						$("#file").off("change",upload).on("change",upload);
					}
					
				},
				error: function (data, status, e){
					alert("There was an error uploading the image.Please try again");
					selection.parent(".photo-preview").remove();
					if($(".photo-preview").length !=0){
						$("#upload-all").removeClass("disabled");
					}
					$("#upload-file,#close-upload,#close-photo-modal").removeClass("disabled");
					$("#file-upload").trigger("reset");
					$("#file").off("change",upload).on("change",upload);
				}
			}
		)
		
	}
	else{
		alert("Please choose an image file with jpeg,jpg or png extension");
	}
}


$(document).ready(function(){
	var browser = BrowserDetect.browser;
	var version = BrowserDetect.version;
	if(browser == "Explorer" || (browser == "Safari" && version < 5)){
		fallbackmode = true;
		fallback();
	}
	else{
		fallbackmode = false;
		regular();
	}
	
	initialisechat();
	check_notifications();
});
$(window).load(function(){
	
	$("#extraactionmodal").on("hidden",function(){
		$(this).find(".loading,.successdiv,.errordiv").hide();
		$(this).find("#externalaction").trigger("reset").show();
	})
	$(".modal").on("show",function(){
		$("body").css("overflow","hidden");
	});

	$(".modal").on("hidden",function(){
		$("body").css("overflow","auto");
		$(this).find(".successdiv,.errordiv,.loading").hide();
		$(this).find("form").trigger("reset").show();
	});
	$("#commentmodal").on("hidden",function(){
		$(this).find(".successdiv,.errordiv").hide();
		$("#prevcomments,#commentform").show();
		$("#commentform").trigger("reset");
	});
	
	$(".checkins").click(function(){
		blockui();
		var venue_id = $(this).parents("li").find(".thumbnail").attr("data-venue-id");
		var data = {"venue_id":venue_id,"action":"get"};
		var parent = this;
		console.log(data);
		$.ajax({
			url:"ajaxapi/checkin.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				else if(data.status == "success"){
					$.unblockUI();
					//console.log(data.message);
					entries = data.message;
					html = "";
					length = entries.length;
					for(var i = 0;i < length; i++){
						html+= "<tr>" +
								"<td class = 'dp-container'><img class = 'dp-container' src = 'http://cheersu.com/img/"+entries[i].dp+"'></td>" +
								"<td><p><h4><a class = 'userlink' href = 'profile.php?userid="
								+entries[i].userid+"'>" + entries[i].name + "</a></p>" +
								"</tr>";
					}
					$("#statsmodal .modal-header h3").html("Checkins for tonight");
					$("#statsmodal table").html(html).parents(".modal").modal();
				}
				else{
					$.unblockUI();
					$("#venues .errordiv").html(data.message).slideDown("300");
				}
			},
			error:function(){
				$.unblockUI();
				$("#venues .errordiv").html("An error occurred").slideDown("300");
			}
		});
		return false;
	});
	
	
	$("#requestvenueform").submit(function(){
		var validation = true;
		var parent = $("#requestvenuemodal");
		$(this).find("input,select").each(function(){
			if($(this).val() == "" || $(this).val() == "NULL"){
				validation = false;
				return false;
			}
		});
		if(validation == false){
			alert("Please complete all the fields");
			return false;
		}
		else{
			var data = $(this).serialize();
			$(this).slideUp("200");
			$(parent).find(".loading").fadeIn("200");
			$.ajax({
				url:"ajaxapi/requests.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						$(parent).find(".loading").fadeOut("200").parents(".modal")
						.find(".successdiv").html(data.message).slideDown();
					}
					else{
						$(parent).find(".loading").fadeOut("200").parents(".modal")
						.find(".errordiv").html(data.message).slideDown();
						$("#requestvenueform").slideDown("200")
					}
				},
				error:function(){
					$(parent).find(".loading").fadeOut("200").parents(".modal")
					.find(".errordiv").html("An error occurred").slideDown();
					$("#requestvenueform").slideDown("200")
				}
			})
		}
		return false;
	});
	
	$(".reservations").click(function(){
		blockui();
		var venue_id = $(this).parents("li").find(".thumbnail").attr("data-venue-id");
		var data = {"venue_id":venue_id,"action":"get"};
		var parent = this;
		console.log(data);
		$.ajax({
			url:"ajaxapi/reserve.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				else if(data.status == "success"){
					$.unblockUI();
//					console.log(data.message);
					entries = data.message;
					html = "";
					length = entries.length;
					for(var i = 0;i < length; i++){
						html+= "<tr>" +
								"<td class = 'dp-container'><img class = 'dp-container' src = 'http://cheersu.com/img/"+entries[i].dp+"'></td>" +
								"<td><p><h4><a class = 'userlink' href = 'profile.php?userid="
								+entries[i].userid+"'>" + entries[i].name + "</a></p>" +
								"</tr>";
					}
					$("#statsmodal .modal-header h3").html("Reservations for tonight");
					$("#statsmodal table").html(html).parents(".modal").modal();
				}
				else{
					$.unblockUI();
					$("#venues .errordiv").html(data.message).slideDown("300");
				}
			},
			error:function(){
				$.unblockUI();
				$("#venues .errordiv").html("An error occurred").slideDown("300");
			}
		});
		return false;
	});
	
	$("#upload-file").click(function(){
		if(!$(this).hasClass("disabled"))
			$(".file:last").trigger("click");
		return false;
	});
	$("#close-photo-modal").click(function(e){
		if($(this).hasClass("disabled") === true){
			return false;
		}
		else if($(".photo-preview").length>0){
			var val = removephotos(e);
			if(val === false){
				return false;
			}
		}
	});
	$("#photomodal").on("hidden",function(){
		$("#upload-section,#file-upload").hide();
		$("#img-thumbnails,#photo-upload").show();
	});
	$("#photo-upload").click(function(){
		
		$("#photo-upload,#img-thumbnails").fadeOut("200",function(){
			$("#file-upload").show();
			$("#upload-section").fadeIn("200");
		});
		return false;
	});
	$("#upload-all").click(function(){
		
		venueid = $("#photomodal").attr("data-venue-id")
		if($(this).hasClass("disabled") != true){
			if(fallbackmode){
				return true;
			}
			$("#photomodal").block({
						message: "<img src = 'img/ajax-loader.gif'/>",
						themedCSS: { 
					        position:'absolute',
					        top:'60%',
					        left:'0%',
					    }, 
						css: { 
				            border: 'none', 
				            padding: '5px', 
				            backgroundColor: '#272B30', 
				            '-webkit-border-radius': '0px', 
				            '-moz-border-radius': '0px',
				            'border-radius':'0px',
				            opacity: .5, 
				            color: '#fff' 
				        }
					})
			var data = "";
			len = $(".photo-preview").length;
			for(var i = 0; i < len; i++){
				var img = $(".photo-preview").eq(i).find(".preview-img").attr("data-image-name");
				data+="filename%5B%5D="+img+"&";
			}
			data+="type=post&venue_id="+venueid;
			console.log(data);
			$.ajax({
				url:"ajaxapi/venue_photo_action.php",
				data:data,
				type:"POST",
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						$("#photomodal").unblock();
						blockui("upload-complete");
						setTimeout(function(){$.unblockUI();},1000);
						var html = "";
						for(var i = 0; i < len; i++){
							var img = $(".photo-preview").eq(i).find(".preview-img").attr("data-image-name");
							html += "<a rel = 'venuegallery' class = 'fancybox' href = 'img/venue_img/" + img + "'>"+
							"<img src = 'img/venue_img/icons/"+ img +"'/></a>"
						}
						console.log(html);
						$("#img-thumbnails").prepend(html);
						$(".fancybox").fancybox({
							nextMethod : 'resizeIn',
					        nextSpeed  : 150,
					        preload:8,
					        prevMethod : false,
								helpers	: {
									title	: {
										type: 'inside'
									},
									thumbs	: {
										width	: 50,
										height	: 50
									}
								}
						});
						$(".photo-preview").remove();
						$("#close-upload").trigger("click");
						
					}
					else{
						$("#photomodal").unblock();
						alert(data.message);
					}
				},
				error:function(){
					$("#photomodal").unblock();
					alert("unable to post photos , please try again later");
				}
			});
			
		}
		return false;
	});
	$("#close-upload").click(function(){
		if($(this).hasClass("disabled") === true){
			return false;
		}
		
		if(fallbackmode == true){
			if($(".photo-preview").length>0){
				var confirmation = confirm("This will delete all your uploaded photos! Do you want to continue?");
				if(confirmation === true){
					$("#upload-section").fadeOut("300",function(){
						$("#photo-upload,#img-thumbnails").fadeIn("200");
						$(".photo-preview").remove();
						$("#upload-all").addClass("disabled");
					});
				}
				return false;
			}
		}
		// console.log("close upload clicked");
		if($(".photo-preview").length>0){
			removephotos();
		}
		else{
			$("#upload-section").fadeOut("300",function(){
				$("#photo-upload,#img-thumbnails").fadeIn("200");
				$(".photo-preview").remove();
				$("#upload-all").addClass("disabled");
			});
		}
		return false;
	});
	
	
	(function ($, F) {
	    F.transitions.resizeIn = function() {
	        var previous = F.previous,
	            current  = F.current,
	            startPos = previous.wrap.stop(true).position(),
	            endPos   = $.extend({opacity : 1}, current.pos);

	        startPos.width  = previous.wrap.width();
	        startPos.height = previous.wrap.height();

	        previous.wrap.stop(true).trigger('onReset').remove();

	        delete endPos.position;

	        current.inner.hide();

	        current.wrap.css(startPos).animate(endPos, {
	            duration : current.nextSpeed,
	            easing   : current.nextEasing,
	            step     : F.transitions.step,
	            complete : function() {
	                F._afterZoomIn();

	                current.inner.fadeIn("fast");
	            }
	        });
	    };

	}(jQuery, jQuery.fancybox));
	
	
	$(".img-upload").click(function(){
		var venueid = $(this).parents("li").find(".thumbnail").attr("data-venue-id");
		var data = {"type":"getimages","venue_id":venueid};
		blockui();
		$.ajax({
			url:"ajaxapi/venue_photo_action.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					if(data.message == "No Images available"){
						errordiv = '<div class = "alert alert-error pull-center errordiv">'+
							'<p>No images for this venue yet</p>'+
							'</div>';
						$("#photomodal .modal-body #img-thumbnails").html(errordiv);
						$("#photomodal").find(".errordiv").show();
					}
					else{
						message = data.message;
						len = message.length;
						html = "";
						for(var i = 0; i < len; i++){
							 html += '<a class = "fancybox" rel = "venuegallery" href = "img/venue_img/'+message[i].venue_photo_source +
							 '"><img src = "img/venue_img/icons/'+ message[i].venue_photo_source +
								 '"/></a>' ;
							
						}
						$("#photomodal .modal-body #img-thumbnails").html(html).
						find(".fancybox")
						.fancybox({
							nextMethod : 'resizeIn',
					        nextSpeed  : 150,
					        preload:8,
					        prevMethod : false,
								helpers	: {
									title	: {
										type: 'inside'
									},
									thumbs	: {
										width	: 50,
										height	: 50
									}
								}
						});
						
					}
					$.unblockUI();
					$("#extraparam").val(venueid);
					$("#photomodal").attr("data-venue-id",venueid).modal();
					
				}
				else{
					$.unblockUI();
					$("#venues .errordiv").html(data.message).slideDown("300");
				}
			},
			error:function(){
				$.unblockUI();
				message = "<strong>An error occured!</strong> Please try again later";
				$("#venues .errordiv").html(message).slideDown("300");
			}
		})
	});
	
	$("#commentform").submit(function(){
		blockui();
		var comment = $("#comment").val();
		var venueid = $(this).parents("#commentmodal").attr("data-venueid");
		var data = {"venue_id":venueid,"comment":comment,"action":"set"};
		$.ajax({
			url:"ajaxapi/comment.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				else if(data.status == "success"){
					$.unblockUI();
					$("#prevcomments,#commentform").slideUp("200",function(){
						$("#commentmodal .successdiv").html(data.message).slideDown('100');
					});
				}
				else{
					$.unblockUI();
					$("#prevcomments,#commentform").slideUp("200",function(){
						$("#commentmodal .errordiv").html(data.message).slideDown('100');
					});
				}
			},
			error:function(){
				$.unblockUI();
				$("#prevcomments,#commentform").slideUp("200",function(){
					$("#commentmodal .errordiv").html("An error occurred").slideDown('100');
				});
			}
		});
		return false;
	});
	
	$(".comment").click(function(){
		blockui();
		var venueid = $(this).parents(".thumbnail").attr("data-venue-id");
		var data = {"venue_id":venueid,"action":"get"};
//		console.log(data);
		$.ajax({
			url:"ajaxapi/comment.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				$("#prevcomments table").html("");
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				else if(data.status == "success"){
					$.unblockUI();
					if(data.message === "No comments available"){
						$("#commentmodal .errordiv").html(data.message).slideDown("200");
					}
					else{
						entries = data.message;
						html = "";
						length = entries.length;
						for(var i = 0;i < length; i++){
							html+= "<tr>" +
									"<td class = 'comment-dp'><img class = 'dp-container' src = 'img/icons/"+entries[i].dp+"'></td>" +
									"<td class = 'comment-comment'><p><a class = 'userlink' href = 'profile.php?userid="
									+entries[i].userid+"'>" + entries[i].name + "</a></p>" +
									"<p>"+entries[i].comment+"</p></td>" +
									"</tr>";
						}
						$("#prevcomments table").html(html);
					}
					$("#commentmodal").attr("data-venueid",venueid).modal();
				}
				else{
					$.unblockUI();
					$("#venues .errordiv").html(data.message).slideDown("300");
				}
			},
			error:function(){
				$.unblockUI();
				message = "<strong>An error occured!</strong> Please try again later";
				$("#venues .errordiv").html(message).slideDown("300");
			}
		});
		return false;
		
		
	});
	$("body").ajaxStart(function(){
		$(".errordiv,.successdiv").hide();
	});
//	$(".thumbnail").hover(function(){
//		$(this).find(" .img-upload").fadeIn("200");
//	},
//	function(){
//		$(this).find(".img-upload").stop(false,false).hide();
//	});
	$(".location").popover({"trigger":"hover"});
	
	var thumbnailheight = $(".thumbnail").eq(0).height();
	var actionbarheight = $(".thumbnail").eq(0).find(".actions").height();	
	var thumbnailwidth = $(".thumbnail").eq(0).width();
	var actionbarwidth = $(".thumbnail").eq(0).find(".actions").width();
	var hpos = (thumbnailwidth-actionbarwidth)/1.8;
	var vpos = (thumbnailheight+ actionbarheight)/2;
	
	$(".thumbnail").find(".actions").css({"top":vpos,"left":hpos});
	
	$(".thumbnail").hover(function(){
		
		$(this).find(".actions").fadeIn("50");
	},
	function(){
		$(this).find(".actions").stop(false,false).hide();
	});
	
//	$("body").ajaxComplete(function(){
//		$("html, body").animate({ scrollTop: 0 }, "200");
//	});
	
	$(".reserve").click(function(){
		blockui();
		var venueid = $(this).parents(".thumbnail").attr("data-venue-id");
		var data = {"venue_id":venueid,"action":"set"};
		var parent = this;
		$.ajax({
			url:"ajaxapi/reserve.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				else if(data.status == "success"){
					$.unblockUI();
//					var checkins = $(parent).parents("li").find(".stats .reservations").html().substring(13);
//					checkins = parseInt(checkins)+1;
//					$(parent).parents("li").find(".stats .reservations").html("Reservations:"+checkins);
					setTimeout(function(){window.location = "";},"700");
					$("#venues .successdiv").html("<strong>You are successfully reserved for the venue!</strong>").slideDown("300");
				}
				else{
					$.unblockUI();
					$("#venues .errordiv").html(data.message).slideDown("300");
				}
			},
			error:function(){
				$.unblockUI();
				message = "<strong>Unable to reserve!</strong> Please try again later";
				$("#venues .errordiv").html(message).slideDown("300");
			}
		});
		return false;
	});
	
	$("#externalaction").submit(function(){
		parent = $("#extraactionmodal");
		var validation = true;
		$(this).find("input,select").each(function(){
			if($(this).val() == ""){
				validation = false;
				$(parent).find(".errordiv p").html("Please complete all the fields").parent().slideDown("200");
			} 
		});
		if(validation == false){
			return false;
		}
		var data = $(this).serialize();
		$(this).slideUp("200",function(){
			$(parent).find(".loading").fadeIn('100');
		});
		$.ajax({
			url:"ajaxapi/externalactivity.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				else if(data.status == "success"){
					$(parent).find(".loading").fadeOut("200").parent().find(".successdiv p").html(data.message).
					parent().slideDown("200");
				}
				else{
					$(parent).find(".loading").fadeOut("200").parent().find(".errordiv p").html(data.message).
					parent().slideDown("200");
				}
			},
			error:function(){
				$(parent).find(".loading").fadeOut("200").parent().find(".errordiv p").html("An error occurred").
				parent().slideDown("200");
			}
		})
		return false;
	});
	$(".checkin").click(function(){
		blockui();
		
		var venueid = $(this).parents(".thumbnail").attr("data-venue-id");
		var data = {"venue_id":venueid,"action":"set"};
		var parent = this;
		$.ajax({
			url:"ajaxapi/checkin.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				else if(data.status == "success"){
					$.unblockUI();
//					var checkins = $(parent).parents("li").find(".stats .checkins").html().substring(9);
//					checkins = parseInt(checkins)+1;
//					$(parent).parents("li").find(".stats .checkins").html("Checkins:"+checkins);
					setTimeout(function(){window.location = "";},"700");
					$("#venues .successdiv").html("<strong>You have successfully checked in!</strong>").slideDown("300");
					
				}
				else{
					$.unblockUI();
					$("#venues .errordiv").html(data.message).slideDown("300");
				}
			},
			error:function(){
				$.unblockUI();
				message = "<strong>Unable to checkin!</strong> Please try again later";
				$("#venues .errordiv").html(message).slideDown("300");
			}
		});
		return false;
	});
});