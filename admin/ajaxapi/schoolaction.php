<?php
session_start();
include 'authentication_ajax_api.php';
include 'connect.php';	
if(isset($_POST['school_id']) && isset($_POST['action'])){
	$schoolid = $_POST['school_id'];
	$action = $_POST['action'];
	if($action == "delete"){
		$query = "DELETE FROM cheersu_schools WHERE school_id = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($schoolid));
		if($stmt->rowCount() < 0){
			$status = "error";
			$message = "Unable to interact with  database";
		}
		else{
			$status = "success";
			$message = "School Successfully deleted";
		}
	}
	else if($action == "insert"){
		if(isset($_POST['school_name']) && isset($_POST['school_state'])){
			$schoolname = $_POST['school_name'];
			$schoolstate = $_POST['school_state'];
			$query = "INSERT INTO cheersu_schools (school_name,school_state) VALUES (?,?) ";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($schoolname,$schoolstate));
			if($stmt->rowCount() != 1){
				$status = "error";
				$message = "Unable to insert into database";
			}
			else{
				$status = "success";
				$message = "School successfully added";
			}
		}
		else{
			$status = "error";
			$message = "Improper parameters passed";
		} 
	}
	else if($action == "update"){
		if(isset($_POST['school_name']) && isset($_POST['school_state']) && isset($_POST['school_address'])){
			$schoolname = $_POST['school_name'];
			$schoolstate = $_POST['school_state'];
			$schooladdress = $_POST['school_address'];
			$query = "UPDATE cheersu_schools SET school_name = ?,school_state = ?,school_postal_address = ? WHERE school_id = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($schoolname,$schoolstate,$schooladdress,$schoolid));
			$status = "success";
			$message = "School successfully updated";
		}
		else{
			$status = "error";
			$message = "Improper parameters passed";
		}
		
	}
	else if($action == "get"){
		$query = "SELECT school_id,school_name,school_state,school_postal_address FROM cheersu_schools WHERE school_id = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($schoolid));
		if($stmt->rowCount() == 0){
			$status = "error";
			$message = "No schools found";
		}
		else{
			$status = "success";
			$message = $stmt->fetch(PDO::FETCH_ASSOC);
		}
	}
}
else{
	$status = "error";
	$message = "Improper parameters passed";
}
include 'json_encoding.php';
?>
