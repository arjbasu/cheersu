$(document).ready(function(){
	initialisechat();
	check_notifications();
	
	(function ($, F) {
	    F.transitions.resizeIn = function() {
	        var previous = F.previous,
	            current  = F.current,
	            startPos = previous.wrap.stop(true).position(),
	            endPos   = $.extend({opacity : 1}, current.pos);

	        startPos.width  = previous.wrap.width();
	        startPos.height = previous.wrap.height();

	        previous.wrap.stop(true).trigger('onReset').remove();

	        delete endPos.position;

	        current.inner.hide();

	        current.wrap.css(startPos).animate(endPos, {
	            duration : current.nextSpeed,
	            easing   : current.nextEasing,
	            step     : F.transitions.step,
	            complete : function() {            	
	            	F._afterZoomIn();
	                current.inner.fadeIn("fast");
	            }
	        });
	    };

	}(jQuery, jQuery.fancybox));

	
	if($(".fancybox").length>0){
		$(".fancybox").fancybox({
			nextMethod : 'resizeIn',
	        nextSpeed  : 250,
	        preload:8,
	        prevMethod : false,
				helpers	: {
					title	: {
						type: 'inside'
					},
					thumbs	: {
						width	: 50,
						height	: 50
					}
				}
		});
	}
		
	$(".acceptclique").click(function(){
		var cliqueid = $(this).parent().attr("data-cliqueid");
		var action = "accept";
		var block = "false";
		var data = {"clique_id":cliqueid,"action":action,"block":block};
		parent = this;
		$.ajax({
			url:"ajaxapi/cliqueactions.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					$(".successdiv").html("Action completed successfully").slideDown("300");
					timer();
					$(parent).parents("tr").fadeOut("300");
				}
				else{
					$(".errordiv").html(data.message).slideDown("300");
					timer();
				}
				
			},
			error:function(data){
				html = "An error occurred";
				$(".errordiv").html(html).slideDown("300");
				timer();
			}
		});
		return false;
	});
	
	$(".rejectclique").click(function(){
		var cliqueid = $(this).parent().attr("data-cliqueid");
		var action = "delete";
		var value = $(this).parent().find(".blockoption:checked").length;
		if(value > 0)
			var block = "block";
		else
			var block = false;
		var data = {"clique_id":cliqueid,"action":action,"block":block};
		console.log(data);
		parent = this;
		$.ajax({
			url:"ajaxapi/cliqueactions.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					$(".successdiv").html("Action completed successfully").slideDown("300");
					timer();
					$(parent).parents("tr").fadeOut("300");
				}
				else{
					$(".errordiv").html(data.message).slideDown("300");
					timer();
				}
				
			},
			error:function(data){
				html = "An error occurred";
				$(".errordiv").html(html).slideDown("300");
				timer();
			}
		});
		return false;
	});

	$(".confirmfriend").click(function(){
		$(this).parent().find("button").fadeOut("200");
		var parent = this;
		var userid = $(this).parents("tr").attr("data-userid");
		var data = {"user_id":userid,"action":"confirm"};
//					console.log(data);
		$.ajax({
			url:"ajaxapi/friend.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated")
					window.location = "index.php";
				else if(data.status == "success"){
					$.unblockUI();
					$(".successdiv").html(data.message).slideDown('200');
					setTimeout(function(){ window.location = "";},600);
				}
				else{
					$.unblockUI();
					$(".errordiv").html(data.message).slideDown("200");
					$(parent).parent().find("button").fadeIn("200");
				}
			},
			error:function(){
				$.unblockUI();
				$(".errordiv").html("An error occurred").slideDown("200");
				$(parent).parent().find("button").fadeIn("200");
			}
		});
	});

	$(".deletefriend").click(function(){
		$(this).parent().find("button").fadeOut("200");
		var parent = this;
		var userid = $(this).parents("tr").attr("data-userid");
		var data = {"user_id":userid,"action":"delete"};
//					console.log(data);
		$.ajax({
			url:"ajaxapi/friend.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated")
					window.location = "index.php";
				else if(data.status == "success"){
					$.unblockUI();
					$(".successdiv").html(data.message).slideDown('200');
					setTimeout(function(){ window.location = "";},600);
				}
				else{
					$.unblockUI();
					$(".errordiv").html(data.message).slideDown("200");
					$(parent).parent().find("button").fadeIn("200");
				}
			},
			error:function(){
				$.unblockUI();
				$(".errordiv").html("An error occurred").slideDown("200");
				$(parent).parent().find("button").fadeIn("200");
			}
		});
	});
});
