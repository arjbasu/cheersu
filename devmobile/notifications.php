<?php
	session_start();
	include 'check_authorization.php';
	include 'connect.php';
	include 'twiginit.php';
	
	$data = array();
	$userid = $_SESSION['user_id'];
	$query = "SELECT user_firstname,user_lastname,user_id,user_dp FROM cheersu_friend_requests_$userid,".
	" cheersu_users WHERE friend_user_id = user_id";
	$result = mysql_query($query);
	if(!$result){
		die("Unable to interact with the database");
	}
	
	else if(mysql_num_rows($result) != 0){
		$friends = array();
		while($temp = mysql_fetch_assoc($result)){
			include 'removeslashes.php';
			if($temp['user_dp'] == ""){
				$temp['user_dp'] = "cheersu_icon.png";
			}
			array_push($friends, $temp);
		}
		
		$data['friendrequests'] = $friends;
	}
	
	$query = "UPDATE cheersu_friend_requests_$userid SET friend_request_seen = 'read'";
	$result = mysql_query($query);
	if(!$result){
		die("Unable to interact with database");
	}
	$query = "UPDATE cheersu_notifications_$userid SET notification_status = 'read'";
	$result = mysql_query($query);
	if(!$result){
		die("Unable to interact with database");
	}
	$query = "SELECT user_firstname,user_lastname,user_id,user_dp FROM cheersu_users,cheersu_notifications_$userid WHERE ".
	"notification_user_id = user_id AND notification_activity_id = 0 ORDER BY notification_timestamp DESC";
	$stmt = $pdo->prepare($query);
	$stmt->execute();
	if($stmt->rowCount() != 0){
		$acceptances = array();
		while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
			if($temp['user_dp'] == ""){
				$temp['user_dp'] = "cheersu_icon.png";
			}
			include 'removeslashes.php';
			array_push($acceptances,$temp);
		}
		$data['acceptances'] = $acceptances;
	}
	$query = "SELECT user_firstname,user_lastname,user_dp,user_id,activity_type,activity_comment,venue_name, school_name,".
	" school_id,datediff(now(),activity_timestamp) as datedif , timediff(now(), activity_timestamp) ".
	"as timedif FROM cheersu_users,cheersu_venues,cheersu_schools,cheersu_activity,cheersu_notifications_$userid".
	" WHERE notification_activity_id = activity_id AND activity_venue_id = venue_id AND venue_schoolid = school_id ".
	" AND activity_user_id = user_id ORDER BY activity_timestamp DESC LIMIT 0, 30 ";
	$result = mysql_query($query);
	if(!$result){
		die("unable to query database for activities");
	}
	else{
		if(mysql_num_rows($result) == 0){
			$data['noresults'] = true;
		}
		else{
			$activities = array();
			
			while($temp = mysql_fetch_assoc($result)){
				$tempactivity = array();
				$tempactivity['venue'] = stripslashes($temp['venue_name']);
				$tempactivity['schoolname'] = stripslashes($temp['school_name']);
				$tempactivity['comment'] = stripslashes($temp['activity_comment']);
				$type = $temp['activity_type'];
				if(stristr($type, "external") != false){
					error_log("$type",0);
					$tempactivity['external'] = true;
					$typearray = explode("|",$type);
					$tempact = $typearray[0];
					error_log("$tempact",0);
					if($tempact == "reserve"){
						$tempactivity['type'] = "reserved on to";
					}
					else if($tempact == "checkin"){
						$tempactivity['type'] = "checked in to";
					}
					$details = explode("|",$tempactivity['comment']);
					$tempactivity['schoolname'] = $details[1];
					$tempactivity['venue'] = $details[0];
				}
				else if($type == "checkin"){
					$tempactivity['type'] = "checked in to";
				}
				else if($type == "reserve"){
					$tempactivity['type'] = "reserved on to";
				}
				else if($type == "comment"){
					$tempactivity['type'] = "commented on";
				}
				else if($type == "status"){
					$tempactivity['type'] = "updated his status:";
					$tempactivity['commentactivity'] = true;
				}
				
				$datediff = $temp['datedif'];
				$timediff = $temp['timedif'];
				if($datediff > 0){
					$tempactivity['timestamp'] = $datediff."d";
				}
				else{
					$timearray = explode(":", $timediff);
					if($timearray[0] > 0){
						if($timearray[0]<10){
							$timearray[0] = substr($timearray[0], 1,1);
						}
						$tempactivity['timestamp'] = $timearray[0]."h";
				
					}
					else if($timearray[1] > 0){
						if($timearray[1]<10){
							$timearray[1] = substr($timearray[1], 1,1);
						}
						$tempactivity['timestamp'] = $timearray[1]."m";
					}
					else{
						if($timearray[2]<10){
							$timearray[2] = substr($timearray[2], 1,1);
						}
						$tempactivity['timestamp'] = $timearray[2]."s";
					}
						
				}
					
				include 'removeslashes.php';
				$tempactivity['name'] = $temp['user_firstname']." ".$temp['user_lastname'];
				$tempactivity['dp'] = $temp['user_dp'];
				$tempactivity['id'] = $temp['user_id'];
				$tempactivity['schoolid'] = $temp['school_id'];
				array_push($activities,$tempactivity);
			}
			$data['activities'] = $activities;
		}
	}
	$data['username'] = $_SESSION['username'];
	echo $twig->render("notifications.twig",$data);
?>