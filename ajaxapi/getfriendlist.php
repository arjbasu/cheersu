<?php
	session_start();
	include 'authentication_ajax_api.php';
	if(!isset($_POST['clique_id'])){
		$status = "error";
		$message = "Improper parameters passed";
	}
	else{
		include '../connect.php';
		$userid = $_SESSION['user_id']; 
		$cliqueid = $_POST['clique_id'];
		$query = "SELECT user_id AS id,CONCAT(user_firstname,' ',user_lastname) AS name,user_dp_icon AS dp
		 FROM cheersu_users INNER JOIN cheersu_friends_$userid ON user_id = friend_user_id
		 WHERE friend_user_id NOT IN(SELECT member_userid FROM cheersu_clique_members WHERE member_cliqueid = ?) 
		 AND friend_user_id NOT IN (SELECT request_to FROM cheersu_clique_requests WHERE request_cliqueid = ?)
		 AND friend_user_id NOT IN (SELECT decline_userid FROM cheersu_clique_decline WHERE decline_cliqueid = ?) 
		 ORDER BY name";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($cliqueid,$cliqueid,$cliqueid));
		$friends = array();
		while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
			if($temp['dp'] == ""){
				$temp['dp'] = "cheersu_icon.png";
			}
			array_push($friends,$temp);
		}
		if(count($friends) == 0){
			$status = "error";
			$message = "You have added all your friends in the clique";
		}
		else{
			$status = "success";
			$message = $friends;
		}
	}
	
	include 'json_encoding.php';
	