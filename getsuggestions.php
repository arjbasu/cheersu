<?php
include 'connect.php';
$userid = $_GET['userid'];
$query = "SELECT user_schoolid FROM cheersu_users WHERE user_id = ?";
$stmt = $pdo->prepare($query);
$stmt->execute(array($userid));

$temp = $stmt->fetch(PDO::FETCH_ASSOC);
$myschoolid = $temp['user_schoolid'];
echo "<br/>$myschoolid<br/>";
function cmp($a, $b)
{
	return $b['mutual'] - $a['mutual'];
}
function get_mutual_friends($userida,$useridb){
	//echo "<br/>$userida:$useridb<br/>";
	$query = "SELECT friend_user_id FROM cheersu_friends_$userida WHERE friend_status = 'confirmed'";
	$result = mysql_query($query);
	//echo $query;
	if(!$result){
		die("Unable to get friendlist");
	}
	else if(mysql_num_rows($result) == 0){
		return 0;
	}
	else{
		$friendlist = "(";
		while($temp = mysql_fetch_assoc($result)){
			$friendlist.= $temp['friend_user_id'].",";
		}
		$friendlist = substr($friendlist, 0, -1).")";
		//echo "<br/>$friendlist<br/>";
		$query = "SELECT COUNT(*) AS count FROM cheersu_friends_$useridb WHERE friend_user_id IN $friendlist AND friend_status = 'confirmed'";
		//echo $query."<br/>";
		$result = mysql_query($query);
		if(!$result || mysql_num_rows($result) == 0){
			die("Unable to tally results:37<br/>$query<br/>");
		}
		else{
			$temp = mysql_fetch_assoc($result);
			//echo "<br/>".$temp['count']."<br/>";
			return $temp['count'];
		}
	}
	
}

function return_main_users(){
	$query = "SELECT user_id,school_name,user_firstname,user_lastname,user_bio,user_dp,user_schoolid FROM cheersu_users,cheersu_schools ".
	"WHERE user_id IN (5,6) AND school_id = user_schoolid";
	$result = mysql_query($query);
	if(!$result){
		die("unable to get suggestions");
	}
	$tempresult = array();

	while($temp = mysql_fetch_assoc($result)){

		if($temp['user_dp'] == ""){
			$temp['user_dp'] = "cheersu_icon.png";
		}
		include 'removeslashes.php';
		array_push($tempresult,$temp);
	}
	return $tempresult;
}
function generate_suggestions(){
	global $myschoolid;
	global $userid;
	global $pdo;
	$returnval = array();
	echo "<br/>$userid<br/>";
	$query = "SELECT user_id,friend_status FROM cheersu_friends_$userid,cheersu_users WHERE "
	." friend_user_id = user_id AND user_blocked = 0";
	$result = mysql_query($query);
	if(!$result){
		die("unable to query databases:76");
	}
	if(mysql_num_rows($result) == 0){
		$nofriends = true;
		$query = "SELECT school_state, user_schoolid FROM cheersu_users,cheersu_schools WHERE user_id = ? ".
				" AND school_id = user_schoolid";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($userid));
		if($stmt->rowCount() < 1){
			die("Unable to query databases:85");
		}
		else{
			$temp = $stmt->fetch(PDO::FETCH_ASSOC);
			$state = $temp['school_state'];
			$school = $temp['schoolid'];
			$query = "SELECT user_firstname,school_name,user_id,user_lastname,user_bio,user_dp,user_schoolid FROM cheersu_users,cheersu_schools WHERE user_schoolid = ? AND user_id <> '$userid' ".
			"AND school_id = user_schoolid AND user_verified = 1 AND user_blocked = 0";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($school));
			if($stmt->rowCount() == 0){
				$query = "SELECT user_firstname,school_name,user_id,user_lastname,user_bio,user_dp,user_schoolid FROM cheersu_users,cheersu_schools WHERE user_schoolid IN ".
						"(SELECT school_id FROM cheersu_schools WHERE school_state = '$state') AND user_id <> '$userid' AND user_verified = 1 AND ".
						"user_blocked = 0 AND school_id = user_schoolid";
				$result = mysql_query($query);
				if(!$result){
					die("Unable to get users");
				}
				else if(mysql_num_rows($result) == 0){
					$tempresult = return_main_users();
					$returnval = $tempresult;
					
				}
				else{
					$tempresult = array();
					while($temp = mysql_fetch_assoc($result)){
						if($temp['user_dp'] == ""){
							$temp['user_dp'] = "cheersu_icon.png";
						}
						include 'removeslashes.php';
						array_push($tempresult,$temp);
					}
					$temps = return_main_users();
					array_merge($tempresult,$temps);
					$returnval = $tempresult;
					$returnval = array_map("unserialize", array_unique(array_map("serialize", $returnval)));
					
				}
					
			}
			else{
				$tempresult = array();
				while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
					if($temp['user_dp'] == ""){
						$temp['user_dp'] = "cheersu_icon.png";
					}
					include 'removeslashes.php';
					array_push($tempresult,$temp);
				}
				$temps = return_main_users();
				array_merge($tempresult,$temps);
				$returnval['suggestions'] = $tempresult;
				$returnval = array_map("unserialize", array_unique(array_map("serialize", $returnval)));
				
			}


		}
	}
	else{
		
		$myfriends = array(); 
		$list = "(";
		while($temp = mysql_fetch_assoc($result)){
			if($temp['friend_status'] == "confirmed")
				array_push($myfriends,$temp['user_id']);
			$list.=$temp['user_id'].",";
		}
		$list .= $userid.")";
		$friendlistlen = count($myfriends);
		//echo $list;
		for($i = 0; $i < $friendlistlen; $i++){
			$friend = $myfriends[$i];
			$query = "SELECT user_id,user_firstname,user_lastname,user_dp,school_name,user_schoolid FROM cheersu_friends_$friend,".
					"cheersu_schools,cheersu_users WHERE user_id = friend_user_id AND school_id = user_schoolid AND user_id NOT IN $list";
			$result = mysql_query($query);
			if(!$result){
				die("unable to fetch friends of friends");
			}
			else if(mysql_num_rows($result)!=0){
				while($temp = mysql_fetch_assoc($result)){
					if($temp['user_dp'] == ""){
						$temp['user_dp'] = "cheersu_icon.png";
					}
					include 'removeslashes.php';
					$temp['mutual'] = get_mutual_friends($userid, $temp['user_id']);
					array_push($returnval,$temp);
				}
			}
		}
		
		$query = "SELECT user_id,user_firstname,user_lastname,user_dp,school_name,user_schoolid FROM ".
					"cheersu_schools,cheersu_users WHERE (user_id = 5 OR user_id = 6) AND school_id = user_schoolid AND user_id NOT IN $list";
		//echo $query;
		$result = mysql_query($query);
		if(!$result){
			die("Unable to query");
		}
		else if(mysql_num_rows($result) != 0){
			while($temp = mysql_fetch_assoc($result)){
				if($temp['user_dp'] == ""){
					$temp['user_dp'] = "cheersu_icon.png";
				}
				include 'removeslashes.php';
				$temp['mutual'] = get_mutual_friends($userid, $temp['user_id']);
				array_push($returnval,$temp);
			}
		}
		$returnval = array_map("unserialize", array_unique(array_map("serialize", $returnval)));
		usort($returnval,"cmp");
		
	}
	//print_r($returnval);
	$myschool = array();
	$otherschool = array();
	$len = count($returnval);
	foreach($returnval as $key=>$value){
		if($value['user_schoolid'] == $myschoolid){
			array_push($myschool,$value);
		}
		else{
			array_push($otherschool,$value);
		}
	}
	$returnval = array();
	$returnval = array_merge($myschool,$otherschool);
	return $returnval;
	
}


	if(isset($_GET['userid'])){
		
		$suggestions = generate_suggestions();
		print_r($suggestions);
		
	}
?>