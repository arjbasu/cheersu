
function initialisechat(){
	
	function setCookie(c_name,value,exdays)
	{
		var exdate=new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
		document.cookie=c_name + "=" + c_value;
	}
	

	//$("#notification-sound")[0].volume = 0.6;
	chatwindows = [];					//the list of chatwindows open
	chatwindowtext = [];				//the chat content of each chat window
	windowanimations = [];				//id of the chat window minimised animations
	windowbgcolor = "";					//the background color of chat header 
	var onlinelistrefresh = 30*1000;	//interval after which the online list will be refreshed in ms
	var chatrefresh = 3*1000;			//interval after which new chat messages will be refreshed in ms
	var blurred = false;				//whether the window is out of focus
	var animation = "";					//id of the window toggle animation
	var toggletitle = "";				//the title to display when a new chat message arrives
	var title = document.title;			//the original title of the page
	
	var template = 
		'<div class = "chatbox" data-state = "open">'+
			'<div class = "chatbox-header well">'+
				'<p><span></span>'+
					'<a href = "#" class = "chatclose pull-right">X</a>'+
				'</p>'+
			'</div>'+
			'<div class = "chat-text">'+
			 '<table></table>' +
			'</div>'+
			'<form class = "chatform">'+
			'<textarea name = "textbox" class = "message" autocomplete = "off"></textarea>'+
			'</form>'+
		'</div>';
//	console.log(template);
	imgurl = $("#chat_dp_icon").attr("src");
	
	
	$(window).blur(function(){
//		console.log(blurred);
		blurred = true;
	})
	.focus(function(){
		clearInterval(animation);
		document.title = title;
//		console.log(blurred);
		blurred = false;
	});
	
//	if($("#suggestionsection").length>0){
//		$("#chatbtn").css("right","36px");
//	}
	
	
	if(document.cookie){
		$.each(document.cookie.split(/; */),function(){
			  var splitCookie = this.split('=');
			  // name is splitCookie[0], value is splitCookie[1]
			  cookiename = splitCookie[0];
			  username = splitCookie[1];
			  
			  if(cookiename.indexOf("chatwindow")!=-1){
				  userid = cookiename.split('|')[1];
				  console.log(username);
				  for(i = 0; i < 5; i++){
					  username = decodeURIComponent(username);
				  }
				  openchatwindow(userid,username);
			  }
			  
			});
		}
	
	
	$("body").on("click",".user_name",function(){
		var userid = $(this).parents("p").attr("data-userid");
		if(chatwindows[userid] === undefined || chatwindows[userid] === null ){
			var username = $(this).html();
			openchatwindow(userid,username);
		}
		
		return false;
	});
	
	$("body").on("mousewheel",".chat-text",function(){
		event.stopPropagation();
		console.log("scrolling");
		var top = parseInt($(this).scrollTop());
		console.log("top:"+top);
		userid = $(this).parents(".chatbox").attr("data-userid");
		if(top < 150 && top > 0){
			var element = $(this).find("table tr:first-child");
			if(element.attr("data-first") != "true"){
				console.log("first is not true");
				var timestamp = element.attr("data-timestamp");
				console.log("top:"+userid+"|"+timestamp);
				fetchpreviousmessages(userid,timestamp,true);
			}
		}
	}).on("keyup",".message",function(e){
		if(e.keyCode == 13)
			$(this).parent(".chatform").trigger("submit");
	});
	$("#chatbtn").click(function(){
		$("#suggestionsection").hide();
		$(this).hide();
		$("#chatlist").height(window.innerHeight-68).show();
		return false;
	});
	$("#hidechatlist").click(function(){
		$("#chatlist").hide();
		$("#suggestionsection").show();
		$("#chatbtn").show();
		return false;
	});
	
	
	$("body").on("submit",".chatform",function(){
		var text = $(this).find(".message").val();
		text = trimfield(text);
		if(text != ""){
//			console.log("not working:"+text.length);
			html = "<tr class = 'line'><td class = 'chatimg-holder'><img src = '" + imgurl+ "' class = 'userdp'/></td><td class = 'message-holder'><p class = 'chatmessage'>"+text+"</p></td></tr>";
			$(this).parents(".chatbox").find(".chat-text table").append(html);
			selection = $(this).parents(".chatbox").find(".chat-text table tr:last");
			$(this).find(".message").val("");
			var chatbox = $(this).parents(".chatbox").find(".chat-text")[0];
			chatbox.scrollTop = chatbox.scrollHeight;
			var userid = $(this).parents(".chatbox").attr("data-userid");
			var data = {"user_from":userid,"user_message":text,"action":"sendmessage"};
			var current = this;
//			console.log(data);
			$.ajax({
				url:"ajaxapi/chat.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					if(data.status != "success"){
						alert("Unable to send message");
						$(current).parents(".chatbox").find(".chat-text .line:last-child").remove();
					}
					else{
						selection.attr("data-timestamp",data.message);
					}
					
				},
				error:function(){
					alert("Unable to send message");
					$(this).parents(".chatbox").find(".chat-text .line:last-child").remove();
				}
			});
		}
		return false;
	});
	
	$("body").on("click",".chatbox-header",function(){
		parent = $(this).parents(".chatbox");
		if(parent.attr("data-state") == "open"){
			parent.find(".chat-text,form").hide();
			$(this).trigger("mouseleave");
			parent.attr("data-state","minimised");
		}
		else{
			var boxid = "#"+parent.attr("id");
			if(windowanimations[boxid]!== undefined || windowanimations[boxid] !== null){
//				console.log(windowanimations);
//				console.log("boxid:"+boxid)
//				console.log("in cancell mode:"+windowanimations[boxid]);
				clearInterval(windowanimations[boxid]);
				windowanimations[boxid] = null;
				$(boxid).find(".chatbox-header").css("background-color","#202328");
			}
			
			parent.attr("data-state","open");
			parent.find(".chat-text,form").show();
			parent.find("input").trigger("focus");
			var chatbox = parent.find(".chat-text")[0];
			chatbox.scrollTop = chatbox.scrollHeight;
		}
	});
	$("body").on("mouseenter",".chatbox-header",function(){
		$(this).css("background-color","orange");
	})
	.on("mouseleave",".chatbox-header",function(){
		$(this).css("background-color","#202328");
	});
	$("body").on("click",".chatclose",function(){
		var userid = $(this).parents(".chatbox").attr("data-userid");
		var html = $(this).parents(".chatbox").find(".chat-text table").html();
//		console.log("userid:"+userid);
		setCookie("chatwindow|"+userid,"hkjhkjh",-1);
		chatwindows[userid] = null;
		chatwindowtext[userid] = html;
		$(this).parents(".chatbox").remove();
		recalibrate();
		return false;
	});
	$(".user_name").click(function(){
		return false;
	});
	
	function recalibrate(){
		var i = 0;
		$(".chatbox").each(function(){
			sl = (parseInt($(this).css("right"))-220)/280;
			if(sl != i){
				rightval = 280*i + 220;
				$(this).css("right",rightval);
			}
			i++;
		});
	}
	
	function togglehead(){
		currentTitle = document.title;
		if(currentTitle.search("New")!=-1){
			document.title = title;
		}
		else{
			document.title = toggletitle;
		}
	}
	
	function togglewindowanimation(id){
		windowbgcolor = $(id).find(".chatbox-header").css("background-color");
		if(windowbgcolor === "rgb(255, 165, 0)"){
			$(id).find(".chatbox-header").css("background-color","#202328")
		}
		else{
			$(id).find(".chatbox-header").css("background-color","orange");
		}
		
	}
	
	function checkmessages(){
		var users = [];
		var data = {"action":"getmessages"};
		$.ajax({
			url:"ajaxapi/chat.php",
			type:"POST",
			dataType:"JSON",
			data:data,
			success:function(data){
				if(data.status == "success"){
					messages = data.message;
					len = messages.length;
					if(len>0){
						if(blurred === true){
							clearInterval(animation);
//							console.log("blurred");
							toggletitle = "New Message(s) from "+ messages[0].user_name;
							animation = setInterval(togglehead,1000);
							
						}
						$("#notification-sound")[0].play();
					}
					
					for(var i = 0; i < messages.length;i++){
						var userid = messages[i].chat_user_from;
						users[messages[i].user_name] = 0;
						html = "<tr class = 'line' data-timestamp = '" + messages[i].chat_time + "'><td class = 'chatimg-holder'><img src = 'img/icons/" + messages[i].user_dp_icon 
						+ "' class = 'userdp'/></td><td class = 'message-holder'><p class = 'chatmessage'>"+
						messages[i].chat_message +"</p></td></tr>";
						if(chatwindows[userid] === undefined || chatwindows[userid] === null){
							var username = messages[i].user_name;
							openchatwindow(userid,username,"true");
							timestamp = messages[i].chat_time;
//							console.log("timestamp:"+timestamp);
							fetchpreviousmessages(userid,timestamp);
						}
						id = "#chat"+userid;
						$(id).find(".chat-text table").append(html)[0];
						if($(id).attr("data-state") === "minimised"){
							if(windowanimations[id] === undefined || windowanimations[id] === null){
								windowanimations[id] = setInterval(togglewindowanimation,700,id);
							}
						}
						 var chatbox = $("#chat"+userid).find(".chat-text")[0];
						chatbox.scrollTop = chatbox.scrollHeight;
					}
				}
			},
			error:function(){
				
			}
			
		});
	}
	
	function sendchatmessage(userid,message){
		
	}
	
	function openchatwindow(userid,username,newchat){
		
		
//		console.log("openchatwindow called with:"+userid+"|"+username);
		
		cookiename = "chatwindow|"+userid;
		setCookie(cookiename,encodeURIComponent(username),365);
//		console.log(template);
		len = $(".chatbox").length;
		rightval = 280*len + 220;
//		console.log("creating chatbox for :"+userid);
//		console.log(template);
		$("body").append(template);
		$(".chatbox:last-child")
		.css("right",rightval)
		.attr("data-userid",userid)
		.attr("id","chat"+userid)
		.find(".chatbox-header p span").html(username).find(".message").trigger("focus");
		if(chatwindowtext[userid] !== undefined){
			$(".chatbox:last-child .chat-text table").html(chatwindowtext[userid]);
		}
		$(".chatbox:last-child").show().find(".message").trigger("focus");
		var chatbox = $(".chatbox:last-child .chat-text")[0];
		chatbox.scrollTop = chatbox.scrollHeight;
		chatwindows[userid] = username;
		if(newchat === undefined){
//			console.log("fetching for new chatwindow");
			if($("#chat"+userid).find(".chat-text table tr").length == 0){
//				console.log("no messages:"+userid);
				fetchpreviousmessages(userid);
			}
			else if($("#chat"+userid).find(".chat-text table tr").length <=30){
//				console.log("somemessages")
				var last = $("#chat"+userid).find("table tr:first-child").attr("data-timestamp");
//				console.log("last:"+last);
				fetchpreviousmessages(userid,last);
			}
		}
		
		
	}
	
	function fetchpreviousmessages(userid,timestamp,history){
		var data;
		if(timestamp === undefined){
			data = {"action":"gethistory","user_id":userid};
		}
		else{
			data = {"action":"gethistory","user_id":userid,"timestamp":timestamp};
		}
		$.ajax({
			url:"ajaxapi/chat.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					messages = data.message;
					html = "";
					len = messages.length;
					for(var i = len-1; i >= 0;i--){
						html += "<tr class = 'line' data-timestamp = '" + messages[i].chat_time + "'><td class = 'chatimg-holder'><img src = 'img/icons/" + messages[i].user_dp_icon 
						+ "' class = 'userdp'/></td><td class = 'message-holder'><p class = 'chatmessage'>"+
						messages[i].chat_message +"</p></td></tr>";
					}
					id = "#chat"+userid;
					if(len == 0){
						$(id).find(".chat-text table tr:first-child").attr("data-first","true");
					}
					
					var chatbox = $(id).find(".chat-text table").prepend(html).parents(".chat-text")[0];
					if(history === undefined)
						chatbox.scrollTop = chatbox.scrollHeight;
					else{
						position = $(id).find(".chat-text table tr").eq(20).position().top;
						$(id).find(".chat-text").scrollTop(position);
					}
				}
			},
			error:function(data){
				
			}
		})
	}
	
	
	function getonlinelist(){
		var data = {"action":"getonlinelist"}; 
		$.ajax({
			url:"ajaxapi/chat.php",
			type:"POST",
			dataType:"JSON",
			data:data,
			success:function(data){
				if(data.status == "success"){
					list = data.message;
					count = data.message.length;
					$("#chatbtn span").html(count);
					html = "";
					if(count!=0){
						for(var i = 0; i < count; i++){
							html+= "<p class = 'chatlist_user' data-userid = '" + list[i].user_id + "'><img src = 'img/icons/" + list[i].user_dp_icon + 
							"' style = 'height:28px;width:28px;float:left'/>&nbsp;&nbsp;<a href = '#' class = 'user_name'>"+
							list[i].user_name + "</a></p>";
						}
					}
					$("#onlinelist").html(html);
				}
				else{
					setTimeout(getonlinelist,5000);
				}
			},
			error:function(){
//				console.log("unable to fetch onlinelist");
				$("#chatbtn span").html("0");
				$("#onlinelist").html("");
				setTimeout(getonlinelist,5000);
			}
			
		});
	}
	
	checkmessages();
	getonlinelist();
	setInterval(getonlinelist,onlinelistrefresh);
	setInterval(checkmessages,chatrefresh);
}
	