<?php
	session_start();
	include 'check_authorization.php';
	if(!isset($_POST['searchquery'])){
		 header("Location:index.php");
	}
	include 'connect.php';
	include 'twiginit.php';
	
	$userid = $_SESSION['user_id'];
	$search = $_POST['searchquery'];
	
	$results = array();
	$newsearch = "%".$search."%";
	$wordsearch = str_replace(" ","%", $search);
	$wordsearch = "%".$wordsearch."%";
	$keywords = explode(" ", $search);
	$length = count($keywords);
	$regexpsearch = "^.*(";
	for($i = 0; $i < $length; $i++){
		$regexpsearch.= $keywords[$i]."|";
	}
	$regexpsearch = substr($reg, 0,-1);
	$regexpsearch.= ").*$";
	
	$query = "SELECT user_firstname,user_lastname,user_dp,user_email,user_id,school_name FROM cheersu_users,cheersu_schools WHERE ".
			"user_verified = 1 AND user_id <> '$userid' AND user_blocked = 0 AND user_schoolid = school_id AND (user_email LIKE ? OR user_firstname LIKE ? OR user_lastname LIKE ? )";
	
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($newsearch,$newsearch,$newsearch));
	if($stmt->rowCount() >0){
		while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
			if($temp['user_dp'] == ""){
				$temp['user_dp'] = "cheersu_icon.png";
			}
			array_push($results, $temp);
		}
	}
	
	$query = "SELECT user_firstname,user_lastname,user_dp,user_email,user_id,school_name FROM cheersu_users,cheersu_schools WHERE ".
			"user_verified = 1 AND user_id <> '$userid' AND  user_blocked = 0 AND user_schoolid = school_id AND (user_email LIKE ? OR user_firstname LIKE ? OR user_lastname LIKE ? )";
	
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($wordsearch,$wordsearch,$wordsearch));
	if($stmt->rowCount() >0){
		while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
			if($temp['user_dp'] == ""){
				$temp['user_dp'] = "cheersu_icon.png";
			}
			array_push($results, $temp);
		}
	}
	
	$query = "SELECT user_firstname,user_lastname,user_dp,user_email,user_id,school_name FROM cheersu_users,cheersu_schools WHERE ".
			"user_verified = 1 AND user_id <> '$userid' AND user_blocked = 0 AND user_schoolid = school_id AND (user_email REGEXP ? OR user_firstname REGEXP ? OR user_lastname REGEXP ? )";
	
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($regexpsearch,$regexpsearch,$regexpsearch));
	if($stmt->rowCount() >0){
		while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
			if($temp['user_dp'] == ""){
				$temp['user_dp'] = "cheersu_icon.png";
			}
			array_push($results, $temp);
		}
	}
	
	$results = array_map("unserialize", array_unique(array_map("serialize", $results)));
	error_log(sizeof($results),0);
	$data = array();
	$data['title'] = "Search Results for $search";
	$data['searchquery'] = $search;
	$data['username'] = $_SESSION['username'];  
	if(sizeof($results) == 0){
		$data['noresults'] = true;
	}
	else{
		
		$data['userresults'] = $results;
	}
	
	echo $twig->render("results.twig",$data);
?>