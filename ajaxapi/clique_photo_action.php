<?php
	session_start();
	include 'authentication_ajax_api.php';
	$status = "";
	$message = "";
	if(isset($_POST['type']) && isset($_POST['clique_id'])){
		include '../connect.php';
		include 'post_notification.php';
		$userid = $_SESSION['user_id'];
		$type = $_POST['type'];
		$cliqueid = $_POST['clique_id'];
		if($type == "getimages"){
			$query = "SELECT * FROM cheersu_clique_photos WHERE clique_photos_cliqueid = ? ORDER BY clique_photos_timestamp DESC";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($cliqueid));
			if($stmt->rowCount() == 0){
				$status = "success";
				$message = "No Images available";
			}
			else{
				$status = "success";
				$clique_image = array();
				while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
					array_push($venue_image,$temp);
				}
				$message = $clique_image;
			}
		}
		else if($type == "delete"){
			if(isset($_POST['filename'])){
				chdir("../");
				foreach($_POST['filename'] as $imgname){
					unlink("img/clique_img/$imgname");
					unlink("img/clique_img/icons/$imgname");
				}
				$status = "success";
				$message = "files successfully deleted";
			}
			else{
				$status = "error";
				$message = "Improper parameters passed";
			}
		}
		else if($type == "post"){
			if(isset($_POST['filename'])){
				$query = "SELECT now() AS timestamp";
				$result = mysql_query($query);
				error_log("NUm rows:".mysql_num_rows($result));
				if($result){
					$temp = mysql_fetch_assoc($result);
					$timestamp = $temp['timestamp'];
					$query = "BEGIN";
					$stmt = $pdo->prepare($query);
					$stmt->execute();
					$query = "INSERT INTO cheersu_clique_photos(clique_photos_userid,clique_photos_source,clique_photos_timestamp,clique_photos_cliqueid) VALUES ";
					$insertquery = array();
					$insertdata = array();
					foreach($_POST['filename'] as $data){
						$insertquery[] = '(?,?,?,?)';
						$insertdata[] = $userid;
						$insertdata[] = $data;
						$insertdata[] = $timestamp;
						$insertdata[] = $cliqueid;
					}
					if(!empty($insertquery)){
						$query.= implode(', ',$insertquery);
						$stmt = $pdo->prepare($query);
						error_log("venuequery:".$query."|");
						$stmt->execute($insertdata);
						if($stmt->rowCount() >= 1){
								
						$query = "INSERT INTO cheersu_cliques_activity(clique_activity_cliqueid,clique_activity_type,clique_activity_userid,clique_activity_timestamp) VALUES (?,?,?,?)";
						$stmt = $pdo->prepare($query);
						$stmt->execute(array($cliqueid,"photo-activity",$userid,$timestamp));
						if($stmt->rowCount() == 1){
							$query = "COMMIT";
							$stmt = $pdo->prepare($query);
							$stmt->execute();
							$status = "success";
							$message = "Photos successfully posted";
						}
						else{
							$query = "ROLLBACK";
							$stmt = $pdo->prepare($query);
							$stmt->execute();
							$status = "success";
							$message = "photos successfully uploaded";
						}
							
							$query = "COMMIT";
							$stmt = $pdo->prepare($query);
							$stmt->execute();
							post_notification("photo");
							$status = "success";
							$message = "photos successfully posted";
								
						}
						else{
							$query = "ROLLBACK";
							$stmt = $pdo->prepare($query);
							$stmt->execute();
							$status = "error";
							$message = "unable to insert into database";
						}
					}
						
				}
				else{
					$status = "error";
					$message = "unable to retrieve time";
				}
			}
			else{
				$status = "error";
				$message = "Improper parameters passed";
			}
		}
	}
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}
	include 'json_encoding.php';
?>