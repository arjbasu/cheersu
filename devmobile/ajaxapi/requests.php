<?php
session_start();
include '../connect.php';
include 'authentication_ajax_api.php';
if(isset($_POST['venue_name']) && isset ($_POST['venue_school_id']) && isset($_POST['venue_address'])){
	$query = "SELECT user_email FROM cheersu_users WHERE user_id = ?";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($_SESSION['user_id']));
	$tempemail = $stmt->fetch(PDO::FETCH_ASSOC);
	$email = $tempemail['user_email'];

	$venuename = $_POST['venue_name'];
	$schoolid = $_POST['venue_school_id'];
	$address = $_POST['venue_address'];

	$query = "INSERT INTO cheersu_venue_requests(venue_request_name,venue_request_school_id,venue_request_address,venue_request_sender) VALUES(?,?,?,?)";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($venuename,$schoolid,$address,$email));

	if($stmt->rowCount() == 1){
		$to = "support@cheersu.com";
		$subject = "Cheersu Venue request from $email";
		$message = "Venue Name: $venuename <br/> Address: $address";
		$headers = "From: CheersU <no-reply@cheersu.com> \r\n" .
				"Reply-To: $email" . "\r\n" .
				"X-Mailer: PHP/" . phpversion();
		mail($to, $subject, $message,$headers);
		$status = "success";
		$message = "Venue Request successfully sent";

	}
	else{
		$status = "error";
		$message = "There was an error sending the venue request";
	}

}

else if(isset($_POST['school_name']) && isset ($_POST['school_state_id'])){
	$query = "SELECT user_email FROM cheersu_users WHERE user_id = ?";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($_SESSION['user_id']));
	$tempemail = $stmt->fetch(PDO::FETCH_ASSOC);
	$email = $tempemail['user_email'];

	$schoolname = $_POST['school_name'];
	$stateid = $_POST['school_state_id'];

	$query = "INSERT INTO cheersu_school_requests(school_request_name,school_request_state_id,school_request_sender) VALUES(?,?,?)";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($schoolname,$stateid,$email));

	if($stmt->rowCount() == 1){
		$to = "support@cheersu.com";
		$subject = "Cheersu School request from $email";
		$message = "School Name: $schoolname";
		$headers = "From: CheersU <no-reply@cheersu.com> \r\n" .
				"Reply-To: $email" . "\r\n" .
				"X-Mailer: PHP/" . phpversion();
		mail($to, $subject, $message,$headers);
		$status = "success";
		$message = "School Request successfully sent";

	}
	else{
		$status = "error";
		$message = "There was an error sending the school request";
	}

}
else{
	$status = "error";
	$message = "Improper parameters passed";
}
include 'json_encoding.php';
?>