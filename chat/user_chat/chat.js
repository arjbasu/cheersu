var onlinelist_status = true; // if true it will show onlinelist
var maxinputHeight = 48; // the height of chat input max will go, without padding
var chatrefreshtime = 6000; // refresh onlinelist and chats ever x secs. 


var open_cb = new Array();
var min_open_cb = new Array();
var keepbox = new Array();


cookie_c_obox = $.cookie('c_openbox');
if(!cookie_c_obox) { 
	$.cookie("c_openbox","", { expires: 30 });
}
min_cookie_box = $.cookie('min_openbox');
if(!min_cookie_box) { 
	$.cookie("min_openbox","", { expires: 30 });
}


// chat with person
function chat(user_id){
	username = document.getElementById('div_'+user_id).innerHTML;
	startchat('startchat',user_id,username);
};
// chat with person


// start chat
function startchat(mode,user_id,username){
	chat_title = (user_id+"|"+username);
	openbox_cookie = $.cookie("c_openbox");
	limit_count_box = getWindowWidth();
	limit_count_box = (limit_count_box - 276) / 207 | 0;
	limit_count_box = limit_count_box - 1;
	limit_cb_to_min = limit_count_box + 1;
	if(mode == "startchat"){
		
		if($.cookie('c_openbox') == 0){
			open_cb.push(chat_title);
			join_open_cb = open_cb.join();
			$.cookie("c_openbox",join_open_cb, { expires: 30 });
		} else {
			split_openbox_cookie = $.cookie("c_openbox").split(",");
			
			nvs = 0;
			for(x in split_openbox_cookie) {
			nvs++;
			}
			if(nvs > limit_count_box){
				if($.cookie('min_openbox') == 0){
					if (split_openbox_cookie.indexOf(chat_title) == -1){
						min_open_cb.push(chat_title);
						join_min_cb = min_open_cb.join();
						$.cookie("min_openbox",join_min_cb, { expires: 30 });
						
						minbox('0','0',limit_cb_to_min);
					}
				} else {
					split_minbox_cookie = $.cookie("min_openbox").split(",");
					if (split_minbox_cookie.indexOf(chat_title) == -1){
						if (split_openbox_cookie.indexOf(chat_title) == -1){
							split_minbox_cookie.push(chat_title);
							join_minbox = split_minbox_cookie.join();
							$.cookie("min_openbox",join_minbox, { expires: 30 });
							
							minbox('0','0',limit_cb_to_min);
						}
					}
				}
			} else {
				if (split_openbox_cookie.indexOf(chat_title) == -1){
				split_openbox_cookie.push(chat_title);
				join_sco = split_openbox_cookie.join();
				$.cookie("c_openbox",join_sco, { expires: 30 });
				}
			}
		}
	}
	
	
	if ($("#UserChat_"+user_id).length > 0) {
			check_boxcookie = $.cookie("c_openbox").split(",");
			boxcookie_nmb = 0;
			for(x in check_boxcookie) {
				boxcookie_x = check_boxcookie[x].split("|");
				boxcookie_width = (x)*(200+7)+227;
				$("#UserChat_"+boxcookie_x[0]).css('right', boxcookie_width+'px');
				if($("#UserChat_"+boxcookie_x[0]).css('display') == 'none') {
					$("#UserChat_"+boxcookie_x[0]).css('display', 'block');
					$("#UserChat_"+boxcookie_x[0]+" #chatinput").css('display', 'block');
					$("#UserChat_"+boxcookie_x[0]+" #chatcontent").css('display','block');
					$("#UserChat_"+boxcookie_x[0]+" #mini_mebox").css('display','block');
				}
			boxcookie_nmb++;
			}
	} else {
		split_min = $.cookie("min_openbox").split(",");
		if (split_min.indexOf(chat_title) == -1){
			$(" <div />" ).attr("id","UserChat_"+user_id)
			.addClass("UserChat")
			.html('<a href="javascript:togglebox(\'UserChat_'+user_id+'\')" class="Chatheader"><div class="cheader_left"><table><tr><td><div id="chat_online" class="chat_online">'+username+'</div></td></tr></table></div><div class="cheader_right"><table><tr><td class="mini_me" valign="top"><a id="mini_mebox" href="javascript:mini_mebox(\''+user_id+'\',\''+username+'\')" style="display:none;"></a></td><td class="mini_x" valign="top"><a href="javascript:closebox(\''+user_id+'\',\''+username+'\')">X</a></td></tr></table></div></a><div id="chatcontent" style="display:none;"></div><div id="chatinput" style="display:none;"><textarea onkeydown="javascript:return sendmsg(event,this,\''+user_id+'\',\''+username+'\');" id="textinput" style="height:16px;" onkeydown=""></textarea></div>')
			.appendTo($("body"));
			$("#UserChat_"+user_id+" #chatinput").css('display', 'block');
			$("#UserChat_"+user_id+" #chatcontent").css('display','block');
			$("#UserChat_"+user_id+" #mini_mebox").css('display','block');
		}	
		
		new_leng = $.cookie("c_openbox");
		new_dleng = new_leng.split(",");
			
		nbs = 0;
		for(x in new_dleng) {
			new_x = new_dleng[x].split("|");
				
			new_width = (x)*(200+7)+227;
			if(user_id == new_x[0]){
			$("#UserChat_"+new_x[0]).css('right', new_width+'px');
			}
			if(nbs > limit_count_box){
			$("#UserChat_"+new_x[0]).css('display', 'none');
			minbox(new_x[0],new_x[1],limit_open_cb);
			}
		nbs++;
		}
	}
	

};
// start chat



// Run Script when page loads
function onstart(){
	limit_open_cb = getWindowWidth();
	limit_open_cb = (limit_open_cb - 276) / 207 | 0;
	limit_open_cb1 = limit_open_cb - 1;
	
	
	if($.cookie('c_openbox') == 0){} else {
	p_leng = $.cookie("c_openbox");
	l_leng = p_leng.split(",");
	numbers_of_cbs = 0;
	for(x in l_leng) {
		r_x = l_leng[x].split("|");	
	chatdiv_id = "UserChat_"+r_x[0];
	chatdiv_title = r_x[1];
		
		if(numbers_of_cbs < limit_open_cb){
			if(chatdiv_title != 0){
				$(" <div />" ).attr("id",chatdiv_id)
				.addClass("UserChat")
				.html('<a href="javascript:togglebox(\''+chatdiv_id+'\')" class="Chatheader"><div class="cheader_left"><table><tr><td><div id="chat_online" class="chat_online">'+chatdiv_title+'</div></td></tr></table></div><div class="cheader_right"><table><tr><td class="mini_me" valign="top"><a id="mini_mebox" href="javascript:mini_mebox(\''+r_x[0]+'\',\''+r_x[1]+'\')" style="display:none;"></a></td><td class="mini_x" valign="top"><a href="javascript:closebox(\''+r_x[0]+'\',\''+r_x[1]+'\')">X</a></td></tr></table></div></a><div id="chatcontent" style="display:none;"></div><div id="chatinput" style="display:none;"><textarea onkeydown="javascript:return sendmsg(event,this,\''+r_x[0]+'\',\''+r_x[1]+'\');" id="textinput" style="height:16px;" onkeydown=""></textarea></div>')
				.appendTo($("body"));
				new_width = (x)*(200+7)+227;
				$("#"+chatdiv_id).css('right', new_width+'px');
				
			}
		
		}
		if(numbers_of_cbs > limit_open_cb1){
			minbox(r_x[0],r_x[1],limit_open_cb);
		} else {
			if($.cookie("min_openbox")){
			minbox(r_x[0],r_x[1],limit_open_cb);
			}
		}

	numbers_of_cbs++;
	}
	}
	
};
onstart();
// Run Script when page loads


// Create min box holder
function minbox(chatdiv_id,chatdiv_title,limit_open_cb){
	if(chatdiv_id != 0){
	minbox_title = (chatdiv_id+"|"+chatdiv_title);
	}
	if ($("#ChatMore").length > 0) {
		if ($("#ChatMore").css('display') == 'none') {
		$("#ChatMore").css('display','block');
		cm_width = (limit_open_cb)*(200+7)+227;
		$("#ChatMore").css('right', cm_width+'px');
		}
	} else {
		$(" <div />" ).attr("id","ChatMore")
		.addClass("ChatMore")
		.html('<a href="javascript:toggleminbox()" id="ChatMoreLink"><div id="ShowMoreLink">0</div><div id="GetMoreLink"><ul id="ShowMoreAdd"></ul></div></a>')
		.appendTo($("body"));
		cm_width = (limit_open_cb)*(200+7)+227;
		$("#ChatMore").css('right', cm_width+'px');
	}
	
	varwidth = $("#GetMoreLink").width();
	varwidth = (varwidth - 45);
	varheight = $("#GetMoreLink").height();
	
	$("#GetMoreLink").css('bottom',22);
	$("#GetMoreLink").css('left',-1);
	
	mbox_c = $.cookie("min_openbox");
	split_mbox_c = mbox_c.split(",");
	
	var min_nbs = 0;
	for(x in split_mbox_c) {
		new_minx = split_mbox_c[x].split("|");
		new_minx_title = new_minx[0]+"|"+new_minx[1];
		if ($("#li_minbox_"+new_minx[0]).length > 0) {
			$("#li_minbox_"+new_minx[0]).css('display','block');
		} else {
			$("#ShowMoreAdd").append('<li id="li_minbox_'+new_minx[0]+'"><a href="javascript:swampbox(\''+new_minx_title+'\')"><div>'+new_minx[1]+'</div></a></li>');
		}
	min_nbs++;
	}
	document.getElementById("ShowMoreLink").innerHTML = (min_nbs);
};
// Create min box holder


// swamp old chat box to new
function swampbox(minbox_title){
	split_min_openbox = $.cookie("min_openbox").split(",");
	split_c_openbox = $.cookie("c_openbox").split(",");
   
	for(x in split_c_openbox) {
		if(x == 4){
		deleteid = split_c_openbox[x];
		roleover_old = split_c_openbox[x].split("|");
		roleover_old_id = roleover_old[0];
		roleover_old_title = roleover_old[1];
		roleover_old_total = roleover_old[0]+"|"+roleover_old[1];
		split_c_openbox.splice(split_c_openbox.indexOf(deleteid), 1);
		
		split_c_openbox.push(minbox_title);
		join_c_openbox = split_c_openbox.join();
		$.cookie("c_openbox",join_c_openbox, { expires: 30 });
		
		split_min_openbox.push(deleteid);
		split_min_openbox.splice(split_min_openbox.indexOf(minbox_title), 1);
		join_min_openbox = split_min_openbox.join();
		$.cookie("min_openbox",join_min_openbox, { expires: 30 });
		
		
		$("#UserChat_"+roleover_old_id).css('display','none');
		roleover_new = minbox_title.split("|");
		roleover_new_id = roleover_new[0];
		roleover_new_title = roleover_new[1];
		
		if ($("#UserChat_"+roleover_new_id).length > 0) {
		$("#UserChat_"+roleover_new_id).css('display','block');
		} else {
		$(" <div />" ).attr("id","UserChat_"+roleover_new_id)
		.addClass("UserChat")
		.html('<a href="javascript:togglebox(\'UserChat_'+roleover_new_id+'\')" class="Chatheader"><div class="cheader_left"><table><tr><td><div id="chat_online" class="chat_online">'+roleover_new_title+'</div></td></tr></table></div><div class="cheader_right"><table><tr><td class="mini_me" valign="top"><a id="mini_mebox" href="javascript:mini_mebox(\''+roleover_new_id+'\',\''+roleover_new_title+'\')" style="display:none;"></a></td><td class="mini_x" valign="top"><a href="javascript:closebox(\''+roleover_new_id+'\',\''+roleover_new_title+'\')">X</a></td></tr></table></div></a><div id="chatcontent" style="display:none;"></div><div id="chatinput" style="display:none;"><textarea onkeydown="javascript:return sendmsg(event,this,\''+roleover_new_id+'\',\''+roleover_new_title+'\');" id="textinput" style="height:16px;" onkeydown=""></textarea></div>')
		.appendTo($("body"));
		}
		roleover_width = (x)*(200+7)+227;
		$("#UserChat_"+roleover_new_id).css('right', roleover_width+'px');
		
		$("#li_minbox_"+roleover_new_id).css('display','none');
		
		if ($("#li_minbox_"+roleover_old_id).length > 0) {
		$("#li_minbox_"+roleover_old_id).css('display','block');
		} else {
		$("#ShowMoreAdd").append('<li id="li_minbox_'+roleover_old_id+'"><a href="javascript:swampbox(\''+roleover_old_total+'\')"><div>'+roleover_old_title+'</div></a></li>');
		}
		}
	}
   

};
// swamp old chat box to new


// close chat box
function closebox(div_id,div_name){
	div_title = "UserChat_"+div_id;
	div_total = div_id+"|"+div_name;
	if ($("#"+div_title).length > 0) {
		$("#"+div_title).css('display','none');
		split_openbox = $.cookie("c_openbox").split(",");
		split_openbox.splice(split_openbox.indexOf(div_total), 1);
		join_openbox = split_openbox.join();
		$.cookie("c_openbox",join_openbox, { expires: 30 });
		
		split_minbox = $.cookie("min_openbox").split(",");
		roleover = split_minbox[0].split("|");
		roleover_id = roleover[0];
		roleover_title = roleover[1];
		roleover_total = roleover_id+"|"+roleover_title;
		
	if(roleover_id == 0){} else {
		if ($("#UserChat_"+roleover_id).length > 0) {
			$("#UserChat_"+roleover_id).css('display','block');
		} else {
			$(" <div />" ).attr("id","UserChat_"+roleover_id)
			.addClass("UserChat")
			.html('<a href="javascript:togglebox(\'UserChat_'+roleover_id+'\')" class="Chatheader"><div class="cheader_left"><table><tr><td><div id="chat_online" class="chat_online">'+roleover_title+'</div></td></tr></table></div><div class="cheader_right"><table><tr><td class="mini_me" valign="top"><a id="mini_mebox" href="javascript:mini_mebox(\''+roleover_id+'\',\''+roleover_title+'\')" style="display:none;"></a></td><td class="mini_x" valign="top"><a href="javascript:closebox(\''+roleover_id+'\',\''+roleover_title+'\')">X</a></td></tr></table></div></a><div id="chatcontent" style="display:none;"></div><div id="chatinput" style="display:none;"><textarea onkeydown="javascript:return sendmsg(event,this,\''+roleover_id+'\',\''+roleover_title+'\');" id="textinput" style="height:16px;" onkeydown=""></textarea></div>')
			.appendTo($("body"));	
		}
			
			split_c_openbox = $.cookie("c_openbox").split(",");
			split_c_openbox.push(roleover_total);
			join_c_openbox = split_c_openbox.join();
			$.cookie("c_openbox",join_c_openbox, { expires: 30 });
	}
			openbox = $.cookie("c_openbox").split(",");
			xo = 0;
			for(x in openbox) {
			roleover_openbox = openbox[x].split("|");
			roleover_width = (xo)*(200+7)+227;
			$("#UserChat_"+roleover_openbox[0]).css('right', roleover_width+'px');
			
				if ($("#li_minbox_"+roleover_openbox[0]).length > 0) {
					$("#li_minbox_"+roleover_openbox[0]).css('display','none');
				}
			
			xo++;
			}
			split_nminbox = $.cookie("min_openbox").split(",");
			split_nminbox.splice(split_nminbox.indexOf(roleover_total), 1);
			join_minbox = split_nminbox.join();
			$.cookie("min_openbox",join_minbox, { expires: 30 });
			
			if($.cookie('min_openbox') == 0){} else {
			newsplit_nminbox = $.cookie("min_openbox").split(",");
			var newx = 1;
			for(x in newsplit_nminbox) {
					document.getElementById("ShowMoreLink").innerHTML = (newx);
			newx++;
			}
			}
			newsplit_nminbox = $.cookie("min_openbox").split(",");
			if(newsplit_nminbox == 0){
			$("#ChatMore").css('display','none');
			}
		

		
	}
};
// close chat box


// Send msg to other user
function sendmsg(event,ethis,id,name) {
	div_id = "UserChat_"+id;
	if(event.keyCode == 13 && event.shiftKey == 0)  {
		textinput = $("#"+div_id+" #textinput").val();
		textinput = textinput.replace(/(<([^>]+)>)/ig,"");
		textinput = textinput.replace(/\n/g, '<br />');
		if (textinput != '') {
			$.ajax({
			 type: "POST",
			 url: "user_chat/chat.php?action=sendmsg",
			 data: "to_name="+ name +"&to_id="+ id +"&message=" + textinput,
			 success: function(check_data){
				if(check_data != '0'){
					$("#"+div_id+" #chatcontent").append(check_data);
				}
			 }
			});
		}
		$("#"+div_id+" #textinput").val("");
		varprop = $('#'+div_id+' #chatcontent').prop("scrollHeight");
		varprop = varprop + 14;
		$('#'+div_id+' #chatcontent').scrollTop(varprop);
	return false;
	}
	if(event.shiftKey == 1 && event.keyCode == 13)  {
		div_height = $("#"+div_id+" #textinput").height();
		if (maxinputHeight > div_height) {
		new_div_height = div_height + 16;
		$("#"+div_id+" #textinput").css('height',new_div_height);
		}
	}
};
// Send msg to other user


// maxi and minize more chat boxes
function toggleminbox(){
	if ($("#GetMoreLink").css('display') == 'none') {
		$("#GetMoreLink").css('display','block');
	} else {
		$("#GetMoreLink").css('display','none');
	}
};
// maxi and minize more chat boxes


// maxi and minize chat box
function togglebox(div_id){
	if($("#"+div_id+" #chatinput").css('display') == 'none') {
		$("#"+div_id+" #chatinput").css('display','block');
		$("#"+div_id+" #chatcontent").css('display','block');
		$("#"+div_id+" #mini_mebox").css('display','block');
	} else {
		$("#"+div_id+" #chatinput").css('display','none');
		$("#"+div_id+" #chatcontent").css('display','none');
		$("#"+div_id+" #mini_mebox").css('display','none');
	}
};
function mini_mebox(div_id,div_name){
	if($("#UserChat_"+div_id+" #chatinput").css('display') == 'block') {
		$("#UserChat_"+div_id+" #chatinput").css('display','none');
		$("#UserChat_"+div_id+" #chatcontent").css('display','none');
		$("#UserChat_"+div_id+" #mini_mebox").css('display','none');
	}
};
// maxi and minize chat box


// Check for new boxes,messages
function checknew(){
	$.ajax({
		url: "user_chat/chat.php?action=checknew",
		cache: false,
		dataType: "json",
		async: false,
		success: function(data) {
		$.each(data.new_messages, function(i,new_message){
			if(new_message){
				user_chat_id = new_message.user_chat_id;
				client_from_id = new_message.client_from_id;
				client_from_name = new_message.client_from_name;
				client_to_id = new_message.client_to_id;
				client_to_name = new_message.client_to_name;
				user_chat_message = new_message.user_chat_message;
				chat_myid = new_message.chat_myid;
				
				if(chat_myid == client_to_id){
					startchat('startchat',client_from_id,client_from_name);
					todiv = client_from_id;
					chat_username = client_from_name;
				}
			}
		});
		$.each(data.all_messages, function(i,all_message){
			if(all_message){
				user_chat_id = all_message.user_chat_id;
				client_from_id = all_message.client_from_id;
				client_from_name = all_message.client_from_name;
				client_to_id = all_message.client_to_id;
				client_to_name = all_message.client_to_name;
				user_chat_message = all_message.user_chat_message;
				chat_myid = all_message.chat_myid;
				chat_from_status = all_message.chat_from_status;
				
				if(chat_myid == client_to_id){
				todiv = client_from_id;
				chat_username = client_from_name;
				} else {
					todiv = client_to_id;
					chat_username = "Me";
				}
				
					if ($('#UserChat_'+todiv).length > 0){
						if ($('#UserChat_'+todiv+' #msg_'+user_chat_id).length <= 0) {
						//$('#UserChat_'+todiv+' #chatcontent').append('<div class="cb_text" id="msg_'+user_chat_id+'"><table><tr><td valign="top"><p style="padding-right:3px;"><b>'+chat_username+'</b>:</p></td><td valign="top">'+user_chat_message+'</td></tr></table></div>');
							console.log(chat_username)
							if(chat_username === "Me"){
								$('#UserChat_'+todiv+' #chatcontent').append('<div class="cb_text" id="msg_'+user_chat_id+'"><table><tr><td valign="top"><p style="padding-right:3px;"><div class = "bubble left green">'+ user_chat_message+'</td></tr></table></div>');
							}
							else{
								$('#UserChat_'+todiv+' #chatcontent').append('<div class="cb_text" id="msg_'+user_chat_id+'"><table><tr><td valign="top"><p style="padding-right:3px;"><div class = "bubble right white">'+ user_chat_message+'</td></tr></table></div>');
							}
						
							if($("#UserChat_"+todiv+" #chatinput").css('display') == 'none') {
								$("#UserChat_"+todiv+" #chatinput").css('display','block');
								$("#UserChat_"+todiv+" #chatcontent").css('display','block');
								$("#UserChat_"+todiv+" #mini_mebox").css('display','block');
							}
						}
						if(chat_myid == client_to_id){
							if(chat_from_status == "on"){
								$("#UserChat_"+todiv+" #chat_online").removeClass("chat_offline").removeClass("chat_online").addClass("chat_online");
							} else {
								$("#UserChat_"+todiv+" #chat_online").removeClass("chat_offline").removeClass("chat_online").addClass("chat_offline");
							}
						}
						varprop = $('#UserChat_'+todiv+' #chatcontent').prop("scrollHeight");
						varprop = varprop + 14;
						$('#UserChat_'+todiv+' #chatcontent').scrollTop(varprop);
					}
					
			}
		});
		}
	});
};
var auto_refresh = setInterval(
    function (){
	checknew();
}, chatrefreshtime);
// Check for new boxes,messages


// Get Screen Width
function getWindowWidth() {
	var windowWidth = 0;
	if (typeof(window.innerWidth) == 'number') {
		windowWidth = window.innerWidth;
	}
	else {
		if (document.documentElement && document.documentElement.clientWidth) {
				windowWidth = document.documentElement.clientWidth;
		}
		else {
			if (document.body && document.body.clientWidth) {
					windowWidth = document.body.clientWidth;
			}
		}
	}
	return windowWidth;
};
// Get Screen Width


// onlinelist
function OnlineList() {
	if($('#onlinelist_holder').css('display') == 'none') {
		$('#onlinelist_holder').css('display','block');
		if($('#page_number').css('display') == 'block') {
			$('#page_number').css('display','block');
			$('#Chatmenu').css('display','block');
		}
	} else {
	$('#onlinelist_holder').css('display','none');
	$('#Chatmenu').css('display','none');
	}
};
function onlinelistoptions() {
	$('#chatoptions_tab').css('display','block');
};
function onlinelistoptions_out() {
	$('#chatoptions_tab').css('display','none');
};

function showusers() {
	$('#chatoptions_tab').css('display','none');
	onlinelist_get('Users','');
};
function showguest() {
	$('#chatoptions_tab').css('display','none');
	onlinelist_get('Guests','');
};
function onlinelist_get(type,page_id) {
	$.ajax({
		url: "user_chat/chat.php?action=onlinelist&page_id="+page_id+"&type="+type+"",
		cache: false,
		dataType: "json",
		success: function(data) {
			document.getElementById("member_holder").innerHTML = ("");
			$.each(data.online_lists, function(i,online_list){
			if(online_list){
				user_id = online_list.user_id;
				user_username = online_list.user_username;
				user_picture = online_list.user_picture;
				user_online = online_list.user_online;
				user_area = online_list.user_area;
				user_amount = online_list.user_amount;
				user_pages = online_list.user_pages;
				user_pagestring = online_list.user_pagestring;
				session_pagenr = online_list.session_pagenr;
				pagenr_start = online_list.pagenr_start;
				pagenr_end = online_list.pagenr_end;
				

				console.log(user_pagestring);
				document.getElementById("Onlinelistheader_left").innerHTML = (user_amount);
				if(user_pages == 0){
				document.getElementById("member_holder").innerHTML = ("<li style='padding:5px;'>No "+user_area+" online.</li>");
				} else {
				$("#member_holder").append('<li><a href="javascript:chat(\''+user_id+'\')"><div class="Chatlistimg"><img src="'+user_picture+'" title="'+user_username+'" alt="'+user_username+'"></div><div id="div_'+user_id+'" class="Chatlisttext">'+user_username+'</div></a></li>');
				}
				
				if($('#onlinelist_holder').css('display') == 'none') {
				$('#onlinelist_holder').css('display','block');
				}
				
				
			}
			});
			if(user_pagestring == ""){
			$('#Chatmenu').css('display','none');
			} else {
				if(user_pagestring == "0"){
				$('#Chatmenu').css('display','none');
				} else {
				$('#Chatmenu').css('display','block');
				}
			}	
			
			document.getElementById("page_number").innerHTML = ("");
			if(session_pagenr == 0){ session_pagenr = 1; }
			
			for (i = 1; i <= user_pagestring; i++){
			if(i == session_pagenr){ hover_class = " onlinelist_hover"; } else { hover_class = " onlinelist_none"; }
				if(pagenr_start == 1){
					if(i <= 6){
						if ($('#id_'+i).length > 0) {
						console.log(i+" yes");
						} else {
						$("#Chatmenu #page_number").append('<a href="javascript:onlinelist_page(\''+i+'\')" id="id_'+i+'" class="page_number'+hover_class+'">'+i+'</a>');
						}
					}
				} else {
					if(i >= pagenr_start && i <= pagenr_end){
					$("#Chatmenu #page_number").append('<a href="javascript:onlinelist_page(\''+i+'\')" id="id_'+i+'" class="page_number'+hover_class+'">'+i+'</a>');
					}
				}
			}
			document.getElementById("Chatmenu_right").innerHTML = ('<a href="javascript:onlinelist_page(\''+user_pagestring+'\')" class="Chatmenu_right"></a>');
			
		}  
	});
};

function onlinelist_page(page_id){
	onlinelist_get('',page_id)
};		
// onlinelist