<?php
	session_start();
	include 'authentication_ajax_api.php';
	include '../connect.php';
	$userid = $_SESSION['user_id'];
	if(isset($_POST['clique_id'])){
		$cliqueid = $_POST['clique_id'];
		$query = "DELETE FROM cheersu_mycliques_$userid WHERE myclique_id = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($cliqueid));
		if($stmt->rowCount() <1){
			$status = "error";
			$message = "Unable to delete from database";
		}
		else{
			$query = "DELETE FROM cheersu_cliques_users_$userid WHERE clique_id = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($cliqueid));
			$status = "success";
			$message = "Clique successfully deleted";
		}
	}
	else{
		$status = "error";
		$message = "improper parameters passed";
	}
	include 'json_encoding.php';
?>