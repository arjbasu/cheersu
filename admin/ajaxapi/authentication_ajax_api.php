<?php
if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH'])
		AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')) {
	die();	
}

if(!(isset($_SESSION['adminlogged']) && isset($_SESSION['username']) )){
	$status = "unauthenticated";
	$message = "Login First!!";
	include 'json_encoding.php';
	die();
}

?>