function showinlinemessage(parent,message){
	//console.log(parent);
	$(parent).parents(".main-cell").find(".main-cell").html(message).show();
	setTimeout(function(parent){
		$(parent).parents(".main-cell").find(".message").fadeOut("200");
	},"1000",parent);
}

fallbackmode = false;
var filehtml = "<input type = 'file' class = 'file' name = 'file[]'/>"
fallbackphotohtml = "<div class = 'photo-preview'>" +
"<img src = 'img/imguploaderplaceholder.gif' class = 'preview-img'/>"+
"</div>";

//This is the fallback image uploading function for browsers which don't
//support the ajaxfileupload plugin like IE (even 10 !) and older versions of Safari.

function fallback_upload(){
	console.log("fallback upload");
	var element = $(".file:last");
	console.log("element");
	console.log("fallback called");
	var file = element.val();
	var extension = file.split(".");
	extension = extension[extension.length-1];
	if(file!="" && (extension == "jpg"|| extension == "jpeg" || extension == "png" || extension == "bmp"||
			extension == "JPG"|| extension == "JPEG" || extension == "PNG" || extension == "BMP"		
	)){
		$(".previews").append(fallbackphotohtml);
		$("#file-upload").append(filehtml);
		$(".file").off("change",fallback_upload).on("change",$(this),fallback_upload);
		$("#upload-all").removeClass("disabled");
	}
	else{
		alert("Please choose an image file with jpeg,jpg or png extension");
	}
}

function fallback(){
	$(".file").on("change",$(this),fallback_upload);
	
}

//This is the regular image uploading function which would work on Firefox , Chrome, Opera
//and Safari newer versions

function regular(){
	$(".file").on("change",$(this),upload);
}


function upload(){
	//console.log("regular upload");
	var file = $("#file").val();
	var extension = file.split(".")
	extension = extension[extension.length-1];
	if(file!="" && (extension == "jpg"|| extension == "jpeg" || extension == "png" || extension == "bmp"||
			extension == "JPG"|| extension == "JPEG" || extension == "PNG" || extension == "BMP"		
	)){
		$(".previews").append(photohtml);
		var selection = $(".previews .photo-preview:last-child .photo-loading").show();
		$("#upload-all,#upload-file,#close-upload").addClass("disabled");
		$.ajaxFileUpload
		(
			{
				url:'ajaxapi/upload_clique.php',
				fileElementId:'file',
				dataType: 'JSON',
				success: function (data){
					data = $.parseJSON(data);
					if(data.status == "success"){
						html = "<img src = 'img/clique_img/icons/"+ data.message+"' class = 'preview-img' data-image-name='"+data.message+"'/>";
						selection.hide().parent(".photo-preview").append(html);
						$("#file-upload").trigger("reset");
						$("#file").off("change",upload).on("change",upload);
						$("#upload-all").removeClass("disabled");
						$("#upload-file,#close-upload").removeClass("disabled");
						
					}
					else{
						alert(data.message);
						selection.parent(".photo-preview").remove();
						if($(".photo-preview").length !=0){
							$("#upload-all").removeClass("disabled");
						}
						$("#upload-file,#close-upload").removeClass("disabled");
						$("#file-upload").trigger("reset");
						$("#file").off("change",upload).on("change",upload);
					}
					
				},
				error: function (data, status, e){
					alert("There was an error uploading the image.Please try again");
					selection.parent(".photo-preview").remove();
					if($(".photo-preview").length !=0){
						$("#upload-all").removeClass("disabled");
					}
					$("#upload-file,#close-upload").removeClass("disabled");
					$("#file-upload").trigger("reset");
					$("#file").off("change",upload).on("change",upload);
				}
			}
		)
		
	}
	else{
		alert("Please choose an image file with jpeg,jpg or png extension");
	}
}



$(document).ready(function(){
	
	var browser = BrowserDetect.browser;
	var version = BrowserDetect.version;
	if(browser == "Explorer" || (browser == "Safari" && version < 5)){
		fallbackmode = true;
		fallback();
	}
	else{
		fallbackmode = false;
		regular();
	}
	
	
	$("#editcliqueform").submit(function(){
		console.log("submitted");
		var newname = $("#editcliquename").val();
		if(newname === "" || null === newname){
			$("#editcliquemodal .errordiv").html("Cliquename cannot be blank").slideDown("300");
			timer();
		}
		else{
			var cliqueid = $("#cliquedetails").attr("data-cliqueid");
			var data = {"clique_id":cliqueid,"clique_name":newname};
			$.ajax({
				url:"ajaxapi/renameclique.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						$("#editcliquemodal .successdiv").html(data.message).slideDown("300");
						timer();
						setTimeout(function(){
							window.location = "";
						},"2000");
					}
					else{
						$("#editcliquemodal .errordiv").html(data.message).slideDown("300");
						timer();
					}
				},
				error:function(data){
					$("#editcliquemodal .errordiv").html("An error occurred").slideDown("300");
					timer();
				}
				
			})
		}
		return false;
	})
	
	
	$(".loadmore").click(function(){
		$(this).append("&nbsp;&nbsp;<img src = 'img/linear-loader.gif'/>");
		parent = this;
		var activity_id = $(this).parents(".main-cell").attr("data-activity-id");
		var timestamp = $(this).parents(".main-cell").find(".displaycomments tr:eq(0)").attr("data-time");
		var data = {"action":"comment","activity_id":activity_id,"type":"get","timestamp":timestamp};
		console.log(data);
		$.ajax({
			url:"ajaxapi/clique_socialactions.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					len = data.message.length;
					var html = "";
					for(var i = len-1; i >=0; i--){
						html+= "<tr class = 'commentstr' data-commentid = '" + data.message[i].commentid +
						"'> <td class = 'dp-container'> <img src = 'img/icons/" + data.message[i].user_dp_icon+
						"' class = 'small-activity-container'/> </td> <td class = 'details'>";
						if(data.message[i].del === true){
							html+= "<small class = 'pull-right'> <a href = '#' class = 'deletecomment'>x</a></small>";
						}
						html+= "<p class = 'school'><b>"  + data.message[i].user_name + "</b></p><p class = 'actualcomment'>" +
						data.message[i].user_comment + "</p></td></tr>";
					}
					$(parent).parents(".main-cell").find(".displaycomments").prepend(html);
					$(parent).hide();
				}
				else{
					showinlinemessage(parent,data.message);
					$(parent).find("img").remove();
					
				}
			},
			error:function(data){
				showinlinemessage(parent,"An error occurred");
				$(parent).find("img").remove();
			}
		});
		return false;
		
	});
	
	$(".displaycomments").each(function(){
		if($(this).find("tr").length>0){
			$(this).show().parent().find(".commentform").show();
		}
	});
	
	$(".numcomments").click(function(){
		console.log(".comments clicked");
		
		if($(this).parents(".main-cell").find(".displaycomments:visible").length == 0){
			$(this).parents(".main-cell").find(".commentform").show().find("");
		}
		return false;
	});
	
	$("body").on("mouseenter",".details",function(){
		$(this).find(".deletecomment").show();
	}).on("mouseleave",".details",function(){
		$(this).find(".deletecomment").hide();
	}).on("click",".deletecomment",function(){
		var parent = $(this).parents("tr");
		console.log(parent.eq(0).html());
		var commentid = parent.attr("data-commentid");
		var activityid = parent.parents(".main-cell").attr('data-activity-id');
		var data = {"comment_id":commentid,"action":"comment","type":"delete","activity_id":activityid}
		console.log(data);
		$.ajax({
			url:"ajaxapi/clique_socialactions.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					var numcomments = parseInt(parent.parents(".main-cell").find(".vote .comments").html());
					numcomments--;
					if(numcomments == 1){
						parent.parents(".main-cell").find(".vote .grammar").html("comment");
					}
					else if(numcomments == 0){
						parent.parents(".main-cell").find(".vote .grammar").html("comments");
					}
					parent.parents(".main-cell").find(".vote .comments").html(numcomments);
					parent.eq(0).fadeOut("200");
				}
				else{
					showinlinemessage(parent,data.message);
				}
			},
			error:function(){
				showinlinemessage(parent,"An error occurred");
			}
		});
		return false;
	});
	
	$("body").on("click",".like",function(){
//		//console.log("like clicked");
		parent = this;
		var activity_id = $(this).parents(".main-cell").attr("data-activity-id");
		var data = {"action":"like","activity_id":activity_id,"type":"post"};
		$.ajax({
			url:"ajaxapi/clique_socialactions.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					var numlikes = parseInt($(parent).parents(".vote").find(".numlikes").html());
					numlikes++;
					if(numlikes == 2){
						var html = "<span class = 'numlikes'> 2</span> likes"; 
						$(parent).parents(".vote").find(".counter").html(html);
					}
					else if(numlikes == 1){
						var html = "<span class = 'numlikes'> 1</span> like"; 
						$(parent).parents(".vote").find(".counter").html(html);
					}
					else{
						$(parent).parents(".vote").find(".numlikes").html(numlikes);
					}
					$(parent).removeClass("like").addClass("unlike").find(".likeaction").html("<i class = 'icon-heart'></i>");
					
				}
				else{
					showinlinemessage(parent,data.message);
					
				}
			},
			error:function(){
				showinlinemessage(parent,"An error occurred");
			}
		});
		return false;
	}).on("click",".unlike",function(){
//		//console.log("unlike clicked");
		parent = this;
		var activity_id = $(this).parents(".main-cell").attr("data-activity-id");
		var data = {"action":"like","activity_id":activity_id,"type":"delete"};
		$.ajax({
			url:"ajaxapi/clique_socialactions.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					var numlikes = parseInt($(parent).parents(".vote").find(".counter .numlikes").html());
					numlikes--;
					if(numlikes == 1){
						var html = "<span class = 'numlikes'> 1</span> like"; 
						$(parent).find(".counter").html(html).html();
					}
					else if(numlikes == 0){
						var html = "<span class = 'numlikes'>0</span> likes"; 
						$(parent).parents(".vote").find(".counter").html(html);
					}
					else{
						$(parent).parents(".vote").find(".numlikes").html(numlikes);
					}
					$(parent).removeClass("unlike").addClass("like").find(".likeaction").html("<i class = 'icon-heart-empty'></i>");
					
				}
				else{
					showinlinemessage(parent,data.message);
					
				}
			},
			error:function(){
				showinlinemessage(parent,"An error occurred");
			}
		});
		return false;
	})
	.on("click",".counter",function(){
		parent = this;
		
		var numlikes = parseInt($(this).find(".numlikes").html());
		if(numlikes != 0){
			blockui();
			var activity_id = $(this).parents(".main-cell").attr("data-activity-id");
			var data = {"action":"like","activity_id":activity_id,"type":"get"};
			console.log(data);
			$.ajax({
				url:"ajaxapi/clique_socialactions.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					$.unblockUI();
					if(data.status == "success"){
						var content = data.message;
						var html = "";
						for(var i = 0; i < content.length; i++){
							html += "<tr><td class = 'dp-container'><img class = 'dp-container' src = 'img/icons/"+content[i].user_dp_icon +
							"'/></td><td><h4 class = 'name'>"+content[i].user_name + "</h4></td></tr>";
						}
						$("#likemodal table").html(html).parents("#likemodal").modal("show");
					}
					else{
						showinlinemessage(parent,data.message);
					}
//					console.log(data);
					
				},
				error:function(){
					$.unblockUI();
					showinlinemessage(parent,"An error occurred");
				}
			});
		}
		return false;
	})
	
	
	
	$(".commentform").submit(function(){
		var tabledisplay = false;
		var parent = this;
		if($(parent).parents(".main-cell").find(".displaycomments:visible").length>0){
			tabledisplay = true;
		}
		
//		$(this).parents(".main-cell").find(".displaycomments").hide();
		var comment = $(this).find(".text-input").val();
		if(comment === "" || null === ""){
			return false;
		}
		var activity_id = $(this).parents(".main-cell").attr("data-activity-id");
		var data = {"action":"comment","activity_id":activity_id,"type":"post","comment":comment};
		$.ajax({
			url:"ajaxapi/clique_socialactions.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					
					var numcomments = parseInt($(parent).parents(".main-cell").find(".vote .comments").html());
					numcomments++;
					if(numcomments == 1){
						$(parent).parents(".main-cell").find(".vote .grammar").html("comment");
					}
					else if(numcomments == 2){
						$(parent).parents(".main-cell").find(".vote .grammar").html("comments");
					}
					$(parent).parents(".main-cell").find(".vote .comments").html(numcomments);
					
					var imgurl = $("#chat_dp_icon").attr("src");
					var name = $("#chat_dp_name").html();
					
					var html = "<tr data-commentid = '" + data.message + "'><td class = 'dp-container'> <img src = '" + imgurl + "' class = 'small-activity-container'/></td>"+
					"<td class = 'details'><small class = 'pull-right'><a href = '#' class = 'deletecomment'>x</a></small><p class = 'school'><b> " + name + "</b></p><p class = 'actualcomment'>"+
					comment + "</p></td></tr";
					
					$(parent).trigger("reset").parents(".main-cell").find(".displaycomments").append(html);
					
					if(tabledisplay === false){
						$(parent).parents(".main-cell").find(".displaycomments").show();
						
					}
					
				}
				else{
					showinlinemessage(parent,data.message);
				}
				
			},
			error:function(){
				showinlinemessage(parent,data.message);
			}
		})
		return false;
	});
	
	
	
	
	
	
	
	
	
	
	$("#cliqueimageadd").click(function(){
		$("#cliquefile").trigger("click");
	});
	$("#cliquefile").on('change',function(){
		var file = $("#cliquefile").val();
		var extension = file.split(".")
		extension = extension[extension.length-1];
		if(file!="" && (extension == "jpg"|| extension == "jpeg" || extension == "png" || extension == "bmp"||
				extension == "JPG"|| extension == "JPEG" || extension == "PNG" || extension == "BMP"		
		)){
			$("#image-form").trigger("submit");
		}
		else{
			alert("Please choose an image with jpg,jpeg,png or bmp extension");
			$("#image-form").trigger("reset");
		}
	});
	
	
	if($(".fancybox").length>0){
		$(".fancybox").fancybox({
			nextMethod : 'resizeIn',
	        nextSpeed  : 250,
	        preload:8,
	        prevMethod : false,
				helpers	: {
					title	: {
						type: 'inside'
					},
					thumbs	: {
						width	: 50,
						height	: 50
					}
				}
		});
	}
	
	scrolltotop();
	check_notifications();
	initialisechat();
	
	
	
	photohtml = "<div class = 'photo-preview'>" +
		"<img src = 'img/photo-upload-ajax.png' class = 'photo-loading'/>"+
	"</div>";
	(function ($, F) {
	F.transitions.resizeIn = function() {
	var previous = F.previous,
	current  = F.current,
	startPos = previous.wrap.stop(true).position(),
	endPos   = $.extend({opacity : 1}, current.pos);
	
	startPos.width  = previous.wrap.width();
	startPos.height = previous.wrap.height();
	
	previous.wrap.stop(true).trigger('onReset').remove();
	
	delete endPos.position;
	
	current.inner.hide();
	
	current.wrap.css(startPos).animate(endPos, {
	duration : current.nextSpeed,
	easing   : current.nextEasing,
	step     : F.transitions.step,
	complete : function() {            	
		F._afterZoomIn();
	    current.inner.fadeIn("fast");
	}
	});
	};
	
	}(jQuery, jQuery.fancybox));
	
	$("#photo-upload").click(function(){
		$("#maindisplay").fadeOut("200",function(){
			$("#upload-section").fadeIn("300");
		});
		
		return false;
	});
	
	$("#upload-file").click(function(){
		if(!$(this).hasClass("disabled"))
			$(".file:last").trigger("click");
		return false;
	});
	
	
	$("#close-upload").click(function(){
		if($(this).hasClass("disabled") === true){
			return false;
		}
		
		if(fallbackmode == true){
			if($(".photo-preview").length>0){
				var confirmation = confirm("This will delete all your uploaded photos! Do you want to continue?");
				if(confirmation === true){
					$("#upload-section").fadeOut("300",function(){
						$("#photo-upload,#statusform").fadeIn("200");
						$(".photo-preview").remove();
						$("#upload-all").addClass("disabled");
					});
				}
				return false;
			}
		}
		
//		//console.log("close upload clicked");
		if($(".photo-preview").length>0){
			var cliqueid = $("#cliquedetails").attr("data-cliqueid");
			var confirmation = confirm("This will delete all your uploaded photos! Do you want to continue?");
			if(confirmation === true){
				blockui();
				var data = "";
				len = $(".photo-preview").length;
				for(var i = 0; i < len; i++){
					var img = $(".photo-preview").eq(i).find(".preview-img").attr("data-image-name");
					data+="filename%5B%5D="+img+"&";
				}
				data+="type=delete&clique_id="+cliqueid;
				//console.log(data);
				$.ajax({
					url:"ajaxapi/clique_photo_action.php",
					data:data,
					type:"POST",
					dataType:"JSON",
					success:function(data){
						if(data.status == "success"){
							$.unblockUI();
							$("#upload-section").fadeOut("300",function(){
								$("#maindisplay").fadeIn("200");
								$(".photo-preview").remove();
								$("#upload-all").addClass("disabled");
							});
						}
						else{
							$.unblockUI();
							alert("unable to delete photos , please try again later");
						}
					},
					error:function(){
						$.unblockUI();
						alert("unable to delete photos , please try again later");
					}
				});
				
			}
		}
		else{
			$("#upload-section").fadeOut("300",function(){
				$("#maindisplay").fadeIn("200");
				$(".photo-preview").remove();
				$("#upload-all").addClass("disabled");
			});
		}
		return false;
	});
	
	
	$("#upload-all").click(function(){
		if($(this).hasClass("disabled") != true){
			if(fallbackmode){
				return true;
			}
			blockui();
			var data = "";
			len = $(".photo-preview").length;
			var cliqueid = $("#cliquedetails").attr('data-cliqueid');
			for(var i = 0; i < len; i++){
				var img = $(".photo-preview").eq(i).find(".preview-img").attr("data-image-name");
				data+="filename%5B%5D="+img+"&";
			}
			data+="type=post&clique_id="+cliqueid;
			//console.log(data);
			$.ajax({
				url:"ajaxapi/clique_photo_action.php",
				data:data,
				type:"POST",
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						$.unblockUI();
						blockui("upload-complete");
						setTimeout(function(){$.unblockUI();},1000);
						$(".photo-preview").remove();
						$("#close-upload").trigger("click");
						window.location = "";
					}
					else{
						$.unblockUI();
						alert(data.message);
					}
				},
				error:function(){
					$.unblockUI();
					alert("unable to post photos , please try again later");
				}
			});
			
		}
		return false;
	});
	
	
	
	
	$("#addpeople").click(function(){
		blockui();
		var cliqueid = $("#cliquedetails").attr("data-cliqueid");
		$.ajax({
			url:"ajaxapi/getfriendlist.php",
			data:{"clique_id":cliqueid},
			type:"POST",
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					var list = data.message;
					html = "";
					
					var lastindex;
					$.each(list, function(index,item){
						if(index % 6 == 0){
							html += "<div class = 'row'>";
						}
						html +=  "<div class = 'span2 friends' data-userid = '" + item.id + "'><img class = 'venue_photo' src = 'img/icons/" + item.dp + "'/>"+
						"<h5 class = 'pull-center fancybox-header'> " + item.name + "</h5>" +
						"</div>";
						if((index+1) % 6 == 0){
							html += "</div>";
						}
						lastindex = index;
					});
					if((lastindex+1)%6 != 0){
						html += "</div>";
					}
					
					var headhtml =  "<button class = 'btn btn-info pull-left' id = 'selectallnone'>Select All/None</button> "+
					"Select Friends" +
				"<button class = 'btn btn-success pull-right disabled' data-disabled = 'true' id = 'addfriendstoclique'>Add</button>"
					
					$("#venuesrow").html(html);
					$("#fancyheader").html(headhtml);
					$("#fancylaunch").trigger("click");
				}
				else{
					$("#clique_feed .errordiv").html(data.message).slideDown();
					timer();
				}
				$.unblockUI();
				
				
			},
			error:function(){
				var html = "An error occurred. Please try again later";
				$("#clique_feed .errordiv").html(html).slideDown();
				timer();
			}
		})
		return false;
	});
	
	$("#seeall").click(function(){
		blockui();
		var cliqueid = $("#cliquedetails").attr("data-cliqueid");
		var data = {"cliqueid":cliqueid,"action":"getmembers"};
		$.ajax({
			url:"ajaxapi/clique_post.php",
			data:data,
			type:"POST",
			dataType:"JSON",
			success:function(data){
				$.unblockUI();
				if(data.status == "success"){
					var list = data.message;
					html = "";
					
					var lastindex;
					$.each(list, function(index,item){
						if(index % 6 == 0){
							html += "<div class = 'row'>";
						}
						html +=  "<div class = 'span2 friends' data-userid = '" + item.id + "'><a href = 'profile.php?userid=" + item.id + "'><img class = 'venue_photo' src = 'img/icons/" + item.dp + "'/>"+
						"</a><h5 class = 'pull-center fancybox-header'> " + item.name + "</h5>" +
						"</div>";
						
						if((index+1) % 6 == 0){
							html += "</div>";
						}
						lastindex = index;
					});
					if((lastindex+1)%6 != 0){
						html += "</div>";
					}
					
					if($("#cliqueimageadd").length > 0){
						var headhtml = "<button class = 'btn btn-info pull-left' id = 'selectallnone' style = 'display:none'>Select All/None</button>" + 
						"<button class = 'btn btn-info pull-left' id = 'editclique'>Edit <i class = 'icon-edit'></i></button>"+
						"People in the Clique" +
						"<button class = 'btn btn-danger pull-right disabled' style = 'display:none' data-disabled = 'true' id = 'removefromclique'>Remove</button>";
					}
					else{
						var headhtml = "People in the Clique";
					}
					 
					
				
					
					$("#venuesrow").html(html);
					$("#fancyheader").html(headhtml);
					$("#fancylaunch").trigger("click");
				}
				else{
					
					$("#clique_feed .errordiv").html(data.message).slideDown();
					timer();
				}
			},
			error:function(){
				$.unblockUI();
				var html = "An error occurred. Please try again later";
				$("#clique_feed .errordiv").html(html).slideDown();
				timer();
			}
		});
		return false;
	});
	
	$("body").on("click","#editclique",function(){
		if($(this).hasClass("btn-info")){
			$(".friends a").addClass("nolink");
			$(this).removeClass("btn-info").html("Done <i class = 'icon-check'></i>");
			$("#selectallnone,#removefromclique").show();
		}
		else{
			$("#removefromclique").attr("data-disabled","true").addClass("disabled");
			$(".friendselected").each(function(){
				$(this).addClass("friends").removeClass("friendselected");
			});
			$(".friends a").removeClass("nolink");
			$(this).addClass("btn-info").html("Edit <i class = 'icon-edit'></i>");
			$("#selectallnone,#removefromclique").hide();
		}
		
	}).on("click",".nolink",function(){
		event.preventDefault();
	});
	
	$("#deleteclique").click(function(){
		var response = confirm("Are you sure you want to delete this clique? This action is irreversible!!");
		if(response === true){
			blockui();
			var cliqueid = $("#cliquedetails").attr("data-cliqueid");
			var data = {"clique_id":cliqueid};
			$.ajax({
				url:"ajaxapi/deleteclique.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						alert("Clique Successfully deleted");
						window.location = "dashboard.php";
					}
					else{
						alert("Unable to delete clique");
					}
				},
				error:function(){
					alert("Unable to delete clique");
				}
			});
		}
	})
	$(".memberslink").tooltip();
	$("#schoolsearch").submit(function(){
		return false;
	});
	function timer(){
		setTimeout(function(){
			$(".errordiv,.successdiv").slideUp("300");
		},"1500");
	}
	
	$("body").on("click",".checkin,.reserve",function(){
		
		$("#venueslist").block({ 
            message:"<img src = 'img/ajax-loader.gif' class = 'pull-center'/>",
            css: {"background-color":"#272B30","border-color":"#272B30"},
		});
		var venueid = $(this).parent().attr("data-venueid");
		var action;
		if($(this).hasClass("checkin")){
			action = "checkin";
		}
		else{
			action = "reserve";
		}
		var cliqueid = $("#cliquedetails").attr("data-cliqueid");
		var data = {"cliqueid":cliqueid,"action":action,"venue_id":venueid};
		console.log(data);
		$.ajax({
			url:"ajaxapi/clique_post.php",
			data:data,
			type:"POST",
			dataType:"JSON",
			success:function(data){
				$("#venueslist").unblock();
				if(data.status == "success"){
					if(action == "checkin"){
						message = "You have successfully checked in to the venue";
					}
					else{
						message = "You have successfully made a reservation at this venue";
					}
					$("#venueslist .successdiv").html(message).slideDown("300");
					timer();
				}
				else{
					$("#venueslist .errordiv").html(data.message).slideDown("300");
					timer();
				}
			},
			error:function(){
				$("#venueslist").unblock();
				message= "An error occurred"
				$("#venueslist .errordiv").html(message).slideDown("300");
			}
		})
	}).on("click","#selectallnone",function(){
		console.log("selected click");
		if($(".friends").length == 0){
			$(".friendselected").removeClass("friendselected").addClass("friends");
			$("#addfriendstoclique,#removefromclique").attr("data-disabled","true").addClass("disabled");
		}
		else{
			$(".friends").removeClass("friends").addClass("friendselected");
			$("#addfriendstoclique,#removefromclique").attr("data-disabled","false").removeClass("disabled");
		}
	}).on("click",".friends",function(){
		$(this).removeClass("friends").addClass("friendselected");
		if($(".friendselected").length > 0){
			$("#addfriendstoclique,#removefromclique").attr("data-disabled","false").removeClass("disabled");
		}
		
	}).on("click",".friendselected",function(){
		$(this).removeClass("friendselected").addClass("friends");
		if($(".friendselected").length == 0){
			$("#addfriendstoclique,#removefromclique").attr("data-disabled","true").addClass("disabled");
		}
		
	}).on("click","#removefromclique",function(){
		if($("#removefromclique").attr('data-disabled') == "true"){
			event.preventDefault();
		}
		var count = $(".friendselected").length;
		if(count == 0){
			alert("Please select at least one friend to add");
		}
		else{	
			var cliqueid = $("#cliquedetails").attr("data-cliqueid");
			userid = "";
			for(var i = 0; i < count; i++){
				var temp = $(".friendselected").eq(i).attr("data-userid");
				userid += "friends_id%5B%5D=" + temp + "&";
			}
			userid += "clique_id="+cliqueid;
			console.log(userid);
			$.ajax({
				url:"ajaxapi/deletefromclique.php",
				data:userid,
				type:"POST",
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						window.location = "";
					}
					else{
						alert(data.message);
					}
				},
				error:function(){
					alert("unable to delete friends");
				}
			});
		}
		
	}).on("click","#addfriendstoclique",function(){
		if($("#addfriendstoclique").attr('data-disabled') == "true"){
			return false;
		}
		var count = $(".friendselected").length;
		if(count == 0){
			alert("Please select at least one friend to add");
		}
		else{
			
			var cliqueid = $("#cliquedetails").attr("data-cliqueid");
			userid = "";
			for(var i = 0; i < count; i++){
				var temp = $(".friendselected").eq(i).attr("data-userid");
				userid += "friends_id%5B%5D=" + temp + "&";
			}
			userid += "clique_id="+cliqueid;
			console.log(userid);
			$.ajax({
				url:"ajaxapi/addtoclique.php",
				data:userid,
				type:"POST",
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						$("#venueslist .successdiv").html("Request sent successfully.").slideDown('300');
						timer();
						$.fancybox.close();
					}
					else{
						$("#venueslist .errordiv").html(data.message).slideDown('300');
						timer();
					}
				},
				error:function(){
					$("#venueslist .errordiv").html("Unable to add friends").slideDown('300');
					timer();
				}
			})
		}
	}).on("click","#backtovenues",function(){
		$("#fancyheader").html("Venues");
		$("#extrainfo").fadeOut("400");
		$.fancybox.update();
		$("#venuesrow").fadeIn("2000");
	}).on("click",".photos",function(){
		var venueid = $(this).parent().attr("data-venueid");
		var data = {"type":"getimages","venue_id":venueid};
		$.fancybox.showLoading();
		$.ajax({
			url:"ajaxapi/venue_photo_action.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					if(data.message == "No Images available"){
						$("#venueslist .errordiv").html("No images available").slideDown("300");
						timer();
					}
					else{
						message = data.message;
						len = message.length;
						html = "<button class = 'btn btn-info pull-left btn-mini' id = 'backtovenues'><i class = 'icon-arrow-left'></i> Back </button><br/><div id = 'photodiv'>";
						for(var i = 0; i < len; i++){
							 html += '<a class = "fancyboxgal" rel = "venuegallery" href = "img/venue_img/'+message[i].venue_photo_source +
							 '"><img class = "img-thumbnails" src = "img/venue_img/icons/'+ message[i].venue_photo_source +
								 '"/></a>' ;
							
						}
						html+="</div>"
						$("#extrainfo").html(html).
						find(".fancyboxgal")
						.fancybox({
							nextMethod : 'resizeIn',
					        nextSpeed  : 150,
					        preload:8,
					        prevMethod : false,
							helpers	: {
								title	: {
									type: 'inside'
								},
								thumbs	: {
									width	: 50,
									height	: 50
								}
							},
							afterClose:function(){
								$("#extrainfo").hide();
								$("#venuesrow").show();
								$("#fancylaunch").trigger("click");
							}
						});
						
					}
					$.fancybox.hideLoading();
					$("#venuesrow").fadeOut("200",function(){
						$.fancybox.update();
						
					})
					$("#extrainfo").fadeIn('300');
				}
				else{
					$.fancybox.hideLoading();
					$("#venueslist .errordiv").html(data.message).slideDown("300");
					timer();
				}
			},
			error:function(){
				$.fancybox.hideLoading();
				message = "<strong>An error occured!</strong> Please try again later";
				$("#venues .errordiv").html(message).slideDown("300");
			}
		})
	}).on("click",".comments",function(){
		$.fancybox.showLoading()
		var venueid = $(this).parent().attr("data-venueid");
		var data = {"venue_id":venueid,"action":"get"};
//		console.log(data);
		$.ajax({
			url:"ajaxapi/comment.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				$("#prevcomments table").html("");
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				else if(data.status == "success"){
					$.fancybox.hideLoading()
					if(data.message === "No comments available"){
						$("#venueslist .errordiv").html(data.message).slideDown("200");
						timer();
					}
					else{
						entries = data.message;
						$("#fancyheader").html("Comments");
						html = "<button class = 'btn btn-info pull-left btn-mini' id = 'backtovenues'><i class = 'icon-arrow-left'></i> Back </button><table>";
						length = entries.length;
						for(var i = 0;i < length; i++){
							html+= "<tr>" +
									"<td class = 'comment-dp'><img class = 'dp-container' src = 'img/icons/"+entries[i].dp+"'></td>" +
									"<td class = 'comment-comment'><p><a class = 'userlink' href = 'profile.php?userid="
									+entries[i].userid+"'>" + entries[i].name + "</a></p>" +
									"<p>"+entries[i].comment+"</p></td>" +
									"</tr>";
						}
						html+= "</table>";
						
						$("#venuesrow").fadeOut("200",function(){
							$.fancybox.update();
							
						})
						$("#extrainfo").html(html).fadeIn("300",function(){
							
						});
					}
					
				}
				else{
					$.fancybox.hideLoading()
					$("#venueslist .errordiv").html(data.message).slideDown("300");
					timer();
				}
			},
			error:function(){
				$("#venueslist").unblock();
				message = "<strong>An error occured!</strong> Please try again later";
				$("#venueslist .errordiv").html(message).slideDown("300");
				timer();
			}
		});
		return false;
		
	});
	
	$("#schoolsearchform").submit(function(){
		$(".schoolname:visible").eq(0).trigger("click");
		return false;
	})
	searchablelist("schoolsearch","schoolname");
	$("#fancylaunch").fancybox({
		openEffect:"fade",
		closeEffect:"close"
	});
	$("#schoollist").on("hidden",function(){
		$("#schoolsearch").val("").trigger("change");
		$(this).unblock();
	}).on("shown",function(){
		$("#schoolsearch").trigger("focus");
		if($("#schoollist .modal-body").attr("data-scroll") !== "true")
			$("#schoollist .modal-body").mCustomScrollbar().attr("data-scroll","true");
	});
	
	$(".schoolname").click(function(){
		$("#schoollist").block({ 
             message:"<img src = 'img/ajax-loader.gif' class = 'pull-center'/>",
             css: {"background-color":"#272B30","border-color":"#272B30"},
		});
		
		
		var schoolid = $(this).attr("data-schoolid");
//		console.log(schoolid)
		var ajaxdata = {"school_id":schoolid};
		$.ajax({
			url:"ajaxapi/getvenues.php",
			type:"POST",
			data:ajaxdata,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					venues = data.message;
					var html = "";
					$.each(venues,function(index,list){
						if(index % 4 == 0){
							html += "<div class = 'row'>";
						}
						html +=  "<div class = 'span3' data-venueid = '" + list.id + "'><h4 class = 'pull-center fancybox-header'> " + list.name + "</h3>"+
							"<img class = 'venue_photo' src = 'venue_cover.php?venue_id="+list.id+"&type=rectangle'/> " +
						"<button class = 'checkin btn btn-small btn-block'>Checkin</button>" +
						"<button class = 'reserve btn btn-small btn-block'>Reserve</button>"+
						"<button class = 'photos btn btn-small btn-info btn-block'>Photos</button>"+
						"<button class = 'comments btn btn-small btn-info btn-block'>Comments</button>"+
						"</div>";
						if((index+1) % 4 == 0){
							html += "</div>";
						}
					});
					$("#fancyheader").html("Venues");
					$("#venuesrow").html(html);
					$("#fancylaunch").trigger("click");
					$("#schoollist").unblock();
				}
				else{
					$("#schoollist").unblock().find(".errordiv").html(data.message).slideDown();
					timer();
					
				}
				
			},
			error:function(){
				
			}
		})
		return false;
	});
	
	$("#statusform").submit(function(){
		var status = trimfield($("#statusupdateinput").val());
		if(status == ""){
			$("#clique_feed .errordiv").html("Please enter text in the box, before posting").slideDown("300");
			timer();
		}
		else{
			var cliqueid = $("#cliquedetails").attr("data-cliqueid");
			var data = {"cliqueid":cliqueid,"action":"simplepost","cliquepost":status};
			console.log(data);
			$.ajax({
				url:"ajaxapi/clique_post.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						window.location = "";
					}
					else{
						$("#clique_feed .errordiv").html(data.message).slideDown();
						timer();
					}
				},
				error:function(){
					html = "An error occurred";
					$("#clique_feed .errordiv").html(html).slideDown();
					timer();
				}
			});
		}
		return false;
	})
});


