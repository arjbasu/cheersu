function blockui(){
	$.blockUI({ message: '<img src="img/ajax-loader.gif"/>' ,
		themedCSS: { 
	        width:  '30px', 
	        top:    '40%', 
	        left:   '35%' 
	    }, 
		css: { 
            border: 'none', 
            padding: '5px', 
            backgroundColor: '#272B30', 
            '-webkit-border-radius': '0px', 
            '-moz-border-radius': '0px',
            'border-radius':'0px',
            opacity: .5, 
            color: '#fff' 
        }
		});
}

$(document).ready(function(){
	$(".modal").on("hidden",function(){
		$(this).find(".errordiv,.successdiv,.loading").hide();
	})
	
	$("#myCarousel").carousel();
	$("select").height("25px");
	
	$("#supportform").submit(function(){
		validation = true
		$(this).find("input,textarea").each(function(){
			if($(this).val() == ""){
				validation = false;
				return false;
			}
		});
		if(validation == false){
			alert("Please Complete all of the fields");
			return false;
		}
	});
	$("#forgotpassform").submit(function(){
		$(".loading,.successdiv,.errordiv").slideUp("100");
		validation = true;
		if($(this).find("input").val() == ""){
			validation = false;
		}
		if(validation == false){
			alert("Please input your email");
		}
		else{
			$("#forgotpassmodal #forgotpassform").slideUp("200",function(){
				$(this).parent().find(".loading").fadeIn("100");
			})
			data = $(this).serialize();
			$.ajax({
				url:"requestresetpassword.php",
				data:data,
				type:"POST",
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						$("#forgotpassmodal .loading").fadeOut("100",function(){
							$("#forgotpassmodal .successdiv").html(data.message).slideDown();
						})
					}
					else{
						$("#forgotpassmodal .loading").fadeOut("100",function(){
							$("#forgotpassmodal .errordiv").html(data.message).slideDown();
						})
					}
				},
				error:function(){
					$("#forgotpassmodal .loading").fadeOut("100",function(){
						$("#forgotpassmodal .errordiv").html("An error occurred")
						$("#forgotpassmodal .errordiv,#forgotpassform").slideDown();
					})
				}
			});
			
		}
		return false;
	})
	
	$("#loginform").submit(function(){
		var validation = true;
		
		if($("#loginemail").val() == "" || $("#loginpassword").val() ==  ""){
			validation = false;
		}
		
		if(validation == false){
			alert("You have to complete all fields before submission");
			return false;
			
		}
		
		else{
			blockui();
			data = $(this).serialize();
			$.ajax({
				url:"login.php",
				data:data,
				type:"POST",
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						window.location = "staging.php";
					}
					else{
						$.unblockUI();
						$("#statusmodal").find(".errordiv").html(data.message).show().parents(".modal").modal("show");
					}
				},
				error:function(){
					$.unblockUI();
					$("#statusmodal").find(".errordiv").html("An error occurred. Unable to login").show().parents(".modal").modal("show");
				}
			});
			
		}
		
		return false;
		});
	$("#signupform").submit(function(){
		var validation = true;
		$(this).find("input,selector").each(function(){
			if($(this).val() == ""|| $(this).val() == "NULL"){
				validation = false;
				return;
			}
		});
		
		if(validation == false){
			alert("You have to complete all fields before submission");
			
		}
		else{
			blockui();
			data = $(this).serialize();
			$.ajax({
				url:"signup.php",
				data:data,
				type:"POST",
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						$.unblockUI();
						$("#signupform").trigger("reset");
						$("#statusmodal").find(".successdiv").html(data.message).show().parents(".modal").modal("show");
					}
					else{
						$.unblockUI();
						$("#statusmodal").find(".errordiv").html(data.message).show().parents(".modal").modal("show");
					}
				},
				error:function(){
					$.unblockUI();
					$("#statusmodal").find(".errordiv").html("An error occurred").show().parents(".modal").modal("show");
				}
			});
			return false;
		}
		return false;
	});
});

$("#forgotpassmodal").on("hidden",function(){
	$(".loading,.errordiv,.successdiv").hide();
})