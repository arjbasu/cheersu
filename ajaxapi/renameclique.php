<?php
session_start();
include 'authentication_ajax_api.php';
include '../connect.php';
$userid = $_SESSION['user_id'];
if(isset($_POST['clique_name']) && isset($_POST['clique_id'])){
	$cliquename = $_POST['clique_name'];
	$cliqueid = $_POST['clique_id'];
	$query = "UPDATE cheersu_cliques SET clique_name = ? WHERE clique_id = ?";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($cliquename,$cliqueid));
	$status = "success";
	$message = "Clique name successfully edited";
}
else{
	$status = "error";
	$message = "Improper parameters passed";
}
include 'json_encoding.php';
?>