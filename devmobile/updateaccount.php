<?php
	session_start();
	if(isset($_SESSION['email_pending'])){
		header("Location:staging.php");
	}
	include 'twiginit.php';
	include 'check_authorization.php';
// 	echo "Start";
	if((isset($_POST['firstname']) && isset($_POST['lastname']) && isset($_POST['bio']) && isset($_POST['email']) 
			&& isset($_POST['school']))){
		include 'connect.php';
// 		echo "Got all params correct";
		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
 		$email = $_POST['email'];
		$bio = $_POST['bio'];
		$userid = $_SESSION['user_id'];
		$school = $_POST['school'];
// 		if(isset($_FILES["file"])){
		
// 			if ($_FILES["file"]["error"] > 0)
// 			{
// 				$filename = "";
// 			}
// 			else
// 			{
// 				if (file_exists("img/" . $_FILES["file"]["name"]))
// 				{
						
// 				}
// 				else
// 				{
// 					move_uploaded_file($_FILES["file"]["tmp_name"],
// 							"img/" . $_FILES["file"]["name"]);
// 					//echo "Stored in: " . "images/" . $_FILES["file"]["name"];
// 					$filename = $_FILES['file']['name'];
// 				}
// 			}
// 		}
// 		else{
// 			$filename = "";
// 		}
		$filename = "";
		$query = "SELECT * FROM cheersu_users WHERE user_id = '$userid'";
// 		echo $userid;
		$result = mysql_query($query);
		if(!$result){
			die("Unable to interact with database");
		}
		else{
			$temp = mysql_fetch_assoc($result);
			$emailold = $temp['user_email'];
			
			if($filename == ""){
				$filename = $temp['user_dp'];
			}
// 			echo $emailold;
		}
  		//echo $filename." $email $firstname $bio $emailold";
		if($emailold == $email){
// 			echo "No change";
			$query = "UPDATE cheersu_users SET user_firstname = ?,user_lastname=?, user_bio = ?, user_dp = ?,user_schoolid = ? WHERE user_id = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($firstname,$lastname,$bio,$filename,$school,$userid));
			$_SESSION['username'] = $firstname;
			header("Location:settings.php");
		}
		else{
// 			echo "Change";
			$query = "SELECT * FROM cheersu_users WHERE user_email = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($email));
			if($stmt->rowCount() == 0){
// 				$str = "$email$name";
// 				$hash = md5($str);
// 				$query = "UPDATE cheersu_users SET user_email_pending = ?, user_firstname = ?,user_lastname=?, user_bio = ?, user_dp = ?,user_schoolid=? user_verification_id = ? WHERE user_id = ?";
// 				$stmt = $pdo->prepare($query);
// 				$stmt->execute(array($email,$firstname,$lastname,$bio,$filename,$school,$hash,$userid));
// 				$subject = "[cheersu] Verification of new email id for $firstname";
// 				$message = "You changed your email id recently\n".
// 						"Please click on the link below to verify your account, you won't be able\n".
// 						"to use this email id:\n".
// 						"http://cheersu.com/verifyemailchange.php?userid=".urlencode($userid)."&hash=$hash\n";
// 				$from = "no-reply@cheersu.com";
// 				$headers = "From: Cheersu <$from> ";
// 				mail($email, $subject, $message,$headers);
				$query = "UPDATE cheersu_users SET user_email = ?, user_firstname = ?,user_lastname=?, user_bio = ?, user_dp = ? WHERE user_id = ?";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($email,$firstname,$lastname,$bio,$filename,$userid));
				$_SESSION['username'] = $firstname;
				header("Location:settings.php");
			}
			else{
				header("Location:settings.php");
			}
		}
	}
	else{
		header("Location:settings.php");
	}
?>