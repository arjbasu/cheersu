console.log("v2.2");
function showinlinemessage(parent,message){
	//console.log(parent);
	$(parent).parents(".news").find(".message").html(message).show();
	setTimeout(function(parent){
		$(parent).parents(".news").find(".message").fadeOut("200");
	},"1000",parent);
}

function timer(){
	setTimeout(function(){
		$(".errordiv,.successdiv").slideUp('200');
	},"1500");
}

fallbackmode = false;

//This is the fallback image uploading function for browsers which don't
//support the ajaxfileupload plugin like IE (even 10 !) and older versions of Safari.
function fallback(){
	$(".file").on("change",$(this),fallback_upload);
	
}

//This is the regular image uploading function which would work on Firefox , Chrome, Opera
//and Safari newer versions

function regular(){
	$(".file").on("change",$(this),upload);
}

function blockui(message){
	if(message === undefined){
		message = '<img src="img/ajax-loader.gif"/>';
	}
	else if(message == "upload-complete"){
		message = "<h4 class = 'notification-text'>All photos uploaded successfully</h4>";
	}
	$.blockUI({ message: message ,
		themedCSS: { 
	        width:  '30px', 
	        top:    '40%', 
	        left:   '35%' 
	    }, 
		css: { 
            border: 'none', 
            padding: '5px', 
            backgroundColor: '#272B30', 
            '-webkit-border-radius': '0px', 
            '-moz-border-radius': '0px',
            'border-radius':'0px',
            opacity: .5, 
            color: 'white' 
        }
		});
}

function fallback_upload(){
	var element = $(".file:last");
	console.log("element");
	console.log("fallback called");
	var file = element.val();
	var extension = file.split(".");
	extension = extension[extension.length-1];
	if(file!="" && (extension == "jpg"|| extension == "jpeg" || extension == "png" || extension == "bmp"||
			extension == "JPG"|| extension == "JPEG" || extension == "PNG" || extension == "BMP"		
	)){
		$(".previews").append(fallbackphotohtml);
		$("#file-upload").append(filehtml);
		$(".file").off("change",fallback_upload).on("change",$(this),fallback_upload);
		$("#upload-all").removeClass("disabled");
	}
	else{
		alert("Please choose an image file with jpeg,jpg or png extension");
	}
}
function upload(){
	//console.log("change");
	var file = $("#file").val();
	var extension = file.split(".");
	extension = extension[extension.length-1];
	if(file!="" && (extension == "jpg"|| extension == "jpeg" || extension == "png" || extension == "bmp"||
			extension == "JPG"|| extension == "JPEG" || extension == "PNG" || extension == "BMP"		
	)){
		$(".previews").append(photohtml);
		var selection = $(".previews .photo-preview:last-child .photo-loading").show();
		$("#upload-all,#upload-file,#close-upload").addClass("disabled");
		$.ajaxFileUpload
		(
			{
				url:'upload.php',
				fileElementId:'file',
				dataType: 'JSON',
				success: function (data){
					data = $.parseJSON(data);
					if(data.status == "success"){
						html = "<img src = 'img/wall_img/icons/"+ data.message+"' class = 'preview-img' data-image-name='"+data.message+"'/>";
						selection.hide().parent(".photo-preview").append(html);
						$("#file-upload").trigger("reset");
						$("#file").off("change",upload).on("change",upload);
						$("#upload-all").removeClass("disabled");
						$("#upload-file,#close-upload").removeClass("disabled");
						
					}
					else{
						alert(data.message);
						selection.parent(".photo-preview").remove();
						if($(".photo-preview").length !=0){
							$("#upload-all").removeClass("disabled");
						}
						$("#upload-file,#close-upload").removeClass("disabled");
						$("#file-upload").trigger("reset");
						$("#file").off("change",upload).on("change",upload);
					}
					
				},
				error: function (data, status, e){
					alert("There was an error uploading the image.Please try again");
					selection.parent(".photo-preview").remove();
					if($(".photo-preview").length !=0){
						$("#upload-all").removeClass("disabled");
					}
					$("#upload-file,#close-upload").removeClass("disabled");
					$("#file-upload").trigger("reset");
					$("#file").off("change",upload).on("change",upload);
				}
			}
		)
		
	}
	else{
		alert("Please choose an image file with jpeg,jpg or png extension");
	}
}
var lock = 0;
var filehtml = "<input type = 'file' class = 'file' name = 'file[]'/>"
fallbackphotohtml = "<div class = 'photo-preview'>" +
"<img src = 'img/imguploaderplaceholder.gif' class = 'preview-img'/>"+
"</div>";
photohtml = "<div class = 'photo-preview'>" +
				"<img src = 'img/photo-upload-ajax.png' class = 'photo-loading'/>"+
			"</div>";
(function ($, F) {
    F.transitions.resizeIn = function() {
        var previous = F.previous,
            current  = F.current,
            startPos = previous.wrap.stop(true).position(),
            endPos   = $.extend({opacity : 1}, current.pos);

        startPos.width  = previous.wrap.width();
        startPos.height = previous.wrap.height();

        previous.wrap.stop(true).trigger('onReset').remove();

        delete endPos.position;

        current.inner.hide();

        current.wrap.css(startPos).animate(endPos, {
            duration : current.nextSpeed,
            easing   : current.nextEasing,
            step     : F.transitions.step,
            complete : function() {            	
            	F._afterZoomIn();
                current.inner.fadeIn("fast");
            }
        });
    };

}(jQuery, jQuery.fancybox));
suggestionmode = false;
$(document).ready(function(){
	
	var browser = BrowserDetect.browser;
	var version = BrowserDetect.version;
	if(browser == "Explorer" || (browser == "Safari" && version < 5)){
		fallbackmode = true;
		fallback();
	}
	else{
		fallbackmode = false;
		regular();
	}
	
	/*Caching Frequently used selectors*/
		var feedtable = $("#feedtable");
		var displaycommentstable = feedtable.find(".displaycomments");
		var news = feedtable.find(".news");
	/*End of cached section */
	
	$(".clique").tooltip();
	
	$(".loadmore",news).click(function(){
		$(this).append("&nbsp;&nbsp;<img src = 'img/linear-loader.gif'/>");
		parent = this;
		var activity_id = $(this).parents(".news").attr("data-activity-id");
		var timestamp = $(this).parents(".news").find(".displaycomments tr:eq(0)").attr("data-time");
		var data = {"action":"comment","activity_id":activity_id,"type":"get","timestamp":timestamp};
		console.log(data);
		$.ajax({
			url:"ajaxapi/socialactions.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					len = data.message.length;
					var html = "";
					for(var i = len-1; i >=0; i--){
						html+= "<tr data-commentid = '" + data.message[i].commentid +
							"'> <td class = 'dp-container'> <img src = 'img/icons/" + data.message[i].user_dp_icon+
						"' class = 'small-activity-container'/> </td> <td class = 'details'>";
						if(data.message[i].del === true){
							html+= "<small class = 'pull-right'> <a href = '#' class = 'deletecomment'>x</a></small>";
						}
						html+= "<p class = 'school'><b>"  + data.message[i].user_name + "</b></p><p class = 'actualcomment'>" +
						data.message[i].user_comment + "</p></td></tr>";
					}
					$(parent).parents(".news").find(".displaycomments").prepend(html);
					$(parent).hide();
				}
				else{
					showinlinemessage(parent,data.message);
					$(parent).find("img").remove();
					
				}
			},
			error:function(data){
				showinlinemessage(parent,"An error occurred");
				$(parent).find("img").remove();
			}
		});
		return false;
		
	});
	
	$(".displaycomments").each(function(){
		if($(this).find("tr").length>0){
			$(this).show().parent().find(".commentform").show();
		}
	});
	initialisechat();
	scrolltotop();
	$("#statusform").submit(function(){
		if(trimfield($("#statusupdateinput").val()) == "" || $("#taglist:visible").length>0)
			return false;
		else{
			if($(".taggedbtn:visible").length > 0){
				taghtml = '[';
				$(".taggedbtn").each(function(){
					taghtml += '{"name":"' +   $(this).find(".taggedbtn-name").html() + '","id":"'+ $(this).attr("data-user-id") + '"},';
				});
				taghtml  = taghtml.slice(0,-1) + ']';
				console.log(taghtml);
				$("#taggedpeople").val(taghtml);
			}
		}
	});
	
	$("#statusupdateinput").on("keyup",function(e){
		if(e.keyCode == 13){							//13 is keyCode for the enter key
			if($(".tagfocused").length>0){
				$(".tagfocused").trigger("click");
			}
		}
		else if(e.keyCode == 40){							//40 is keycode for arrow down key
			if($(".tagfocused").length == 0){
				$(".tagsuggestionname:visible").eq(0).addClass("tagfocused");
			}
			else{
				
				var visibleElements = $(".tagsuggestionname:visible")
				visibleLength = visibleElements.length;
				var index = visibleElements.index($(".tagfocused"));
				if(index != visibleLength - 1){
//					console.log("not length");
					index++;
				}
				else{
					index = 0;
				}
				$(".tagsuggestionname:visible").removeClass("tagfocused").eq(index).addClass("tagfocused");
			}
		}
		else if(e.keyCode == 38){						//38 is keycode for arrow up key
			if($(".tagfocused").length == 0){
				$(".tagsuggestionname:visible").last().addClass("tagfocused");
			}
			else{
				var visibleElements = $(".tagsuggestionname:visible");
				visibleLength = visibleElements.length;
				var index = visibleElements.index($(".tagfocused"));
				if(index == 0){
//					console.log("not length");
					index = visibleLength - 1;
				}
				else{
					index--;
				}
				$(".tagsuggestionname:visible").removeClass("tagfocused").eq(index).addClass("tagfocused");
			}
			
		}
		else{
			var text = $(this).val();
			if((pos = text.search("@"))!=-1){
				if($("#taglist:visible").length>0){
//					console.log("taglist visible")
					var initial = text.substr(pos+1).toLowerCase();
					$(".tagsuggestionname").each(function(){
						var name = $(this).html().toLowerCase();
//						console.log("name:"+initial);
						if(name.search(initial) == -1)
							$(this).hide();
						else
							$(this).show();
					});
				}
				
				var width = $("#statusupdateinput").width();
				var top = $("#statusupdateinput").position().top + 35;
				$("#taglist").width(width+13).css("top",top).show();
				
			}
			else{
				$("#taglist").hide().find(".tagsuggestionname").show().removeClass("tagfocused");
			}
		}
	})
//	$("#statusupdateinput,.taggedbtn").focus(function(){
//		$("#photo-upload").hide();
//	})
//	.blur(function(){
//		$("#photo-upload").show();
//	});
	
	$("body").on("mouseover",".taggedbtn",function(){
		$(this).removeClass("btn-warning").addClass("btn-danger").find(".tagremove").show();
	})
	.on("mouseout",".taggedbtn",function(){
		$(this).removeClass("btn-danger").addClass("btn-warning").find(".tagremove").hide();
	})
	.on("click",".taggedbtn",function(){
		var userid = $(this).attr("data-user-id");
		$("#tagsuggestion"+userid).removeClass("hiddensuggestion").addClass("tagsuggestionname");
		$(this).remove();
		if($(".taggedbtn").length == 0){
			$(".tagdetails").hide();
		}
		$("#statusupdateinput").focus();
		
	})
	news.on("click",".like",function(){
//		//console.log("like clicked");
		parent = this;
		var activity_id = $(this).parents(".news").attr("data-activity-id");
		var data = {"action":"like","activity_id":activity_id,"type":"post"};
		$.ajax({
			url:"ajaxapi/socialactions.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					var numlikes = parseInt($(parent).parents(".vote").find(".counter .numlikes").html());
					numlikes++;
					
					if(numlikes == 2){
						var html = "<span class = 'numlikes'> 2</span> likes"; 
						$(parent).find(".counter").html(html).html();
					}
					else if(numlikes == 1){
						var html = "<span class = 'numlikes'> 1</span> like"; 
						$(parent).parents(".vote").find(".counter").html(html);
					}
					else{
						$(parent).parents(".vote").find(".numlikes").html(numlikes);
					}
					$(parent).removeClass("like").addClass("unlike").find(".likeaction").html("<i class = 'icon-heart'></i>");
//					$(parent).find('.likeaction').hide().parent().find('.counter').show();
				}
				else{
					showinlinemessage(parent,data.message);
					
				}
			},
			error:function(){
				showinlinemessage(parent,"An error occurred");
			}
		});
		return false;
	}).on("click",".unlike",function(){
//		//console.log("unlike clicked");
		parent = this;
		var activity_id = $(this).parents(".news").attr("data-activity-id");
		var data = {"action":"like","activity_id":activity_id,"type":"delete"};
		$.ajax({
			url:"ajaxapi/socialactions.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					var numlikes = parseInt($(parent).parents(".vote").find(".counter .numlikes").html());
					numlikes--;
					if(numlikes == 1){
						var html = "<span class = 'numlikes'> 1</span> like"; 
						$(parent).find(".counter").html(html).html();
					}
					else if(numlikes == 0){
						var html = "<span class = 'numlikes'>0</span> likes"; 
						$(parent).parents(".vote").find(".counter").html(html);
					}
					else{
						$(parent).parents(".vote").find(".numlikes").html(numlikes);
					}
					$(parent).removeClass("unlike").addClass("like").find(".likeaction").html("<i class = 'icon-heart-empty'></i>");
//					$(parent).find('.likeaction').hide().parent().find('.counter').show();
				}
				else{
					showinlinemessage(parent,data.message);
					
				}
			},
			error:function(){
				showinlinemessage(parent,"An error occurred");
			}
		});
		return false;
	})
	.on("click",".counter",function(){
		parent = this;
		
		var numlikes = parseInt($(this).find(".numlikes").html());
		if(numlikes != 0){
			blockui();
			var activity_id = $(this).parents(".news").attr("data-activity-id");
			var data = {"action":"like","activity_id":activity_id,"type":"get"};
			console.log(data);
			$.ajax({
				url:"ajaxapi/socialactions.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					$.unblockUI();
					if(data.status == "success"){
						var content = data.message;
						var html = "";
						for(var i = 0; i < content.length; i++){
							html += "<tr><td class = 'dp-container'><img class = 'dp-container' src = 'img/icons/"+content[i].user_dp_icon +
							"'/></td><td><h4 class = 'name'>"+content[i].user_name + "</h4></td></tr>";
						}
						$("#likemodal table").html(html).parents("#likemodal").modal("show");
					}
					else{
						showinlinemessage(parent,data.message);
					}
//					console.log(data);
					
				},
				error:function(){
					$.unblockUI();
					showinlinemessage(parent,"An error occurred");
				}
			});
		}
		return false;
	})
//	.on("mouseenter",".like,.unlike",function(){
//		$(this).find(".counter").hide().parent().find(".likeaction").show();
//	})
//	.on("mouseleave",".like,.unlike",function(){
//		$(this).find(".counter").show().parent().find(".likeaction").hide();
//	})
	displaycommentstable.on("mouseenter",".details",function(){
		$(this).find(".deletecomment").show();
	}).on("mouseleave",".details",function(){
		$(this).find(".deletecomment").hide();
	}).on("click",".deletecomment",function(){
		var parent = $(this).parents("tr");
		var commentid = parent.attr("data-commentid");
		var activityid = parent.parents(".news").attr('data-activity-id');
		var data = {"comment_id":commentid,"action":"comment","type":"delete","activity_id":activityid}
		console.log(data);
		$.ajax({
			url:"ajaxapi/socialactions.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					$(parent).eq(0).fadeOut("200");
					var numcomments = parseInt(parent.parents(".news").find(".vote .comments").html());
					numcomments--;
					if(numcomments == 1){
						$(parent).parents(".news").find(".vote .grammar").html("comment");
					}
					else if(numcomments == 0){
						$(parent).parents(".news").find(".vote .grammar").html("comments");
					}
					parent.parents(".news").find(".vote .comments").html(numcomments);
				}
				else{
					showinlinemessage(parent,data.message);
				}
			},
			error:function(){
				showinlinemessage(parent,"An error occurred");
			}
		});
		return false;
	});
	
	$(".tagsuggestionname").click(function(){
		console.log("clickd:"+this.id);
		var name = $(this).html();
		var userid = $(this).attr("data-user-id");
		var btnhtml = "<button class = 'btn btn-mini btn-warning taggedbtn' data-user-id = '" + userid + "'>"+
						"<span class = 'taggedbtn-name'>" +name + "</span>"+
						"<span class = 'tagremove'> X " +
					  "</button>";
		var text = $("#statusupdateinput").val();
		var pos = text.search("@");
		text = text.substr(0,pos);
		
		$(".tagdetails").append(btnhtml).show();
		$("#taglist").hide();
		$(".tagsuggestionname").show().removeClass("tagfocused");
		$(this).hide().removeClass("tagsuggestionname").addClass("hiddensuggestion");
		$("#statusupdateinput").val(text).focus();

		return false;
	});

	$(".numcomments").click(function(){
		console.log(".comments clicked");
		
		if($(this).parents(".news").find(".displaycomments:visible").length == 0){
			$(this).parents(".news").find(".commentform").show();
		}
		return false;
	});
	
	
//	$(".like-tooltip").click(function(){
//		var numlikes = parseInt($(parent).parents(".news").find(".votes-stats .likes").html());
//		if(numlikes > 0){
//			
//			parent = this;
//			var activity_id = $(this).parents(".news").attr("data-activity-id");
//			var data = {"action":"like","activity_id":activity_id,"type":"get"};
//			$.ajax({
//				url:"ajaxapi/socialactions.php",
//				type:"POST",
//				data:data,
//				dataType:"JSON",
//				success:function(data){
//					if(data.status == "success"){
//						len = data.message.length;
//						var html = "";
//						for(var i = 0; i < len; i++){
//							html+= data.message[i].user_name + "<br/>";
//						}
//						$(parent).tooltip({
//							title:html,
//							animation:true,
//							placement:"right",
//						}).tooltip("show");
//						setTimeout(function(parent){
//							$(parent).tooltip("destroy");
//						},"2000",parent)
//					}
//				},
//				error:function(data){
//					
//				}
//			});
//		}
//		
//		return false;
//	});
	$(".comment").click(function(){
		$(this).parents(".news").find(".commentform").show().find(".text-input").focus();
		return false;
	});
	
	$(".commentform").submit(function(){
		var tabledisplay = false;
		var parent = this;
		if($(parent).parents(".news").find(".displaycomments:visible").length>0){
			tabledisplay = true;
		}
		
//		$(this).parents(".news").find(".displaycomments").hide();
		var comment = $(this).find(".text-input").val();
		if(comment === "" || null === ""){
			return false;
		}
		var activity_id = $(this).parents(".news").attr("data-activity-id");
		var data = {"action":"comment","activity_id":activity_id,"type":"post","comment":comment};
		$.ajax({
			url:"ajaxapi/socialactions.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					
					var numcomments = parseInt($(parent).parents(".news").find(".vote .comments").html());
					numcomments++;
					if(numcomments == 1){
						$(parent).parents(".news").find(".vote .grammar").html("comment");
					}
					else if(numcomments == 2){
						$(parent).parents(".news").find(".vote .grammar").html("comments");
					}
					$(parent).parents(".news").find(".vote .comments").html(numcomments);
					
					var imgurl = $("#personal_dp img").attr("src");
					var name = $("#personal_name h3").html();
					
					var html = "<tr data-commentid = '" + data.message + "'> <td class = 'dp-container'> <img src = '" + imgurl + "' class = 'small-activity-container'/></td>"+
					"<td class = 'details'><small class = 'pull-right'><a href = '#' class = 'deletecomment'>x</a></small>" +
					"<p class = 'school'><b> " + name + "</b></p><p class = 'actualcomment'>"+
					comment + "</p></td></tr";
					
					$(parent).trigger("reset").parents(".news").find(".displaycomments").append(html);
					
					if(tabledisplay === false){
						$(parent).parents(".news").find(".displaycomments").show();
						
					}
					
				}
				else{
					showinlinemessage(parent,data.message);
				}
				
			},
			error:function(){
				showinlinemessage(parent,data.message);
			}
		})
		return false;
	});
	$(".hide-comment-form").click(function(){
		$(this).parent().hide().find(".text-input").val("").parents(".news").find(".displaycomments").hide();
		return false;
	});
	if($(".fancybox").length>0){
		$(".fancybox").fancybox({
			nextMethod : 'resizeIn',
	        nextSpeed  : 250,
	        preload:8,
	        prevMethod : false,
				helpers	: {
					title	: {
						type: 'inside'
					},
					thumbs	: {
						width	: 50,
						height	: 50
					}
				}
		});
	}
	
	$("#photo-upload").click(function(){
		$("#photo-upload,#statusform").fadeOut("200",function(){
			$("#upload-section").fadeIn("300");
		});
		
		return false;
	});
	$("#upload-file").click(function(){
		if(!$(this).hasClass("disabled"))
			$(".file:last").trigger("click");
		return false;
	});
	$("#upload-all").click(function(){
		if($(this).hasClass("disabled") != true){
			if(fallbackmode){
				return true;
			}
			blockui();
			var data = "";
			len = $(".photo-preview").length;
			for(var i = 0; i < len; i++){
				var img = $(".photo-preview").eq(i).find(".preview-img").attr("data-image-name");
				data+="filename%5B%5D="+img+"&";
			}
			data+="action=post";
			//console.log(data);
			$.ajax({
				url:"ajaxapi/photo-action.php",
				data:data,
				type:"POST",
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						$.unblockUI();
						blockui("upload-complete");
						setTimeout(function(){$.unblockUI();},1000);
						$(".photo-preview").remove();
						$("#close-upload").trigger("click");
						window.location = "";
					}
					else{
						$.unblockUI();
						alert(data.message);
					}
				},
				error:function(){
					$.unblockUI();
					alert("unable to post photos , please try again later");
				}
			});
			
		}
		return false;
	});
	$("#close-upload").click(function(){
		if($(this).hasClass("disabled") === true){
			return false;
		}
		if(fallbackmode == true){
			if($(".photo-preview").length>0){
				var confirmation = confirm("This will delete all your uploaded photos! Do you want to continue?");
				if(confirmation === true){
					$("#upload-section").fadeOut("300",function(){
						$("#photo-upload,#statusform").fadeIn("200");
						$(".photo-preview").remove();
						$("#upload-all").addClass("disabled");
					});
				}
				return false;
			}
		}
//		//console.log("close upload clicked");
		if($(".photo-preview").length>0){
			var confirmation = confirm("This will delete all your uploaded photos! Do you want to continue?");
			if(confirmation === true){
				blockui();
				var data = "";
				len = $(".photo-preview").length;
				for(var i = 0; i < len; i++){
					var img = $(".photo-preview").eq(i).find(".preview-img").attr("data-image-name");
					data+="filename%5B%5D="+img+"&";
				}
				data+="action=delete";
				//console.log(data);
				$.ajax({
					url:"ajaxapi/photo-action.php",
					data:data,
					type:"POST",
					dataType:"JSON",
					success:function(data){
						if(data.status == "success"){
							$.unblockUI();
							$("#upload-section").fadeOut("300",function(){
								$("#photo-upload,#statusform").fadeIn("200");
								$(".photo-preview").remove();
								$("#upload-all").addClass("disabled");
							});
						}
						else{
							$.unblockUI();
							alert("unable to delete photos , please try again later");
						}
					},
					error:function(){
						$.unblockUI();
						alert("unable to delete photos , please try again later");
					}
				});
				
			}
		}
		else{
			$("#upload-section").fadeOut("300",function(){
				$("#photo-upload,#statusform").fadeIn("200");
				$(".photo-preview").remove();
				$("#upload-all").addClass("disabled");
			});
		}
		return false;
	})
	var height = parseInt($("#personal").height());
	$("#personal").height(height-70);
	check_notifications();
	
	$(".closesuggestion").click(function(){
		//console.log("lock:"+lock);
		if(lock == 0){
			var userid = $(this).attr('data-userid');
			$.ajax({
				url:"ajaxapi/hidesuggestion.php",
				type:"POST",
				data:{"user_id":userid},
				dataType:"POST",
				success:function(data){
					if(data.status != "success"){
						$("#feedsection .errorsection").message(data.message).slideDown("200");
					}
				},
				error:function(data){
					html = "unable to remove suggestion";
					$("#feedsection .errorsection").message(html).slideDown("200");
					timer();
				}
			});
			lock = 1;
			$(this).parents("tr").removeClass("suggestion").addClass("dismissedSuggestion")
			.fadeOut("200",function(){
				if($(".suggestion:hidden").length <= 0 && $(".suggestion").length <= 0){
					html = '<div class = "alert alert-error pull-center">'+
					'No more suggestions' +
					'</div>';
					$("#suggestions").html(html);
					lock = 0;
				}
				else{
					$(".suggestion:hidden").eq(0).removeClass("hidden").show();
					lock = 0;
				}
				
			});
		}
		return false;
	});
	$("#popular_list .popular_venue").hover(function(){
		$(this).find(".venue_actions").css("visibility","visible");
	},
	function(){
		$(this).find(".venue_actions").css("visibility","hidden");
	});
	
	function hidevenuemessages(parent,html,div){
		$.unblockUI();
		$(parent).parents("#popular_list").find(div).html(html).slideDown("300");
		setTimeout(function(parent){
			$(parent).parents("#popular_list").find(div).fadeOut("200");
		},"1500",parent)
	}
	
	$(".venue_checkin").click(function(){
		blockui();
		var venueid = $(this).parents(".popular_venue").attr("data-venue-id");
		var data = {"venue_id":venueid,"action":"set"};
		var parent = this;
		$.ajax({
			url:"ajaxapi/checkin.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				else if(data.status == "success"){
					$(parent).parents(".popular_venue").find(".action-list").hide().parents(".popular_venue").find(".venue_actions").show();
					html = "<small>You have checked in to the venue!</small>";
					hidevenuemessages(parent,html,".successdiv")
				}
				else{
					hidevenuemessages(parent,data.message,".errordiv");
				}
			},
			error:function(){
				message = "<small>Unable to check-in! Please try again later</small>";
				hidevenuemessages(parent,message,".errordiv");
			}
		});
		return false;
		
	});
	
	$(".venue_reserve").click(function(){
		blockui();
		var venueid = $(this).parents(".popular_venue").attr("data-venue-id");
		var data = {"venue_id":venueid,"action":"set"};
		var parent = this;
		$.ajax({
			url:"ajaxapi/reserve.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				else if(data.status == "success"){
					$(parent).parents(".popular_venue").find(".action-list").hide().parents(".popular_venue").find(".venue_actions").show();
					html = "<small>You have reserved the venue!</small>";
					hidevenuemessages(parent,html,".successdiv")
				}
				else{
					hidevenuemessages(parent,data.message,".errordiv");
				}
			},
			error:function(){
				message = "<small>Unable to reserve! Please try again later</small>";
				hidevenuemessages(parent,message,".errordiv");
			}
		});
		return false;
	});
	
	
	$("#popular_list .venue_actions").click(function(){
//		console.log('click');
		$("#popular_list .action-list").slideUp('100').parents(".popular_venue").find(".venue_actions").show()
		$(this).hide().parents(".popular_venue").find(".action-list").slideDown("100");
	});
	$("#popular_list .action-list .venue_close").click(function(){
//		console.log("click");
		$(this).parents(".action-list").slideUp("100").parents(".popular_venue").find(".venue_actions").show();
	});
	$("#currentcliquelist .table").on("mouseenter","tr",function(){
		$(this).find(".deletecliqueuser").show();
	})
	.on("mouseleave","tr",function(){
		$(this).find(".deletecliqueuser").hide();
	});
	$("body").ajaxStart(function(){
		$(".loading,.successdiv,.errordiv").hide();
	})
	
	$("#cliqueadd").on("hidden",function(){
		$(".successdiv,.errordiv,.loading").hide();
		$("#addcliqueform").trigger("reset").show();
	});
	
	$("#editcliquemodal").on("hidden",function(){
		$(this).find(".successdiv,.errordiv,.loading").hide();
		$(this).find("#currentcliquelist .table,#friendlist .table").html("");
		$(this).find("#friendlist").hide();
		$("#add-friends").show();
		$("#add-to-clique").hide();
		$("#currentcliquelist").show();
	});
	
	
	$("body").on("click",".deletecliqueuser",function(){
		
		$("#editcliquemodal #currentcliquelist").slideUp("200")
		.parents(".modal").find(".loading").fadeIn("200");
		var userid = $(this).parents("tr").attr("data-userid");
		var cliqueid = $(this).parents(".modal").attr("data-cliqueid");
		var data = {"clique_id":cliqueid,"user_id":userid};
		$.ajax({
			url:"ajaxapi/deletecliqueuser.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				else if(data.status == "success"){
					var data = {"clique_id":cliqueid};
					$.ajax({
						url:"ajaxapi/getcliquelist.php",
						type:"POST",
						data:data,
						dataType:"JSON",
						success:function(data){
							if(data.status == "unauthenticated"){
								window.location = "index.php";
							}
							else if(data.status == "success"){
								if(data.message == "No friends added"){
									$("#editcliquemodal").attr("data-cliqueid",cliqueid).modal();
									$("#editcliquemodal .errordiv").html(data.message).slideDown("200");
								}
								else{
									var html = "";
									var cliquelist = data.message;
									for(i = 0; i < cliquelist.length; i++){
										html+="<tr data-userid = '"+ cliquelist[i].id + "'>"+ 
												"<td class = 'dp-container'><img class = 'dp-container' src = 'img/"+cliquelist[i].dp+"'/></td>"+
												"<td><h4>" + cliquelist[i].name + " <button class = 'deletecliqueuser btn btn-mini btn-danger'><i class = 'icon-minus'></i></h4> " +
														"</td></tr";
									}
									$("#editcliquemodal").attr("data-cliqueid",cliqueid).modal();
									$("#currentcliquelist table").html(html);
									$("#editcliquemodal").find(".loading").fadeOut("200").parents(".modal").find("#currentcliquelist").slideDown("200");
								}
							}
							else{
								$("#editcliquemodal").find(".loading").fadeOut("200").parents(".modal")
								.find(".errordiv").html(data.message).slideDown("200");
							}
						},
						error:function(){
							$("#editcliquemodal").find(".loading").fadeOut("200").parents(".modal")
							.find(".errordiv").html(data.message).slideDown("200");
						}
					})
				}
			},
			error:function(){
				
			}
		})
	});
	
	$("#add-to-clique").click(function(){
		$("#friendlist").slideUp("200").parents(".modal").find(".loading").fadeIn('200');
		if($(".checkfriend:checked").length<1){
			alert("You have to select at least one friend");
			return false;
		}
		var cliqueid = $(this).parents(".modal").attr("data-cliqueid");
		var friends = "";
		$(".checkfriend:checked").each(function(){
			friends+= "friends_id%5B%5D="+$(this).val() + "&";
		});
		friends+="clique_id="+cliqueid;
		//console.log(friends);
		$.ajax({
			url:"ajaxapi/addtoclique.php",
			type:"POST",
			data:friends,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				else if(data.status == "success"){
					$("#editcliquemodal .loading").fadeOut("200").parents(".modal").find(".successdiv").html(data.message).slideDown("200");
					setTimeout(function(){ window.location = "";},"700");
				}
				else{
					$("#editcliquemodal .loading").fadeOut("200").parents(".modal").find(".errordiv").html(data.message).slideDown("200");
					$("#friendlist").slideDown("200");
				}
			},
			error:function(){
				$("#editcliquemodal .loading").fadeOut("200").parents(".modal").find(".errordiv").html("an error occurred").slideDown("200");
				$("#friendlist").slideDown("200");
			}
		})
	});
	$("#add-friends").click(function(){
		$("#currentcliquelist,#add-friends").slideUp("300");
		$("#editcliquemodal .loading").fadeIn("200");
		var cliqueid = $(this).parents(".modal").attr("data-cliqueid");
		var data = {"clique_id":cliqueid};
		$.ajax({
			url:"ajaxapi/getfriendlist.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				else if(data.status == "success"){
					var friends = data.message;
					var html = "";
					for(i = 0; i < friends.length; i++){
						html+= "<tr><td>" +
								"<input class = 'checkfriend' type = 'checkbox' value = '"+friends[i].id+"' /></td>" +
										"<td><img class = 'dp-container' src = 'img/"+ friends[i].dp+ "'/>" +
										"<td><h4>" + friends[i].name+"</h4></td></tr>";
					}
					$("#friendlist .table").html(html);
					$("#add-to-clique").fadeIn("200");
					$("#editcliquemodal .loading").fadeOut("200").parents(".modal").find("#friendlist").slideDown("200");
				}
				else{
					$("#editcliquemodal .loading").fadeOut("200").parents(".modal").find(".errordiv").html(data.message)
					.slideDown("200");
				}
			},
			error:function(){
				
			}
		})
	})
	$(".editclique").click(function(){
		var cliqueid = $(this).parents("h5").find(".clique").attr("data-id");
		var cliquename = $(this).parents("h5").find(".clique").html();
		var data = {"clique_id":cliqueid};
		//console.log(data);
		$.ajax({
			url:"ajaxapi/getcliquelist.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				else if(data.status == "success"){
					if(data.message == "No friends added"){
						$("#editcliquemodal .modal-header h2").html("Edit "+cliquename);
						$("#editcliquemodal").attr("data-cliqueid",cliqueid).modal();
						$("#editcliquemodal .errordiv").html(data.message).slideDown("200")
						.parents("#editcliquemodal").modal();
					}
					else{
						$("#editcliquemodal .modal-header h2").html("Edit "+cliquename);
						var html = "";
						var cliquelist = data.message;
						for(i = 0; i < cliquelist.length; i++){
							html+="<tr data-userid = '"+ cliquelist[i].id + "'>"+ 
									"<td class = 'dp-container'><img class = 'dp-container' src = 'img/"+cliquelist[i].dp+"'/></td>"+
									"<td><h4>" + cliquelist[i].name + " <button class = 'deletecliqueuser btn btn-mini btn-danger'><i class = 'icon-minus'></i></h4> " +
											"</td></tr>";
						}
						$("#currentcliquelist table").html(html);
						$("#editcliquemodal").attr("data-cliqueid",cliqueid).modal();
					}
					 
				}
				else{
					$.unblockUI();
					$("#editcliquemodal").attr("data-cliqueid",cliqueid).modal();
					$(".content .errordiv").html(data.message).slideDown("300");
				}
			},
			error:function(){
				$.unblockUI();
				$(".content .errordiv").html("An error occurred").slideDown("300");
			}
		})
	});
	
	$(".deleteclique").click(function(){
//		//console.log("deleteclique clicked");
		var cliqueid = $(this).parents("h4").find(".clique").attr("data-id");
		var data = {"clique_id":cliqueid};
		blockui();
		$.ajax({
			url:"ajaxapi/deleteclique.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success")
					window.location = "";
				else
					$.unblockUI();
			},
			error:function(){
				$.unblockUI();
			}
		})
	});
	$("#addcliqueform").submit(function(){
		$(this).slideUp("200");
		$("#cliqueadd .loading").fadeIn("100");
		var data = $(this).serialize();
		$.ajax({
			url:"ajaxapi/addclique.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					$("#cliqueadd .loading").fadeOut("200",function(){
						$("#cliqueadd .successdiv").html(data.message).slideDown("200");
						setTimeout(function(){ window.location = "";},300);
					});
				}
				else{
					$("#cliqueadd .loading").fadeOut("100",function(){
						$("#cliqueadd .errordiv").html(data.message).add("#addcliqueform").slideDown("200");
					});
				}
			},
			error:function(){
				$("#cliqueadd .loading").fadeOut("100",function(){
					$("#cliqueadd .errordiv").html("An error occurred!").add("#addcliqueform").slideDown("200");
				});
			}
		});
		return false;
	});
	$(".renameclique").click(function(){
		var cliquename = $(this).parents("h5").find("a").html();
		var cliqueid = $(this).parents("h5").find("a").attr("data-id");
		$("#renamecliqueform").find("input[name = 'clique_name']").val(cliquename)
		.parent().find("input[name = 'clique_id']").val(cliqueid).parents(".modal").find(".modal-header span").html(cliquename);
		$("#renamecliquemodal").modal("show");
	});
	
	$("#renamecliqueform").submit(function(){
		var validation = true;
		$(this).find("input").each(function(){
			if($(this).val == "" || $(this).val == "NULL"){
				validation = false;
			}
		});
		if(validation == false){
			$(this).parents(".modal").find(".errordiv").html("Please enter a name for the clique").slideDown("200");
			return false;
		}
		$("#renamecliqueform,#renamemodal").slideUp("200",function(){
			$("#renamecliquemodal .loading").fadeIn("200",function(){
				var data = $("#renamecliqueform").serialize();
				$.ajax({
					url:"ajaxapi/renameclique.php",
					type:"POST",
					data:data,
					dataType:"JSON",
					success:function(data){
						if(data.status == "unauthenticated"){
							window.location = "index.php";
						}
						else if(data.status == "success"){
							$("#renamecliquemodal").find(".loading").fadeOut("200",function(){
								$("#renamecliquemodal .successdiv").html(data.message).slideDown("200",function(){
									setTimeout(function(){ window.location = "";},"400");
								});
							});
						}
						else{
							$("#renamecliquemodal").find(".loading").fadeOut("200",function(){
								$("#renamecliquemodal .errordiv").html(data.message).slideDown("200",function(){
									$("#renamecliqueform,#renamemodal").slideDown("200");
								});
							});
						}
					},
					error:function(){
						$("#renamecliquemodal").find(".loading").fadeOut("200",function(){
							$("#renamecliquemodal .errordiv").html("An error occurred").slideDown("200",function(){
								$("#renamecliqueform,#renamemodal").slideDown("200");
							});
						});
					}
				});
			});
		});
		return false;
	});
});