<?php
if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH'])
		AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')) {
	die();	
}

if(!(isset($_SESSION['logged']) && isset($_SESSION['username']) && isset($_SESSION['user_id']))){
	$status = "unauthenticated";
	$message = "Login First!!";
	include 'json_encoding.php';
	die();
}

?>