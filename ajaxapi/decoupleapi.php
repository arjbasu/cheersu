<?php
	session_start();
	include 'authentication_ajax_api.php';
	if(isset($_POST['api_type'])){
		$type = $_POST['api_type'];
		include '../connect.php';
		$userid = $_SESSION['user_id'];
		if($type == "fb"){
			$query = "UPDATE cheersu_users SET user_fb_id = ?, user_fb_autopost = ? WHERE user_id = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array("",0,$userid));
			$status = "success";
			$message = "Facebook Account successfully removed";
		}
		else{
			$status = "error";
			$message = "Unable to disconnect FB account. Please try again later";
		}
	}
	else{
		$status = "error";
		$message = "improper parameters passed";

	}
	include 'json_encoding.php';
?>
