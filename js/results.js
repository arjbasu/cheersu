function timer(){
	setTimeout(function(){
		$(".errordiv,.successdiv").slideUp("300");
	},"1500");
}


(function ($, F) {
    F.transitions.resizeIn = function() {
        var previous = F.previous,
            current  = F.current,
            startPos = previous.wrap.stop(true).position(),
            endPos   = $.extend({opacity : 1}, current.pos);

        startPos.width  = previous.wrap.width();
        startPos.height = previous.wrap.height();

        previous.wrap.stop(true).trigger('onReset').remove();

        delete endPos.position;

        current.inner.hide();

        current.wrap.css(startPos).animate(endPos, {
            duration : current.nextSpeed,
            easing   : current.nextEasing,
            step     : F.transitions.step,
            complete : function() {            	
            	F._afterZoomIn();
                current.inner.fadeIn("fast");
            }
        });
    };

}(jQuery, jQuery.fancybox));
	$(document).ready(function(){
		initialisechat();
		$("body").ajaxStart(function(){
			$(".errordiv,.successdiv").hide();
		});
		
		$("#commentmodal").on("hidden",function(){
			$(this).find(".successdiv,.errordiv").hide();
			$("#prevcomments,#commentform").show();
			$("#commentform").trigger("reset");
		});
		check_notifications();
		
		$("#commentform").submit(function(){
			blockui();
			var comment = $("#comment").val();
			var venueid = $("#commentmodal").attr("data-venueid");
			var data = {"venue_id":venueid,"comment":comment,"action":"set"};
			$.ajax({
				url:"ajaxapi/comment.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					if(data.status == "unauthenticated"){
						window.location = "index.php";
					}
					else if(data.status == "success"){
						$.unblockUI();
						$("#prevcomments,#commentform").slideUp("200",function(){
							$("#commentmodal .successdiv").html(data.message).slideDown('100');
						});
					}
					else{
						$.unblockUI();
						$("#prevcomments,#commentform").slideUp("200",function(){
							$("#commentmodal .errordiv").html(data.message).slideDown('100');
						});
					}
				},
				error:function(){
					$.unblockUI();
					$("#prevcomments,#commentform").slideUp("200",function(){
						$("#commentmodal .errordiv").html("An error occurred").slideDown('100');
					});
				}
			});
			return false;
		});
		
		
		
		$(".comment").click(function(){
			blockui();
			var venueid = $(this).parent().attr("data-venueid");
			var data = {"venue_id":venueid,"action":"get"};
//			console.log(data);
			$.ajax({
				url:"ajaxapi/comment.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					$("#prevcomments table").html("");
					if(data.status == "unauthenticated"){
						window.location = "index.php";
					}
					else if(data.status == "success"){
						$.unblockUI();
						if(data.message === "No comments available"){
							$("#commentmodal .errordiv").html(data.message).slideDown("200");
						}
						else{
							entries = data.message;
							html = "";
							length = entries.length;
							for(var i = 0;i < length; i++){
								html+= "<tr>" +
										"<td class = 'comment-dp'><img class = 'dp-container' src = 'img/icons/"+entries[i].dp+"'></td>" +
										"<td class = 'comment-comment'><p><a class = 'userlink' href = 'profile.php?userid="
										+entries[i].userid+"'>" + entries[i].name + "</a></p>" +
										"<p>"+entries[i].comment+"</p></td>" +
										"</tr>";
							}
							$("#prevcomments table").html(html);
						}
						$("#commentmodal").attr("data-venueid",venueid).modal();
					}
					else{
						$.unblockUI();
						$("#venues .errordiv").html(data.message).slideDown("300");
					}
				},
				error:function(){
					$.unblockUI();
					message = "<strong>An error occured!</strong> Please try again later";
					$("#venues .errordiv").html(message).slideDown("300");
				}
			});
			return false;
			
			
		});
		
		$(".photos").click(function(){
			var venueid = $(this).parent().attr("data-venueid");
			var data = {"type":"getimages","venue_id":venueid};
			blockui();
			$.ajax({
				url:"ajaxapi/venue_photo_action.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						if(data.message == "No Images available"){
							errordiv = '<div class = "alert alert-error pull-center errordiv">'+
								'<p>No images for this venue yet</p>'+
								'</div>';
							$("#photomodal .modal-body #img-thumbnails").html(errordiv);
							$("#photomodal").find(".errordiv").show();
						}
						else{
							message = data.message;
							len = message.length;
							html = "";
							for(var i = 0; i < len; i++){
								 html += '<a class = "fancybox" rel = "venuegallery" href = "img/venue_img/'+message[i].venue_photo_source +
								 '"><img class = "img-thumbnails" src = "img/venue_img/icons/'+ message[i].venue_photo_source +
									 '"/></a>' ;
								
							}
							$("#photomodal .modal-body #img-thumbnails").html(html).
							find(".fancybox")
							.fancybox({
								nextMethod : 'resizeIn',
						        nextSpeed  : 150,
						        preload:8,
						        prevMethod : false,
									helpers	: {
										title	: {
											type: 'inside'
										},
										thumbs	: {
											width	: 50,
											height	: 50
										}
									}
							});
							
						}
						$.unblockUI();
						$("#photomodal").attr("data-venue-id",venueid).modal();
					}
					else{
						$.unblockUI();
						$("#venues .errordiv").html(data.message).slideDown("300");
					}
				},
				error:function(){
					$.unblockUI();
					message = "<strong>An error occured!</strong> Please try again later";
					$("#venues .errordiv").html(message).slideDown("300");
				}
			})
		});
		$(".checkin").click(function(){
			blockui();
			var venueid = $(this).parent().attr("data-venueid");
			var data = {"venue_id":venueid,"action":"set"}
			$.ajax({
				url:"ajaxapi/checkin.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					$.unblockUI();
					if(data.status == "success"){
						
						$(".successdiv").html(data.message).slideDown();
						timer();
					}
					else{
						$(".errordiv").html(data.message).slideDown();
						timer();
					}
				},
				error:function(){
						$.unblockUI();
						html = "An error occurred. Please try again later";
						$(".errordiv").html(html).slideDown();
						timer();
				}
			});
		});
		$(".reserve").click(function(){
			blockui();
			var venueid = $(this).parent().attr("data-venueid");
			var data = {"venue_id":venueid,"action":"set"}
			$.ajax({
				url:"ajaxapi/reserve.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					$.unblockUI();
					if(data.status == "success"){
						
						$(".successdiv").html(data.message).slideDown();
						timer();
					}
					else{
						$(".errordiv").html(data.message).slideDown();
						timer();
					}
				},
				error:function(){
						$.unblockUI();
						html = "An error occurred. Please try again later";
						$(".errordiv").html(html).slideDown();
						timer();
				}
			});
			
		});
	});