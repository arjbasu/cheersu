<?php
	session_start();
	include '../connect.php';
	include 'authentication_ajax_api.php';
	if(isset($_POST['clique_id']) && isset($_POST['action']) && isset($_POST['block'])){
		$cliqueid = stripslashes($_POST['clique_id']);
		$action = $_POST['action'];
		$block = $_POST['block'];
		$userid = $_SESSION['user_id'];
		$query = "SELECT request_to FROM cheersu_clique_requests WHERE request_to = $userid AND request_cliqueid = $cliqueid";
		$result = mysql_query($query);
		if(!$result){
			$status = "error";
			$message = "Error in DB connection";
			
		}
		else{
			if(mysql_num_rows($result) == 0){
				$status = "error";
				$message = "Request not found";
			}
			else{
				if($action == "accept"){
					$query = "INSERT INTO cheersu_clique_members(member_userid,member_cliqueid) VALUES (?,?)";
					$stmt = $pdo->prepare($query);
					$result = $stmt->execute(array($userid,$cliqueid));
					if(!$result){
						$status = "error";
						$message = "Error in DB connection";
					}
					else{
						$query = "DELETE FROM cheersu_clique_requests WHERE request_to = ? AND request_cliqueid = ?";
						$stmt = $pdo->prepare($query);
						$result = $stmt->execute(array($userid,$cliqueid));
						if(!$result){
							$status = "error";
							$message = "Error in DB connection";	
						}
						else if($stmt->rowCount() == 1){
							$status = "success";
							$message = "";
						}
						else {
							$status = "error";
							$message = "Error in DB connection";	
						}
					}
					
				}
				else if($action == "delete"){
					
					$query = "DELETE FROM cheersu_clique_requests WHERE request_to = ? AND request_cliqueid = ?";
					$stmt = $pdo->prepare($query);
					$result = $stmt->execute(array($userid,$cliqueid));
					if(!$result){
						$status = "error";
						$message = "Error in DB connection";	
					}
					else if($stmt->rowCount() == 1){
						if($block == "block"){
							$query = "INSERT INTO cheersu_clique_decline(decline_cliqueid,decline_userid) VALUES (?,?)";
							$stmt = $pdo->prepare($query);
							$result = $stmt->execute(array($cliqueid,$userid));
							if(!$result){
								$status = "error";
								$message = "Error in DV connection";
							}
							else{
								$status = "success";
								$message = "";		
							}
						}
						else{
							$status = "success";
							$message = "";
						}
						
					}
					else {
						$status = "error";
						$message = "Error in DB connection";	
					}
				}
				else{
					$status = "error";
					$message = "Improper parameters passed1";
				}
			}
		}
	}
	else {
		$status = "error";
		$message = "Improper parameters passed2";	
	}
	include 'json_encoding.php';
?>