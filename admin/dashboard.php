<?php
	session_start();
	include 'twiginit.php';
	include 'check_authorization_admin.php';
	include 'connect.php';
	$query = "SELECT user_id,user_firstname,user_lastname,user_dp_icon AS user_dp,user_blocked,user_email,school_name FROM cheersu_users,cheersu_schools WHERE user_schoolid = school_id AND user_verified = 1";
	$result = mysql_query($query);
	$data = array();
	$users = array();
	$venues = array();
	$schools = array();
	$schoolist = array();
	$statelist = array();
	if(!$result){
		die("Unable to fetch from database");
	}
	else{
		if(mysql_num_rows($result) == 0){
			$data['zerouserresults'] = true;
		}
		else{
			while($temp = mysql_fetch_assoc($result)){
				if($temp['user_dp'] == ""){
					$temp['user_dp'] = "cheersu_icon.png";
				}
				array_push($users, $temp);
			}
			$data['users'] = $users;
		}
	}
	$query = "SELECT school_name,school_id FROM cheersu_schools WHERE school_id <> 0 ORDER BY school_name";
	$result = mysql_query($query);
	if(!$result){
		die("Unalbe to interact with databaes");
	}
	else{
		$tempschool = array();
		while($tempschool = mysql_fetch_assoc($result)){
			array_push($schoolist,$tempschool);
		}
		$data['schoollist'] = $schoolist;
	}
	
	$query = "SELECT state_name,state_id FROM cheersu_states ORDER BY state_name";
	$result = mysql_query($query);
	if(!$result){
		die("unable to interact with database");
	}
	else{
		$tempstate = array();
		while($tempstate = mysql_fetch_assoc($result)){
			array_push($statelist,$tempstate);	
		}
		$data['statelist'] = $statelist;
	}
	$query = "SELECT venue_id,venue_name,school_name,venue_address FROM cheersu_venues,cheersu_schools WHERE venue_schoolid = school_id AND venue_id <> 0 ORDER BY venue_name";
	$result = mysql_query($query);
	if(!$result){
		die("Unable to interact with database");
	}
	else{
		if(mysql_num_rows($result) == 0){
			$data['zerovenueresults'] = true;
		}
		else{
			while($temp = mysql_fetch_assoc($result)){
				$temp['venue_name'] = stripslashes($temp['venue_name']);
				$temp['img'] = str_replace(" ", "+", $temp['venue_address']); 
				array_push($venues, $temp);
			}
			$data['venues'] = $venues;
		}
	}
	
	$query = "SELECT school_id,school_name,state_name FROM cheersu_schools,cheersu_states WHERE school_state = state_id AND school_id <> 0 ORDER BY school_name";
	$result = mysql_query($query);
	if(!$result){
		die("Unable to interact with database");
	}
	else{
		if(mysql_num_rows($result) == 0){
			$data['zeroschoolresults'] = true;
		}
		else{
			while($temp = mysql_fetch_assoc($result)){
				array_push($schools, $temp);
			}
			$data['schools'] = $schools;
		}
	}
	echo $twig->render("dashboard.twig",$data);
?>