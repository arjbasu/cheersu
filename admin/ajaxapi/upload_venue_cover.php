<?php
	session_start();
	if($_FILES["file"]["error"] > 0 && isset($_SESSION['adminlogged']) && isset($_SESSION['username'])){
		$status = "error";
		$error = $_FILES["file"]["error"];
		switch($error){
			case 1:
			case 2:$message = "Maximum File size exceeded";break;
			case 3:$message = "File Partially uploaded. Refresh the page and try again";break;
			case 4:$message = "No File Uploaded";break;
			case 5:
			case 6:
			case 7:
			case 8:$message = "The file uploader has encountered an error. Please try after sometime";break;
				
		}
	}
	else{
		$extarray = explode(".", $_FILES['file']['name']);
		$extension = $extarray[count($extarray)-1];
		error_log("extension|$extension",0);
		$name = md5($_FILES["file"]["name"].$_SESSION['user_firstname']);
		$name .= ".$extension";
		chdir("../../");
		chdir("dev");
		if (!file_exists("img/venue_cover_img/$name")){
				move_uploaded_file($_FILES["file"]["tmp_name"],
		"img/venue_cover_img/$name");
		include('SimpleImage.php');
		$image = new SimpleImage();
		$image->load("img/venue_cover_img/$name");
		if($image->getWidth() > 1200)
			$image->resizeToWidth("1200");
			$image->save("img/venue_cover_img/$name");
			define('DESIRED_IMAGE_WIDTH', 200);
			define('DESIRED_IMAGE_HEIGHT', 200);
				
			$source_path = "img/venue_cover_img/$name";
				
			/*
			 * Add file validation code here
			*/
				
			list($source_width, $source_height, $source_type) = getimagesize($source_path);
			$type = "";
			switch ($source_type) {
				case IMAGETYPE_GIF:{
					$source_gdim = imagecreatefromgif($source_path);
					$type = "GIF";
					break;
				}
				case IMAGETYPE_JPEG:{
					$type = "jpeg";
					$source_gdim = imagecreatefromjpeg($source_path);
					break;
				}
				case IMAGETYPE_PNG:{
					$type = "png";
					$source_gdim = imagecreatefrompng($source_path);
					break;
				}
			}
				
			$source_aspect_ratio = $source_width / $source_height;
			$desired_aspect_ratio = DESIRED_IMAGE_WIDTH / DESIRED_IMAGE_HEIGHT;
				
			if ($source_aspect_ratio > $desired_aspect_ratio) {
				/*
				 * Triggered when source image is wider
				*/
				$temp_height = DESIRED_IMAGE_HEIGHT;
				$temp_width = ( int ) (DESIRED_IMAGE_HEIGHT * $source_aspect_ratio);
			} else {
				/*
				 * Triggered otherwise (i.e. source image is similar or taller)
				*/
				$temp_width = DESIRED_IMAGE_WIDTH;
				$temp_height = ( int ) (DESIRED_IMAGE_WIDTH / $source_aspect_ratio);
			}
				
			/*
			 * Resize the image into a temporary GD image
			*/
				
			$temp_gdim = imagecreatetruecolor($temp_width, $temp_height);
			imagecopyresampled(
			$temp_gdim,
			$source_gdim,
			0, 0,
			0, 0,
			$temp_width, $temp_height,
			$source_width, $source_height
			);
				
			/*
			 * Copy cropped region from temporary image into the desired GD image
			*/
				
			$x0 = ($temp_width - DESIRED_IMAGE_WIDTH) / 2;
			$y0 = ($temp_height - DESIRED_IMAGE_HEIGHT) / 2;
			$desired_gdim = imagecreatetruecolor(DESIRED_IMAGE_WIDTH, DESIRED_IMAGE_HEIGHT);
			imagecopy(
			$desired_gdim,
			$temp_gdim,
			0, 0,
			$x0, $y0,
			DESIRED_IMAGE_WIDTH, DESIRED_IMAGE_HEIGHT
			);
				
			/*
			 * Render the image
			* Alternatively, you can save the image in file-system or database
			*/
			if($type == "jpeg"){
				imagejpeg($desired_gdim,"img/venue_cover_img/icon_square/$name");
			}
			else if($type == "png"){
				imagepng($desired_gdim,"img/venue_cover_img/icon_square/$name");
			}
			else if($type = "gif"){
				imagegif($desired_gdim,"img/venue_cover_img/icon_square/$name");
			}
			
			
			
			
			define('DESIRED_IMAGE_WIDTH_RECT', 300);
			define('DESIRED_IMAGE_HEIGHT_RECT', 200);
				
			$source_path = "img/venue_cover_img/$name";
				
			/*
			 * Add file validation code here
			*/
				
			list($source_width, $source_height, $source_type) = getimagesize($source_path);
			$type = "";
			switch ($source_type) {
				case IMAGETYPE_GIF:{
					$source_gdim = imagecreatefromgif($source_path);
					$type = "GIF";
					break;
				}
				case IMAGETYPE_JPEG:{
					$type = "jpeg";
					$source_gdim = imagecreatefromjpeg($source_path);
					break;
				}
				case IMAGETYPE_PNG:{
					$type = "png";
					$source_gdim = imagecreatefrompng($source_path);
					break;
				}
			}
				
			$source_aspect_ratio = $source_width / $source_height;
			$desired_aspect_ratio = DESIRED_IMAGE_WIDTH_RECT / DESIRED_IMAGE_HEIGHT_RECT;
				
			if ($source_aspect_ratio > $desired_aspect_ratio) {
				/*
				 * Triggered when source image is wider
				*/
				$temp_height = DESIRED_IMAGE_HEIGHT_RECT;
				$temp_width = ( int ) (DESIRED_IMAGE_HEIGHT_RECT * $source_aspect_ratio);
			} else {
				/*
				 * Triggered otherwise (i.e. source image is similar or taller)
				*/
				$temp_width = DESIRED_IMAGE_WIDTH_RECT;
				$temp_height = ( int ) (DESIRED_IMAGE_WIDTH_RECT / $source_aspect_ratio);
			}
				
			/*
			 * Resize the image into a temporary GD image
			*/
				
			$temp_gdim = imagecreatetruecolor($temp_width, $temp_height);
			imagecopyresampled(
			$temp_gdim,
			$source_gdim,
			0, 0,
			0, 0,
			$temp_width, $temp_height,
			$source_width, $source_height
			);
				
			/*
			 * Copy cropped region from temporary image into the desired GD image
			*/
				
			$x0 = ($temp_width - DESIRED_IMAGE_WIDTH_RECT) / 2;
			$y0 = ($temp_height - DESIRED_IMAGE_HEIGHT_RECT) / 2;
			$desired_gdim = imagecreatetruecolor(DESIRED_IMAGE_WIDTH_RECT, DESIRED_IMAGE_HEIGHT_RECT);
			imagecopy(
			$desired_gdim,
			$temp_gdim,
			0, 0,
			$x0, $y0,
			DESIRED_IMAGE_WIDTH_RECT, DESIRED_IMAGE_HEIGHT_RECT
			);
				
			/*
			 * Render the image
			* Alternatively, you can save the image in file-system or database
			*/
			if($type == "jpeg"){
				imagejpeg($desired_gdim,"img/venue_cover_img/icon_rectangle/$name");
			}
			else if($type == "png"){
				imagepng($desired_gdim,"img/venue_cover_img/icon_rectangle/$name");
			}
			else if($type = "gif"){
				imagegif($desired_gdim,"img/venue_cover_img/icon_rectangle/$name");
			}
			
		}
		$status = "success";
		$message = "$name";
		include 'json_encoding.php';
	}
?>