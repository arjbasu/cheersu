<?php
session_start();
include 'authentication_ajax_api.php';
include '../connect.php';
$userid = $_SESSION['user_id'];
if(!(isset($_POST['friends_id']) && isset($_POST['clique_id']))){
	$status = "error";
	$message = "Improper parameters passed";
	include 'json_encoding.php';
	die();
}
$cliqueid = stripslashes($_POST['clique_id']);

$query = "SELECT clique_creator FROM cheersu_cliques WHERE clique_id = ?";
$stmt = $pdo->prepare($query);
$stmt->execute(array($cliqueid));
$temp = $stmt->fetch(PDO::FETCH_ASSOC);
$owner = $temp['clique_creator'];
if($owner != $userid){
	$status = "error";
	$message = "Permission Denied. Only Clique Administrators can delete members";
}
else{
	$list = array();
	foreach($_POST['friends_id'] as $key => $friendid){
		array_push($list,$friendid);
	}
	
	$friendlist = "(".implode(",", $list).")";
	error_log("friendlist:$friendlist",0);
	$query = "DELETE FROM cheersu_clique_members WHERE member_cliqueid = '$cliqueid' AND member_userid IN $friendlist";
	$stmt = $pdo->prepare($query);
	error_log("Deletequery1:$query",0);
	$stmt->execute();
	
	if($stmt->rowCount() == 1){
		$query = "DELETE FROM cheersu_cliques_activity WHERE clique_activity_cliqueid = '$cliqueid' AND clique_activity_userid IN $friendlist";
		error_log("Deletequery1:$query",0);
		$stmt = $pdo->prepare($query);
		$result = $stmt->execute();
		if($result){
			$status = "success";
			$message = "";
		}
		else{
			$status = "error";
			$message = "Unable to remove friend.";
		}
	}
	else{
		$status = "error";
		$message = "Unable to remove friend..";
	}
}
include 'json_encoding.php';
?>