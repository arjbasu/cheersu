<?php
	session_start();
	if (isset($_SERVER['HTTP_X_REQUESTED_WITH'])
			AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest') {
		include 'authentication_ajax_api.php';
		include '../connect.php';
		if(isset($_POST['param']) && isset($_POST['paramtype'])){
			$type = $_POST['paramtype'];
			$parameter = $_POST['param'];
			if($type == "name"){
				$query = "SELECT school_name,school_id FROM cheersu_schools,cheersu_states".
						" WHERE school_state = state_id AND state_name = ? ORDER BY school_name ASC";
			}
			else if($type == "id"){
				$query = "SELECT school_name,school_id FROM cheersu_schools WHERE school_state = ? ORDER BY school_name ASC";
			}
			else{
				$response['status'] = "error";
				$reesponse['message'] = "Improper parameters passed";
				die(json_encode($response));
			}
			
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($parameter));
			if($stmt->rowCount() == 0){
				$status = "error";
				$message = "No Schools found";
			} 
			else{
				$status = "success";
				$message = array();
				$tempschool = array();
				while ($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
					$tempschool['id'] = $temp['school_id'];
					$tempschool['name'] = $temp['school_name'];
					array_push($message, $tempschool);
				}
			}
		}
		else{
			$status = "error";
			$message = "Improper parameters passed";
		}
		include 'json_encoding.php';
	}
	else{
		header("Location:index.php");
	}
?>