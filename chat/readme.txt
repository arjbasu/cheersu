
	Important This is a beta version
	please send us a email to support@entertainstudios.com
	If you see any errors.


---- Version 1.3 Includes ----
	Full script rewrite.
	Bugs Fixed.
	Open Chatbox.
	Close Chatbox.
	Maximize & Minimize chat boxes.
	Offline messages will be saved.
	Automatically calculate how many chat boxes there can be open in users screen.
	Add boxes to chatbox Minimize holder.
	Onlinelist with page selectors.
	Easy add script to your user mysql database.
	Multiply options in config file.
	User and guests in 2 different mysql tables.
	User Online / Offline icon with chatbox.



Install:

1 - Open user_chat/config.php add mysql information for mysql connection. 
    Add the information in the empty fields. 
	Example:
	define('DB_SERVER', '--Here--');		Example: 'localhost'
	define('DB_USERNAME', '--Here--');		Example: 'root'
	define('DB_PASSWORD', '--Here--');		Example: 'fd345f'
	define('DB_DATABASE', '--Here--');		Example: 'user_chat_db'
	define("DB_PREFIX", '--Here--');		Example: 'd1_'
	
	
	Change the path to user images.
	Example:
	define("DB_USERIMG_PATH", '--Here--');    Example: 'user_images/'
	

	define("DB_GUEST_CAN_CHAT", '--Here--'); // true = Guest can chat // false = Only members can chat
	define("DB_GUEST_CAN_CHATWM", '--Here--'); // true = Guest can chat with members // false = off

	define("TOTAL_PR_ROW", 6); // how many users do you want to display in online list at a time.
	
	define("USER_TABLE", 'user'); // The table in mysql with your users
	define("USER_TABLE_ID", 'user_id'); id of users
	define("USER_TABLE_USERNAME", 'username'); nickname / username / email of user
	define("USER_TABLE_PASSWORD", 'password'); password of user
	define("USER_TABLE_LASTACTIVITY", 'lastactivity'); lastonline of user
	define("USER_TABLE_PICTURE", 'picture'); picture of user
	
	
	// true: will trun on onlinelist, false will trun off onlinelist.
	define("CHAT_ONLINELIST", 'true');

	// the amount of time user is online if not responing , in mins
	define("USER_TIMEOUT", '5');
	
	
	
	
	
3 - import mysql.sql to your mysql database.


4 - Use index.php to see how your page need to be.