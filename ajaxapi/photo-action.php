<?php
	session_start();
	include 'authentication_ajax_api.php';
	$status = "";
	$message = "";
	if(isset($_POST['action']) && isset($_POST['filename'])){
		$userid = $_SESSION['user_id'];
		include '../connect.php';
		$action = $_POST['action'];
		$filenames = array();
		foreach($_POST['filename'] as $key=>$value){
			array_push($filenames,$value);
		}
		if($action == "delete"){
			chdir("../");
			$len = count($filenames);
			for($i = 0; $i < $len;$i++){
				unlink("img/wall_img/".$filenames[$i]);
				unlink("img/wall_img/icons/".$filenames[$i]);
			}	
			$status = "success";
			$message = "files successfully deleted";
		}
		else if($action == "post"){
			$query = "SELECT now() AS timestamp";
			$result = mysql_query($query);
			error_log("NUm rows:".mysql_num_rows($result));
			if($result){
				$temp = mysql_fetch_assoc($result);
				$timestamp = $temp['timestamp'];
				$query = "BEGIN";
				$stmt = $pdo->prepare($query);
				$stmt->execute();
				$query = "INSERT INTO cheersu_user_photos(user_photo_user_id,user_photo_source,user_photo_timestamp) VALUES ";
				$insertquery = array();
				$insertdata = array();
				foreach($filenames as $data){
					$insertquery[] = '(?,?,?)';
					$insertdata[] = $userid;
					$insertdata[] = $data;
					$insertdata[] = $timestamp;
				}
				if(!empty($insertquery)){
					$query.= implode(', ',$insertquery);
					$stmt = $pdo->prepare($query);
					error_log("query:".$query."|");
					$stmt->execute($insertdata);
					if($stmt->rowCount() >= 1){
						
						$query = "INSERT INTO cheersu_activity(activity_user_id,activity_type,activity_venue_id,activity_timestamp) VALUES (?,?,?,?)";
						$stmt = $pdo->prepare($query);
						$stmt->execute(array($userid,"photo-activity",0,$timestamp));
						if($stmt->rowCount() == 1){
							$query = "COMMIT";
							$stmt = $pdo->prepare($query);
							$stmt->execute();
							$status = "success";
							$message = "Photos successfully posted";
						}
						else{
							$query = "ROLLBACK";
							$stmt = $pdo->prepare($query);
							$stmt->execute();
							$status = "success";
							$message = "photos successfully uploaded";
						}
						
					} 
					else{
						$query = "ROLLBACK";
						$stmt = $pdo->prepare($query);
						$stmt->execute();
						$status = "error";
						$message = "unable to insert into database";
					}
				}
				
			}
			else{
				$status = "error";
				$message = "unable to retrieve time";
			}
		}
		else{
			$status = "error";
			$message = "improper parameters passed";
		}
	}
	else{
		$status = "error";
		$message = "improper parameters passed";
	}
	include 'json_encoding.php';
?>