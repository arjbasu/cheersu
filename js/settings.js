function fb_login() {
    FB.login(function(response) {
        if (response.authResponse) {
        	FB.api('/me', function(response) {
        		  data = {"api_type":"fb","api_key":response.id};
        		  //console.log(data);
        		  $.ajax({
        			  url:"ajaxapi/setapiparams.php",
        			  type:"POST",
        			  dataType:"JSON",
        			  data:data,
        			  success:function(data){
        				  if(data.status != "success"){
        					  alert("unable to connect fb account.Please try again later");
        					  FB.logout();
        				  }
        				  else{
        					  alert("You have successfully integrated your Facebook Account");
        					  window.location = "";
        				  }
        			  },
        			  error:function(){
        				  alert("unable to connect fb account.Please try again later");
        				  FB.logout();
        			  }
        		  });
        		});
        } 
        
        else {
        	alert("unable to connect fb account.Please give allow all the necessary permissions");
        	FB.logout();
        }
      },{scope:'publish_stream,offline_access'});
}


$(document).ready(function(){
			check_notifications();
			initialisechat();
			$("#fb-decouple").click(function(){
				$.ajax({
					url:"ajaxapi/decoupleapi.php",
					type:"POST",
					data:{"api_type":"fb"},
					dataType:"JSON",
					success:function(data){
						if(data.status == "success"){
							alert(data.message);
							window.location = "";
						}
						else{
							alert(data.message);
						}
					},
					error:function(data){
						alert("An error occurred. Please try again later");
						
					}
				})
			})
			$("#passwordchange").submit(function(){
				$(".errordiv,.successdiv").hide();
				validation = true;
				$(this).find("input").each(function(){
					if($(this).val()==""){
						validation = false;
						return;
					}
				});
				
				
				
				if(validation == false){
					$(".errordiv").html("Please complete all the fields").slideDown("200");
				}
				else{
					var newpass = $("#newpass").val();
					var confirmpass = $("#confirmpass").val();
					if(newpass!=confirmpass){
						$(".errordiv").html("Passwords do not match").slideDown("200");
					}
					else{
						data = $(this).serialize();
						$(this).slideUp("100",function(){
							$(".loading").fadeIn("100");
							$.ajax({
								url:"changepassword.php",
								type:"POST",
								data:data,
								dataType:"JSON",
								success:function(data){
									if(data.status == "unauthenticated"){
										window.location = "index.php";
									}
									if(data.status == "success"){
										$(".loading").fadeOut("100",function(){
											$(".successdiv").html(data.message).slideDown("200");
										});
									}
									else{
										$(".loading").fadeOut("100",function(){
											$(".errordiv").html(data.message);
											$(".errordiv,#passwordchange").slideDown("200");
										});
									}
								},
								error:function(){
									$(".loading").fadeOut("100",function(){
										$(".errordiv").html("An error occurred");
										$(".errordiv,#passwordchange").slideDown("200");
									});
								}
							});	
						});	
					}
					
				}
				return false;
			});
			$("#fb-integrate").click(function(){
				FB.getLoginStatus(function(response) {
				  	  if (response.status === 'connected') {
				  	    // connected
				  		  console.log("connected");
				  		  fb_login();
				  	  } else if (response.status === 'not_authorized') {
				  	    // not_authorized
				  		  console.log("not authorized");
				  		  fb_login();
				  	  } else {
				  	    // not_logged_in
				  		  fb_login();
				  		  console.log("not logged in");
				  	  }
				  	 });
			  	 return false;
			});
			$(".modal").on("hidden",function(){
				$(".loading,.errordiv,.successdiv").hide();
				$("#passwordchange").show().trigger("reset");
			});
		});