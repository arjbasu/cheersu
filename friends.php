<?php
	session_start();
	include 'check_authorization.php';
	include 'connect.php';
	include 'twiginit.php';
	$data = array();
	$userid = $_SESSION['user_id'];
	$data['own'] = true;
	if(isset($_GET['userid'])){
		$paramid = $_GET['userid'];
		if($paramid != "" && $paramid != "$userid"){
			$userid = $paramid;
			$data['own'] = false;	
		}
	}
	if($data['own'] == false){
		$query = "SELECT user_firstname FROM cheersu_users WHERE user_id = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($userid));
		if($stmt->rowCount() < 1){
			die("unable to fetch username");
		}
		else{
			$temp = $stmt->fetch(PDO::FETCH_ASSOC);
			$data['name'] = $temp['user_firstname'];
		}
	}
	
	$query = "SELECT user_id,user_firstname,user_lastname,user_dp_icon FROM cheersu_users,cheersu_friends_$userid ".
	"WHERE user_id = friend_user_id AND friend_status = 'confirmed' ORDER BY user_firstname";
	$result = mysql_query($query);
	if(!$result){
		$data['h3message'] = "Unable to get the list of friends";
	}
	else if(mysql_num_rows($result) == 0){
		$data['noresults'] = true;
		$data['h3message'] = "No Friends Added!";
		$data['strongmessage'] = "Use the search bar to find your friends";
	}
	else{
		$friends = array();
		$tempfriend = array();
		while($temp = mysql_fetch_assoc($result)){
			$tempfriend['userid'] = $temp['user_id'];
			$tempfriend['name'] = stripslashes($temp['user_firstname'])." ".stripslashes($temp['user_lastname']);
			if($temp['user_dp_icon'] == ""){
				$tempfriend['dp'] = "cheersu_icon.png";
			}
			else{
				$tempfriend['dp'] = stripslashes($temp['user_dp_icon']);
			}
			
			array_push($friends,$tempfriend);
		}
		$data['friendlist'] = $friends;
		
	}
	$query = "SELECT user_dp_icon FROM cheersu_users WHERE user_id = $userid";
	$result = mysql_query($query);
	$temp = mysql_fetch_row($result);
	$data['chat_dp_icon'] = $temp[0];
	echo $twig->render("friends.twig",$data);
	
?>