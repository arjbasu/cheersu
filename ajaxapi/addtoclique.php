<?php
session_start();
include 'authentication_ajax_api.php';
include '../connect.php';
$userid = $_SESSION['user_id'];
if(!(isset($_POST['friends_id']) && isset($_POST['clique_id']))){
	$status = "error";
	$message = "Improper parameters passed";
	include 'json_encoding.php';
	die();
}
$cliqueid = $_POST['clique_id'];
$query = "";
$dataarray = array();
$query= "INSERT INTO cheersu_clique_requests(request_from,request_to,request_cliqueid) VALUES ";
$insertquery = array();
$insertdata = array();
foreach($_POST['friends_id'] as $key => $friendid){
	$insertquery[] = '(?,?,?)';
	$insertdata[] = $userid;
	$insertdata[] = $friendid;
	$insertdata[] = $cliqueid;
}
if(!empty($insertquery)){
	$query.= implode(', ',$insertquery);
	$stmt = $pdo->prepare($query);
	error_log("query:".$query."|");
	$stmt->execute($insertdata);
	if($stmt->rowCount() <1){
		$status = "error";
		$message = "Unable to insert into database";
	}
	else{
		$status = "success";
		$message = "Requests successfully sent";
	}
}
else{
	$status = "error";
	$message = "Improper Parameters passed";
}

include 'json_encoding.php';

?>