<?php date_default_timezone_set('Asia/Calcutta');
require('user_chat/config.php'); 
?>
<!DOCTYPE html>
<html>
<head>
<title>Facebook User Chat System Beta</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
<link rel="stylesheet" type="text/css" href="bubbles.css">
<!-- Need for chat script to work -->
<link rel="stylesheet" type="text/css" href="user_chat/chat.css">
<!-- Need for chat script to work -->
</head>
<body>
<div id="wrapper">
	<div id="header">
	<div id="logo"></div>
	</div>
	<div id="content">
	<?php
	if(isset($_SESSION[''.CHAT_SESSION_UID.''])){
		echo "Welcome ".$_SESSION[''.CHAT_SESSION_UNAME.'']." | <a href='logout.php'>Logout</a>";
	} else {
		if(isset($_SESSION['guest_id'])){
			echo "Welcome ".$_SESSION['guest_name']." | <a href='login.php'>Login</a> Or <a href='create.php'>Create New User</a>";
		}
	}
	?>
	<br /><br />
	<h3>Version 1.3 Beta Includes</h3>
	<ul>
	<li>Full script rewrite.</li>
	<li>Bugs Fixed.</li>
	<li>Open Chatbox</li>
	<li>Close Chatbox</li>
	<li>Maximize & Minimize chat boxes.</li>
	<li>Offline messages will be saved.</li>
	<li>Automatically calculate how many chat boxes there can be open in users screen.</li>
	<li>Add boxes to chatbox Minimize holder.</li>
	<li>Onlinelist with page selectors.</li>
	<li>Easy add script to your user mysql database.</li>
	<li>Multiply options in config file.</li>
	<li>User and guests in 2 different mysql tables.</li>
	<li>User Online / Offline icon with chatbox.</li>
	</ul>
	</div>
</div>

<!-- Need for chat script to work-->
<script type="text/javascript" src="js/jquery.js"></script>
<script src="js/jquery.cookie.js"></script>
<?php
if(DB_GUEST_CAN_CHAT == "true"){
echo '<script src="user_chat/chat.js"></script>';
require('user_chat/onlinelist.php');
}elseif(isset($_SESSION[''.CHAT_SESSION_UID.''])){
echo '<script src="user_chat/chat.js"></script>';
require('user_chat/onlinelist.php');
}
?>
<!-- Need for chat script to work-->
</body>
</html>