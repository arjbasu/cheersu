<?php
	session_start();
	include 'authentication_ajax_api.php';
	include '../connect.php';
	date_default_timezone_set('Asia/Calcutta');
	$userid = $_SESSION['user_id'];
	$status = "";
	$message = "";
	function getonlinelist(){
		global $userid;
		global $status;
		global $message;
		$CHAT_USER_TIMEOUT = strtotime("- 1 minutes");
		$friendquery = "SELECT CONCAT(user_firstname,' ',user_lastname) AS user_name,user_dp_icon,user_id FROM cheersu_users,".
				"cheersu_friends_$userid WHERE user_id = friend_user_id AND friend_status = 'confirmed' ".
				"AND user_lastactivity > $CHAT_USER_TIMEOUT ORDER BY user_firstname";
// 		error_log("Friendquery:".$friendquery,0);
		$result = mysql_query($friendquery);
		if(!$result){
			$status = "error";
			$message = "Unable to fetch online list";
		}
		else{
			if(mysql_num_rows($result) == 0){
				$status = "success";
				$message = array();
			}
			else{
				$status = "success";
				$message = array();
				while($temp = mysql_fetch_assoc($result)){
					if($temp['user_dp_icon'] == ""){
						$temp['user_dp_icon'] = "cheersu_icon.png";
					}
					array_push($message, $temp);
				}
			}
		}
	}
	
	function getmessages($user = 0,$timestamp = 0){
// 		error_log("getmessages called",0);
		global $userid;
		global $status;
		global $message;
		$extra = "";
		
		if($user == 0){
// 			error_log("getmessags called with no parameters",0);
			mysql_query("BEGIN");
			$query = "SELECT chat_user_from,chat_time,chat_message,user_dp_icon,CONCAT(user_firstname,' ',user_lastname) AS user_name FROM cheersu_chat LEFT JOIN cheersu_users on user_id = chat_user_from ".
			"WHERE chat_user_to = '$userid' AND chat_status = 'unread' ORDER BY chat_time ASC";
			$result1 = mysql_query($query);
// 			error_log("chatquery:$query",0);
			$query2 = "UPDATE cheersu_chat SET chat_status = 'read' WHERE chat_user_to = '$userid'";
			$result2 = mysql_query($query2);
			if($result1 && $result2){
				mysql_query("COMMIT");
				$status = "success";
				$message = array();
				while($temp = mysql_fetch_assoc($result1)){
					$temp['chat_message'] = stripslashes($temp['chat_message']);
					if($temp['user_dp_icon'] == ""){
						$temp['user_dp_icon'] = "cheersu_icon.png";
					}
					array_push($message, $temp);
				}
			}
			else{
				mysql_query("ROLLBACK");
				$status = "error";
				$message = "unable to fetch chat messages";
			}
		}
		
		else{
			$extraclause = "";
			if($timestamp != 0){
				$extraclause = " AND chat_time < $timestamp ";
// 				error_log("extraclause:$extraclause",0);
			}
			$query = "SELECT chat_user_from,chat_time,chat_message,user_dp_icon,CONCAT(user_firstname,' ',user_lastname) AS user_name FROM cheersu_chat LEFT JOIN cheersu_users on user_id = chat_user_from ".
			"WHERE ((chat_user_to = '$userid' AND chat_user_from = '$user')  OR (chat_user_to = '$user' AND chat_user_from = '$userid')) AND chat_status = 'read' $extraclause ORDER BY chat_time DESC LIMIT 20";
			
			$result = mysql_query($query);
			if(!$result){
				$status = "error";
				$message = "Unable to load earlier messages";
			}
			else if(mysql_num_rows($result) == 0){
				$status = "success";
				$message = array();
			}
			else{
				$status = "success";
				$message = array();
				while($temp = mysql_fetch_assoc($result)){
					if($temp['user_dp_icon'] == ""){
						$temp['user_dp_icon'] = "cheersu_icon.png";
					}
					array_push($message, $temp);
				}
			}
		}
	}
	
	function sendmessage($user_to,$user_message){
		global $userid;
		global $status;
		global $message;
		global $pdo;
		$time = time();
		$query = "INSERT INTO cheersu_chat (chat_user_from,chat_user_to,chat_message,chat_time) VALUES (?,?,?,?)";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($userid,$user_to,$user_message,$time));
		if($stmt->rowCount() == 1){
			$status = "success";
			$message = $time;
		}
		else{
			$status = "error";
			$message = "unable to send message";
		}
		
	}
	
	if(isset($_POST['action'])){
		$time = time();
		$query = "UPDATE cheersu_users SET user_lastactivity = '$time' WHERE user_id = $userid";
		$result = mysql_query($query);
		$action = $_POST['action'];
		if($action == "getonlinelist")
			getonlinelist();
		else if($action == "getmessages"){
			getmessages();
		}
		else if($action == "sendmessage"){
			if(!(isset($_POST["user_from"]) && isset($_POST['user_message']))){
				$status = "error";
				$message = "improper parameters passed";
			}
			else{
				sendmessage($_POST['user_from'],$_POST['user_message']);
			}
		}
		else if($action == "getmessages"){
			getmessages();
		}
		else if($action == "gethistory"){
			if(isset($_POST['user_id']) && isset($_POST['timestamp'])){
				getmessages($_POST['user_id'],$_POST['timestamp']);
			}
			else if(isset($_POST['user_id'])){
				getmessages($_POST['user_id']);
			}
			else{
				$staus = "error";
				$message = "improper parameters passed";
			}
		}
		else{
			$status = "error";
			$message = "improper parameters passed";
			
		}
	}
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}
	include 'json_encoding.php';
?>