<?php
	session_start();
	include 'check_authorization.php';
	include 'connect.php';
	include 'twiginit.php';
	
	$data = array();
	$userid = $_SESSION['user_id'];
	$query = "SELECT user_firstname,user_lastname,user_id,user_dp_icon FROM cheersu_friend_requests_$userid,".
	" cheersu_users WHERE friend_user_id = user_id";
	$result = mysql_query($query);
	if(!$result){
		die("Unable to interact with the database");
	}
	
	else if(mysql_num_rows($result) != 0){
		$friends = array();
		while($temp = mysql_fetch_assoc($result)){
			include 'removeslashes.php';
			if($temp['user_dp_icon'] == ""){
				$temp['user_dp_icon'] = "cheersu_icon.png";
			}
			array_push($friends, $temp);
		}
		
		$data['friendrequests'] = $friends;
	}
	
	$query = "SELECT clique_name AS cliquename ,clique_id AS cliqueid, request_from AS userid,CONCAT(user_firstname,' ',user_lastname ) AS username,user_dp_icon AS dp FROM 
	cheersu_clique_requests INNER JOIN cheersu_cliques ON request_cliqueid = clique_id INNER JOIN cheersu_users ON
	 user_id = request_from AND request_to = ?";
	 $stmt = $pdo->prepare($query);
	 $result = $stmt->execute(array($userid));
	 if(!$result){
	 	die("Error");
	 }
	 else{
	 	$cliquerequest = array();
		 while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
		 	array_push($cliquerequest,$temp);
		 }
		 $data['cliquerequests'] = $cliquerequest;
	 }
	 
	$query = "UPDATE cheersu_clique_requests SET request_status = 'read' WHERE request_to = $userid";
	$result = mysql_query($query);
	if(!$result){
		die("Unable to interact with database");
	}
	 
	
	$query = "UPDATE cheersu_friend_requests_$userid SET friend_request_seen = 'read'";
	$result = mysql_query($query);
	if(!$result){
		die("Unable to interact with database");
	}
	$query = "UPDATE cheersu_notifications_$userid SET notification_status = 'read'";
	$result = mysql_query($query);
	if(!$result){
		die("Unable to interact with database");
	}
	
	// $query = "SELECT activity_id,user_firstname,user_lastname,user_dp_icon,user_id,activity_type,activity_comment,venue_name, school_name,".
	// " school_id,datediff(now(),activity_timestamp) as datedif , timediff(now(), activity_timestamp) ".
	// "as timedif FROM cheersu_users,cheersu_venues,cheersu_schools,cheersu_activity,cheersu_notifications_$userid".
	// " WHERE notification_activity_id = activity_id AND activity_venue_id = venue_id AND venue_schoolid = school_id ".
	// " AND activity_user_id = user_id ORDER BY activity_timestamp DESC LIMIT 0, 30 ";
	
	$query = "SELECT activity_id,venue_name,school_id,school_name,activity_comment,activity_type,activity_timestamp,
	CONCAT(user_firstname,' ',user_lastname ) AS user_name,user_dp_icon,user_id,notification_type,activity_comment,
	datediff(now(),notification_timestamp) as datedif ,timediff(now(),notification_timestamp) AS timedif FROM cheersu_notifications_$userid  
	LEFT JOIN cheersu_users ON notification_user_id = user_id LEFT JOIN (cheersu_activity INNER JOIN cheersu_venues ON activity_venue_id = venue_id
	 INNER JOIN cheersu_schools ON venue_schoolid = school_id) ON activity_id = notification_activity_id
	 ORDER BY notification_timestamp DESC LIMIT 200";
	$result = mysql_query($query);
	if(!$result){
		die("unable to query database for activities");
	}
	else{
		if(mysql_num_rows($result) == 0){
			$data['noresults'] = true;
		}
		else{
			$activities = array();
			
			while($temp = mysql_fetch_assoc($result)){
				$tempactivity = array();
				$tempactivity['comment'] = stripslashes($temp['activity_comment']);
				$tempactivity['school_name'] = $temp['school_name'];
				$tempactivity['activity_status'] = $temp['activity_comment'];
				$tempactivity['venue_name'] = $temp['venue_name'];
				$tempactivity['school_id'] = $temp['school_id'];
				$type = $temp['notification_type'];
				$activitytype = $temp['activity_type'];
				//$tempactivity['user_comment'] = $temp['user_comment_message'];
				error_log("type:$type",0);
				
				if($activitytype == 'photo-activity'){
					error_log("photo-activity:".$temp['user_id'].$temp['activity_timestamp'],0);
					$query = "SELECT user_photo_source AS source FROM cheersu_user_photos WHERE user_photo_timestamp = ? AND user_photo_user_id = ?";
					$stmt = $pdo->prepare($query);
					$stmt->execute(array($temp['activity_timestamp'],$userid));
					$photos = array();
					while($phototemp = $stmt->fetch(PDO::FETCH_ASSOC)){
						array_push($photos,$phototemp);
					}
					error_log("LEnght:".count($photos));
					$tempactivity['photos'] = $photos;
				}
				
				if($type == "tag"){
					error_log("tag",0);
					$tempactivity['status'] = true;
					$activityid = $temp['activity_id'];
					$query = "SELECT user_tag_userid AS id,user_tag_username AS name FROM cheersu_user_tag WHERE user_tag_activityid = '$activityid'";
					$result3 = mysql_query($query);
					if($result3){
						$taglist = array();
						while($tempres = mysql_fetch_assoc($result3)){
							array_push($taglist,$tempres);
						}
						$tempactivity['tagged'] = $taglist;
					}
					else{
						die("error");
					}
				}
				
				else if(strpos($type,"clique")!== false){
					error_log("clique:$type",0);
					$cliquearray = explode("|", $type);
					$tempactivity['clique'] = true;
					$tempactivity['cliquename'] = $cliquearray[2];
					$tempactivity['cliqueid'] = $cliquearray[3];
					$tempactivity['cliqueact'] = $cliquearray[1];
					
				}
				
				else if($type == "poke"){
					$tempactivity['poke'] = true;
				}
				else if($type == "acceptance"){
					$tempactivity['acceptance'] = true;
				}
				else if($type == "like"){
					$tempactivity['like'] = true;
					$tempactivity['activity_type'] = $temp['activity_type'];					
				}
				else if($type = "comment"){
					$tempactivity['comment'] = true;
					$tempactivity['activity_type'] = $temp['activity_type'];
				}
				
				
				$datediff = $temp['datedif'];
				$timediff = $temp['timedif'];
				if($datediff > 0){
					$tempactivity['timestamp'] = $datediff."d";
				}
				else{
					$timearray = explode(":", $timediff);
					if($timearray[0] > 0){
						if($timearray[0]<10){
							$timearray[0] = substr($timearray[0], 1,1);
						}
						$tempactivity['timestamp'] = $timearray[0]."h";
				
					}
					else if($timearray[1] > 0){
						if($timearray[1]<10){
							$timearray[1] = substr($timearray[1], 1,1);
						}
						$tempactivity['timestamp'] = $timearray[1]."m";
					}
					else{
						if($timearray[2]<10){
							$timearray[2] = substr($timearray[2], 1,1);
						}
						$tempactivity['timestamp'] = $timearray[2]."s";
					}
						
				}
					
				
				$tempactivity['name'] = $temp['user_name'];
				$tempactivity['dp'] = $temp['user_dp_icon'];
				$tempactivity['id'] = $temp['user_id'];
				$tempactivity['schoolid'] = $temp['school_id'];
				array_push($activities,$tempactivity);
			}
			$data['activities'] = $activities;
		}
	}
	$query = "SELECT user_dp_icon,CONCAT(user_firstname,' ',user_lastname) FROM cheersu_users WHERE user_id = $userid";
	$result = mysql_query($query);
	$temp = mysql_fetch_row($result);
	$data['chat_dp_icon'] = $temp[0];
	$data['myname'] = $temp[1];
	// print_r($data);
	echo $twig->render("notifications.twig",$data);
?>