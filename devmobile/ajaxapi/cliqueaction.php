<?php
	session_start();
	include 'authentication_ajax_api.php';
	include '../connect.php';
	$userid = $_SESSION['user_id'];
	if(isset($_POST['clique_id']) && isset($_POST['clique_user_id']) && isset($_POST['action'])){
		$cliqueid = $_POST['clique_id'];
		$clique_userid = $_POST['clique_user_id'];
		$action = $_POST['action'];
		$userid = $_SESSION['user_id'];
		if($action == "add"){
			$query = "SELECT * FROM cheersu_cliques_user_$userid WHERE clique_user_id = ? AND clique_myclique_id = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($clique_userid,$cliqueid));
			if($stmt->rowCount() > 0){
				$status = "error";
				$message = "The User already exists in the clique";
			}
			else{
				$query = "INSERT INTO cheersu_cliques_user_$userid(clique_user_id,clique_myclique_id) ".
						"VALUES(?,?)";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($clique_userid,$cliqueid));
				if($stmt->rowCount() == 1){
					$status = "success";
					$message = "User successfully added";
				}
				else{
					$status = "error";
					$message = "Unable to add user";
				}
			}
		}
		else if ($action == "delete"){
			$query = "SELECT * FROM cheersu_cliques_user_$userid WHERE clique_user_id = ? AND clique_myclique_id = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($clique_userid,$cliqueid));
			if($stmt->rowCount() == 0){
				$status = "error";
				$message = "The User does not exist in the clique";
			}
			else{
				$query = "DELETE FROM cheersu_cliques_user_$userid ".
						"WHERE clique_user_id = ? AND clique_myclique_id = ?";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($clique_userid,$cliqueid));
				if($stmt->rowCount() == 1){
					$status = "success";
					$message = "User successfully deleted";
				}
				else{
					$status = "error";
					$message = "Unable to delete user";
				}
			}
		}
	}
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}
	include 'json_encoding.php';
?>