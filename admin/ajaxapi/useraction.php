<?php
	session_start();
	include 'authentication_ajax_api.php';
	include 'connect.php';
	if(isset($_POST['user_id']) && isset($_POST['action'])){
		$userid = $_POST['user_id'];
		$action = $_POST['action'];
		if($action == "block"){
			$query = "UPDATE cheersu_users SET user_blocked = 1 WHERE user_id = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($userid));
			$status = "success";
			$message = "User Successfully blocked";			
		}
		else if($action == "unblock"){
			$query = "UPDATE cheersu_users SET user_blocked = 0 WHERE user_id = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($userid));
			$status = "success";
			$message = "User Successfully unblocked";
		}
		else{
			$status = "error";
			$message = "Improper action defined";
		}
	}
	include 'json_encoding.php';
?>
