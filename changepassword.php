<?php
session_start();
include 'ajaxapi/authentication_ajax_api.php';
if(isset($_POST['currentPass']) && isset($_POST['newPass']) &&isset($_POST['confirmPass'])){
	include 'connect.php';
	$userid = $_SESSION['user_id'];
	$currentPassword = $_POST['currentPass'];
	$newPassword = $_POST['newPass'];
	$confirmPassword = $_POST['confirmPass'];
	$hashedpass = crypt($currentPassword,'$1$foreverdope12$');
	$query = "SELECT * FROM cheersu_users WHERE user_id = ? AND user_passhash = ?";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($userid,$hashedpass));
	if($stmt->rowCount() == 1){
		if($confirmPassword != $newPassword){
			$status = "Error";
			$message = "Passwords do not match";
		}
		else{
			$hashednew = crypt($newPassword,'$1$foreverdope12$');
			$query = "UPDATE cheersu_users SET user_passhash = ? WHERE user_id = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($hashednew,$userid));
			$status = "success";
			$message = "You have successfully changed your password";
		}
	}
	else{
		$status = "Error";
		$message = "Incorrect password supplied";
	}
}

else{
	$status = "Error";
	$message = "Improper parameters passed";
}
include 'json_encoding.php';
?>