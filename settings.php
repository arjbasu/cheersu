<?php
	session_start();
	include 'check_authorization.php';
	include 'connect.php';
	include 'twiginit.php';
	$userid = $_SESSION['user_id'];
	$query = "SELECT * FROM cheersu_users WHERE user_id = '$userid'";
	$result = mysql_query($query);
	if(!$result){
		die("Unable to query databases");
	}
	else{
		include 'removeslashes.php';
		$temp = mysql_fetch_assoc($result);
		$firstname = $temp['user_firstname'];
		$lastname = $temp['user_lastname'];
		$email = $temp['user_email'];
		$emailpending = $temp['user_email_pending'];
		$bio = $temp['user_bio'];
		$status = $temp['user_status'];
		$school = $temp['user_schoolid'];
		$img = $temp['user_dp'];
		$fb_status = $temp['user_fb_id'];
		
		if($img == ""){
			$img = "300x200.gif";
		}
		$query = "SELECT school_name,school_id FROM cheersu_schools ORDER BY school_name";
		$result = mysql_query($query);
		if(!$result){
			die("Unable to query");
		}
		else{
			$schoolllist = array();
			while($temp = mysql_fetch_assoc($result)){
				array_push($schoolllist, $temp);
			}
		}
		if($fb_status == "" || $fb_status == "NULL"){
			$fb_status = 0;
		}
		else{
			$fb_status = 1;
		}
	}
	$data = array(
				"firstname"=>stripslashes($firstname),
				"lastname" =>stripslashes($lastname),
				"email"=>$email,
				"image"=>$img,
				"status"=>$status,
				"bio"=>$bio,
				"myschool" => $school,
				"email_pending"=>$emailpending,
				"schoollist" => $schoolllist,
				"fb_status" => $fb_status
			);
	$query = "SELECT user_dp_icon FROM cheersu_users WHERE user_id = $userid";
	$result = mysql_query($query);
	$temp = mysql_fetch_row($result);
	$data['chat_dp_icon'] = $temp[0];
	
	echo $twig->render('settings.twig',$data);
	
?>