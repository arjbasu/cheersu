<?php
session_start();

include 'check_authorization.php';
include 'twiginit.php';
include 'connect.php';
$userid = $_SESSION['user_id'];

$query = "SELECT user_schoolid FROM cheersu_users WHERE user_id = ?";
$stmt = $pdo->prepare($query);
$stmt->execute(array($userid));

$temp = $stmt->fetch(PDO::FETCH_ASSOC);
$myschoolid = $temp['user_schoolid'];

function cmp($a, $b)
{
	return $b['mutual'] - $a['mutual'];
}
function get_mutual_friends($userida,$useridb){
	//echo "<br/>$userida:$useridb<br/>";
	$query = "SELECT friend_user_id FROM cheersu_friends_$userida WHERE friend_status = 'confirmed'";
	$result = mysql_query($query);
	//echo $query;
	if(!$result){
		die("Unable to get friendlist");
	}
	else if(mysql_num_rows($result) == 0){
		return 0;
	}
	else{
		$friendlist = "(";
		while($temp = mysql_fetch_assoc($result)){
			$friendlist.= $temp['friend_user_id'].",";
		}
		$friendlist = substr($friendlist, 0, -1).")";
		//echo "<br/>$friendlist<br/>";
		$query = "SELECT COUNT(*) AS count FROM cheersu_friends_$useridb WHERE friend_user_id IN $friendlist AND friend_status = 'confirmed'";
		//echo $query."<br/>";
		$result = mysql_query($query);
		if(!$result || mysql_num_rows($result) == 0){
			die("Unable to tally results");
		}
		else{
			$temp = mysql_fetch_assoc($result);
			//echo "<br/>".$temp['count']."<br/>";
			return $temp['count'];
		}
	}
	
}

function return_main_users(){
	$query = "SELECT user_id,school_name,user_firstname,user_lastname,user_bio,user_dp_icon,user_schoolid FROM cheersu_users,cheersu_schools ".
	"WHERE user_id IN (5,6) AND school_id = user_schoolid";
	$result = mysql_query($query);
	if(!$result){
		die("unable to get suggestions");
	}
	$tempresult = array();

	while($temp = mysql_fetch_assoc($result)){

		if($temp['user_dp_icon'] == ""){
			$temp['user_dp_icon'] = "cheersu_icon.png";
		}
		include 'removeslashes.php';
		array_push($tempresult,$temp);
	}
	return $tempresult;
}
function generate_suggestions(){
	global $myschoolid;
	global $userid;
	global $pdo;
	$returnval = array();
	$query = "SELECT user_id,friend_status FROM cheersu_friends_$userid,cheersu_users WHERE "
	." friend_user_id = user_id AND user_blocked = 0";
	$result = mysql_query($query);
	if(!$result){
		die("unable to query databases");
	}
	if(mysql_num_rows($result) == 0){
		$nofriends = true;
		$query = "SELECT school_state, user_schoolid FROM cheersu_users,cheersu_schools WHERE user_id = ? ".
				" AND school_id = user_schoolid";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($userid));
		if($stmt->rowCount() < 1){
			die("Unable to query databases");
		}
		else{
			$temp = $stmt->fetch(PDO::FETCH_ASSOC);
			$state = $temp['school_state'];
			$school = $temp['schoolid'];
			$query = "SELECT user_firstname,school_name,user_id,user_lastname,user_bio,user_dp_icon,user_schoolid FROM cheersu_users,cheersu_schools WHERE user_schoolid = ? AND user_id <> '$userid' ".
			"AND school_id = user_schoolid AND user_verified = 1 AND user_blocked = 0";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($school));
			if($stmt->rowCount() == 0){
				$query = "SELECT user_firstname,school_name,user_id,user_lastname,user_bio,user_dp_icon,user_schoolid FROM cheersu_users,cheersu_schools WHERE user_schoolid IN ".
						"(SELECT school_id FROM cheersu_schools WHERE school_state = '$state') AND user_id <> '$userid' AND user_verified = 1 AND ".
						"user_blocked = 0 AND school_id = user_schoolid";
				$result = mysql_query($query);
				if(!$result){
					die("Unable to get users");
				}
				else if(mysql_num_rows($result) == 0){
					$tempresult = return_main_users();
					$returnval = $tempresult;
					
				}
				else{
					$tempresult = array();
					while($temp = mysql_fetch_assoc($result)){
						if($temp['user_dp_icon'] == ""){
							$temp['user_dp_icon'] = "cheersu_icon.png";
						}
						include 'removeslashes.php';
						array_push($tempresult,$temp);
					}
					$temps = return_main_users();
					array_merge($tempresult,$temps);
					$returnval = $tempresult;
					$returnval = array_map("unserialize", array_unique(array_map("serialize", $returnval)));
					
				}
					
			}
			else{
				$tempresult = array();
				while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
					if($temp['user_dp_icon'] == ""){
						$temp['user_dp_icon'] = "cheersu_icon.png";
					}
					include 'removeslashes.php';
					array_push($tempresult,$temp);
				}
				$temps = return_main_users();
				array_merge($tempresult,$temps);
				$returnval['suggestions'] = $tempresult;
				$returnval = array_map("unserialize", array_unique(array_map("serialize", $returnval)));
				
			}


		}
	}
	else{
		
		$myfriends = array(); 
		$list = "(";
		while($temp = mysql_fetch_assoc($result)){
			if($temp['friend_status'] == "confirmed")
				array_push($myfriends,$temp['user_id']);
			$list.=$temp['user_id'].",";
		}
		$list .= $userid.")";
		$friendlistlen = count($myfriends);
		//echo $list;
		$user_id = $_SESSION['user_id'];
		for($i = 0; $i < $friendlistlen; $i++){
			$friend = $myfriends[$i];
			$query = "SELECT user_id,user_firstname,user_lastname,user_dp_icon,school_name,user_schoolid FROM cheersu_friends_$friend,".
					"cheersu_schools,cheersu_users WHERE user_id = friend_user_id AND school_id = user_schoolid AND user_id NOT IN $list AND 
					user_id NOT IN (SELECT hidden_blocked_userid FROM cheersu_hidden WHERE hidden_userid = $user_id)";
			$result = mysql_query($query);
			if(!$result){
				die("unable to fetch friends of friends");
			}
			else if(mysql_num_rows($result)!=0){
				while($temp = mysql_fetch_assoc($result)){
					if($temp['user_dp_icon'] == ""){
						$temp['user_dp_icon'] = "cheersu_icon.png";
					}
					include 'removeslashes.php';
					$temp['mutual'] = get_mutual_friends($userid, $temp['user_id']);
					array_push($returnval,$temp);
				}
			}
		}
		
		$query = "SELECT user_id,user_firstname,user_lastname,user_dp_icon,school_name,user_schoolid FROM ".
					"cheersu_schools,cheersu_users WHERE (user_id = 5 OR user_id = 6) AND school_id = user_schoolid AND user_id NOT IN $list AND user_id NOT IN $list AND 
					user_id NOT IN (SELECT hidden_blocked_userid FROM cheersu_hidden WHERE hidden_userid = $user_id)";
		//echo $query;
		$result = mysql_query($query);
		if(!$result){
			die("Unable to query");
		}
		else if(mysql_num_rows($result) != 0){
			while($temp = mysql_fetch_assoc($result)){
				if($temp['user_dp_icon'] == ""){
					$temp['user_dp_icon'] = "cheersu_icon.png";
				}
				include 'removeslashes.php';
				$temp['mutual'] = get_mutual_friends($userid, $temp['user_id']);
				array_push($returnval,$temp);
			}
		}
		$returnval = array_map("unserialize", array_unique(array_map("serialize", $returnval)));
		usort($returnval,"cmp");
		
	}
	//print_r($returnval);
	$myschool = array();
	$otherschool = array();
	$len = count($returnval);
	foreach($returnval as $key=>$value){
		if($value['user_schoolid'] == $myschoolid){
			array_push($myschool,$value);
		}
		else{
			array_push($otherschool,$value);
		}
	}
	$returnval = array();
	$returnval = array_merge($myschool,$otherschool);
	return $returnval;
	
}
$query = "SELECT user_id,user_firstname, user_lastname, user_bio, user_status, user_dp_icon  
		 FROM cheersu_users 
		 WHERE user_id = '$userid'";
$result = mysql_query($query);
$data = array();
if(!$result){
	die("Unable to query databases");
}
else{
	$temp = mysql_fetch_assoc($result);
	$data['userid'] = $temp['user_id'];
	$firstname = stripslashes($temp['user_firstname']);
	$lastname = stripslashes($temp['user_lastname']);
	$bio = $temp['user_bio'];
	$status = stripslashes($temp['user_status']);
	$img = stripslashes($temp['user_dp_icon']);
	if($img == ""){
		$img = "cheersu_icon.png";
	}

	$data['name'] = $firstname." ".$lastname;
	$data['dp'] = $img;
	$data['chat_dp_icon'] = $img;
	$data['status'] = stripslashes($status);
	
	$data['username'] = $_SESSION['username'];
	$query = "SELECT * FROM cheersu_activity WHERE activity_type = 'checkin' AND activity_user_id = ?";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($userid));
	$checkins = $stmt->rowCount();
	$data['checkins'] = $checkins;
	
	$query = "SELECT venue_name, venue_address, venue_reservations, venue_checkins, venue_id FROM cheersu_venues INNER JOIN ".
	" cheersu_users ON venue_schoolid = user_schoolid AND user_id = '$userid' ORDER BY ".
	"(venue_reservations+venue_checkins) DESC LIMIT 3";
	
	$result = mysql_query($query);
	if(!$result){
		die("unable to query");
	}
	else{
		if(mysql_num_rows($result)!=0){
			$venuearray = array();
			while($temp = mysql_fetch_assoc($result)){
				$temp['venue_name'] = stripslashes($temp['venue_name']);
				$temp['img'] = urldecode($temp['venue_address']);
				array_push($venuearray,$temp); 
			}
		$data['popular_venues'] = $venuearray;	
		}
	}

	// $query = "SELECT myclique_id,myclique_title FROM cheersu_mycliques_$userid";
	// $stmt = $pdo->prepare($query);
	// $stmt->execute();
	// if($stmt->rowCount() == 0){
		// $nocliques = true;
		// $data['nocliques'] = true;
	// }
	// else{
		// $cliques = array();
		// while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
			// array_push($cliques,$temp);
		// }
		// $data['cliques'] = $cliques;
	// }
	
	$query = "SELECT clique_name AS name , clique_id AS id,clique_image AS img,
	COUNT(clique_activity_userid) AS popularity1
	 from cheersu_cliques LEFT JOIN cheersu_cliques_activity
	ON clique_activity_cliqueid = clique_id WHERE clique_id IN 
	( SELECT member_cliqueid FROM cheersu_clique_members WHERE member_userid = $userid)
	GROUP BY id ORDER BY popularity1 DESC";
	$result = mysql_query($query);
	if(!$result){
		$data['nocliques'] = true;
	}
	else{
		error_log(mysql_num_rows($result),0);
		$cliques = array();
		while($temp = mysql_fetch_assoc($result)){
			if($temp['img'] == ''){
				$temp['img'] = 'cheersu_icon.png';
			}
			array_push($cliques,$temp);
		}
		$data['cliques'] = $cliques;
	}
	
	$query = "SELECT user_id, CONCAT(user_firstname,' ',user_lastname) AS user_name FROM cheersu_friends_$userid INNER JOIN ".
	"cheersu_users ON user_id = friend_user_id WHERE friend_status = 'confirmed' ORDER BY user_name";
	$result = mysql_query($query);
	$friends = mysql_num_rows($result);
	$data['numfriends'] = $friends;
	if($friends == 0){
		$nofriends = true;
		$data['nofriends'] = true;
	}
	else{
		$taggingfriendlist = array();
		$list = "($userid,";
		while($temp = mysql_fetch_assoc($result)){
			$list.=$temp['user_id'].",";
			array_push($taggingfriendlist,$temp);
			
		}
		$data['friendlist'] = $taggingfriendlist;
		// 			//error_log("LIST before:".$list,0);
		$list = substr($list, 0, -1).")";
		
		$query = "SELECT activity_id FROM cheersu_activity WHERE activity_user_id IN $list ORDER BY activity_timestamp DESC LIMIT 100";
		//error_log("activityidquery:$query",0);
		$result = mysql_query($query);
		if(!$result){
			die("unable to proceed");
		}
		
		$commentarray = array();
		if(mysql_num_rows($result) != 0){
			$activityidlist = "(";
			while($temp = mysql_fetch_row($result)){
				$activityidlist.=$temp[0].",";
			}
			$activityidlist = substr($activityidlist, 0, -1).")";
			
			$query = "SELECT user_comment_id AS commentid,user_comment_activity_id AS actid,user_comment_message AS comment,user_comment_id AS id,user_comment_timestamp AS time,
			 		CONCAT(user_firstname,' ',user_lastname) AS name,user_id AS userid,user_dp_icon AS dp 
					FROM cheersu_user_comments INNER JOIN cheersu_users ON user_comment_user_id = user_id WHERE user_comment_activity_id IN $activityidlist
					ORDER BY user_comment_activity_id DESC,user_comment_timestamp DESC";
			//error_log("commentquery:$query",0);
			
			$result = mysql_query($query);
			if(!$result){
				die("unable to proceed");
			}
			
			while($temp = mysql_fetch_assoc($result)){
				$activityid = $temp['actid'];
				$temp['comment'] = stripslashes($temp['comment']);
				if(array_key_exists($activityid,$commentarray)){
					if(count($commentarray[$activityid]) < 3){
						
						array_unshift($commentarray[$activityid],$temp);
					}
				}
				else{
					$commentarray[$activityid] = array();
					array_unshift($commentarray[$activityid],$temp);
				}
			}
		}
		// print_r($commentarray);
		
		
			
		$query = "SELECT activity_id,user_id,user_firstname,user_lastname,user_dp_icon,venue_name,school_name,activity_type,activity_comment,".
				"school_id,activity_timestamp, datediff(now(),activity_timestamp) as datedif , timediff(now(),activity_timestamp) as timedif,".
				" COUNT(distinct like_user_id) AS likes , COUNT(distinct user_comment_id) AS user_comments, like_act ".
				" FROM cheersu_activity INNER JOIN cheersu_users ON activity_user_id = user_id INNER JOIN (cheersu_venues INNER JOIN ".
				"cheersu_schools ON venue_schoolid = school_id) ON activity_venue_id = venue_id LEFT OUTER JOIN cheersu_likes ON ".
				"like_activity_id = activity_id LEFT OUTER JOIN cheersu_user_comments ON user_comment_activity_id = activity_id ".
				"LEFT OUTER JOIN (SELECT like_activity_id AS like_act FROM cheersu_likes WHERE like_user_id = '$userid') ld ON like_act = activity_id ".
				"WHERE user_id IN $list GROUP BY activity_id ORDER BY activity_timestamp DESC LIMIT 100";
		//error_log("LIST before:".$query,0);
		$stmt = $pdo->prepare($query);
		$stmt->execute();
		if($stmt->rowCount() == 0){
			$nofriendactivity = true;
			$data['nofriendactivity'] = true;
		}
		else{
			$activitylist = array();
			$tempactivity = array();
			while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
				$tempactivity = array();
				$tempactivity['dp'] = $temp['user_dp_icon'];
				if($tempactivity['dp'] == "")
					$tempactivity['dp'] = "cheersu_icon.png";
					
				$type = $temp['activity_type'];
				$tempactivity['activity_id'] = $temp['activity_id'];
				if(array_key_exists($temp['activity_id'],$commentarray)){
					$tempactivity['topcomments'] = $commentarray[$temp['activity_id']];
				}
				$tempactivity['comment'] = stripslashes($temp['activity_comment']);
				$tempactivity['venue'] = stripslashes($temp['venue_name']);
				$tempactivity['schoolname'] = stripslashes($temp['school_name']);
				$tempactivity['likes'] = $temp['likes'];
				$tempactivity['liked'] = $temp['like_act'];
				$tempactivity['user_comments'] = $temp['user_comments'];
				if($type == "checkin"){
					$tempactivity['type'] = "Checked in to";
				}
				else if($type == "reserve"){
					$tempactivity['type'] = "Made a reservation at";
				}
				else if($type == "comment"){
					$tempactivity['type'] = "commented on";
				}
				else if($type == "taggedstatus"){
					$tempactivity['status'] = true;
					$activityid = $tempactivity['activity_id'];
					$query = "SELECT user_tag_userid AS id,user_tag_username AS name FROM cheersu_user_tag WHERE user_tag_activityid = '$activityid'";
					$result3 = mysql_query($query);
					if($result3){
						$taglist = array();
						while($tempres = mysql_fetch_assoc($result3)){
							array_push($taglist,$tempres);
						}
						$tempactivity['tagged'] = $taglist;
					}
					else{
						die("error");
					}
				}
				else if($type == "status"){
					$tempactivity['type'] = "updated his status:";
					$comment = $tempactivity['comment'];
					// $taglist = explode("|", $comment);
					// if(count($taglist) > 1){
						// error_log("Taglist:".$taglist[1],0);
						// $json_str = array_splice($taglist,-1);
						// $json = json_decode($json_str[0]);
						// error_log("Array_size:".count($json),0);
						// if($json != NULL || count($json) > 0){
							// $tempactivity['tagged'] = $json;
							// $originalcomment = implode("|",array_slice($taglist,-1));
							// $tempactivity['comment'] = $originalcomment;
						// }
					// }
					$tempactivity['status'] = true;
					
				}
				else if($type == "photo-activity"){
// 					//error_log("name:".$temp['user_firstname']);
					$photos = array();
					$numphotos = 0;
					$timestamp = $temp['activity_timestamp'];
					//error_log("timestamp:$timestamp");
					$query = "SELECT user_photo_source FROM cheersu_user_photos WHERE user_photo_timestamp = '$timestamp'";
					$result = mysql_query($query);
					if(!$result){
						die("unable to query databases");
					}
					else{
						$numphotos = mysql_num_rows($result);
						while($tempimg = mysql_fetch_assoc($result)){
							array_push($photos,$tempimg['user_photo_source']);
							//error_log("img".$tempimg,0);
							
						}
					}
					$tempactivity['photoactivity'] = true;
					$tempactivity['photos'] = $photos;
					$tempactivity['numphotos'] = $numphotos;
				}
				else if(stristr($type, "external") != false){
					//error_log("$type",0);
					$tempactivity['external'] = true;
					$typearray = explode("|",$type);
					$tempact = $typearray[0];
					//error_log("$tempact",0);
					if($tempact == "reserve"){
						$tempactivity['type'] = "Made a reservation at";
					}
					else if($tempact == "checkin"){
						$tempactivity['type'] = "Checked in to";
					}
					$details = explode("|",$tempactivity['comment']);
					$tempactivity['schoolname'] = $details[1];
					$tempactivity['venue'] = $details[0];
				}
					
				$tempactivity['name'] = stripslashes($temp['user_firstname']." ".$temp['user_lastname']);
				$tempactivity['schoolid'] = $temp['school_id'];
				$tempactivity['id'] = $temp['user_id'];
				$datediff = $temp['datedif'];
				$timediff = $temp['timedif'];
				if($datediff > 0){
					$tempactivity['timestamp'] = $datediff."d";
						
				}
				else{
					$timearray = explode(":", $timediff);
					if($timearray[0] > 0){
						if($timearray[0]<10){
							$timearray[0] = substr($timearray[0], 1,1);
						}
						$tempactivity['timestamp'] = $timearray[0]."h";

					}
					else if($timearray[1] > 0){
						if($timearray[1]<10){
							$timearray[1] = substr($timearray[1], 1,1);
						}
						$tempactivity['timestamp'] = $timearray[1]."m";
					}
					else{
						if($timearray[2]<10){
							$timearray[2] = substr($timearray[2], 1,1);
						}
						$tempactivity['timestamp'] = $timearray[2]."s";
					}
				}
				array_push($activitylist,$tempactivity);
			}
			$data['activitylist'] = $activitylist;
			
		}
	}
 	// $suggestions = generate_suggestions();
 	// if(count($suggestions) > 0){
		// $data['suggestions'] = $suggestions;
	// }
	// print_r($data);
	echo $twig->render("dashboard.twig",$data);
	

}

?>