<?php
	session_start();
	if(isset($_SESSION['email_pending'])){
		header("Location:staging.php");
	}
	include 'twiginit.php';
	include 'check_authorization.php';
	
	if((isset($_POST['firstname']) && isset($_POST['lastname']) && isset($_POST['bio']) && isset($_POST['email'])
			&& isset($_POST['school']))){
		include 'connect.php';
		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
		$email = $_POST['email'];
		$bio = $_POST['bio'];
		$userid = $_SESSION['user_id'];
		$school = $_POST['school'];
		if(isset($_FILES["file"])){
		
			if ($_FILES["file"]["error"] > 0)
			{
				$filename = "";
			}
			else
			{
				$extarray = explode(".", $_FILES['file']['name']);
				$extension = $extarray[count($extarray)-1];
				error_log("extension|$extension",0);
				$name = md5($_FILES["file"]["name"].$_SESSION['user_firstname']);
				$name .= ".$extension";	
				if (file_exists("img/$name")){
						
				}
				else
				{
					move_uploaded_file($_FILES["file"]["tmp_name"],
							"img/$name");
							
					include 'ajaxapi/SimpleImage.php';
					$image = new SimpleImage();
					$image->load("img/$name");
					if($image->getWidth() > 800)
						$image->resizeToWidth("800");
					$image->save("img/$name");
					define('DESIRED_IMAGE_WIDTH', 200);
					define('DESIRED_IMAGE_HEIGHT', 200);
					
					$source_path = "img/$name";
					
					/*
					 * Add file validation code here
					*/
					
					list($source_width, $source_height, $source_type) = getimagesize($source_path);
					$type = "";
					switch ($source_type) {
						case IMAGETYPE_GIF:{
							$source_gdim = imagecreatefromgif($source_path);
							$type = "GIF";
							break;
						}
						case IMAGETYPE_JPEG:{
							$type = "jpeg";
							$source_gdim = imagecreatefromjpeg($source_path);
							break;
						}
						case IMAGETYPE_PNG:{
							$type = "png";
							$source_gdim = imagecreatefrompng($source_path);
							break;
						}
					}
					
					$source_aspect_ratio = $source_width / $source_height;
					$desired_aspect_ratio = DESIRED_IMAGE_WIDTH / DESIRED_IMAGE_HEIGHT;
					
					if ($source_aspect_ratio > $desired_aspect_ratio) {
						/*
						 * Triggered when source image is wider
						*/
						$temp_height = DESIRED_IMAGE_HEIGHT;
						$temp_width = ( int ) (DESIRED_IMAGE_HEIGHT * $source_aspect_ratio);
					} else {
						/*
						 * Triggered otherwise (i.e. source image is similar or taller)
						*/
						$temp_width = DESIRED_IMAGE_WIDTH;
						$temp_height = ( int ) (DESIRED_IMAGE_WIDTH / $source_aspect_ratio);
					}
					
					/*
					 * Resize the image into a temporary GD image
					*/
					
					$temp_gdim = imagecreatetruecolor($temp_width, $temp_height);
					imagecopyresampled(
					$temp_gdim,
					$source_gdim,
					0, 0,
					0, 0,
					$temp_width, $temp_height,
					$source_width, $source_height
					);
					
					/*
					 * Copy cropped region from temporary image into the desired GD image
					*/
					
					$x0 = ($temp_width - DESIRED_IMAGE_WIDTH) / 2;
					$y0 = ($temp_height - DESIRED_IMAGE_HEIGHT) / 2;
					$desired_gdim = imagecreatetruecolor(DESIRED_IMAGE_WIDTH, DESIRED_IMAGE_HEIGHT);
					imagecopy(
					$desired_gdim,
					$temp_gdim,
					0, 0,
					$x0, $y0,
					DESIRED_IMAGE_WIDTH, DESIRED_IMAGE_HEIGHT
					);
					
					/*
					 * Render the image
					* Alternatively, you can save the image in file-system or database
					*/
					if($type == "jpeg"){
						imagejpeg($desired_gdim,"img/icons/$name");
					}
					else if($type == "png"){
						imagepng($desired_gdim,"img/icons/$name");
					}
					else if($type = "gif"){
						imagegif($desired_gdim,"img/icons/$name");
					}
					//echo "Stored in: " . "images/" . $_FILES["file"]["name"];
					$filename = $name;
				}
			}
		}
		else{
			$filename = "";
		}
		$query = "SELECT * FROM cheersu_users WHERE user_id = '$userid'";
// 		echo $userid;
		$result = mysql_query($query);
		if(!$result){
			die("Unable to interact with database");
		}
		else{
			$temp = mysql_fetch_assoc($result);
			$emailold = $temp['user_email'];
			
			if($filename == ""){
				$filename = $temp['user_dp'];
			}
// 			echo $emailold;
		}
  		//echo $filename." $email $firstname $bio $emailold";
		if($emailold == $email){
// 			echo "No change";
			$query = "UPDATE cheersu_users SET user_firstname = ?,user_lastname=?, user_bio = ?, user_dp = ?,user_dp_icon=?,user_schoolid = ? WHERE user_id = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($firstname,$lastname,$bio,$filename,$filename,$school,$userid));
			$_SESSION['username'] = $firstname;
			header("Location:settings.php");
		}
		else{
// 			echo "Change";
			$query = "SELECT * FROM cheersu_users WHERE user_email = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($email));
			if($stmt->rowCount() == 0){
// 				$str = "$email$name";
// 				$hash = md5($str);
// 				$query = "UPDATE cheersu_users SET user_email_pending = ?, user_firstname = ?,user_lastname=?, user_bio = ?, user_dp = ?,user_schoolid=? user_verification_id = ? WHERE user_id = ?";
// 				$stmt = $pdo->prepare($query);
// 				$stmt->execute(array($email,$firstname,$lastname,$bio,$filename,$school,$hash,$userid));
// 				$subject = "[cheersu] Verification of new email id for $firstname";
// 				$message = "You changed your email id recently\n".
// 						"Please click on the link below to verify your account, you won't be able\n".
// 						"to use this email id:\n".
// 						"http://cheersu.com/verifyemailchange.php?userid=".urlencode($userid)."&hash=$hash\n";
// 				$from = "no-reply@cheersu.com";
// 				$headers = "From: Cheersu <$from> ";
// 				mail($email, $subject, $message,$headers);
				$query = "UPDATE cheersu_users SET user_email = ?, user_firstname = ?,user_lastname=?, user_bio = ?, user_dp = ? WHERE user_id = ?";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($email,$firstname,$lastname,$bio,$filename,$userid));
				$_SESSION['username'] = $firstname;
				header("Location:settings.php");
			}
			else{
				echo $twig->render("staging.html",array(
						"title"=>"Email already exists",
						"message" =>"The Email you entered already exists !"
				));
			}
		}
	}
	else{
		echo $twig->render("staging.html",array(
				"title"=>"Improper arguments passed",
				"message" =>"Improper arguments passed to the script"
				));
	}
?>