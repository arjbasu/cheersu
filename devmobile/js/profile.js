var ajaxinprogress = false;
$(document).on("pageinit",function(){
	check_notifications();
});
$(document).on("pageshow",function(){
	$("body").ajaxStart(function(){
		ajaxinprogress = true;
	});
	$("body").ajaxStop(function(){
		ajaxinprogress = false;
	});
	$("#addfriend").click(function(){
		if(ajaxinprogress == true){
			return false;
		}
		$.mobile.loading('show');
		var userid = $("#addfriend").attr("data-userid");
		var data = {"user_id":userid,"action":"add"};
		console.log(data);
		$.ajax({
			url:"ajaxapi/friend.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated")
					window.location = "index.php";
				else if(data.status == "success"){
					$.mobile.loading('hide');
					$("#notification p").html(data.message).parents("#notification").popup("open");
					setTimeout(function(){ window.location = "";},600);
				}
				else{
					$.mobile.loading('hide');
					$("#notification p").html(data.message).parents("#notification").popup("open");
				}
			},
			error:function(){
				$.mobile.loading('hide');
				$("#notification p").html("An error occurred").parents("#notification").popup("open");
			}
		});
		return false;
		
	});
	
	$("#unfriend").click(function(){
		if(ajaxinprogress == true){
			return false;
		}
		$.mobile.loading('show');
		var userid = $("#unfriend").attr("data-userid");
		var data = {"user_id":userid,"action":"delete"};
		console.log(data);
//		console.log(data);
		$.ajax({
			url:"ajaxapi/friend.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated")
					window.location = "index.php";
				else if(data.status == "success"){
					$.mobile.loading('hide');
					$("#notification p").html(data.message).parents("#notification").popup("open");
					setTimeout(function(){ window.location = "";},600);
				}
				else{
					$.mobile.loading('hide');
					$("#notification p").html(data.message).parents("#notification").popup("open");
				}
			},
			error:function(){
				$.mobile.loading('hide');
				$("#notification p").html("An error occurred").parents("#notification").popup("open");
			}
		});
		return false;
	});
	
	$("#confirmfriend").click(function(){
		if(ajaxinprogress == true){
			return false;
		}
		$.mobile.loading('show');
		var userid = $("#confirmfriend").attr("data-userid");
		var data = {"user_id":userid,"action":"confirm"};
		console.log(data);
//		console.log(data);
		$.ajax({
			url:"ajaxapi/friend.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated")
					window.location = "index.php";
				else if(data.status == "success"){
					$.mobile.loading('hide');
					$("#notification p").html(data.message).parents("#notification").popup("open");
					setTimeout(function(){ window.location = "";},600);
				}
				else{
					$.mobile.loading('hide');
					$("#notification p").html(data.message).parents("#notification").popup("open");
				}
			},
			error:function(){
				$.mobile.loading('hide');
				$("#notification p").html("An error occurred").parents("#notification").popup("open");
			}
		});
		return false;
	});
});