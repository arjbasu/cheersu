<?php
session_start();
include 'check_authorization.php';
include 'connect.php';
include 'twiginit.php';
$userid = $_SESSION['user_id'];
if(isset($_POST['status'])){
	$status = $_POST['status'];
	if($status != ""){
		$query = "UPDATE cheersu_users SET user_status = ? WHERE user_id = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($status,$userid));
		$query = "INSERT INTO cheersu_activity(activity_user_id,activity_type,activity_venue_id,activity_comment) VALUES(?,?,?,?)";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($userid,"status","0",$status));
	}
}
header("Location:dashboard.php");
?>