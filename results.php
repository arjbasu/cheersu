<?php
	session_start();
	include 'check_authorization.php';
	if(!isset($_POST['searchquery'])){
		 header("Location:index.php");
	}
	include 'connect.php';
	include 'twiginit.php';
	
	$userid = $_SESSION['user_id'];
	$search = $_POST['searchquery'];
	
	$results = array();
	$newsearch = "%".$search."%";
	$wordsearch = str_replace(" ","%", $search);
	$wordsearch = "%".$wordsearch."%";
	$keywords = explode(" ", $search);
	$length = count($keywords);
	$regexpsearch = "^.*(";
	for($i = 0; $i < $length; $i++){
		$regexpsearch.= strtoupper($keywords[$i])."|";
	}
	$regexpsearch = substr($regexpsearch, 0,-1);
	$regexpsearch.= ").*$";
	
	error_log("Queries:$newsearch|$wordsearch|$regexpsearch");
	
	$query = "SELECT user_firstname,user_lastname,user_dp_icon AS user_dp,user_email,user_id,school_name FROM cheersu_users,cheersu_schools WHERE ".
			"user_verified = 1 AND user_id <> '$userid' AND user_blocked = 0 AND user_schoolid = school_id AND (user_email LIKE ? OR ".
			"UPPER(CONCAT_WS(' ', user_firstname,user_lastname)) LIKE UPPER(?) )";
	
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($newsearch,$newsearch));
	if($stmt->rowCount() >0){
		while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
			$temp['user_dp'] = stripslashes($temp['user_dp']);
			$temp['user_lastname'] = stripslashes($temp['user_lastname']);
			$temp['user_firstname'] = stripslashes($temp['user_firstname']);
			if($temp['user_dp'] == ""){
				$temp['user_dp'] = "cheersu_icon.png";
			}
			array_push($results, $temp);
		}
	}
	
	$query = "SELECT user_firstname,user_lastname,user_dp_icon AS user_dp,user_email,user_id,school_name FROM cheersu_users,cheersu_schools WHERE ".
			"user_verified = 1 AND user_id <> '$userid' AND  user_blocked = 0 AND user_schoolid = school_id AND (user_email LIKE ? OR ".
			"UPPER(CONCAT_WS(' ', user_firstname,user_lastname)) LIKE UPPER(?) )";
	
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($wordsearch,$wordsearch));
	if($stmt->rowCount() >0){
		while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
			$temp['user_dp'] = stripslashes($temp['user_dp']);
			$temp['user_lastname'] = stripslashes($temp['user_lastname']);
			$temp['user_firstname'] = stripslashes($temp['user_firstname']);
			if($temp['user_dp'] == ""){
				$temp['user_dp'] = "cheersu_icon.png";
			}
			array_push($results, $temp);
		}
	}
	
	$query = "SELECT user_firstname,user_lastname,user_dp_icon AS user_dp,user_email,user_id,school_name FROM cheersu_users,cheersu_schools WHERE ".
			"user_verified = 1 AND user_id <> '$userid' AND user_blocked = 0 AND user_schoolid = school_id AND (user_email REGEXP ? OR UPPER(CONCAT_WS(' ', user_firstname,user_lastname)) REGEXP ? )";
	
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($regexpsearch,$regexpsearch));
	if($stmt->rowCount() >0){
		while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
			$temp['user_dp'] = stripslashes($temp['user_dp']);
			$temp['user_lastname'] = stripslashes($temp['user_lastname']);
			$temp['user_firstname'] = stripslashes($temp['user_firstname']);
			if($temp['user_dp'] == ""){
				$temp['user_dp'] = "cheersu_icon.png";
			}
			array_push($results, $temp);
		}
	}
	
	$results = array_map("unserialize", array_unique(array_map("serialize", $results)));
	error_log(sizeof($results),0);
	$data = array();
	$search = stripslashes($search);
	$data['title'] = "Search Results for $search";
	$data['searchquery'] = $search;
	if(sizeof($results) == 0){
		$data['noresults'] = true;
	}
	else{
		
		$data['userresults'] = $results;
	}
	$data['numuserresults'] = count($results);
	
	$schoolresults = array();
	$query = "SELECT school_name AS name, school_id AS id,COUNT(venue_id) AS count FROM cheersu_schools LEFT JOIN cheersu_venues 
	ON venue_schoolid = school_id FROM cheersu_schools WHERE UPPER(school_name) LIKE ? GROUP BY school_name";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($newsearch));
	if($stmt->rowCount() > 0){
		while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
			$temp['name'] = stripslashes($temp['name']);
			array_push($schoolresults,$temp);
		}
	}
	
	$query = "SELECT school_name AS name, school_id AS id,COUNT(venue_id) AS count FROM cheersu_schools LEFT JOIN cheersu_venues 
	ON venue_schoolid = school_id FROM cheersu_schools WHERE UPPER(school_name) LIKE ? GROUP BY school_name";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($wordsearch));
	if($stmt->rowCount() > 0){
		while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
			$temp['name'] = stripslashes($temp['name']);
			array_push($schoolresults,$temp);
		}
	}
	
	$query = "SELECT school_name AS name, school_id AS id,COUNT(venue_id) AS count FROM cheersu_schools LEFT JOIN cheersu_venues 
	ON venue_schoolid = school_id WHERE school_name REGEXP ? GROUP BY school_name";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($regexpsearch));
	if($stmt->rowCount() > 0){
		while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
			$temp['name'] = stripslashes($temp['name']);
			array_push($schoolresults,$temp);
		}
	}
	
	$schoolresults = array_map("unserialize", array_unique(array_map("serialize", $schoolresults)));
	if(sizeof($schoolresults) == 0){
		$data['noschoolresults'] = true;
	}
	else{
		
		$data['schoolresults'] = $schoolresults;
	}
	
	$data['numschoolresults'] = count($schoolresults);
	
	$venueresults = array();
	$query = "SELECT venue_name AS name, venue_id AS id, venue_address AS img FROM cheersu_venues WHERE UPPER(venue_name) LIKE UPPER(?)";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($newsearch));
	if($stmt->rowCount() > 0){
		while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
			$temp['name'] = stripslashes($temp['name']);
			$temp['img'] = urlencode($temp['img']);
			array_push($venueresults,$temp);
		}
	}
	
	$query = "SELECT venue_name AS name, venue_id AS id, venue_address AS img FROM cheersu_venues WHERE UPPER(venue_name) LIKE ()?)";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($wordsearch));
	if($stmt->rowCount() > 0){
		while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
			$temp['name'] = stripslashes($temp['name']);
			$temp['img'] = urlencode($temp['img']);
			array_push($venueresults,$temp);
		}
	}
	
	$query = "SELECT venue_name AS name, venue_id AS id, venue_address AS img FROM cheersu_venues WHERE venue_name REGEXP ?";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($regexpsearch));
	if($stmt->rowCount() > 0){
		while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
			$temp['name'] = stripslashes($temp['name']);
			$temp['img'] = urlencode($temp['img']);
			array_push($venueresults,$temp);
		}
	}
	
	$venueresults = array_map("unserialize", array_unique(array_map("serialize", $venueresults)));
	if(sizeof($venueresults) == 0){
		$data['novenueresults'] = true;
	}
	else{
		
		$data['venueresults'] = $venueresults;
	}
	
	$data['numvenueresults'] = count($venueresults);
	if(count($venueresults) >= count($schoolresults)){
		if(count($results) >= count($venueresults)){
			$data['usermax'] = true;
		}		
		else{
			$data['venuemax'] = true;
		}
	}
	else{
		if(count($results) >= count($schoolresults)){
			$data['usermax'] = true;
		}		
		else{
			$data['schoolmax'] = true;
		}
	}
	$query = "SELECT user_dp_icon FROM cheersu_users WHERE user_id = $userid";
	$result = mysql_query($query);
	$temp = mysql_fetch_row($result);
	$data['chat_dp_icon'] = $temp[0];
	
	echo $twig->render("results.twig",$data);
?>