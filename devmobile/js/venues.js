var clickcount = 0;
$(document).on('pageload', function(event, ui) {
  //Find all of the pages and dialogs in the DOM
    var response = ui.xhr.responseText;
    var data = $(response).filter('[data-role="dialog"]');
    console.log(data[1]);

  //Make sure that the given psuedo page does not already exist before adding it
  //Skip the first matched element, since jQM automatically inserted it for us
    for (var i = 0; i <= data.length - 1; i++) {
      var current = data.eq(i);

      if (current.attr('id') && !document.getElementById(current.attr('id'))) {
        current.appendTo('body');
      }
    }
  });

$(document).on("pageshow",function(){
	check_notifications();
	idparam = $("#schoolidparam").html();
	console.log("schoolid:"+idparam);
	console.log("pageinit");
	var ajaxinprogress = false;
	$("body").ajaxStart(function(){
		ajaxinprogress = true;
	});
	$("body").ajaxStop(function(){
		ajaxinprogress = false;
	});
	
	$(".checkin").click(function(){
		console.log("checkin called");
		if(ajaxinprogress === true)
			return false;
		
		$.mobile.loading('show');
		var venueid = $(this).parents(".collapsible").attr("data-venueid");
		var data = {"venue_id":venueid,"action":"set"};
		var parent = this;
		$.ajax({
			url:"ajaxapi/checkin.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				else if(data.status == "success"){
					$.mobile.loading('hide')
					var checkins = $(parent).parents(".collapsible").find(".checkins .numcheckins").html();
					checkins = parseInt(checkins)+1;
					$(parent).parents(".collapsible").find(".checkins .numcheckins").html(checkins);
					$("#notification").find("p").html(data.message).parents("#notification").popup("open");
					
				}
				else{
					$.mobile.loading('hide')
					$("#notification").find("p").html(data.message).parents("#notification").popup("open");
				}
			},
			error:function(){
				$.mobile.loading('hide')
				$("#notification").find("p").html("An error occurred").parents("#notification").popup("open");
			}
		});
		return false;
	});
	
	$(".reserve").click(function(){
		if(ajaxinprogress === true)
			return false;
		$.mobile.loading('show');
		var venueid = $(this).parents(".collapsible").attr("data-venueid");
		var data = {"venue_id":venueid,"action":"set"};
		var parent = this;
		$.ajax({
			url:"ajaxapi/reserve.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				else if(data.status == "success"){
					$.mobile.loading('hide')
					var checkins = $(parent).parents(".collapsible").find(".reservations .numreservations").html();
					checkins = parseInt(checkins)+1;
					$(parent).parents(".collapsible").find(".reservations .numreservations").html(checkins);
					$("#notification").find("p").html(data.message).parents("#notification").popup("open");
					
				}
				else{
					$.mobile.loading('hide')
					$("#notification").find("p").html(data.message).parents("#notification").popup("open");
				}
			},
			error:function(){
				$.mobile.loading('hide')
				$("#notification").find("p").html("An error occurred").parents("#notification").popup("open");
			}
		});
		return false;
		
	});
	$(".comment").click(function(){
		console.log("comment");
		if(ajaxinprogress === true)
			return false;
		$.mobile.loading('show');
		var venueid = $(this).parents(".collapsible").attr("data-venueid");
		var data = {"venue_id":venueid,"action":"get"};
		$.ajax({
			url:"ajaxapi/comment.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				else if(data.status == "success"){
					$("#commentdialog").attr("data-venueid",venueid)
					$("#prevcomments").html("");
					if(data.message != "No comments available"){
						$.mobile.loading('hide')
						comments = data.message;
						var html = "<table>";
						for(var i = 0; i < comments.length; i++){
							html +=	"<tr><td class = 'dp-container'><img src = 'http://cheersu.com/img/"+ comments[i].dp + "' class = 'dp-container'/></td>"+ 
								"<td><h3>"+comments[i].name + "</h3>"+
									"<p>" + comments[i].comment + "</p></td>";
						}
						html+="</table>"
						$("#commentdialog #prevcomments").html(html).parents("#commentdialog").attr("data-venueid",venueid);
						$.mobile.changePage("#commentdialog");
					}
					else{
						$.mobile.loading('hide')
						$("#prevcomments").html("<h3 class = 'nocomments'>Add the first comment</h3>");
						$.mobile.changePage("#commentdialog");
					}
				}
				else{
					$.mobile.loading('hide')
					$("#notification").find("p").html(data.message).parents("#notification").popup("open");
				}
				
			},
			error:function(){
				$("#notification").find("p").html("An error occurred").parents("#notification").popup("open");
			}
		})
		
		return false;
		
	});
	
	$("#commentform").submit(function(){
		if(ajaxinprogress === true)
			return false;
		$.mobile.loading('show');
		var comment = $("#comment").val();
		var venueid = $(this).parents("#commentdialog").attr("data-venueid");
		var data = {"venue_id":venueid,"comment":comment,"action":"set"};
		$.ajax({
			url:"ajaxapi/comment.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				else if(data.status == "success"){
					$.mobile.loading('hide')
					$("#notification-dialog").find("p").html(data.message).parents("#notification-dialog").popup("open");
					$("#commentform").trigger("reset");
					$.mobile.changePage("venues.php");
				}
				else{
					$.mobile.loading('hide')
					$("#notification-dialog").find("p").html(data.message).parents("#notification-dialog").popup("open");
				}
			},
			error:function(){
				$.mobile.loading('hide')
				$("#notification-dialog").find("p").html("An error occurred").parents("#notification-dialog").popup("open");
			}
		});
		
		return false;
	})
	
	$(".reservations").click(function(){
		if(ajaxinprogress === true)
			return false;
		$.mobile.loading('show');
		var venueid = $(this).parents(".collapsible").attr("data-venueid");
		var data = {"venue_id":venueid,"action":"get"};
		console.log(data);
		$.ajax({
			url:"ajaxapi/reserve.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				else if(data.status == "success"){
					$.mobile.loading('hide')
					entries = data.message;
					html = "";
					length = entries.length;
					for(var i = 0;i < length; i++){
						html+= "<tr>" +
								"<td class = 'dp-container'><img class = 'dp-container' src = 'http://cheersu.com/img/"+entries[i].dp+"'/></td>" +
								"<td><h4><a class = 'userlink' href = 'profile.php?userid="
								+entries[i].userid+"'>" + entries[i].name + "</a></h4></td>" +
								"</tr>";
					}
					$("#statsdialog .header h3").html("Reservations for tonight");
					$("#statsdialog .content table").html(html).parents("#statsdialog");
					$.mobile.changePage("#statsdialog");
				}
				else{
					$.mobile.loading('hide')
					$("#notification").find("p").html(data.message).parents("#notification").popup("open");
				}
			},
			error:function(){
				$.mobile.loading('hide')
				$("#notification").find("p").html("An error occurred").parents("#notification").popup("open");
			}
		});
		return false;
	});
	
	$(".checkins").click(function(){
		if(ajaxinprogress === true)
			return false;
		$.mobile.loading('show');
		var venueid = $(this).parents(".collapsible").attr("data-venueid");
		var data = {"venue_id":venueid,"action":"get"};
		console.log(data);
		$.ajax({
			url:"ajaxapi/checkin.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				else if(data.status == "success"){
					$.mobile.loading('hide')
					entries = data.message;
					html = "";
					length = entries.length;
					html = "";
					for(var i = 0;i < length; i++){
						html+= "<tr>" +
								"<td class = 'dp-container'><img class = 'dp-container' src = 'http://cheersu.com/img/"+entries[i].dp+"'/></td>" +
								"<td><h4><a class = 'userlink' href = 'profile.php?userid="
								+entries[i].userid+"'>" + entries[i].name + "</a></h4></td>" +
								"</tr>";
					}
					$("#statsdialog .header h3").html("Checkins for tonight");
					$("#statsdialog .content table").html(html).parents("#statsdialog");
					$.mobile.changePage("#statsdialog");
				}
				else{
					$.mobile.loading('hide')
					$("#notification").find("p").html(data.message).parents("#notification").popup("open");
				}
			},
			error:function(){
				$.mobile.loading('hide')
				$("#notification").find("p").html("An error occurred").parents("#notification").popup("open");
			}
		});
		return false;
	});
	
	
	$(".close-dialog").click(function(){
		if(clickcount == 0){
			idparam = $("#schoolidparam1").html();
			console.log("closedialog clicked");
			console.log("schoolid:"+idparam);
			$.mobile.changePage("venues.php?schoolid="+idparam);
			
			
			clickcount++;
		}
	})
	
});