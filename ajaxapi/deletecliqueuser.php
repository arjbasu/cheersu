<?php
session_start();
include 'authentication_ajax_api.php';
include '../connect.php';
$userid = $_SESSION['user_id'];
if(!(isset($_POST['clique_id']) && isset($_POST['user_id']))){
	$status = "error";
	$message = "Improper parameters passed";
	include 'json_encoding.php';
	die();
}
$cliqueid = $_POST['clique_id'];
$cliqueuserid = $_POST['user_id'];

$query = "DELETE FROM cheersu_cliques_users_$userid WHERE clique_user_id = ? AND clique_myclique_id = ?";
$stmt = $pdo->prepare($query);
$stmt->execute(array($cliqueuserid,$cliqueid));
if($stmt->rowCount() != 1){
	$status = "error";
	$message = "Unable to delete from database";
}
else{
	$query = "SELECT * FROM cheersu_cliques_users_$userid WHERE clique_user_id = ?";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($cliqueuserid));
	if($stmt->rowCount() == 0){
		$query = "DELETE FROM cheersu_notifications_$userid WHERE notification_user_id = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($cliqueuserid));
	}
	$status = "success";
	$message = "User successfully deleted";
}

include 'json_encoding.php';
?>