<?php
	session_start();
	include 'authentication_ajax_api.php';
	include '../connect.php';
	if(isset($_POST['user_id'])){
		$userid = $_SESSION['user_id'];
		$blocked_userid = $_POST['user_id'];
		$userid = stripslashes($userid);
		$query = "INSERT INTO cheersu_hidden(hidden_userid,hidden_blocked_userid) VALUES (?,?)";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($userid,$blocked_userid));
		if($stmt->rowCount() == 1){
			$status = "success";
			$message = "";	
		}
		else {
			$status = "error";
			$message = "Unable to insert into database";	
		}
		
	}
	else {
		$status = "error";
		$message = "Improper parameters passed";	
	}
	include 'json_encoding.php';
?>