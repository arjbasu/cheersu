<?php
	session_start();
	include 'authentication_ajax_api.php';
	$status = "";
	$message = "";
	if(isset($_POST['action']) && isset($_POST['activity_id']) && $_POST['activity_id'] > 0 && isset($_POST['type'])){
		include '../connect.php';
		$userid = $_SESSION['user_id'];
		$type = $_POST['type'];
		$action = $_POST['action'];
		$activityid = $_POST['activity_id'];
		if($action == "like"){
			if($type == "post"){
				$query = "INSERT INTO cheersu_likes (like_user_id,like_cliqueactivity_id) VALUES (?,?)";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($userid,$activityid));
				if($stmt->rowCount() == 1){
					$query = "SELECT clique_activity_userid,clique_name,clique_id FROM cheersu_cliques_activity INNER JOIN
					cheersu_cliques ON clique_activity_cliqueid = clique_id  
					WHERE clique_activityid = ?";
					$stmt = $pdo->prepare($query);
					$stmt->execute(array($activityid));
					$temp = $stmt->fetch(PDO::FETCH_ASSOC);
					$activityuserid = $temp['clique_activity_userid'];
					$cliqueid = $temp['clique_id'];
					$cliquename = $temp['clique_name'];
					if($activityuserid != $userid){
						$query = "INSERT INTO cheersu_notifications_$activityuserid(notification_activity_id,notification_user_id,notification_type) 
						VALUES (?,?,?)";
						//error_log("Activityquery:".$query,0);
						$stmt = $pdo->prepare($query);
						$stmt->execute(array($activityid,$userid,"clique|like|$cliquename|$cliqueid"));
						if($stmt->rowCount() == 1){
							$status = "success";
							$message = "Post successfully liked";
						}
						else{
							$status = "error";
							$message = "Unable to like post";
						}
					}
					else{
						$status = "success";
						$message = "Post successfully liked";	
					}
					
				}
				else{
					$status = "error";
					$message = "unable to like post";
				}
			}
			else if($type == "get"){
				$query = "SELECT CONCAT(user_firstname,' ',user_lastname) AS user_name,user_dp_icon FROM cheersu_likes INNER JOIN cheersu_users ".
				"ON like_user_id = user_id WHERE like_cliqueactivity_id = ?";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($activityid));
				if($stmt->rowCount() == 0){
					$status = "error";
					$message = "No results";
				}
				else if($stmt->rowCount()>0){
					$status = "success";
					$message = array();
					while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
						array_push($message,$temp);
					}
				}
			}
			else if($type == "delete"){
				$query = "DELETE FROM cheersu_likes WHERE like_cliqueactivity_id = ? && like_user_id = ?";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($activityid,$userid));
				if($stmt->rowCount()==1){
					$status = "success";
					$message = "Successfully unliked";
				}
				else{
					$status = "error";
					$message = "unable to unlike";
				}
			}
			else{
				$status = "error";
				$message = "Improper parameters passed";	
			}
		}
		else if($action == "comment"){
			
				if($type == "post"){
					if(isset($_POST['comment'])){
					$comment = $_POST['comment'];
					$query = "INSERT INTO cheersu_user_comments (user_comment_user_id,user_comment_cliqueactivityid,user_comment_message) VALUES (?,?,?)";
					$stmt = $pdo->prepare($query);
					$stmt->execute(array($userid,$activityid,$comment));
					if($stmt->rowCount() == 1){
						$query = "SELECT clique_activity_userid,clique_id,clique_name FROM cheersu_cliques_activity INNER JOIN
						cheersu_cliques ON clique_activity_cliqueid = clique_id  
						WHERE clique_activityid = ?";
						$stmt = $pdo->prepare($query);
						$stmt->execute(array($activityid));
						$temp = $stmt->fetch(PDO::FETCH_ASSOC);
						$activityuserid = $temp['clique_activity_userid'];
						$cliquename = $temp['clique_name'];
						$cliqueid = $temp['clique_id'];
						if($activityuserid != $userid){
							$query = "INSERT INTO cheersu_notifications_$activityuserid(notification_activity_id,notification_user_id,notification_type) 
							VALUES (?,?,?)";
							//error_log("Activityquery:".$query,0);
							$stmt = $pdo->prepare($query);
							$stmt->execute(array($activityid,$userid,"clique|comment|$cliquename|$cliqueid"));
							if($stmt->rowCount() == 1){
								$status = "success";
								$query = "SELECT user_comment_id FROM cheersu_user_comments WHERE user_comment_cliqueactivityid = ? AND user_comment_user_id = ? ORDER BY user_comment_timestamp DESC LIMIT 1";
								$stmt = $pdo->prepare($query);
								$stmt->execute(array($activityid,$userid));
								$temp = $stmt->fetch(PDO::FETCH_ASSOC);
								$message = $temp['user_comment_id'];
							}
							else{
								$status = "error";
								$message = "Unable to comment";
							}
						
						}
						else{
							$status = "success";
							$query = "SELECT user_comment_id FROM cheersu_user_comments WHERE user_comment_cliqueactivityid = ? AND user_comment_user_id = ? ORDER BY user_comment_timestamp DESC LIMIT 1";
								$stmt = $pdo->prepare($query);
								$stmt->execute(array($activityid,$userid));
								$temp = $stmt->fetch(PDO::FETCH_ASSOC);
								$message = $temp['user_comment_id'];
						}
					}
					else{
						$status = "error";
						$message = "unable to comment on post";
					}
				}	
				else{
					$status = "error";
					$message = "Improper parameters passed";
				}
			}
			else if($type == "get"){
				$clause = "";
				if(isset($_POST['timestamp'])){
					$timestamp = $_POST['timestamp'];
					$clause = "AND user_comment_timestamp < '$timestamp'";
				}
				$query = "SELECT CONCAT(user_firstname,' ',user_lastname) AS user_name,user_dp_icon,user_comment_message AS user_comment, user_comment_user_id AS commentuserid,user_comment_id AS commentid FROM cheersu_user_comments INNER JOIN cheersu_users ".
				"ON user_comment_user_id = user_id WHERE user_comment_cliqueactivityid = ? $clause ORDER BY user_comment_timestamp DESC";
				//error_log("commentfetch:$query",0);
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($activityid));
				if($stmt->rowCount() == 0){
					$status = "success";
					$message = "No results";
				}
				else if($stmt->rowCount()>0){
					$status = "success";
					$message = array();
					while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
						if($temp['user_dp_icon'] == ""){
							$temp['user_dp_icon'] = "cheersu_icon.png";
						}		
						if($temp['commentuserid'] == $userid){
							$temp['del'] = true;
						}
						else{
							$temp['del'] = false;	
						}
						array_push($message,$temp);
					}
				}
			}
			else if($type == "delete"){
				if(isset($_POST['comment_id'])){
					$commentid = stripslashes($_POST['comment_id']);
					$query = "SELECT user_comment_user_id FROM cheersu_user_comments WHERE user_comment_id = '$commentid'";
					$result = mysql_query($query);
					if(mysql_num_rows($result) == 0){
						$status = "error";
						$message = "Comment does not exist";
					}
					else{
						$temp = mysql_fetch_row($result);
						$commentuserid = $temp[0];
						if($commentuserid == $userid){
							$query = "DELETE FROM cheersu_user_comments WHERE user_comment_id = ?";
							$stmt = $pdo->prepare($query);
							$stmt->execute(array($commentid));
							if($stmt->rowCount()==1){
								$status = "success";
								$message = "Comment Successfully deleted";
							}
							else{
								$status = "error";
								$message = "Unable to delete comment";
							}		
						}
						else{
							$status = "error";
							$message = "You cannot delete comments other than your own";
						}
					}
					
				}
				else{
					$status = "error";
					$message = "Improper parameters passed";
				}
			}
			else{
				$status = "error";
				$message = "Improper parameters passed";	
			}
		}
		else{
			$status = "error";
			$message = "Improper parameters passed(final)";
		}
		
	}
	include 'json_encoding.php';
?>