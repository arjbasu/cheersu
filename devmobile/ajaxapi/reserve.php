<?php
session_start();
include 'authentication_ajax_api.php';
if(isset($_POST['venue_id']) && isset($_POST['action'])){
	include '../connect.php';
	$action = $_POST['action'];
	$venueid = $_POST['venue_id'];
	if($action == "set"){
		$userid = $_SESSION['user_id'];
			
		$query = "SELECT * FROM cheersu_reservations WHERE reservation_user_id = ? ORDER BY reservation_timestamp DESC LIMIT 1";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($userid));
		if($stmt->rowCount() > 0){
			$temp = $stmt->fetch(PDO::FETCH_ASSOC);
			if ($venueid == $temp['reservation_venue_id']){
				$status = "error";
				$message = "You have currently made reservations for this venue";
				include 'json_encoding.php';
				die();
			}
		}
		
		$query = "INSERT INTO cheersu_reservations(reservation_user_id,reservation_venue_id) VALUES (?,?)";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($userid,$venueid));
		if($stmt->rowCount() == 1){
			$query = "INSERT into cheersu_activity(activity_user_id,activity_type,activity_venue_id) ".
					"VALUES(?,?,?)";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($userid,"reserve",$venueid));
			if($stmt->rowCount() == 1){
				$query = "SELECT * FROM cheersu_reservations WHERE reservation_user_id = '$userid' AND reservation_venue_id = '$venueid'";
				$result = mysql_query($query);
				if(!$result){
					$status = "error";
					$message = "Unable to checkin";
				}
				if(mysql_num_rows($result) == 1){
					$query = "UPDATE cheersu_venues SET venue_reservations = venue_reservations+1 WHERE venue_id = '$venueid'";
					$result = mysql_query($query);
					if(!$result){
						$status = "error";
						$message = "Unable to interact with database";
					}
					else if(mysql_affected_rows() == 1){
						$status = "success";
						$message = "You have successfully checked in";
					}	
				}
				else{
					$status = "success";
					$message = "You have successfully checked in";
				}
			}
			else{
				$status = "error";
				$message = "Unable to interact with database";
			}
			
		}
		else{
			$status = "error";
			$message = "Unable to interact with database";
		}
	}
	else if($action == "get"){
		$query = "SELECT user_id,user_firstname,user_lastname,user_dp ".
				"FROM cheersu_reservations,cheersu_users ".
				"WHERE user_id = reservation_user_id AND reservation_venue_id = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($venueid));
		if($stmt->rowCount() == 0){
			$status = "error";
			$message = "No Reservations for this venue yet";
		}
		else{
			$message = array();
			
			while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
				$tempcheckin = array();
				if($temp['user_dp'] == ""){
					$tempcheckin['dp'] = "cheersu_icon.png";
				}
				else{
					$tempcheckin['dp'] = $temp['user_dp'];
				}
				include 'removeslashes.php';
				$tempcheckin['userid'] = $temp['user_id'];
				$tempcheckin['name'] = $temp['user_firstname']." ".$temp['user_lastname'];
				$tempcheckin['time'] = $temp['reservation_timestamp'];
				
				array_push($message,$tempcheckin);
	
			}
			$message = array_map("unserialize", array_unique(array_map("serialize", $message)));
			$status = "success";
		}
	}
	else{
		$status = "error";
		$message = "Improper action";
	}
}
else{
	$status = "error";
	$message = "Improper parameters passed";
}
include 'json_encoding.php';
?>