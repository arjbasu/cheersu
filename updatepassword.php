<?php
	if(!(isset($_POST['newpassword']) && isset($_POST['confirmpassword']) && isset($_POST['hash'])
	&& isset($_POST['userid']))){
		$status = "error";
		$message = "Improper Parameters passed";
	}
	else{
		include 'connect.php';
		$newpass = $_POST['newpassword'];
		$confirmpass = $_POST['confirmpassword'];
		$hash = $_POST['hash'];
		$userid = $_POST['userid'];
		if($newpass != $confirmpass){
			$status = "error";
			$message = "Passwords do not match";
		}
		else{
			$query = "SELECT * FROM cheersu_users WHERE user_id = ? AND user_verification_id = ? AND user_password_reset = 1";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($userid,$hash));
			if($stmt->rowCount() != 1){
				$status = "error";
				$message = "Wrong password reset parameters";
			}
			else{
				$hashedpassword = crypt($newpass,'$1$foreverdope12$');
				$query = "UPDATE cheersu_users SET user_passhash = ? ,user_password_reset = 0 WHERE user_id = ?";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($hashedpassword,$userid));
				$status = "success";
				$message = "Password Successfully updated";
			}
		}
	}
	include 'json_encoding.php';
	?>