<?php
	session_start();
	include 'authentication_ajax_api.php';
	include 'connect.php';
	if(isset($_POST['venue_id']) && isset($_POST['action'])){
		$venueid = $_POST['venue_id'];
		$action = $_POST['action'];
		if($action == "delete"){
			$query = "DELETE FROM cheersu_venues WHERE venue_id = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($venueid));
			if($stmt->rowCount() < 0){
				$status = "error";
				$message = "Unable to interact with  database";
			}
			else{
				$status = "success";
				$message = "Venue Successfully deleted";
			}
		}
		else if($action == "update"){
			if(isset($_POST['venue_name']) && isset($_POST['venue_address']) && isset($_POST['venue_school'])){
				$venuename = $_POST['venue_name'];
				$venueaddress = $_POST['venue_address'];
				$venueschoolid = $_POST['venue_school'];
				$venueimg = $_POST['venue_img'];
				$venueid = $_POST['venue_id'];
				$query = "UPDATE cheersu_venues SET venue_name = ?, venue_address = ?, venue_schoolid = ?,venue_cover = ? WHERE venue_id = ?";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($venuename,$venueaddress,$venueschoolid,$venueimg,$venueid));
				$status = "success";
				$message = "Venue successfully updated";
			}
			else{
				$status = "error";
				$message = "Improper parameters passed for update";
			}
		}
		else if($action == "insert"){
			if(isset($_POST['venue_name']) && isset($_POST['venue_address']) && isset($_POST['venue_school'])){
				$venuename = $_POST['venue_name'];
				$venueaddress = $_POST['venue_address'];
				$venueschoolid = $_POST['venue_school'];
				$query = "INSERT INTO cheersu_venues(venue_name,venue_schoolid,venue_address) VALUES (?,?,?)";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($venuename,$venueschoolid,$venueaddress));
				if($stmt->rowCount() == 1){
					$status = "success";
					$message = "Venue successfully inserted";
				}
				else{
					$status = "error";
					$message = "Unable to insert into venue";
				}
			}
			else{
				$status = "error";
				$message = "Improper Parameters passed for insert";
			}
		}
		else if($action == "get"){
			$query = "SELECT venue_id,venue_name,venue_schoolid,venue_address,venue_cover FROM cheersu_venues WHERE venue_id = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($venueid));
			if($stmt->rowCount() != 1){
				$status = "error";
				$message = "No venues found";
			}
			else{
				$status = "success";
				$message = $stmt->fetch(PDO::FETCH_ASSOC);
			}
		} 
	}
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}
	include 'json_encoding.php';
?>