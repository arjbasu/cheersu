<?php
	session_start();
	include 'authentication_ajax_api.php';
	include '../connect.php';
	$userid = $_SESSION['user_id'];
	if(isset($_POST['clique_name'])){
		$cliquename = $_POST['clique_name'];
		if($cliquename == ""){
			$status = "error";
			$message = "Clique name cannot be empty";
			include 'json_encoding.php';
			die();
		}
		if(isset($_POST['clique_description'])){
			$cliquedescription = $_POST['clique_description']; 
		}
		else{
			$cliquedescription = "";
		}
		
		$query = "INSERT INTO cheersu_cliques (clique_name,clique_description,clique_creator) VALUES (?,?,?)";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($cliquename,$cliquedescription,$userid));
		if($stmt->rowCount() < 1){
			$status = "error";
			$message = "Error Inserting into database";
		}
		else{
			
			$query = "INSERT INTO cheersu_clique_members (member_userid,member_cliqueid) ".
			"SELECT clique_creator,clique_id FROM cheersu_cliques WHERE clique_name = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($cliquename));
			if($stmt->rowCount() < 1){
				$status = "error";
				$message = "Error Inserting into database";
			}
			else{
				$status = "success";
				$message = "Clique successfully added";
			}
		}	
		
	}
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}
	include 'json_encoding.php';
?>