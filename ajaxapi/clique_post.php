<?php
	session_start();
	include '../connect.php';
	
	include 'post_notification.php';
	function begin(){
		global $stmt;
		global $pdo;
		$query = "BEGIN";
		$stmt = $pdo->prepare($query);
		$stmt->execute();
	}
	
	function commit(){
		global $stmt;
		global $pdo;
		$query = "COMMIT";
		$stmt = $pdo->prepare($query);
		$stmt->execute();
	}
	
	function rollback(){
		global $stmt;
		global $pdo;
		$query = "ROLLBACK";
		$stmt = $pdo->prepare($query);
		$stmt->execute();
	}
	$stmt = "";
	include 'authentication_ajax_api.php';
	
	$userid = $_SESSION['user_id'];
	if(isset($_POST['action']) && isset($_POST['cliqueid'])){
		$cliqueid = $_POST['cliqueid'];
		$action = $_POST['action'];
		error_log("Simplepost:$action");
		if($action == "simplepost"){
			if(isset($_POST['cliquepost'])){
				
				begin();
				$post = $_POST['cliquepost'];
				$query = "SELECT NOW()";
				$result = mysql_query($query);
				$temp = mysql_fetch_row($result);
				$time = $temp[0];
				$query = " INSERT INTO cheersu_clique_posts (post_userid,post_cliqueid,post_content,post_timestamp) VALUES (?,?,?,?)";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($userid,$cliqueid,$post,$time));
				if($stmt->rowCount() == 1){
					$query = "SELECT post_id FROM cheersu_clique_posts WHERE post_userid = ? AND post_cliqueid = ? AND post_timestamp = ? ORDER BY post_timestamp DESC LIMIT 1";
					$stmt = $pdo->prepare($query);
					$stmt->execute(array($userid,$cliqueid,$time));
					$temp = $stmt->fetch(PDO::FETCH_ASSOC);
					$postid = $temp['post_id'];
					$query = "INSERT INTO cheersu_cliques_activity (clique_activity_type,clique_activity_userid,clique_activity_cliqueid, ".
					"clique_activity_postid) VALUES (?,?,?,?) ";
					$stmt = $pdo->prepare($query);
					$stmt->execute(array("post",$userid,$cliqueid,$postid));
					if($stmt->rowCount() == 1){
						commit();
						post_notification("post");
						$status = "success";
						$message = "Successfully posted";
					}
					else{
						rollback();
						$status = "error";
						$message = "Unable to add all posts";
					}
				}	
				else{
					rollback();
					$status = "error";
					$message = "Unable to add all posts";
				}
			}
			
		}	
		else if($action == "checkin" || $action == "reserve"){
			if(isset($_POST['venue_id'])){
				$venueid = $_POST['venue_id'];
				$query = "INSERT INTO cheersu_cliques_activity(clique_activity_type,clique_activity_userid,clique_activity_cliqueid,".
				"clique_activity_venueid) VALUES (?,?,?,?)";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($action,$userid,$cliqueid,$venueid));
				if($stmt->rowCount() == 1){
					$status = "success";
					$message = "You have successfully completed the action";
					post_notification($action);
				}
				else{
					$status = "error";
					$message = "Unable to insert into database";
				}
			}
			else{
				$status = "error";
				$message = "Improper parameters passed";
			}
		}
		else if($action == "getmembers"){
			$query = "SELECT user_id AS id, CONCAT(user_firstname,' ',user_lastname) AS name, user_dp_icon 
			AS dp FROM cheersu_users INNER JOIN cheersu_clique_members ON user_id = member_userid  AND member_cliqueid = ? AND member_userid <> ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($cliqueid,$userid));
			$users = array();
			while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
				if($temp['dp'] == ""){
					$temp['dp'] = "cheersu_icon.png";
				}
				array_push($users,$temp);
			}
			$status = "success";
			$message = $users;
		}
		else{
			$status = "error";
			$message = "Improper parameteres passed";	
		}
	}
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}
	include 'json_encoding.php';
?>