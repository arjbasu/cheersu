<?php
session_start();

	if(isset($_GET['venue_id'])){
		$venueid = $_GET['venue_id'];
		include 'connect.php';
		$query = "SELECT venue_schoolid,venue_name, venue_address FROM cheersu_venues WHERE venue_id = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($venueid));
		if($stmt->rowCount() == 1){
			
			
			$temp = $stmt->fetch(PDO::FETCH_ASSOC);
			$schoolid = $temp['venue_schoolid'];
			if((isset($_SESSION['logged']) && isset($_SESSION['username']) && isset($_SESSION['user_id']))){
				header("Location:http://dev.cheersu.com/venues.php?schoolid=$schoolid");
			}
			$url = "http://dev.cheersu.com/generatevenue.php?venue_id=$venueid";
			$name = stripslashes($temp['venue_name']);
			$address = urldecode($temp['venue_address']);
			$img = "http://dev.cheersu.com/venue_cover.php?venue_id=52&type=rectangle";
		}
	}
?>
<!DOCTYPE>
<html>
<title><?php echo $name;?></title>
 <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# cheersu_integrate: http://ogp.me/ns/fb/cheersu_integrate#">
 
  <meta property="fb:app_id" content="225446017593925" /> 
  <meta property="og:title"  content="<?php echo $name;?>" /> 
  <meta property="og:image"  content="<?php echo $img;?>" /> 
  <meta property="og:url"    content="<?php echo $url;?>" />
  <meta property="og:type"   content="cheersu_integrate:venue" /> 
  
  </head>
  <body>
  </body>
 </html> 