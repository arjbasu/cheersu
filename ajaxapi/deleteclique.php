<?php
	session_start();
	include 'authentication_ajax_api.php';
	include '../connect.php';
	$userid = $_SESSION['user_id'];
	if(isset($_POST['clique_id'])){
		$cliqueid = stripslashes($_POST['clique_id']);
		$query = "SELECT clique_creator FROM cheersu_cliques WHERE clique_id = $cliqueid";
		$result = mysql_query($query);
		if(mysql_num_rows($result) == 0){
			$status = "error";
			$message = "No clique exists";
			$result = array("status"=>$status,"message"=>$message);
			die(json_encode($result));
		}
		$tempres = mysql_fetch_assoc($result);
		$creator = $tempres['clique_creator'];
		if($creator != $userid){
			$status = "error";
			$message = "Permission Denied";
			$result = array("status"=>$status,"message"=>$message);
			die(json_encode($result));
		}
		$query = "DELETE FROM cheersu_cliques WHERE clique_id = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($cliqueid));
		if($stmt->rowCount() <1){
			$status = "error";
			$message = "Unable to delete from database";
		}
		else{
			$query = "DELETE FROM cheersu_clique_members WHERE member_cliqueid = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($cliqueid));
			if($stmt->rowCount() <1){
				$status = "error";
				$message = "Unable to delete members";
			}
			else{
				$query = "DELETE FROM cheersu_cliques_activity WHERE clique_activity_cliqueid = ?";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($cliqueid));
				
				$query = "DELETE FROM cheersu_clique_photos WHERE clique_photos_cliqueid = ?";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($cliqueid));
				
				$query = "DELETE FROM cheersu_clique_posts WHERE post_cliqueid = ?";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($cliqueid));
				
				$status = "success";
				$message = "Clique Successfully Deleted";
			}
		}
	}
	else{
		$status = "error";
		$message = "improper parameters passed";
	}
	include 'json_encoding.php';
?>