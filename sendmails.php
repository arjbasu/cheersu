<?php
	include 'check_authorization.php';
	include 'twiginit.php';
	if(isset($_POST['subject']) && isset($_POST['message'])){
		$query = "SELECT user_email FROM cheersu_users WHERE user_id = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($_SESSION['user_id']));
		$tempemail = $stmt->fetch(PDO::FETCH_ASSOC);
		$email = $tempemail['user_email'];
		$to = "support@cheersu.com";
		$subject = "[Cheersu Support Email] ".$_POST['subject']." from $email";
		$message = $_POST['message'];
		$headers = "From: CheersU <no-reply@cheersu.com> \r\n" .
				"Reply-To: $email" . "\r\n" .
				"X-Mailer: PHP/" . phpversion();
		mail($to, $subject, $message,$headers);
		echo $twig->render("genericlogged.twig",array("message"=>"Your response has been successfully recorded"));
	}
	
?>