<?php
	session_start();
	include 'check_authorization.php';
	include 'connect.php';
	include 'twiginit.php';
	$userid = $_SESSION['user_id'];
	$query = "SELECT * FROM cheersu_users WHERE user_id = '$userid'";
	$result = mysql_query($query);
	if(!$result){
		die("Unable to query databases");
	}
	else{
		include 'removeslashes.php';
		$temp = mysql_fetch_assoc($result);
		$firstname = $temp['user_firstname'];
		$lastname = $temp['user_lastname'];
		$email = $temp['user_email'];
		$emailpending = $temp['user_email_pending'];
		$bio = $temp['user_bio'];
		$status = $temp['user_status'];
		$school = $temp['user_schoolid'];
		$img = $temp['user_dp'];
		
		if($img == ""){
			$img = "300x200.gif";
		}
		$query = "SELECT school_name,school_id FROM cheersu_schools ORDER BY school_name";
		$result = mysql_query($query);
		if(!$result){
			die("Unable to query");
		}
		else{
			$schoolllist = array();
			while($temp = mysql_fetch_assoc($result)){
				array_push($schoolllist, $temp);
			}
		}
	}
	
	echo $twig->render('settings.twig',array(
				"firstname"=>stripslashes($firstname),
				"lastname" =>stripslashes($lastname),
				"email"=>$email,
				"image"=>$img,
				"status"=>$status,
				"bio"=>$bio,
				"username"=>$_SESSION['username'],
				"myschool" => $school,
				"email_pending"=>$emailpending,
				"schoollist" => $schoolllist 
			));
	
?>