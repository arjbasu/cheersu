<?php
	session_start();
	include 'check_authorization.php';
	include 'connect.php';
	include 'twiginit.php';
	$userid = $_SESSION['user_id'];
	if(isset($_GET['schoolid']) && $_GET['schoolid']!=""){
		$school = $_GET['schoolid'];
	}
	else{
		$query = "SELECT user_schoolid FROM cheersu_users WHERE user_id = '$userid'";
		$result = mysql_query($query);
		$temp = mysql_fetch_assoc($result);
		$school = $temp['user_schoolid'];
	}
	$schools = array();
	$query = "SELECT school_name,school_id FROM cheersu_schools WHERE school_id <> 0 ORDER BY school_name";
	$result = mysql_query($query);
	while($temp =  mysql_fetch_assoc($result)){
		array_push($schools,$temp);
	}
	$query = "SELECT school_name FROM cheersu_schools WHERE school_id = '$school'";
	$result = mysql_query($query);
	$temp = mysql_fetch_assoc($result);
	$schoolname = $temp['school_name'];
	
	$query = "SELECT DISTINCT(venue_name),venue_id,venue_address,venue_checkins,venue_reservations FROM cheersu_venues WHERE venue_schoolid = ? ";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($school));
	if($stmt->rowCount() == 0){
		
		echo $twig->render("venues.twig",array("noresults"=> "true","School"=>$schoolname,"Schools"=>$schools));
	}
	else{
		$venues = array();
		while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
			$temp['venue_name'] = stripslashes($temp['venue_name']);
			$temp['venue_img'] = urldecode($temp['venue_address']);
			array_push($venues,$temp);
// 			error_log(print_r($temp),1);
		}
		$data = array("venue_list"=>$venues,"School"=>$schoolname,"Schools"=>$schools,"Schoolid"=>$school);
		$query = "SELECT user_dp_icon FROM cheersu_users WHERE user_id = $userid";
		$result = mysql_query($query);
		$temp = mysql_fetch_row($result);
		$data['chat_dp_icon'] = $temp[0];
		echo $twig->render("venues.twig",$data);
	}
	
?>